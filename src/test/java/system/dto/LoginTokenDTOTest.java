package system.dto;

import org.junit.Before;
import org.junit.Test;
import project.dto.rest.TaskRestDTO;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;

public class LoginTokenDTOTest {

    String name;
    String email;
    String profile;
    String message;
    Boolean isActive;
    LocalDateTime approvedDateTime;
    String confirmationCode;

    LoginTokenDTO newToken;
    LoginTokenDTO anotherToken;
    LoginTokenDTO loginTokenDTO;

    @Before
    public void setUp() throws Exception {

        name = "Asdrubal";
        email = "asdrubal@jj.com";
        profile = "DIRECTOR";
        message = "User Successfully Logged";
        confirmationCode = "1111";
        isActive = true;
        approvedDateTime = null;

        newToken = new LoginTokenDTO(name, email, profile, true, null, null, confirmationCode, message);
        anotherToken = new LoginTokenDTO(name, email, profile);
    }

    @Test
    public void testLoginTokenDTOFourParameterConstructor() {

        assertTrue(newToken instanceof LoginTokenDTO);
    }

    @Test
    public void testLoginTokenDTOOnlyOneParameterConstructor() {

        LoginTokenDTO tokenWithMessageOnly = new LoginTokenDTO(message);

        assertTrue(tokenWithMessageOnly instanceof LoginTokenDTO);
    }

    @Test
    public void testGetMessage() {

        String expected = "User Successfully Logged";
        String result = newToken.getMessage();

        assertEquals(expected, result);
    }

    @Test
    public void testSetMessage() {

        String newMessage = "User failed to log in.";

        newToken.setMessage(newMessage);
        String result = newToken.getMessage();

        assertEquals(newMessage, result);

    }

    @Test
    public void testGetName() {

        String expected = "Asdrubal";
        String result = newToken.getName();

        assertEquals(expected, result);
    }

    @Test
    public void testSetName() {

        String newName = "Gertulio";

        anotherToken.setName(newName);
        String result = anotherToken.getName();

        assertEquals(newName, result);
    }

    @Test
    public void testGetEmail() {

        String expected = "asdrubal@jj.com";
        String result = newToken.getEmail();

        assertEquals(expected, result);
    }

    @Test
    public void testSetEmail() {

        String newEmail = "gertulio@switch.org";

        newToken.setEmail(newEmail);
        String result = newToken.getEmail();

        assertEquals(newEmail, result);
    }

    @Test
    public void testGetProfile() {

        String expected = "DIRECTOR";
        String result = newToken.getProfile();

        assertEquals(expected, result);
    }

    @Test
    public void testSetProfile() {

        String newProfile = "REGISTEREDUSER";

        newToken.setProfile(newProfile);
        String result = newToken.getProfile();

        assertEquals(newProfile, result);
    }

    @Test
    public void testSetConfirmationCode() {

        String newCode = "1234";

        newToken.setConfirmationCode(newCode);
        String result = newToken.getConfirmationCode();

        assertEquals(newCode, result);
    }

    @Test
    public void testSetApprovedDate() {

        LocalDateTime newApprovedDate = LocalDateTime.of(2018, 01, 01, 10, 01);

        newToken.setApprovedDateTime(newApprovedDate);
        LocalDateTime result = newToken.getApprovedDateTime();

        assertEquals(newApprovedDate, result);
    }

    @Test
    public void testSetActive() {

        boolean newActivatinSatus = false;

        newToken.setActive(newActivatinSatus);
        boolean result = newToken.isActive();

        assertEquals(newActivatinSatus, result);
    }

    @Test
    public void testSetLasLogin() {

        LocalDateTime lastLogin = LocalDateTime.of(2018, 01, 01, 10, 01);

        newToken.setLastLoginDate(lastLogin);
        LocalDateTime result = newToken.getLastLoginDate();

        assertEquals(lastLogin, result);
    }

    @Test
    public void getStatusCode() {

        newToken.setStatusCode(5);
        int expected = 5;

        assertEquals(expected, newToken.getStatusCode());
    }

    @Test
    public void testEquals() {
        loginTokenDTO = new LoginTokenDTO();
        loginTokenDTO.setEmail("pedro@mail.com");
        assertTrue(newToken.equals(newToken));
        assertFalse(newToken.equals(loginTokenDTO));
        assertFalse(newToken.equals(email));
        assertTrue(loginTokenDTO instanceof LoginTokenDTO);
    }
}
