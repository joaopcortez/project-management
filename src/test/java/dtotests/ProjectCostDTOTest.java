package dtotests;

import org.junit.Before;
import org.junit.Test;
import project.dto.ProjectCostDTO;

import static org.junit.Assert.assertEquals;

public class ProjectCostDTOTest {

    ProjectCostDTO projectCostDTO;

    @Before
    public void setUp() {
        projectCostDTO = new ProjectCostDTO();
        projectCostDTO.setTo("testSet");
    }

    @Test
    public void testGetTo() {
        String expected = "testSet";
        assertEquals(expected, projectCostDTO.getTo());
    }

}
