package dtotests.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import project.dto.ProjectCollaboratorDTO;
import project.dto.rest.ProjectDTO;

public class ProjectDTOTest {

    ProjectDTO projectDTO;
    ProjectDTO projectDTO2;
    ProjectDTO projectDTO3;

    @Before
    public void setUp() {

        projectDTO = new ProjectDTO();
        projectDTO.setProjectId("1");
        projectDTO.setUnit("20.0");
        projectDTO.setStartDate(LocalDateTime.now().minusDays(4));
        projectDTO.setName("HATEOAS");
        projectDTO.setFinalDate(LocalDateTime.now().plusDays(10));
        projectDTO.setGlobalBudget(1000);
        projectDTO.setDescription("The Glory of Rest");
        projectDTO.setUserEmail("asdrubal@switch.com");

        //Given an equal parameters dto
        projectDTO2 = new ProjectDTO();
        projectDTO2.setProjectId(projectDTO.getProjectId());
        projectDTO2.setUnit("20.0");
        projectDTO2.setStartDate(projectDTO.getStartDate());
        projectDTO2.setName("HATEOAS");
        projectDTO2.setFinalDate(projectDTO.getFinalDate());
        projectDTO2.setGlobalBudget(1000);
        projectDTO2.setDescription("The Glory of Rest");
        projectDTO2.setUserEmail("asdrubal@switch.com");

        //given a diferente parameters dto
        projectDTO3 = new ProjectDTO();
        projectDTO3.setProjectId("4");
    }

    @Test
    public void testHashCode() {
        //then
        assertEquals(projectDTO2.hashCode(), projectDTO.hashCode());
        assertNotEquals(projectDTO.hashCode(), projectDTO3.hashCode());

        projectDTO3.setProjectId(null);
        assertEquals(31, projectDTO3.hashCode());
    }

    @Test
    public void testEqualsSucessSameObject() {

        assertTrue(projectDTO2.equals(projectDTO2));
    }

    @Test
    public void testEqualsSucess() {

        assertTrue(projectDTO.equals(projectDTO2));
    }

    @Test
    public void testEqualsFail() {

        assertFalse(projectDTO3.equals(projectDTO));
    }

    @Test
    public void testEqualsFailProjectIdNull() {

        projectDTO2.setProjectId(null);
        assertFalse(projectDTO2.equals(projectDTO));
    }

    @Test
    public void testEqualsFailTwoProjectIdNull() {

        projectDTO.setProjectId(null);
        projectDTO2.setProjectId(null);
        assertTrue(projectDTO2.equals(projectDTO));
    }

    @Test
    public void testEqualsFailureDifferentClass() {

        ProjectCollaboratorDTO projectCollaboratorDTO = new ProjectCollaboratorDTO();

        assertFalse(projectDTO.equals(projectCollaboratorDTO));
    }

    @Test
    public void testGetMessage() {

        projectDTO3.setMessageError("error");
        String expected = "error";
        assertEquals(expected, projectDTO3.getMessageError());
    }
}