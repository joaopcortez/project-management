package dtotests.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import project.dto.UserRegistryDTO;
import project.dto.rest.UserRestDTO;



public class UserRestDTOTest {

	
	UserRestDTO userDTO;

    @Before
    public void setUp() {
    	
    	userDTO = new UserRestDTO();
		
    	userDTO.setName("Luis");
		userDTO.setEmail("luis@switch.com");
		userDTO.setPhone("123456789");
		userDTO.setTaxPayerId("123456789");
		userDTO.setBirthDate(LocalDate.of(1977, 3, 5));
		userDTO.setProfile("DIRECTOR");
    }

    @Test
    public void testGetName() {
    	assertTrue(userDTO.getName().equals("Luis"));
    }

    @Test
    public void testSetName() {
    	userDTO.setName("Fabio");
    	
    	assertTrue(userDTO.getName().equals("Fabio"));
    }

    @Test
    public void testGetAndSetEmail() {
    	userDTO.setEmail("asdrubal@switch.com");
    	
    	assertTrue(userDTO.getEmail().equals("asdrubal@switch.com"));
    }
    


    @Test
    public void testGetAndSetPhone() {
    	userDTO.setPhone("987654321");

        assertTrue(userDTO.getPhone().equals("987654321"));
    }

    @Test
    public void testGetAndSetTin() {
    	userDTO.setTaxPayerId("987654321");
    	
    	assertTrue(userDTO.getTaxPayerId().equals("987654321"));
    }

    @Test
    public void testGetAndSetBirthDate() {
    	userDTO.setBirthDate(LocalDate.of(1977, 3, 6));

    	assertTrue(userDTO.getBirthDate().equals(LocalDate.of(1977, 3, 6)));
    }

    
    @Test
    public void testGetAndSetProfile() {
    	userDTO.setProfile("COLLABORATOR");
    	
    	assertTrue(userDTO.getProfile().equals("COLLABORATOR"));
    }
    
    @Test
    public void testToString() {
    	  String result = userDTO.toString();
    	  
          String expected ="UserEditDataRestDTO [name=" + "Luis" + ", email=" + "luis@switch.com" + ", phone=" + "123456789" + ", taxPayerId="
                  + "123456789" + ", birthDate=" + "1977-03-05" + ", profile=" + "DIRECTOR" + "]";
          
 
          assertEquals(expected, result);
    }
    
    @Test
    public void testToStringFail() {
    	  String result = userDTO.toString();
          String expected ="[utilizador=[nome_utilizador=Fabio, email_utilizador=luis@switch.com, telefone=123456789, tin=123456789, birthdate=1977-03-05]";
          assertNotEquals(expected, result);
    }
    @Test
    public void testEqualsSucess() {
    	UserRestDTO testDTO = new UserRestDTO();
    	testDTO.setEmail("luis@switch.com");

        assertEquals(testDTO, testDTO);
        assertEquals(testDTO, userDTO);
    
    }

    @Test
    public void testEqualsFail() {

    	UserRestDTO testDTO =new UserRestDTO();
    	testDTO.setEmail("maria@switch.com");
    	
        assertNotEquals(testDTO, userDTO);
    }

    @Test
    public void testEqualsFailureObjectNull() {

    	UserRestDTO trNull = new UserRestDTO();
    	trNull.setEmail(null);
   
    	
        assertNotEquals(userDTO, trNull);
        assertFalse(trNull.equals(userDTO));

    }
    @Test
    public void testEqualsFailureObjectNull2() {

    	UserRestDTO testNull = null;
        assertNotEquals(userDTO, testNull);
    }
    @Test
    public void testEqualsFailureObjectNullEmail() {

    	UserRestDTO trNull = new UserRestDTO();;
    	trNull.setEmail(null);
    	userDTO.setEmail(null);
   
        assertEquals(userDTO, trNull);
    }

    @Test
    public void testEqualsFailureDifferentClass() {

        UserRegistryDTO userRestDTO = new UserRegistryDTO();
     
        assertNotEquals(userRestDTO,userDTO);
    }
    @Test
    public void testHashCode() {
    	UserRestDTO userRegistryDTO1 = new UserRestDTO();
    	userRegistryDTO1.setEmail("maria@switch.com");
    	
    	UserRestDTO userRegistryDTO2 = new UserRestDTO();
    	userRegistryDTO2.setEmail("luis@switch.com");
    	
    	assertEquals(userDTO.hashCode(), userRegistryDTO2.hashCode());
    	assertNotEquals(userRegistryDTO1.hashCode(), userRegistryDTO2.hashCode());

    	
    }
	

}
