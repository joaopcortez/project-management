package dtotests.rest;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import project.dto.rest.ProfileRestDTO;

public class ProfileRestDTOTest {
	
	ProfileRestDTO testDTO;
	ProfileRestDTO testDTO2;
	
	@Before
	public void setUp()  {
		

		testDTO = new ProfileRestDTO();
	
		testDTO.setProfile("collaborator");
		
	}

	@Test
	public void testGetProfile() {
		
		assertEquals("collaborator", testDTO.getProfile());
	}
	
}
