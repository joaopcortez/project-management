package dtotests.rest;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import project.dto.rest.CreateReportRestDTO;

public class CreateReportRestDTOTest {
	
	CreateReportRestDTO testDTO;
	CreateReportRestDTO testDTO2;
	
	@Before
	public void setUp()  {
		

		testDTO = new CreateReportRestDTO();
		
		testDTO.setQuantity(1);
		testDTO.setStartDate(LocalDateTime.of(2018, 3, 5,0,0));
		testDTO.setEndDate(LocalDateTime.of(2018, 5, 5,0,0));
		testDTO.setProjectCollaboratorID(1);
		testDTO.setTaskID("task1");
		testDTO.setProjectCollaboratorEmail("joana@switch.com");
		
	}
	
	@Test
	public void testGetQuantity() {
		
		assertEquals(1, testDTO.getQuantity(), 0.01);
	}
	
	@Test
	public void testGetStartDate() {
		
		assertEquals(LocalDateTime.of(2018, 3, 5,0,0), testDTO.getStartDate());
	}
	
	@Test
	public void testGetEndDate() {
		
		assertEquals(LocalDateTime.of(2018, 5, 5,0,0), testDTO.getEndDate());
	}
	
	@Test
	public void testGetProjectCollaboratorID() {
		
		assertEquals(1, testDTO.getProjectCollaboratorID());
	}
	
	@Test
	public void testGetTaskId() {
		
		assertEquals("task1", testDTO.getTaskID());
	}
	
	@Test
	public void testGetProjectCollaboratorEmail() {
		
		assertEquals("joana@switch.com", testDTO.getProjectCollaboratorEmail());
	}

}
