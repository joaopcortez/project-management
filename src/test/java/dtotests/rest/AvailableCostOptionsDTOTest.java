package dtotests.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import project.dto.rest.AvailableCostOptionsDTO;

public class AvailableCostOptionsDTOTest {

	AvailableCostOptionsDTO availableCostOptionsDTO;
	AvailableCostOptionsDTO availableCostOptionsDTO2;
	AvailableCostOptionsDTO availableCostOptionsDTO3;
	List<String> costOptionsList;

	@Before
	public void setUp() {
		availableCostOptionsDTO = new AvailableCostOptionsDTO();
		costOptionsList = new ArrayList<>();
		costOptionsList.add("AverageCost");
		costOptionsList.add("LastTimePeriodCost");
		availableCostOptionsDTO.setAvailableCalculationOptions(costOptionsList);
		availableCostOptionsDTO.setCalculationOption("LastTimePeriodCost");
	}

	@Test
	public void testGetCalculationOption() {

		assertEquals("LastTimePeriodCost", availableCostOptionsDTO.getCalculationOption());
	}

	@Test
	public void testGetAvailableCostOptions() {
		List<String> expected = new ArrayList<>();
		expected.add("AverageCost");
		expected.add("LastTimePeriodCost");
		assertEquals(expected, availableCostOptionsDTO.getAvailableCalculationOptions());
	}

	@Test
	public void testNotEqualsAvailableCostOptions() {
		List<String> testDTO = new ArrayList<>();
		testDTO.add("LastTimePeriodCost");
		assertNotEquals(testDTO, costOptionsList);
	}

	@Test
	public void testEqualsAvailableCostOptions() {
		List<String> testDTO = new ArrayList<>();
		testDTO.add("AverageCost");
		testDTO.add("LastTimePeriodCost");
		assertEquals(testDTO, costOptionsList);
		assertEquals(testDTO, testDTO);
	}

	@Test
	public void testEquals() {
		assertTrue(availableCostOptionsDTO.equals(availableCostOptionsDTO));
		assertFalse(availableCostOptionsDTO.equals(availableCostOptionsDTO2));

		availableCostOptionsDTO3 = new AvailableCostOptionsDTO();
		availableCostOptionsDTO3.setAvailableCalculationOptions(costOptionsList);
		availableCostOptionsDTO3.setCalculationOption("LastTimePeriodCost");

		assertEquals(availableCostOptionsDTO, availableCostOptionsDTO3);

	}

	@Test
	public void testEquals2() {

		availableCostOptionsDTO3 = new AvailableCostOptionsDTO();
		availableCostOptionsDTO3.setCalculationOption("AverageCost");
		assertFalse(availableCostOptionsDTO.equals(availableCostOptionsDTO3));
		
		availableCostOptionsDTO3.setCalculationOption("LastTimePeriodCost");
		List<String> costOptionsList2 = new ArrayList<>(); 
		costOptionsList2.add("NewCost");
		availableCostOptionsDTO3.setAvailableCalculationOptions(costOptionsList2);
		
		assertFalse(availableCostOptionsDTO.equals(availableCostOptionsDTO3));		
	}

	@Test
	public void testHashCode() {
		// then

		availableCostOptionsDTO2 = new AvailableCostOptionsDTO();
		availableCostOptionsDTO2.setAvailableCalculationOptions(costOptionsList);
		availableCostOptionsDTO2.setCalculationOption("LastTimePeriodCost");

		assertEquals(availableCostOptionsDTO.hashCode(), availableCostOptionsDTO2.hashCode());

		assertEquals(2058585105, availableCostOptionsDTO2.hashCode());
	}

}
