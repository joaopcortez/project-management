package jparepositoriestest;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import project.jparepositories.ProjectRepository;
import project.model.project.Project;

public class ProjectRepositoryClass implements ProjectRepository {



    Set<Project> list = new LinkedHashSet<>();
//save

    @Override
    public List<Project> findAll() {
        return new ArrayList<Project>(list);
    }

    @Override
    public List<Project> findAll(Sort sort) {
        //Auto-generated method stub
        return new ArrayList<>();
    }

    @Override
    public List<Project> findAllById(Iterable<String> ids) {
        //Auto-generated method stub
        return new ArrayList<>();
    }

    @Override
    public <S extends Project> List<S> saveAll(Iterable<S> entities) {
        //Auto-generated method stub
        return new ArrayList<>();
    }

    @Override
    public void flush() {
        //Auto-generated method stub

    }

    @Override
    public <S extends Project> S saveAndFlush(S entity) {
        //Auto-generated method stub
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Project> entities) {
        //Auto-generated method stub

    }

    @Override
    public void deleteAllInBatch() {
        //Auto-generated method stub

    }

    @Override
    public Project getOneByCode(String id) {
        for (Project p : list) {
            if (p.getId().equals(id)) {
                return p;
            }
        }
        return null;
    }

    @Override
    public <S extends Project> List<S> findAll(Example<S> example) {
        //Auto-generated method stub
        return new ArrayList<>();
    }

    @Override
    public <S extends Project> List<S> findAll(Example<S> example, Sort sort) {
        // Auto-generated method stub
        return new ArrayList<>();
    }

    @Override
    public Page<Project> findAll(Pageable pageable) {
        // Auto-generated method stub
        return null;
    }

    @Override
    public <S extends Project> S save(S entity) {
        list.add(entity);
        return entity;
    }

    @Override
    public Optional<Project> findById(String id) {
        //Auto-generated method stub
        return Optional.empty();
    }

    @Override
    public boolean existsById(String id) {
        for (Project project : list) {
            if (project.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public long count() {
        //Auto-generated method stub
        return 0;
    }

    @Override
    public void deleteById(String id) {
        //Auto-generated method stub

    }

    @Override
    public void delete(Project entity) {
        //Auto-generated method stub

    }

    @Override
    public void deleteAll(Iterable<? extends Project> entities) {
        //Auto-generated method stub

    }

    @Override
    public void deleteAll() {
        //Auto-generated method stub

    }

    @Override
    public <S extends Project> Optional<S> findOne(Example<S> example) {
        //Auto-generated method stub
        return Optional.empty();
    }

    @Override
    public <S extends Project> Page<S> findAll(Example<S> example, Pageable pageable) {
        //Auto-generated method stub
        return null;
    }

    @Override
    public <S extends Project> long count(Example<S> example) {
        //Auto-generated method stub
        return 0;
    }

    @Override
    public <S extends Project> boolean exists(Example<S> example) {
        //Auto-generated method stub
        return false;
    }

    @Override
    public Project getOne(String id) {
        return null;
    }
}
