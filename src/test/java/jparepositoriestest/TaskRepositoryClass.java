package jparepositoriestest;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import project.jparepositories.TaskRepository;
import project.model.project.Project;
import project.model.task.Task;

import java.util.*;

public class TaskRepositoryClass implements TaskRepository {
	Set<Task> list = new LinkedHashSet<>();
	// save

	@Override
	public List<Task> findAll() {
		return new ArrayList<>(list);
	}

	@Override
	public List<Task> findAll(Sort sort) {
		// Auto-generated method stub
		return new ArrayList<Task>(list);
	}

	@Override
	public List<Task> findAllById(Iterable<String> ids) {
		// Auto-generated method stub
		return new ArrayList<>();
	}

	@Override
	public <S extends Task> List<S> saveAll(Iterable<S> entities) {
		// Auto-generated method stub
		return new ArrayList<>();
	}

	@Override
	public void flush() {
		// Auto-generated method stub

	}

	@Override
	public <S extends Task> S saveAndFlush(S entity) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void deleteInBatch(Iterable<Task> entities) {
		// Auto-generated method stub

	}

	@Override
	public void deleteAllInBatch() {
		// Auto-generated method stub

	}

	@Override
	public Task getOne(String id) {
		for (Task task : list) {
			if (task.getId().equals(id)) {
				return task;
			}
		}
		return null;
	}

	@Override
	public <S extends Task> List<S> findAll(Example<S> example) {
		// Auto-generated method stub
		return new ArrayList<>();
	}

	@Override
	public <S extends Task> List<S> findAll(Example<S> example, Sort sort) {
		// Auto-generated method stub
		return new ArrayList<>();
	}

	@Override
	public Page<Task> findAll(Pageable pageable) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Task> S save(S entity) {
		list.add(entity);
		return entity;
	}

	@Override
	public Optional<Task> findById(String id) {
		// Auto-generated method stub
		return Optional.empty();
	}

	@Override
	public boolean existsById(String id) {
		for (Task t : list) {
			if (t.getId().equals(id)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public long count() {
		// Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(String id) {
		// Auto-generated method stub

	}

	@Override
	public void delete(Task entity) {
		list.remove(entity);

	}

	@Override
	public void deleteAll(Iterable<? extends Task> entities) {
		// Auto-generated method stub

	}

	@Override
	public void deleteAll() {
		// Auto-generated method stub

	}

	@Override
	public <S extends Task> Optional<S> findOne(Example<S> example) {
		// Auto-generated method stub
		return Optional.empty();
	}

	@Override
	public <S extends Task> Page<S> findAll(Example<S> example, Pageable pageable) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Task> long count(Example<S> example) {
		// Auto-generated method stub
		return 0;
	}

	@Override
	public <S extends Task> boolean exists(Example<S> example) {
		// Auto-generated method stub
		return false;
	}

	@Override
	public List<Task> findByProject(Project project) {
		List<Task> taskList = new ArrayList<>();
		for (Task t : list) {
			if (t.getProject().equals(project)) {
				taskList.add(t);
			}
		}
		return taskList;
	}

	@Override
	public Task getOneByTaskId(String id) {
		for (Task t : list) {
			if (t.getId().equals(id)) {
				return t;
			}
		}
		return null;
	}

}
