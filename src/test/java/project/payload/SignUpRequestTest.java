package project.payload;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SignUpRequestTest {

    private SignUpRequest signUpRequest;

    @Before
    public void setUp() throws Exception {
        signUpRequest = new SignUpRequest();
        signUpRequest.setAddressId("1234567");
        signUpRequest.setBirthDate("1999-01-01");
        signUpRequest.setCity("New York");
        signUpRequest.setCountry("US");
        signUpRequest.setEmail("switch@it.com");
        signUpRequest.setName("Adelaide");
        signUpRequest.setPassword("12345");
        signUpRequest.setPhone("123456");
        signUpRequest.setPostalCode("9700-001");
        signUpRequest.setProfile("ROLE_COLLABORATOR");
    }

    @Test
    public void getSetEmail() {
        assertEquals("switch@it.com", signUpRequest.getEmail());
    }

    @Test
    public void getSetAddressId() {
        assertEquals("1234567", signUpRequest.getAddressId());
    }

    @Test
    public void getSetBirthDate() {
        assertEquals("1999-01-01", signUpRequest.getBirthDate());
    }

    @Test
    public void getSetCity() {
        assertEquals("New York", signUpRequest.getCity());
    }

    @Test
    public void getSetCountry() {
        assertEquals("US", signUpRequest.getCountry());
    }

    @Test
    public void getSetName() {
        assertEquals("Adelaide", signUpRequest.getName());
    }

    @Test
    public void getSetPassword() {
        assertEquals("12345", signUpRequest.getPassword());
    }

    @Test
    public void getSetPhone() {
        assertEquals("123456", signUpRequest.getPhone());
    }

    @Test
    public void getSetPostalCode() {
        assertEquals("9700-001", signUpRequest.getPostalCode());
    }


    @Test
    public void getSetProfile() {
        assertEquals("ROLE_COLLABORATOR", signUpRequest.getProfile());
    }
}