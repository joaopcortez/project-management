package project.payload;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;

import org.junit.Before;
import org.junit.Test;

public class UserIdentityAvailabilityTest {

	UserIdentityAvailability userIdentityAvailability;
	private boolean available;

	@Before
	public void setUp() {
		available = true;
		userIdentityAvailability = new UserIdentityAvailability(available);
	}

	@Test
	public void testUserIdentityAvailabilityConstructorThroughReflection() {

		userIdentityAvailability = null;

		try {
			Constructor<UserIdentityAvailability> constructor = UserIdentityAvailability.class.getDeclaredConstructor();
			constructor.setAccessible(true);
			userIdentityAvailability = constructor.newInstance();
		} catch (Exception e) {

		}
		assertTrue(userIdentityAvailability instanceof UserIdentityAvailability);
	}

	@Test
	public void getAvailable() {
		boolean av = userIdentityAvailability.getAvailable();
		assertTrue(av);
		userIdentityAvailability.setAvailable(false);
		assertFalse(userIdentityAvailability.getAvailable());
	}
}