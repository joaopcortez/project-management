package project.controllers.rest;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import project.dto.rest.TaskCollaboratorRegistryRESTDTO;
import project.dto.rest.TaskRestDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.user.User;
import project.security.UserPrincipal;
import project.services.ProjectCollaboratorService;
import project.services.TaskProjectService;
import project.services.TaskService;

import javax.mail.internet.AddressException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AddOrRemoveTaskRESTControllerTest {

    private AddOrRemoveTaskRESTController addOrRemoveTaskRESTController;
    private UserService userService;
    private ProjectService projectService;
    private TaskService taskService;
    private TaskProjectService taskProjectService;
    private ProjectCollaboratorService projectCollaboratorService;

    private ProjectRepositoryClass projectRepositoryClass;
    private ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    private TaskRepositoryClass taskRepositoryClass;
    private UserRepositoryClass userRepositoryClass;

    private Project project;
    private Task task1;
    private Task task2;
    private Task task3;
    private ProjectCollaborator col1, col2;
    private User user1, user2;

    UserPrincipal userPrincipal;
    
    @Before
    public void setUp() throws AddressException {

        projectRepositoryClass = new ProjectRepositoryClass();

        taskRepositoryClass = new TaskRepositoryClass();

        userRepositoryClass = new UserRepositoryClass();

        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();

        projectService = new ProjectService(projectRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);
        taskProjectService = new TaskProjectService(projectRepositoryClass, taskRepositoryClass);
        userService = new UserService(userRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);

        addOrRemoveTaskRESTController = new AddOrRemoveTaskRESTController(taskService, taskProjectService);

        addOrRemoveTaskRESTController = new AddOrRemoveTaskRESTController(taskService, taskProjectService);
        userService.addUser("Asdrubal", "123456789", "asdrubal@switch.com", "123456789", LocalDate.of(1977, 3, 5), "1",
                "Rua sem saída", "4250-357", "Porto", "Portugal");
        userService.addUser("Manel", "123456789", "manel@jj.com", "123456789", LocalDate.of(1978, 11, 12), "2",
                "Rua sem saída", "4250-357", "Porto", "Portugal");

        user1 = userService.searchUserByEmail("asdrubal@switch.com");
        user2 = userService.searchUserByEmail("manel@jj.com");
        user1.setProfileCollaborator();
        user2.setProfileCollaborator();
        user1.setActive();
        user2.setActive();

        List<String>listProjectWhereProjectManager = new ArrayList<>();
        List<String>listTasksWhereIsProjectManager = new ArrayList<>();
        List<String>listTasksWhereIsTaskCollaborator = new ArrayList<>();
        userPrincipal = UserPrincipal.create(user1, listProjectWhereProjectManager, listTasksWhereIsTaskCollaborator, listTasksWhereIsProjectManager);
        
        userService.updateUser(user1);
        userService.updateUser(user2);

        projectService.addProject("1", "Project1");
        project = projectService.getProjectByID("1");

        projectCollaboratorService.setProjectManager(project, user1);
        projectCollaboratorService.addProjectCollaborator(project, user2, 6);

        col1 = projectCollaboratorService.getProjectCollaborator("1", "asdrubal@switch.com");
        col2 = projectCollaboratorService.getProjectCollaborator("1", "manel@jj.com");

        taskService.addTask(project, "task1");
        taskService.addTask(project, "task2");
        taskService.addTask(project, "task3");

        task1 = taskService.findTaskByID("1-1");
        task2 = taskService.findTaskByID("1-2");
        task3 = taskService.findTaskByID("1-3");

        taskService.updateTask(task1);
        taskService.updateTask(task2);
        taskService.updateTask(task3);

    }

    /**
     * US345
     * <p>
     * GIVEN: A project with three tasks not initiated and in Planned State
     * WHEN: The project Manager remove a not initiated task named task1
     * THEN: The task was deleted and removed from the project task list
     */
    @Test
    public void removeTaskTest() {

        //GIVEN

        List<TaskRestDTO> expected = new ArrayList<>();
        TaskRestDTO task1Dto = task1.toDTO();
        TaskRestDTO task2Dto = task2.toDTO();
        TaskRestDTO task3Dto = task3.toDTO();

        expected.add(task1Dto);
        expected.add(task2Dto);
        expected.add(task3Dto);
        ResponseEntity<List<TaskRestDTO>> result = addOrRemoveTaskRESTController.getProjectTasks(project.getId(), userPrincipal);

        assertEquals(result.getBody().toString(), expected.toString());
        assertEquals(HttpStatus.OK, result.getStatusCode());

        assertTrue(task1.getStatus().isOnCreatedState());
        assertTrue(task2.getStatus().isOnCreatedState());
        assertTrue(task3.getStatus().isOnCreatedState());

        //WHEN

        addOrRemoveTaskRESTController.removeTask(task1.getId());

        //THEN

        result = addOrRemoveTaskRESTController.getProjectTasks(project.getId(), userPrincipal);

        assertNull(taskService.findTaskByID(task1.getId()));
        expected.remove(task1Dto);
        assertEquals(result.getBody().toString(), expected.toString());

    }

    /**
     * US345
     * <p>
     * GIVEN: A project with three tasks and one of them is already initiated
     * WHEN: The project Manager remove a  initiated task named task1
     * THEN: We get an HTTP status of OK but the task could not be removed and is still on the project task list
     */
    @Test
    public void removeTaskTestFail() {

        //GIVEN
        taskService.addProjectCollaboratorToTask(col2, task2);

        TaskCollaboratorRegistry colTaskRegistry = task2
                .getTaskCollaboratorRegistryByID(task2.getId() + "-" + col2.getUser().getEmail());
        colTaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));

        task2.addReport(col2, 3, LocalDateTime.now().minusDays(10), LocalDateTime.now().minusDays(2));
        taskService.updateTask(task2);

        List<TaskRestDTO> expected = new ArrayList<>();
        TaskRestDTO task1Dto = task1.toDTO();
        TaskRestDTO task2Dto = task2.toDTO();
        TaskRestDTO task3Dto = task3.toDTO();

        expected.add(task1Dto);
        expected.add(task2Dto);
        expected.add(task3Dto);
        ResponseEntity<List<TaskRestDTO>> result = addOrRemoveTaskRESTController.getProjectTasks(project.getId(), userPrincipal);

        assertEquals(result.getBody().toString(), expected.toString());
        assertEquals(HttpStatus.OK, result.getStatusCode());

        assertTrue(task2.getStatus().isOnInProgressState());

        //WHEN

        addOrRemoveTaskRESTController.removeTask(task2.getId());

        //THEN

        result = addOrRemoveTaskRESTController.getProjectTasks(project.getId(), userPrincipal);

        assertNotNull(taskService.findTaskByID(task2.getId()));

        assertEquals(result.getBody().toString(), expected.toString());
        assertEquals(HttpStatus.OK, result.getStatusCode());

    }

    @Test
    public void createTaskTest() {

        /*
         * GIVEN: A project with three tasks not initiated and in Planned State
         */

        assertEquals(3, project.getTasksList().size());

        /*
         * WHEN: A task is created
         *
         */
        TaskRestDTO taskRestDTO = new TaskRestDTO();
        taskRestDTO.setProjectId(project.getId());
        taskRestDTO.setTitle("smile");
        addOrRemoveTaskRESTController.createTask(project.getId(), taskRestDTO, userPrincipal);

        /*
         * THEN: The task was created
         *
         */

        assertEquals(4, project.getTasksList().size());

    }

    @Test
    public void createTaskTest_fail() {

        /*
         * GIVEN: A project with three tasks not initiated and in Planned State
         */

        assertEquals(3, project.getTasksList().size());

        /*
         * WHEN: A task is created with a wrong project id
         *
         */
        TaskRestDTO taskRestDTO = new TaskRestDTO();
        taskRestDTO.setProjectId("2");
        taskRestDTO.setTitle("smile");
        addOrRemoveTaskRESTController.createTask(project.getId(), taskRestDTO, userPrincipal);

        /*
         * THEN: The task was not created
         *
         */

        assertEquals(3, project.getTasksList().size());

    }
    
    
    @Test
    public void addTasksDependenciesTest() {

        //GIVEN

        TaskRestDTO task3Dto = task3.toDTO();

        //WHEN

        ResponseEntity<TaskRestDTO> result =addOrRemoveTaskRESTController.addTasksDependencies(task1.getId(), task3Dto);

        //THEN

        assertEquals(result.getBody().toString(), task3Dto.toString());
        assertEquals(HttpStatus.OK, result.getStatusCode());

    }
    
    @Test
    public void getTasksDependenciesTest() {

        //GIVEN
        List<TaskRestDTO> expectedEmptyBody = new ArrayList<>();
        List<TaskRestDTO> expectedFinal = new ArrayList<>();

        TaskRestDTO task2Dto = task2.toDTO();
        TaskRestDTO task3Dto = task3.toDTO();
        expectedFinal.add(task2Dto);
        expectedFinal.add(task3Dto);

        ResponseEntity<List<TaskRestDTO>> result = addOrRemoveTaskRESTController.getTasksDependencies(task1.getId(), userPrincipal);

        assertEquals(result.getBody().toString(), expectedEmptyBody.toString());
        assertEquals(HttpStatus.OK, result.getStatusCode());

        //WHEN
        task1.addTaskDependency(task2);
        task1.addTaskDependency(task3);
        taskService.updateTask(task1);
        taskService.updateTask(task2);
        taskService.updateTask(task3);

        //THEN

        result = addOrRemoveTaskRESTController.getTasksDependencies(task1.getId(), userPrincipal);

        //assertEquals(result.getBody().toString(), expected.toString());
        assertNotNull(taskService.findTaskByID(task1.getId()));  
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(expectedFinal, taskService.listTaskDependencies(task1.getId()));
    }

    @Test
    public void getTasksCollaboratorsTest() {

        //GIVEN
        task1.addProjectCollaborator(col2);
        task1.addReport(col2,10,LocalDateTime.now().minusDays(7), LocalDateTime.now().minusDays(1));
        List<TaskCollaboratorRegistryRESTDTO> expected = task1.activeTaskCollaboratorRegistryToDTO();


        //WHEN
        ResponseEntity<List<TaskCollaboratorRegistryRESTDTO>> result = addOrRemoveTaskRESTController.getTasksCollaborators(task1.getId(), userPrincipal);

        //THEN

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(expected, result.getBody());

    }
    
    @Test
    public void getAllTasksCollaboratorsTest() {

        //GIVEN
        task1.addProjectCollaborator(col2);
        task1.addReport(col2,10,LocalDateTime.now().minusDays(7), LocalDateTime.now().minusDays(1));
        List<TaskCollaboratorRegistryRESTDTO> expected = task1.activeTaskCollaboratorRegistryToDTO();


        //WHEN
        ResponseEntity<List<TaskCollaboratorRegistryRESTDTO>> result = addOrRemoveTaskRESTController.getAllTasksCollaborators(task1.getId(), userPrincipal);

        //THEN

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(expected, result.getBody());

    }
 

    @Test
    public void listTasksThatCanBeADDedAsDependencyTest() {

    	//GIVEN
    	
    	task2.setEffectiveStartDate(LocalDateTime.of(2018,01,23,0,0));
    	
    	TaskRestDTO task3Dto = task3.toDTO();
    	TaskRestDTO task2Dto = task2.toDTO();
    	
    	List<TaskRestDTO> expected = new ArrayList<>();
    	expected.add(task2Dto);
    	expected.add(task3Dto);

    	//WHEN

    	ResponseEntity<List<TaskRestDTO>> result = addOrRemoveTaskRESTController.listTasksThatCanBeADDedAsDependency(task1.getId(), userPrincipal);


    	//THEN
   
    	assertEquals(result.getBody().toString(), expected.toString());
    	assertEquals(HttpStatus.OK, result.getStatusCode());
    	assertNotNull(taskService.findTaskByID(task1.getId()));

    }

}
