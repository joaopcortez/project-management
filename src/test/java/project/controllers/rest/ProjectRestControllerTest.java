package project.controllers.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import project.dto.rest.AvailableCostOptionsDTO;
import project.dto.rest.ProjectDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.services.ProjectCollaboratorService;
import project.services.TaskProjectCollaboratorService;
import project.model.user.User;

public class ProjectRestControllerTest {

	ProjectRestController projectController;

	UserService userService;
	UserRepositoryClass userRepositoryClass;
	ProjectService projectService;
	ProjectRepositoryClass projectRepositoryClass;
	TaskRepositoryClass taskRepositoryClass;
	ProjectCollaboratorService projectCollaboratorService;
	ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
	TaskProjectCollaboratorService taskProjectCollaboratorService;

	Project project1, project2;

	@Before
	public void setUp() throws Exception {

		userRepositoryClass = new UserRepositoryClass();
		userService = new UserService(userRepositoryClass);
		projectRepositoryClass = new ProjectRepositoryClass();
		projectService = new ProjectService(projectRepositoryClass);
		taskRepositoryClass = new TaskRepositoryClass();
		projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
		projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
		taskProjectCollaboratorService = new TaskProjectCollaboratorService(taskRepositoryClass, projectCollaboratorRepositoryClass, projectRepositoryClass);

		projectController = new ProjectRestController(projectService,taskProjectCollaboratorService);

		userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", LocalDate.of(1999, 12, 25), "1",
				"Rua do Amial", "4250-444", "Porto", "Portugal");
		User user = userService.searchUserByEmail("asdrubal@jj.com");

		projectService.addProject("1", "Project 1");
		projectService.addProject("2", "Project 2");
		project1 = projectService.getProjectByID("1");
		project2 = projectService.getProjectByID("2");

		projectCollaboratorService.setProjectManager(project1, user);
		projectCollaboratorService.setProjectManager(project2, user);

	}

	@Test
	public void testProjectRestController() {

		assertTrue(projectController instanceof ProjectRestController);
	}

	/**
	 * Given: a list of all company's projects, all of which are active
	 * When: we ask to list all ACTIVE projects 
	 * Then: verify that body of the response is equal to expected and HTTP status is OK
	 */
	@Test
	public void testListProjectsByActiveStatusSuccess() {
		// Given
		boolean isActive = true;

		List<String> expected = new ArrayList<>();
		expected.add(project1.toDTO().toString());
		expected.add(project2.toDTO().toString());

		// When
		ResponseEntity<List<ProjectDTO>> result = projectController.listProjectsByActiveStatus(isActive);

		// Then
		assertEquals(HttpStatus.OK, result.getStatusCode());
		for (ProjectDTO projDTO : result.getBody()) {
			assertTrue(expected.contains(projDTO.toString()));
		}
	}

	/**
	 * Given: a list of all company's projects, all of which are active
	 * When: we ask for a list of all INACTIVE projects
	 * Then: we verify that the obtained list is empty and HttpStatus is OK.
	 */
	@Test
	public void testListProjectsByActiveStatusEmptyList() {
		// Given
		boolean isActive = false;

		// When
		ResponseEntity<List<ProjectDTO>> result = projectController.listProjectsByActiveStatus(isActive);

		// Then
		assertEquals(HttpStatus.OK, result.getStatusCode());
		assertTrue(result.getBody().isEmpty());
	}
	
	/**
	 * GIVEN: a list of all company's projects
	 * WHEN: we ask for a company project with a given ID
	 * THEN: we receive the projectDTO representing the project.
	 */
	@Test
	public void testGetProjectByIdSuccess() {
		
		//Given
		String expected = project1.toDTO().toString();
		
		//When
		ResponseEntity<ProjectDTO> result = projectController.getProjectDTOById(project1.getId());
		
		//Then
		assertEquals(HttpStatus.OK, result.getStatusCode());
		assertEquals(expected, result.getBody().toString());
	}
	
	/**
	 * GIVEN: a list of all company's projects
	 * WHEN: we ask for a project that isn't registered in the company
	 * THEN: we receive the HttpStatus not found.
	 */
	@Test
	public void testGetProjectByIdFailureProjectNotFound() {
		
		//Given
		
		//When
		ResponseEntity<ProjectDTO> result = projectController.getProjectDTOById("id non existant");
		
		//Then
		assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
	}

	/**
	 * GIVEN: all classes that implement the CostCalculation interface
	 * WHEN: we ask for a DTO that contains a list of strings representing all available cost options
	 * THEN: we receive the HttpStatus ok.
	 */
	@Test
	public void testGetAvailableCostOptions() {

		//Given

		//When
		ResponseEntity<AvailableCostOptionsDTO> result = projectController.getAvailableCostOptions();
		
		//Then
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}
}
