package project.controllers.rest;

import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import project.dto.rest.UserRegistryRestDTO;
import project.dto.rest.UserRestDTO;
import project.model.UserService;
import project.model.user.User;
import project.model.user.UserIdVO;

import javax.mail.internet.AddressException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class UserRegistryRESTControllerTest {

	UserService userService;
	UserRepositoryClass userRepositoryClass;

	private UserRegistryRESTController userRegistryRESTController;
	private User user1, user2, user3;
	UserIdVO userIdVO;

	@Before
	public void setUp() throws AddressException {
		userRepositoryClass = new UserRepositoryClass();
		userService = new UserService(userRepositoryClass);
		userRegistryRESTController = new UserRegistryRESTController(userService);

		userService.addUser("Asdrubal", "123456789", "asdrubal@jj.com", "123456789", LocalDate.of(1977, 3, 5), "1",
				"Rua sem saída", "4250-357", "Porto", "Portugal");
		userService.addUser("Manel", "123456789", "manel@jj.com", "123456789", LocalDate.of(1978, 11, 12), "2",
				"Rua sem saída", "4250-357", "Porto", "Portugal");
		userService.addUser("Joaquim", "123456789", "joaquim@jj.com", "123456789", LocalDate.of(1978, 11, 12), "2",
				"Rua sem saída", "4250-357", "Porto", "Portugal");

		user1 = userService.searchUserByEmail("asdrubal@jj.com");
		user1.setProfileCollaborator();
		user1.setActive();

		user2 = userService.searchUserByEmail("manel@jj.com");
		user2.setProfileDirector();
		user2.setActive();

		user3 = userService.searchUserByEmail("joaquim@jj.com");
		user3.setProfileCollaborator();
		user3.setInactive();

		userService.updateUser(user1);
		userService.updateUser(user2);
		userService.updateUser(user3);
	}

	@Test
	public void testListAllUsers() {
		//GIVEN: A list simulating all company's users
		List<UserRestDTO> expectedList = new ArrayList<>();
		expectedList.add(user1.toDTO());
		expectedList.add(user2.toDTO());
		expectedList.add(user3.toDTO());
		UserRegistryRESTController.addSelfLinkToEachElement(expectedList);
		
		//WHEN: We ask the company for all users
		ResponseEntity<List<UserRestDTO>> userListResult = userRegistryRESTController.listAllUsers();
		
		//THEN
		assertEquals(HttpStatus.OK, userListResult.getStatusCode());
		assertEquals(expectedList.toString(), userListResult.getBody().toString());
	}
	

	@Test
	public void testSearchByProfileDirector() {
		//GIVEN: An expected list with one user with profile DIRECTOR, company has user2 
		//defined as profile DIRECTOR
		List<UserRestDTO> expectedList = new ArrayList<>();
		expectedList.add(user2.toDTO());
		UserRegistryRESTController.addSelfLinkToEachElement(expectedList);
		
		//WHEN: We ask the company for all users with profile DIRECTOR
		ResponseEntity<List<UserRestDTO>> userListResult = userRegistryRESTController.listUsersByProfile("ROLE_DIRECTOR");
		
		//THEN
		assertEquals(HttpStatus.OK, userListResult.getStatusCode());
		assertEquals(expectedList, userListResult.getBody());
	}

	@Test
	public void testSearchByProfileCollaborator() {
		//GIVEN: A list with two users with profile COLLABORATOR, company has user1 and user3 defined as having profile COLLABORATOR
		List<UserRestDTO> expectedList = new ArrayList<>();
		expectedList.add(user1.toDTO());
		UserRegistryRESTController.addSelfLinkToEachElement(expectedList);
		
		//WHEN: We ask the company for all users with profile COLLABORATOR
		ResponseEntity<List<UserRestDTO>> userListResult = userRegistryRESTController.listUsersByProfile("ROLE_COLLABORATOR");
		
		//THEN
		assertEquals(HttpStatus.OK, userListResult.getStatusCode());
		assertEquals(expectedList, userListResult.getBody());
	}

	@Test
	public void testSearchByProfileUserNotFound() {
		//GIVEN: a list of user in which none is an ROLE_ADMINISTRATOR
		//WHEN: We ask for all users with profile ADMINISTRATOR
		ResponseEntity<List<UserRestDTO>> userListResult = userRegistryRESTController.listUsersByProfile("ROLE_ADMINISTRATOR");
		
		//THEN we get OK because request was successfull and empty list because we didn't find any administrator
		assertEquals(HttpStatus.OK, userListResult.getStatusCode());
		assertTrue(userListResult.getBody().isEmpty());
	}
	
	@Test
	public void testAddUserSuccess() throws AddressException {
		//GIVEN: A user that not exist on application
		userIdVO = UserIdVO.create("luis@switch.com");
		assertFalse(userRepositoryClass.existsById(userIdVO));

		//WHEN: Add data to user above to do registration and add user on application
		
		UserRegistryRestDTO userDTO = new UserRegistryRestDTO();
		userDTO.setName("Luis");
		userDTO.setEmail("luis@switch.com");
		userDTO.setPhone("123456789");
		userDTO.setTin("123456789");
		userDTO.setBirthDate(LocalDate.of(1977, 3, 5));
		userDTO.setAddressId("1");
		userDTO.setStreet("Rua sem saida");
		userDTO.setPostalCode("3700");
		userDTO.setCity("Porto");
		userDTO.setCountry("Portugal");
		
		HttpEntity<UserRegistryRestDTO> result = userRegistryRESTController.addUser(userDTO);
		
		//THEN: The new user was added with success
		
		ResponseEntity<UserRegistryRestDTO> expected = new ResponseEntity<>(userDTO, HttpStatus.CREATED);

		assertEquals (expected, result);
		assertTrue(userRepositoryClass.existsById(userIdVO));

	}
	@Test
	public void testAddUserFail() throws AddressException {
		//GIVEN: A user that exists on application
		userIdVO = UserIdVO.create("asdrubal@jj.com");
		assertTrue(userRepositoryClass.existsById(userIdVO));

		//WHEN: Add data to user above to do registration and add user on application
		UserRegistryRestDTO userDTO = new UserRegistryRestDTO();
		userDTO.setName("Asdrubal");
		userDTO.setEmail("asdrubal@jj.com");
		userDTO.setPhone("123456789");
		userDTO.setTin("123456789");
		userDTO.setBirthDate(LocalDate.of(1977, 3, 5));
		userDTO.setAddressId("1");
		userDTO.setStreet("Rua sem saida");
		userDTO.setPostalCode("3700");
		userDTO.setCity("Porto");
		userDTO.setCountry("Portugal");
		
		HttpEntity<UserRegistryRestDTO> result = userRegistryRESTController.addUser(userDTO);

		//THEN: The new user wasn't added because already exist on application
		ResponseEntity<UserRegistryRestDTO> expected = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		assertEquals (expected, result);
	}
	
	@Test
	public void testListUsersByEmail() {
		//GIVEN: An expected user found with a given email
		UserRestDTO expected = user1.toDTO();
		expected.add(linkTo(methodOn(UserRegistryRESTController.class).listUsersByEmail(expected.getEmail())).withSelfRel());
		
		//WHEN: We ask for the user with that email
		ResponseEntity<UserRestDTO> result = userRegistryRESTController.listUsersByEmail(user1.getEmail());
		
		//THEN we get HttpStatus.OK and the user
		assertEquals(HttpStatus.OK, result.getStatusCode());
		assertEquals(expected, result.getBody());
	}
	
	@Test
	public void testListUsersByEmailNotFound() throws AddressException {
		//GIVEN: A user which is not in the company's records
		UserRestDTO userNotInCompany = (new User("Gertúlio", "123456789", "gertulio@switch.com", "123456789", LocalDate.of(1977, 3, 5), "1",
				"Rua sem saída", "4250-357", "Porto", "Portugal").toDTO());
		
		//WHEN: We ask for the user with that email
		ResponseEntity<UserRestDTO> result = userRegistryRESTController.listUsersByEmail(userNotInCompany.getEmail());
		
		//THEN: We get a HttpStatus of BAD_REQUEST, indicating a user is not found.
		assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
		assertNull(result.getBody());
	}
}
