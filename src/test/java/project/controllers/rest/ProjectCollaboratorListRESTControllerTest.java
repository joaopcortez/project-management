package project.controllers.rest;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import project.dto.rest.ProjectCollaboratorRestDTO;
import project.dto.rest.UserRestDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.user.User;
import project.services.ProjectCollaboratorService;

public class ProjectCollaboratorListRESTControllerTest {

	ProjectCollaboratorListRESTController projectCollaboratorListRestController;
	
	
	UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;
    LocalDate birth;

    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;

    User userProjectManager, userDirector;
    User userProjectCollaborator, userProjectCollaborator2, userProjectCollaborator3;
    ProjectCollaborator projectCollaborator, projectCollaborator2, projectCollaborator3, projectManager,projectDirector;
    Project project,projectTest;
    Task task;
    LocalDateTime d1;
    LocalDateTime d2;
    
    ProjectCollaboratorRestDTO projectCollaboratorRestDTO;
    String expectedMessage, message, projectId;

	
    @Before
    public void setUp() throws AddressException {
    	
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        
        projectCollaboratorListRestController= new ProjectCollaboratorListRESTController(projectCollaboratorService,userService,projectService);
        
        birth = LocalDate.of(1999, 11, 11);

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Manuel", "96 452 56 56", "manuel@gmail.com", "114622", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Josefina", "96 452 56 56", "josefina@gmail.com", "114622", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Bebiana", "96 452 56 56", "bebiana@gmail.com", "114622", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");

        userProjectManager = userService.searchUserByEmail("asdrubal@jj.com");
        userProjectCollaborator = userService.searchUserByEmail("joaquim@gmail.com");
        userProjectCollaborator2 = userService.searchUserByEmail("manuel@gmail.com");
        userProjectCollaborator3=userService.searchUserByEmail("josefina@gmail.com");
        userDirector=userService.searchUserByEmail("bebiana@gmail.com");

        userProjectManager.setProfileCollaborator();
        userDirector.setProfileDirector();
        userProjectCollaborator.setProfileCollaborator();
        userProjectCollaborator2.setProfileCollaborator();
        userProjectCollaborator3.setProfileCollaborator();
       

        userService.updateUser(userProjectManager);
        userService.updateUser(userDirector);
        userService.updateUser(userProjectCollaborator);
        userService.updateUser(userProjectCollaborator2);
        userService.updateUser(userProjectCollaborator3);

        projectService.addProject("1", "Project 1");
        project = projectService.getProjectByID("1");
        projectCollaboratorService.setProjectManager(project, userProjectManager);
        projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator, 2);

        projectService.addProject("2", "Project 2");
        projectTest = projectService.getProjectByID("2");
        projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator, 2);

        
        projectManager = project.findProjectCollaborator(userProjectManager);
        projectCollaborator = project.findProjectCollaborator(userProjectCollaborator);
        projectCollaborator2 = project.findProjectCollaborator(userProjectCollaborator2);
        projectCollaborator3 = project.findProjectCollaborator(userProjectCollaborator3);
        
    }
	@Test
	public void listProjectCollaborators() {
		
		
		
		// GIVEN
	
				List<ProjectCollaboratorRestDTO> expectedlist = new ArrayList<>();
				
				ProjectCollaboratorRestDTO dto1= new ProjectCollaboratorRestDTO();
				ProjectCollaboratorRestDTO dto2= new ProjectCollaboratorRestDTO();
				
				dto1.setUserId(projectManager.getUser().getEmail());
				dto1.setProjectId(project.getId());
				dto1.setCost(projectManager.getCurrentCost());
				
				
				dto2.setUserId(projectCollaborator.getUser().getEmail());
				dto2.setProjectId(project.getId());
				dto2.setCost(projectCollaborator.getCurrentCost());
				
				expectedlist.add(dto1);
				expectedlist.add(dto2);
				

				// WHEN
				HttpEntity<List<ProjectCollaboratorRestDTO>> result =projectCollaboratorListRestController.listProjectCollaborators(project.getId());

				// THEN
				ResponseEntity<List<ProjectCollaboratorRestDTO>> expected = new ResponseEntity<>(expectedlist, HttpStatus.OK);
				assertEquals(expected.toString(), result.toString());
		
	}
	
	
	@Test
	public void listCollaboratorsNotInProject() {
		
		// GIVEN
				List<UserRestDTO> expectedlist = new ArrayList<>();
				
				userProjectCollaborator2.isActive();
				userProjectCollaborator3.setInactive();
				
				expectedlist.add(userProjectCollaborator2.toDTO());

				// WHEN
				HttpEntity<List<UserRestDTO>> result =projectCollaboratorListRestController.listCollaboratorsNotToProject(project.getId());

				// THEN
				ResponseEntity<List<UserRestDTO>> expected = new ResponseEntity<>(expectedlist, HttpStatus.OK);
				assertEquals(expected.toString(), result.toString());
		
		
		
	}
	
	@Test
	public void listUnassignedProjectCollaborators() {
		
		// GIVEN
				List<ProjectCollaboratorRestDTO> expectedlist = new ArrayList<>();
		
					ProjectCollaboratorRestDTO dto1= new ProjectCollaboratorRestDTO();
					ProjectCollaboratorRestDTO dto2= new ProjectCollaboratorRestDTO();
		
					dto1.setUserId(projectManager.getUser().getEmail());
					dto1.setProjectId(project.getId());
					dto1.setCost(projectManager.getCurrentCost());
		
		
					dto2.setUserId(projectCollaborator.getUser().getEmail());
					dto2.setProjectId(project.getId());
					dto2.setCost(projectCollaborator.getCurrentCost());
		
					expectedlist.add(dto2);

		// WHEN
				HttpEntity<List<ProjectCollaboratorRestDTO>> result =projectCollaboratorListRestController.listUnassignedProjectCollaborators(project.getId());

		// THEN
				ResponseEntity<List<ProjectCollaboratorRestDTO>> expected = new ResponseEntity<>(expectedlist, HttpStatus.OK);
				assertEquals(expected.toString(), result.toString());
		
		
		
	}
}
