package project.controllers.rest;

import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import project.dto.rest.ProjectDTO;
import project.model.project.ProjectService;
import project.services.ProjectUserService;
import project.model.user.User;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class CreateProjectRestControllerTest {

    CreateProjectRestController createProjectRestController;

    ProjectUserService projectUserService;
    ProjectService projectService;

    ProjectRepositoryClass projectRepositoryClass;
    UserRepositoryClass userRepositoryClass;

    ProjectDTO projectDTO;
    ProjectDTO projectExpectedDTO;
    User user;
    ResponseEntity<ProjectDTO> creationResponse;

    @Before
    public void setUp() throws Exception {
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectUserService = new ProjectUserService(projectRepositoryClass, userRepositoryClass);
        createProjectRestController = new CreateProjectRestController(projectUserService);
        projectService = new ProjectService(projectRepositoryClass);

        user = new User("Ana", "123", "asdrubal@switch.com", "2345", LocalDate.of(2018, 5, 31), "2", "Rua ", "4433", "cidade", "país");

        user.setProfileCollaborator();
        user.setActive();
        userRepositoryClass.save(user);

        projectDTO = new ProjectDTO();
        projectDTO.setName("Project name");
        projectDTO.setDescription("Project description");
        projectDTO.setStartDate(LocalDateTime.of(2018, 5, 1, 0, 0));
        projectDTO.setFinalDate(LocalDateTime.of(2018, 8, 1, 0, 0));
        projectDTO.setUserEmail("asdrubal@switch.com");
        projectDTO.setUnit("HOURS");
        projectDTO.setGlobalBudget(90.0);
        projectDTO.setIsActive(true);

        projectExpectedDTO = projectDTO;
        projectExpectedDTO.add(linkTo(methodOn(CreateProjectRestController.class).createProject(projectDTO)).withSelfRel());

    }

    @Test
    @Transactional
    public void testCreateProjectSuccess() {
         /*
        //GIVEN: An projectDTO with correct data to create project.
        //WHEN: We post these projectDTO in HTTP client.
        //THEN We get HttpStatus.CREATED and the created project DTO.
         */

        //GIVEN:
        projectDTO.setProjectId("Project Created");

        //WHEN:
        creationResponse = createProjectRestController.createProject(projectDTO);

        //THEN
        assertEquals("Project Created", creationResponse.getBody().getProjectId());
        assertEquals(HttpStatus.CREATED, creationResponse.getStatusCode());
        assertEquals(projectExpectedDTO.toString(), creationResponse.getBody().toString());
    }

    @Test
    @Transactional
    public void testCreateProjectFail() {
         /*
        //GIVEN: An projectDTO with correct data to create project.
        //WHEN: We post these projectDTO in HTTP client.
        //THEN We get HttpStatus.CREATED and the created project DTO.
         */

        //GIVEN:
        projectDTO.setProjectId(null);

        //WHEN
        creationResponse = createProjectRestController.createProject(projectDTO);

        //THEN
        assertEquals(HttpStatus.BAD_REQUEST, creationResponse.getStatusCode());
    }


    @Test
    @Transactional
    public void testListAllProjects() {

        //GIVEN: A created project
        projectDTO.setProjectId("1");
        createProjectRestController.createProject(projectDTO);

        //WHEN: we ask for a list
        ResponseEntity<List<ProjectDTO>> listResponseEntity = createProjectRestController.listAllProjects();

        //THEN: status code is ok
        assertEquals(HttpStatus.OK, listResponseEntity.getStatusCode());
    }



    @Test
    @Transactional
    public void testGetProjects() {

        //GIVEN: A created project
        projectDTO.setProjectId("1");
        creationResponse= createProjectRestController.createProject(projectDTO);

       // WHEN: we get the project
        ProjectDTO projectDTOOut = createProjectRestController.getProject(creationResponse.getBody().getProjectId());
        //THEN: status code is ok
        assertEquals(projectDTO.toString(), projectDTOOut.toString());
    }

}