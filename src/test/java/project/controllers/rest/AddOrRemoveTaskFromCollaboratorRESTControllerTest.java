package project.controllers.rest;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import project.dto.rest.TaskProjectCollaboratorRestDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.user.User;
import project.services.ProjectCollaboratorService;
import project.services.TaskProjectCollaboratorService;
import project.services.TaskService;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

@DirtiesContext(classMode=ClassMode.BEFORE_CLASS)
public class AddOrRemoveTaskFromCollaboratorRESTControllerTest {

    AddOrRemoveTaskFromCollaboratorRESTController addOrRemoveTaskFromCollaboratorRESTController;
    TaskProjectCollaboratorService taskProjectCollaboratorService;
    UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;
    TaskService taskService;
    LocalDate birth;

    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;

    User userProjectManager;
    User userProjectCollaborator;
    ProjectCollaborator projectCollaborator, projectManager;
    Project project;
    Task task;
    LocalDateTime d1;
    LocalDateTime d2;
    TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO;
    String expectedMessage, message, taskId;

    @Before
    public void setUp() throws AddressException {
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();
        ProjectCollaborator.setStartIdGenerator(1);
        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);
        taskProjectCollaboratorService = new TaskProjectCollaboratorService(taskRepositoryClass, projectCollaboratorRepositoryClass, projectRepositoryClass);
        addOrRemoveTaskFromCollaboratorRESTController = new AddOrRemoveTaskFromCollaboratorRESTController(taskProjectCollaboratorService);
        birth = LocalDate.of(1999, 11, 11);

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");

        userProjectManager = userService.searchUserByEmail("asdrubal@jj.com");
        userProjectCollaborator = userService.searchUserByEmail("joaquim@gmail.com");

        userProjectManager.setProfileCollaborator();
        userProjectCollaborator.setProfileCollaborator();

        userService.updateUser(userProjectManager);
        userService.updateUser(userProjectCollaborator);

        projectService.addProject("1", "Project 1");
        project = projectService.getProjectByID("1");
        projectCollaboratorService.setProjectManager(project, userProjectManager);
        projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator, 2);

        projectManager = projectCollaboratorService.getProjectCollaborator(project.getId(), userProjectManager.getEmail());
        projectCollaborator = projectCollaboratorService.getProjectCollaborator(project.getId(), userProjectCollaborator.getEmail());

        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
        d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

        taskService.addTask(project, "task", "task", d1, d2, 1.2, 1.3);
        task = taskService.findTaskByID("1-1");
        taskProjectCollaboratorRestDTO = new TaskProjectCollaboratorRestDTO();
        taskProjectCollaboratorRestDTO.setProjectCollaboratorId(projectCollaborator.getId());
        taskProjectCollaboratorRestDTO.setEmail(projectCollaborator.getUser().getEmail());
        taskId = task.getId();
    }

    @Test
    @Transactional
    public void addProjectCollaboratorToTaskSucess() {

        //Given:
        assertFalse(task.listActiveCollaborators().contains(projectCollaborator));
        //When:
        HttpEntity<TaskProjectCollaboratorRestDTO> resp = addOrRemoveTaskFromCollaboratorRESTController.addTaskToCollaborator(taskProjectCollaboratorRestDTO, taskId);
        TaskProjectCollaboratorRestDTO result = new TaskProjectCollaboratorRestDTO();
        result.setEmail(taskProjectCollaboratorRestDTO.getEmail());
        result.setTaskId(taskId);
        result.setProjectCollaboratorId(2);
        HttpEntity<TaskProjectCollaboratorRestDTO> expected = new ResponseEntity<>(result,HttpStatus.CREATED);
        //Then:
        expectedMessage = "Project ROLE_COLLABORATOR added";
        assertEquals(expected, resp);

    }

    @Test
    @Transactional
    public void addProjectCollaboratorToTaskFail() {

        //Given:
        addOrRemoveTaskFromCollaboratorRESTController.addTaskToCollaborator(taskProjectCollaboratorRestDTO, taskId);
        task.listActiveCollaborators().contains(projectCollaborator);
        //When:
        HttpEntity<TaskProjectCollaboratorRestDTO> resp = addOrRemoveTaskFromCollaboratorRESTController.addTaskToCollaborator(taskProjectCollaboratorRestDTO, taskId);
        //Then:
        TaskProjectCollaboratorRestDTO result = new TaskProjectCollaboratorRestDTO();
        result.setEmail(taskProjectCollaboratorRestDTO.getEmail());
        result.setTaskId(taskId);
        result.setProjectCollaboratorId(0);
        HttpEntity<TaskProjectCollaboratorRestDTO> expected = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        assertFalse(taskProjectCollaboratorService.addProjectCollaboratorToTask(taskProjectCollaboratorRestDTO));
        
        assertEquals(resp, expected);

    }
    
    
    /**
	 * GIVEN: A task with a project collaborator
	 * WHEN: A project collaborator is remove from task
	 * THEN: A project collaborator was remove from the task active collaborator list
	 */
    
    @Test
    @Transactional
    public void removeProjectCollaboratorFromTaskSucess() {
    	
        
    	//Given
    	addOrRemoveTaskFromCollaboratorRESTController.addTaskToCollaborator(taskProjectCollaboratorRestDTO, taskId);
        task.listActiveCollaborators().contains(projectCollaborator);
        //When
        addOrRemoveTaskFromCollaboratorRESTController.removeTaskFromCollaborator(taskProjectCollaboratorRestDTO, taskId);
        
        //Then
        assertFalse(task.listActiveCollaborators().contains(projectCollaborator));
    }
    
    
    /**
	 * GIVEN: A task without a project collaborator
	 * WHEN: A project collaborator was remove from task
	 * THEN: A project collaborator can not be remove from the task active collaborator list
	 */
    
    @Test
    @Transactional
    public void removeProjectCollaboratorFromTaskFail() {

    	//Given
    	 assertFalse(task.listActiveCollaborators().contains(projectCollaborator));
    	 
    	 //When
    	addOrRemoveTaskFromCollaboratorRESTController.removeTaskFromCollaborator(taskProjectCollaboratorRestDTO, taskId);

    	//Then
        assertEquals(taskProjectCollaboratorRestDTO,taskProjectCollaboratorService.removeProjectCollaboratorFromTaskRest(taskProjectCollaboratorRestDTO,taskId));
        assertFalse(task.listActiveCollaborators().contains(projectCollaborator));

    }
    
    /**
	 * GIVEN: a task that doesn't exist
	 * WHEN: we try to remove task from collaborator
	 * THEN: we receive a taskProjectCollaboratorRestDTO with its taskID set to null, signifying a task
	 * 		not found.
	 */
    @Test
    @Transactional
    public void testRemoveProjectCollaboratorFromTaskFailureTaskNull() {

    	//Given
    	 String invalidTaskID = "not a valid ID";
    	 
    	 //When
    	 ResponseEntity<TaskProjectCollaboratorRestDTO> result =addOrRemoveTaskFromCollaboratorRESTController.removeTaskFromCollaborator(taskProjectCollaboratorRestDTO, invalidTaskID);

    	//Then
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    } 
}