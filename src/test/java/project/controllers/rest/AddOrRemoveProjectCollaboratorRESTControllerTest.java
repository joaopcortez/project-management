package project.controllers.rest;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.dto.rest.ProjectCollaboratorRestDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.services.ProjectUserService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.model.task.Task;
import project.model.user.User;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class AddOrRemoveProjectCollaboratorRESTControllerTest {

	AddOrRemoveProjectCollaboratorControllerREST addOrRemoveProjectCollaboratorRESTControllerTest;
	
		ProjectUserService projectUserService;
		
	    UserService userService;
	    ProjectService projectService;
	    ProjectCollaboratorService projectCollaboratorService;
	    LocalDate birth;

	    UserRepositoryClass userRepositoryClass;
	    ProjectRepositoryClass projectRepositoryClass;
	    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;

	    User userProjectManager, userDirector;
	    User userProjectCollaborator, userProjectCollaborator2, userProjectCollaborator3;
	    ProjectCollaborator projectCollaborator, projectCollaborator2, projectCollaborator3, projectManager,projectDirector;
	    Project project,projectTest;
	    Task task;
	    LocalDateTime d1;
	    LocalDateTime d2;
	    
	    ProjectCollaboratorRestDTO projectCollaboratorRestDTO;
	    String expectedMessage, message, projectId;

	    @Before
	    public void setUp() throws AddressException {
	    	
	        userRepositoryClass = new UserRepositoryClass();
	        projectRepositoryClass = new ProjectRepositoryClass();
	        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();

	        userService = new UserService(userRepositoryClass);
	        projectService = new ProjectService(projectRepositoryClass);
	        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
	        
	        projectUserService = new ProjectUserService(projectRepositoryClass,userRepositoryClass);
	        addOrRemoveProjectCollaboratorRESTControllerTest = new AddOrRemoveProjectCollaboratorControllerREST(projectUserService);
	        
	        birth = LocalDate.of(1999, 11, 11);

	        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
	                "4250-444", "Porto", "Portugal");
	        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
	                "4250-444", "Porto", "Portugal");
	        userService.addUser("Manuel", "96 452 56 56", "manuel@gmail.com", "114622", birth, "1", "Rua do Amial",
	                "4250-444", "Porto", "Portugal");
	        userService.addUser("Josefina", "96 452 56 56", "josefina@gmail.com", "114622", birth, "1", "Rua do Amial",
	                "4250-444", "Porto", "Portugal");
	        userService.addUser("Bebiana", "96 452 56 56", "bebiana@gmail.com", "114622", birth, "1", "Rua do Amial",
	                "4250-444", "Porto", "Portugal");

	        userProjectManager = userService.searchUserByEmail("asdrubal@jj.com");
	        userProjectCollaborator = userService.searchUserByEmail("joaquim@gmail.com");
	        userProjectCollaborator2 = userService.searchUserByEmail("manuel@gmail.com");
	        userProjectCollaborator3=userService.searchUserByEmail("josefina@gmail.com");
	        userDirector=userService.searchUserByEmail("bebiana@gmail.com");

	        userProjectManager.setProfileCollaborator();
	        userDirector.setProfileDirector();
	        userProjectCollaborator.setProfileCollaborator();
	        userProjectCollaborator2.setProfileCollaborator();
	        userProjectCollaborator3.setInactive();

	        userService.updateUser(userProjectManager);
	        userService.updateUser(userDirector);
	        userService.updateUser(userProjectCollaborator);
	        userService.updateUser(userProjectCollaborator2);
	        userService.updateUser(userProjectCollaborator3);

	        projectService.addProject("1", "Project 1");
	        project = projectService.getProjectByID("1");
	        projectCollaboratorService.setProjectManager(project, userProjectManager);
	        projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator, 2);

	        projectService.addProject("2", "Project 2");
	        projectTest = projectService.getProjectByID("2");
	        projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator, 2);

	        
	        projectManager = project.findProjectCollaborator(userProjectManager);
	        projectCollaborator = project.findProjectCollaborator(userProjectCollaborator);
	        projectCollaborator2 = project.findProjectCollaborator(userProjectCollaborator2);
	        projectCollaborator3 = project.findProjectCollaborator(userProjectCollaborator3);
	        
	    }
	    
	    

	    /**
    	 * GIVEN: A project without a project collaborator(projectCollaborator2)
    	 * WHEN: The project collaborator(projectCollaborator2) was added to the project
    	 * THEN: A project collaborator was added to the project collaborator list
    	 * 
    	 */
	    
	    @Test
	    @Transactional
	    public void testAddCollaboratorToProjectSucess() {

	    	//Given
	        assertFalse(project.hasActiveProjectCollaborator(projectCollaborator2));

	        //When
	    	projectCollaboratorRestDTO = new ProjectCollaboratorRestDTO();
	        projectCollaboratorRestDTO.setUserId(userProjectCollaborator2.getEmail());
	        projectCollaboratorRestDTO.setCost(2.5);
	        projectId=project.getId();
	        
	        addOrRemoveProjectCollaboratorRESTControllerTest.addCollaboratorToProject(projectCollaboratorRestDTO ,projectId);
	        
	        projectCollaborator2 = new ProjectCollaborator(project, userProjectCollaborator2, 2.5);
	        
	        //Then
	        assertTrue(project.hasActiveProjectCollaborator(projectCollaborator2));
	    }
	    
	    
	    /**
    	 * GIVEN: A project with a project collaborator
    	 * WHEN: The same project collaborator was added again to the project
    	 * THEN: A project collaborator wasn't added again to the project collaborator list
    	 */
	    
	    @Test
	    @Transactional
	    public void testAddCollaboratorToProjectFail() {

	    	 
	    	//Given
	        assertTrue(project.hasActiveProjectCollaborator(projectCollaborator));
	        
	        //When
	    	projectCollaboratorRestDTO = new ProjectCollaboratorRestDTO();
	        projectCollaboratorRestDTO.setUserId(userProjectCollaborator.getEmail());
	        projectCollaboratorRestDTO.setCost(2.5);
	        projectId = project.getId();
	        
	        addOrRemoveProjectCollaboratorRESTControllerTest.addCollaboratorToProject(projectCollaboratorRestDTO,projectId);
	        
	        projectCollaborator = new ProjectCollaborator(project, userProjectCollaborator, 2.5);
	        
	        //Then
	        assertTrue(project.hasActiveProjectCollaborator(projectCollaborator));
	    }
	    
	    
	    /**
    	 * GIVEN: A null user
    	 * WHEN: A null user was added to the project
    	 * THEN: A null user wasn't added to the project collaborator list
    	 */
	    
	    @Test
	    @Transactional
	    public void testAddCollaboratorToProjectFail_UserNull() {

	    	//Given

	        //When
	    	projectCollaboratorRestDTO = new ProjectCollaboratorRestDTO();
	        projectCollaboratorRestDTO.setUserId(null);
	        projectCollaboratorRestDTO.setCost(2);
	        projectId = project.getId();
	    	
	        addOrRemoveProjectCollaboratorRESTControllerTest.addCollaboratorToProject(projectCollaboratorRestDTO,projectId);
	        projectCollaborator = new ProjectCollaborator(project, null, 2.5);
	        //Then
	        assertEquals(projectCollaboratorRestDTO,projectUserService.addProjectCollaboratorToProjectRest(projectCollaboratorRestDTO,projectId));
	        
	    }
	    
	    
	    /**
    	 * GIVEN: A null project
    	 * WHEN: A project collaborator was added to the project
    	 * THEN: A project collaborator wasn't added to the project
    	 */
	    
	    @Test
	    @Transactional
	    public void testAddCollaboratorToProjectFail_ProjectNull() {

	    	//Given
	        assertFalse(project.listActiveProjectCollaborators().contains(projectCollaborator2));
	        
	        //When
	        projectCollaboratorRestDTO = new ProjectCollaboratorRestDTO();
	        projectCollaboratorRestDTO.setUserId(userProjectCollaborator2.getEmail());
	        projectCollaboratorRestDTO.setCost(2);
	        projectCollaboratorRestDTO.setProjectId(null);
	        
	        addOrRemoveProjectCollaboratorRESTControllerTest.addCollaboratorToProject(projectCollaboratorRestDTO,projectCollaboratorRestDTO.getProjectId());
	        projectCollaborator2 = new ProjectCollaborator(null, userProjectCollaborator2, 2.5);
	        //Then
	        assertEquals(projectCollaboratorRestDTO,projectUserService.addProjectCollaboratorToProjectRest(projectCollaboratorRestDTO,projectId));
	        assertFalse(project.listActiveProjectCollaborators().contains(projectCollaborator2));
	    }
	    
	    
	    /**
    	 * GIVEN: A project without project manager
    	 * WHEN: A project collaborator was added to the project
    	 * THEN: A project collaborator wasn't added to the project
    	 */
	    
	    @Test
	    @Transactional
	    public void testAddCollaboratorToProjectFail_ProjectManagerNull() {

	    	//Given
	        assertFalse(projectTest.listActiveProjectCollaborators().contains(projectCollaborator2));
	        
	        //When
	    	projectCollaboratorRestDTO = new ProjectCollaboratorRestDTO();
	        projectCollaboratorRestDTO.setUserId(userProjectCollaborator2.getEmail());
	        projectCollaboratorRestDTO.setCost(2);
	        projectId = projectTest.getId();
	        
	        addOrRemoveProjectCollaboratorRESTControllerTest.addCollaboratorToProject(projectCollaboratorRestDTO,projectId);
	        projectCollaborator2 = new ProjectCollaborator(projectTest, userProjectCollaborator2, 2.5);
	        //Then
	        expectedMessage = "Project ROLE_COLLABORATOR not added";
	        assertEquals(projectCollaboratorRestDTO,projectUserService.addProjectCollaboratorToProjectRest(projectCollaboratorRestDTO,projectId));
	        assertFalse(projectTest.listActiveProjectCollaborators().contains(projectCollaborator2));
	    }
	    
	    
	    /**
    	 * GIVEN: A inactive user
    	 * WHEN: A project collaborator was added to the project
    	 * THEN: A project collaborator wasn't added to the project
    	 */
	    
	    @Test
	    @Transactional
	    public void testAddCollaboratorToProjectFail_InactiveUser() {

	    	//Given	        
	        assertFalse(project.listActiveProjectCollaborators().contains(projectCollaborator3));
	        
	        //When
	    	projectCollaboratorRestDTO = new ProjectCollaboratorRestDTO();
	        projectCollaboratorRestDTO.setUserId(userProjectCollaborator3.getEmail());
	        projectCollaboratorRestDTO.setCost(2);
	        projectId = project.getId();
	        addOrRemoveProjectCollaboratorRESTControllerTest.addCollaboratorToProject(projectCollaboratorRestDTO,projectId);
	        projectCollaborator3 = new ProjectCollaborator(projectTest, userProjectCollaborator3, 2.5);
	        //Then
	        assertEquals(projectCollaboratorRestDTO,projectUserService.addProjectCollaboratorToProjectRest(projectCollaboratorRestDTO,projectId));
	        assertFalse(project.listActiveProjectCollaborators().contains(projectCollaborator3));
	    }
	    
	    
	    /**
    	 * GIVEN: A user that isn't a collaborator
    	 * WHEN: A project collaborator was added to the project
    	 * THEN: A project collaborator wasn't added to the project
    	 */
	    
	    @Test
	    @Transactional
	    public void testAddCollaboratorToProjectFail_NotCollaborator() {

	    	//Given
	        assertFalse(project.listActiveProjectCollaborators().contains(projectDirector));
	        
	        //When
	    	projectCollaboratorRestDTO = new ProjectCollaboratorRestDTO();
	        projectCollaboratorRestDTO.setUserId(userDirector.getEmail());
	        projectCollaboratorRestDTO.setCost(2);
	        projectId = project.getId();
	        
	        addOrRemoveProjectCollaboratorRESTControllerTest.addCollaboratorToProject(projectCollaboratorRestDTO,projectId);
	        projectDirector = new ProjectCollaborator(projectTest, userDirector, 2.5);
	        //Then
	        assertEquals(projectCollaboratorRestDTO,projectUserService.addProjectCollaboratorToProjectRest(projectCollaboratorRestDTO,projectId));
	        assertFalse(project.listActiveProjectCollaborators().contains(projectDirector));
	    }
	
	
}
