package project.controllers.rest;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import project.dto.rest.LoadUserDataDTO;
import project.dto.rest.UserRestDTO;
import project.model.UserService;
import project.model.project.ProjectService;
import project.model.user.User;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;
import project.jparepositories.RoleRepository;

import javax.mail.internet.AddressException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class LoadDataControllerRESTTest {

    LoadDataControllerREST loadDataControllerREST;
    UserService userService;
    ProjectService projectService;
    TaskService taskService;
    ProjectCollaboratorService projectCollaboratorService;
    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    
    @Autowired
    RoleRepository roleRepository;

    User user;

    @Before
    public void setUp() throws AddressException {
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        
        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        
        loadDataControllerREST = new LoadDataControllerREST(userService, projectService, taskService, projectCollaboratorService);

    }

    /**
     * Given : a filename with JSON input, we extract the value of inputFile field,
     * When : load the loadUserDataFromXMLFile method,
     * Then : we receive a list of users from that file and a CREATED message.
     * @throws Exception
     */
    @Test
    public void testLoadDataControllerSuccess() throws Exception {

        // Given
        String filename = "{\n " + "\t\"inputFile\" : \"firstUsers.xml\"\n" + "}";
        LoadUserDataDTO loadUserDataDTO = new LoadUserDataDTO();
        JSONObject obj = new JSONObject(filename);
        loadUserDataDTO.setFileName(obj.getString("inputFile"));


        // When
        HttpEntity<List<UserRestDTO>> result = loadDataControllerREST.loadUserDataFromXMLFile(filename);
        List<UserRestDTO> usersList = userService.listAllUsersToUI();
        ResponseEntity<List<UserRestDTO>> expected = new ResponseEntity<>(usersList, HttpStatus.CREATED);

        // Then
        assertEquals(expected.toString(), result.toString());
    }

    /**
     * Given : a filename with wrong JSON input, we extract the value of inputFile field, witch is null,
     * When : load the loadUserDataFromXMLFile method,
     * Then : receive a NOT_FOUND message.
     * @throws Exception
     */
    @Test
    public void testLoadDataControllerFail() throws Exception {

        // Given
        String filename = "{\n " + "\t\"inputFile\" : \"\"\n" + "}";
        LoadUserDataDTO loadUserDataDTO = new LoadUserDataDTO();
        JSONObject obj = new JSONObject(filename);
        loadUserDataDTO.setFileName(obj.getString("inputFile"));

        // When
        HttpEntity<List<UserRestDTO>> result = loadDataControllerREST.loadUserDataFromXMLFile(filename);
        ResponseEntity<List<UserRestDTO>> expected = new ResponseEntity<>(HttpStatus.NOT_FOUND);

        // Then
        assertEquals(expected, result);
    }
}
