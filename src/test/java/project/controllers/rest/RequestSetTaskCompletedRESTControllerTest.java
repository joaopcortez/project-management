package project.controllers.rest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import project.dto.rest.TaskProjectCollaboratorRestDTO;
import project.dto.rest.TaskRestDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.user.User;
import project.security.UserPrincipal;
import project.services.ProjectCollaboratorService;
import project.services.ProjectUserService;
import project.services.TaskProjectCollaboratorService;
import project.services.TaskService;

public class RequestSetTaskCompletedRESTControllerTest {

	
	 UserPrincipal userPrincipal;
	 User user1;
	 User user2;
	 
	 private ProjectService projectService;
	 
	 private TaskService taskService;
	 
	 private UserService userService;
	 
	 private TaskProjectCollaboratorService taskProjectCollaboratorService;
	 
	 private ProjectUserService projectUserService;
	 
	 ProjectCollaboratorService projectCollaboratorService;
	 
	 private ProjectRepositoryClass projectRepositoryClass;
     private TaskRepositoryClass taskRepositoryClass;
     private UserRepositoryClass userRepositoryClass;
     private ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
     
     Project project;
     Task task1, task2;
     ProjectCollaborator col1, col2;
     
     RequestSetTaskCompletedRESTController requestSetTaskCompletedRESTController;
 

		@Before
		public void setUp() throws AddressException {
			
			projectRepositoryClass = new ProjectRepositoryClass();
	        taskRepositoryClass = new TaskRepositoryClass();
	        userRepositoryClass= new UserRepositoryClass();
	        projectCollaboratorRepositoryClass= new ProjectCollaboratorRepositoryClass();
			
			projectService = new ProjectService(projectRepositoryClass);
	        taskService = new TaskService(taskRepositoryClass);
	        userService=new  UserService(userRepositoryClass);
	        taskProjectCollaboratorService = new TaskProjectCollaboratorService(taskRepositoryClass, projectCollaboratorRepositoryClass, projectRepositoryClass);
			projectUserService=new ProjectUserService(projectRepositoryClass, userRepositoryClass);
	        projectCollaboratorService=new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
	        
	        requestSetTaskCompletedRESTController= new RequestSetTaskCompletedRESTController(taskProjectCollaboratorService, taskService);
	        
	        
			userService.addUser("Asdrubal", "123456789", "asdrubal@switch.com", "123456789", LocalDate.of(1977, 3, 5), "1",
					"Rua sem saída", "4250-357", "Porto", "Portugal");
			userService.addUser("Manel", "123456789", "manel@jj.com", "123456789", LocalDate.of(1978, 11, 12), "2",
					"Rua sem saída", "4250-357", "Porto", "Portugal");
		
             user1=userService.searchUserByEmail("asdrubal@switch.com");
             user2=userService.searchUserByEmail("manel@jj.com");
             user1.setProfileCollaborator();
             user2.setProfileCollaborator();
             user1.setActive();
             user2.setActive();
             
             
             List<String>listProjectWhereProjectManager = new ArrayList<>();
             List<String>listTasksWhereIsProjectManager = new ArrayList<>();
             List<String>listTasksWhereIsTaskCollaborator = new ArrayList<>();
             userPrincipal = UserPrincipal.create(user1, listProjectWhereProjectManager, listTasksWhereIsTaskCollaborator, listTasksWhereIsProjectManager);
             
             userService.updateUser(user1);
             userService.updateUser(user2);
            
             projectService.addProject("1", "Project1");
             project=projectService.getProjectByID("1");
             
             projectCollaboratorService.setProjectManager(project, user1);
             projectCollaboratorService.addProjectCollaborator(project, user2, 6);
             
             col1=projectCollaboratorService.getProjectCollaborator("1", "asdrubal@switch.com");
             col2=projectCollaboratorService.getProjectCollaborator("1","manel@jj.com");
         
             taskService.addTask(project, "task1");
             taskService.addTask(project, "task2");
             
             task1=taskService.findTaskByID("1-1");
             task2=taskService.findTaskByID("1-2");
             
             taskService.addProjectCollaboratorToTask(col2, task1);
             
             taskService.updateTask(task1);
             taskService.updateTask(task2);
             
             TaskCollaboratorRegistry colTaskRegistry = task1
                     .getTaskCollaboratorRegistryByID(task1.getId() + "-" + col2.getUser().getEmail());
             colTaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));

            task1.addReport(col2, 3, LocalDateTime.now().minusDays(10), LocalDateTime.now().minusDays(2));
            taskService.updateTask(task1);
            
            projectService.updateProject(project);
            projectCollaboratorService.updateProjectCollaborator(col1);
            projectCollaboratorService.updateProjectCollaborator(col2);
		
		}
	
	
		/**
		 * US250
		 * 
		 * GIVEN  a task in progress with a collaborator associated
		 * WHEN the collaborator send a request for set the task completed, using the appropriate URI 
		 * THEN we get an HTTP status of OK and the task save the collaborator email .
		 */
	

	@Test
	public void  requestCompletedTaskFromCollaborator() {
		//GIVEN
		col2.setId(1);
		String taskId= task1.getId();
		assertTrue(task1.getStatus().isOnInProgressState());
		assertTrue(task1.hasProjectCollaborator(col2));
		TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO=new TaskProjectCollaboratorRestDTO();
		taskProjectCollaboratorRestDTO.setEmail(col2.getUser().getEmail());

		

		//WHEN
       
	        ResponseEntity<TaskRestDTO> result= requestSetTaskCompletedRESTController.requestCompletedTaskFromCollaborator(taskProjectCollaboratorRestDTO, taskId);
	        ResponseEntity<TaskRestDTO>  resultGet = requestSetTaskCompletedRESTController.getTask(taskId, userPrincipal);
	        
	        
		//THEN
	       
		
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertTrue(result.getBody().getCompletedRequest().equals(col2.getUser().getEmail()));
		assertThat(resultGet.getStatusCode()).isEqualTo(HttpStatus.OK);

    	assertTrue(resultGet.getBody().getCompletedRequest().equals(col2.getUser().getEmail()));
	}
	
	/**
	 * US250
	 * 
	 * GIVEN  a task not in progress and a collaborator that is not associated with that task
	 * WHEN the collaborator send a request for set the task completed, using the appropriate URI 
	 * THEN we get an HTTP status of BAD_REQUEST and the task don't save the collaborator email .
	 */
	@Test
	public void  requestCompletedTaskFromCollaboratorFail() {
		//GIVEN
	
		assertFalse(task2.getStatus().isOnInProgressState());
		assertFalse(task2.hasProjectCollaborator(col2));
		TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO=new TaskProjectCollaboratorRestDTO();
		taskProjectCollaboratorRestDTO.setEmail(col2.getUser().getEmail());

		   Map<String,String> uriParams=new HashMap<String,String>();
	        uriParams.put("id", task2.getId());
	     
		//WHEN
       
	        ResponseEntity<TaskRestDTO> result= requestSetTaskCompletedRESTController.requestCompletedTaskFromCollaborator(taskProjectCollaboratorRestDTO, task2.getId());
	        ResponseEntity<TaskRestDTO>  resultGet = requestSetTaskCompletedRESTController.getTask(task2.getId(), userPrincipal);
	        
	        
		//THEN
	       
	
	        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
	        assertNull(result.getBody());
        assertThat(resultGet.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertNull(resultGet.getBody().getCompletedRequest());
	}

}
