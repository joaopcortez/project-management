package project.controllers.rest;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import project.dto.rest.ProjectCollaboratorRestDTO;
import project.dto.rest.TaskProjectCollaboratorRestDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.user.User;
import project.services.ProjectCollaboratorService;
import project.services.TaskProjectCollaboratorService;
import project.services.TaskService;

public class ListProjectCollaboratorsToAddTaskRESTControllerTest {
	ListProjectCollaboratorsToAddTaskRESTController listProjectCollaboratorsToAddTaskRESTController;
	    TaskProjectCollaboratorService taskProjectCollaboratorService;
	    UserService userService;
	    ProjectService projectService;
	    ProjectCollaboratorService projectCollaboratorService;
	    TaskService taskService;
	    LocalDate birth;

	    UserRepositoryClass userRepositoryClass;
	    ProjectRepositoryClass projectRepositoryClass;
	    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
	    TaskRepositoryClass taskRepositoryClass;

	    User userProjectManager;
	    User userProjectCollaborator;
	    ProjectCollaborator projectCollaborator, projectManager;
	    Project project;
	    Task task;
	    LocalDateTime d1;
	    LocalDateTime d2;
	    TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO;
	    String expectedMessage, message, taskId;

	    @Before
	    public void setUp() throws AddressException {
	        userRepositoryClass = new UserRepositoryClass();
	        projectRepositoryClass = new ProjectRepositoryClass();
	        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
	        taskRepositoryClass = new TaskRepositoryClass();
	        ProjectCollaborator.setStartIdGenerator(1);
	        userService = new UserService(userRepositoryClass);
	        projectService = new ProjectService(projectRepositoryClass);
	        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
	        taskService = new TaskService(taskRepositoryClass);
	        taskProjectCollaboratorService = new TaskProjectCollaboratorService(taskRepositoryClass, projectCollaboratorRepositoryClass, projectRepositoryClass);
	        listProjectCollaboratorsToAddTaskRESTController = new ListProjectCollaboratorsToAddTaskRESTController(projectService,taskService,projectCollaboratorService);
	        birth = LocalDate.of(1999, 11, 11);

	        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
	                "4250-444", "Porto", "Portugal");
	        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
	                "4250-444", "Porto", "Portugal");

	        userProjectManager = userService.searchUserByEmail("asdrubal@jj.com");
	        userProjectCollaborator = userService.searchUserByEmail("joaquim@gmail.com");

	        userProjectManager.setProfileCollaborator();
	        userProjectCollaborator.setProfileCollaborator();

	        userService.updateUser(userProjectManager);
	        userService.updateUser(userProjectCollaborator);

	        projectService.addProject("1", "Project 1");
	        project = projectService.getProjectByID("1");
	        projectCollaboratorService.setProjectManager(project, userProjectManager);
	        projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator, 2);

	        projectManager = projectCollaboratorService.getProjectCollaborator(project.getId(), userProjectManager.getEmail());
	        projectCollaborator = projectCollaboratorService.getProjectCollaborator(project.getId(), userProjectCollaborator.getEmail());

	        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
	        d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

	        taskService.addTask(project, "task", "task", d1, d2, 1.2, 1.3);
	        task = taskService.findTaskByID("1-1");
	    
	        taskId = task.getId();
	    }

	@Test
	public void testListCollaboratorsNotInTask() {
		
		// GIVEN
		
		
		List<ProjectCollaboratorRestDTO> expectedlist = new ArrayList<>();
		
		ProjectCollaboratorRestDTO dto1=new ProjectCollaboratorRestDTO();
		ProjectCollaboratorRestDTO dto2=new ProjectCollaboratorRestDTO();
		
		dto1.setUserId(projectManager.getUser().getEmail());
		dto1.setUserName(projectManager.getUser().getName());
		dto1.setProjectId(projectManager.getProject().getId());
		dto1.setCost(projectManager.getCurrentCost());
		
		dto2.setUserId(projectCollaborator.getUser().getEmail());
		dto2.setUserName(projectCollaborator.getUser().getName());
		dto2.setProjectId(projectCollaborator.getProject().getId());
		dto2.setCost(projectCollaborator.getCurrentCost());
		
	
		expectedlist.add(dto2);

		// WHEN
		HttpEntity<List<ProjectCollaboratorRestDTO>> result =listProjectCollaboratorsToAddTaskRESTController.listCollaboratorsNotInTask(task.getId());
		// THEN
		ResponseEntity<List<ProjectCollaboratorRestDTO>> expected = new ResponseEntity<>(expectedlist, HttpStatus.OK);
      

		assertEquals(expected.toString(), result.toString());
	
	}
	
	
	@Test
	public void testListCollaboratorsNotInTaskFail() {
		
		// GIVEN
		
		
		List<ProjectCollaboratorRestDTO> expectedlist = new ArrayList<>();
		taskService.addProjectCollaboratorToTask(projectCollaborator, task);
		
		
	
		

		// WHEN
		HttpEntity<List<ProjectCollaboratorRestDTO>> result =listProjectCollaboratorsToAddTaskRESTController.listCollaboratorsNotInTask(task.getId());
		// THEN
		ResponseEntity<List<ProjectCollaboratorRestDTO>> expected = new ResponseEntity<>(expectedlist, HttpStatus.OK);
      

		assertEquals(expected.toString(), result.toString());
	
	}


}
