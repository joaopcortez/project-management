package project.controllers.authcontroller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import project.jparepositories.RoleRepository;
import project.model.UserService;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.payload.JwtAuthenticationResponse;
import project.payload.LoginRequest;
import project.payload.UserIdentityAvailability;
import project.payload.UserProfile;
import project.security.UserPrincipal;
import system.dto.LoginTokenDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class UserControllerTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;
	@Autowired
	UserService userService;
	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	private User user1;
	
	private JwtAuthenticationResponse jwt;
	
	UserPrincipal userPrincipal;
	
	@Before
	public void setUp() throws Exception {
		
		Role rolesCollaborator = roleRepository.getOneByName(RoleName.ROLE_COLLABORATOR);

		if (!roleRepository.existsByName(RoleName.ROLE_COLLABORATOR)) {
			rolesCollaborator = new Role(RoleName.ROLE_COLLABORATOR);
			roleRepository.save(rolesCollaborator);
		}

		userService.addUser("Asdrubal", "123456789", "asdrubal@jj.com", "123456789", LocalDate.of(1977, 3, 5), "1",
				"Rua sem saída", "4250-357", "Porto", "Portugal");
		
		String pass = passwordEncoder.encode("12345");

		user1 = userService.searchUserByEmail("asdrubal@jj.com");
		user1.setPassword(pass);
		user1.setRoles(Collections.singleton(rolesCollaborator));
		user1.setActive();

		userService.updateUser(user1);

		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("asdrubal@jj.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);
		
		jwt = responseEntity.getBody();
	}

	/**
	 * GIVEN a logged in user, 
	 * WHEN we ask for the current user
	 * THEN we get his name and email
	 */
	@Test
	public void testGetCurrentUser() {
		
		//Given
		LoginTokenDTO loginDTO = new LoginTokenDTO();
		loginDTO.setEmail("asdrubal@jj.com");
		loginDTO.setName("Asdrubal");
		
		//When
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		
		ResponseEntity<LoginTokenDTO> responseGet = restTemplate.exchange("/api/user/me",
				HttpMethod.GET, entity, LoginTokenDTO.class);
		
		//Then
		assertThat(responseGet.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertEquals(loginDTO.getName(), responseGet.getBody().getName());
		assertEquals(loginDTO.getEmail(), responseGet.getBody().getEmail());
	}

	/**
	 * GIVEN an email that is not present in the database,
	 * WHEN we ask to check email availability,
	 * THEN we get a response with status OK and a body with availability TRUE.
	 */
	@Test
	public void testCheckEmailAvailabilityIsAvailable() {
		
		//Given
		boolean available = true;
		UserIdentityAvailability userIdentityExpected = new UserIdentityAvailability(available);
		String email = "john@switch.org";
		
		//When
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		
		ResponseEntity<UserIdentityAvailability> response = restTemplate.exchange("/api/user/checkEmailAvailability?email=" + email,
				HttpMethod.GET, entity, UserIdentityAvailability.class);
		
		//Then
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertEquals(userIdentityExpected.getAvailable(), response.getBody().getAvailable());
	}

	/**
	 * GIVEN an email that is already present in the database,
	 * WHEN we ask to check email availability,
	 * THEN we get a response with status OK and a body with availability FALSE.
	 */
	@Test
	public void testCheckEmailAvailabilityIsNotAvailable() {
		
		//Given
		boolean available = false;
		UserIdentityAvailability userIdentityExpected = new UserIdentityAvailability(available);
		String email = "asdrubal@jj.com";
		
		//When
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		
		ResponseEntity<UserIdentityAvailability> response = restTemplate.exchange("/api/user/checkEmailAvailability?email=" + email,
				HttpMethod.GET, entity, UserIdentityAvailability.class);
		
		//Then
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertEquals(userIdentityExpected.getAvailable(), response.getBody().getAvailable());
	}

	/**
	 * GIVEN a user that is present in the database,
	 * WHEN we ask for his profile
	 * THEN we get a response with status OK and a body with his name and email.
	 */
	@Test
	public void testGetUserProfileSuccess() {
		
		//Given
		UserProfile expectedUserProfile = new UserProfile(user1);
		Map<String, String> uriParams = new HashMap<String, String>();
		uriParams.put("email", user1.getEmail());
		
		//When
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		
		ResponseEntity<UserProfile> response = restTemplate.exchange("/api/users/{email}", HttpMethod.GET, entity,
				UserProfile.class, uriParams);
		
		//Then
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertEquals(expectedUserProfile.getEmail(), response.getBody().getEmail());
		assertEquals(expectedUserProfile.getName(), response.getBody().getName());
	}
	
	/**
	 * GIVEN a user that is not registered in the database,
	 * WHEN we ask for his profile
	 * THEN we get a response with name and email null.
	 */
	@Test
	public void testGetUserProfileFailureEmailNotFound() {
		
		//Given
		Map<String, String> uriParams = new HashMap<String, String>();
		uriParams.put("email", "emailNonExistantInDB@sss.dd");
		
		//When
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		
		ResponseEntity<UserProfile> response = restTemplate.exchange("/api/users/{email}", HttpMethod.GET, entity,
				UserProfile.class, uriParams);
		
		//Then
		assertNull(response.getBody().getEmail());
		assertNull(response.getBody().getName());
	}

}
