package project.controllers.authcontroller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import project.jparepositories.RoleRepository;
import project.model.UserService;
import project.model.user.User;
import project.model.user.UserLoginData;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.payload.ApiAuthResponse;
import project.payload.JwtAuthenticationResponse;
import project.payload.LoginRequest;
import project.payload.SignUpRequest;

import javax.mail.internet.AddressException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
public class AuthControllerTest {

    @Autowired
    AuthController authController;
    @Autowired
    BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private UserService userService;
    @Autowired
    private RoleRepository roleRepository;
    private User user, user2;

    @Before
    public void setUp() throws AddressException {
        Role roleCollab = roleRepository.getOneByName(RoleName.ROLE_COLLABORATOR);

        userService.addUser("Moon", "919898967", "Universe@stars.moon",
                "123321123", LocalDate.of(1980, 12, 18),
                "Casa", "Street", "4100-100", "NY", "Mars");

        userService.addUser("Moon", "919898967", "pedro@mail.com",
                "123321123", LocalDate.of(1980, 12, 18),
                "Casa", "Street", "4100-100", "NY", "Mars");

        user = userService.searchUserByEmail("Universe@stars.moon");
        user2 = userService.searchUserByEmail("pedro@mail.com");

        String pass = passwordEncoder.encode("qwerty1!");
        user.setPassword(pass);
        user.setRoles(Collections.singleton(roleCollab));

        user2.setPassword(pass);
        user2.setRoles(Collections.singleton(roleCollab));
        UserLoginData userLoginData = new UserLoginData();
        userLoginData.setApprovedDateTime(LocalDateTime.now());
        user2.setUserLoginData(userLoginData);

        userService.updateUser(user);
        userService.updateUser(user2);

    }

    @Test
    public void authenticateUser_success() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsernameOrEmail(user2.getEmail());
        loginRequest.setPassword("qwerty1!");

        ResponseEntity responseEntity = authController.authenticateUser(loginRequest);
        assertEquals(HttpStatus.ACCEPTED, responseEntity.getStatusCode());

    }

    @Test//todo
    public void authenticateUser_successFirstLogin() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsernameOrEmail("Universe@stars.moon");
        loginRequest.setPassword("qwerty1!");

        ResponseEntity responseEntity = authController.authenticateUser(loginRequest);
        JwtAuthenticationResponse jwt = (JwtAuthenticationResponse) responseEntity.getBody();
        assertTrue(jwt.isNeedActivation());
        assertEquals(HttpStatus.ACCEPTED, responseEntity.getStatusCode());

    }

    @Test
    public void registerUser_success() throws AddressException {

        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setEmail("adela@switch.com");
        signUpRequest.setPassword("1234545");
        signUpRequest.setPhone("917393727");
        signUpRequest.setName("LisaAdelaide");
        signUpRequest.setBirthDate("1980-12-18");
        signUpRequest.setTaxPayerId("12345678");
        signUpRequest.setAddressId("casara");
        signUpRequest.setStreet("anyhjhjstreet");
        signUpRequest.setCity("Awaykjhkjh");
        signUpRequest.setCountry("fakjhkjhaway");
        signUpRequest.setPostalCode("4100-001");

        ResponseEntity responseEntity = authController.registerUser(signUpRequest);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());

    }

    @Test
    public void registerUser_failMailInUse() throws AddressException {

        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setEmail("Universe@stars.moon");
        signUpRequest.setPassword("1234545");
        signUpRequest.setPhone("917393727");
        signUpRequest.setName("LisaAdelaide");
        signUpRequest.setBirthDate("1980-12-18");
        signUpRequest.setTaxPayerId("12345678");
        signUpRequest.setAddressId("casara");
        signUpRequest.setStreet("anyhjhjstreet");
        signUpRequest.setCity("Awaykjhkjh");
        signUpRequest.setCountry("fakjhkjhaway");
        signUpRequest.setPostalCode("4100-001");

        ResponseEntity responseEntity = authController.registerUser(signUpRequest);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody().toString(), new ApiAuthResponse(false, "Email Address already in use!").toString());

    }

}