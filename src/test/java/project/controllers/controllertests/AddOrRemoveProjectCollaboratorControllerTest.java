package project.controllers.controllertests;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.controllers.consolecontrollers.AddOrRemoveProjectCollaboratorController;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;
import project.model.user.User;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AddOrRemoveProjectCollaboratorControllerTest {

    UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;
    TaskService taskService;

    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;

    LocalDate birth;
    List<User> expectedUser;
    Project project1;
    User user1;
    User user2;
    User user3;
    AddOrRemoveProjectCollaboratorController tarpc;

    @Before
    public void initialize() throws AddressException {
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);

        tarpc = new AddOrRemoveProjectCollaboratorController(userService, projectService, projectCollaboratorService, taskService);

        birth = LocalDate.of(1999, 11, 11);

        expectedUser = new ArrayList<>();

        projectService.addProject("111", "Project US350");

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1",
                "Rua do Amial", "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1",
                "Rua do Amial", "4250-444", "Porto", "Portugal");
        userService.addUser("Joana", "93 333 33 33", "joana@gmail.com", "112222", birth, "1",
                "Rua do Amial", "4250-444", "Porto", "Portugal");

        project1 = projectService.getProjectByID("111");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");
        user3 = userService.searchUserByEmail("joana@gmail.com");

        user1.setProfileCollaborator();
        user2.setProfileCollaborator();
        user3.setProfileCollaborator();
        projectCollaboratorService.setProjectManager(projectService.getProjectByID("111"), user1);

        userService.updateUser(user1);
        userService.updateUser(user2);
        userService.updateUser(user3);

        projectService.updateProject(project1);
        project1 = projectService.getProjectByID("111");

    }

    @Test
    @Transactional
    public void testAddProjectCollaboratorSucess() {
        /*
        GIVEN:
        Assert joana@gmail.com doesn't isn't in the projecCollaboratorList.
         */
        assertNull(projectCollaboratorService.getProjectCollaborator("111", "joana@gmail.com"));

         /*
        WHEN:
        Add joana@gmail.com AS projecCollaborator of project "111".
         */
        tarpc.addProjectCollaborator("111", "joana@gmail.com", 10.0);

        /*
        THEN:
        Assert joana@gmail.com is in the projecCollaboratorList.
         */
        assertNotNull(projectCollaboratorService.getProjectCollaborator("111", "joana@gmail.com"));

    }
    
    @Test
    @Transactional
    public void testAddProjectCollaboratorSucessCollaboratorPreviouslyInProject() {
        /*
        GIVEN:
        Assert joana@gmail.com isn't in the projecCollaboratorList.
         */
        assertNull(projectCollaboratorService.getProjectCollaborator("111", "joana@gmail.com"));
        
        //Adding collaborator to project and asserting she's now an active projectCollaborator
        tarpc.addProjectCollaborator("111", "joana@gmail.com", 10.0); 
        ProjectCollaborator projCollab = projectCollaboratorService.getProjectCollaborator("111", "joana@gmail.com");
        assertTrue(project1.hasActiveProjectCollaborator(projCollab));
        
        //Removing her from project collaborators and asserting she's not active in the project
        tarpc.removeProjectCollaborator("111", "joana@gmail.com");
        assertFalse(project1.hasActiveProjectCollaborator(projCollab));

         /*
        WHEN:
        Adding again joana@gmail.com as projectCollaborator of project "111".
         */
        tarpc.addProjectCollaborator("111", "joana@gmail.com", 10.0);
        /*
        THEN:
        Assert joana@gmail.com is in the projecCollaboratorList.
         */
        assertNotNull(projectCollaboratorService.getProjectCollaborator("111", "joana@gmail.com"));
        projCollab = projectCollaboratorService.getProjectCollaborator("111", "joana@gmail.com");
        assertTrue(project1.hasActiveProjectCollaborator(projCollab));

    }

    @Test
    @Transactional
    public void testAddProjectCollaboratorProjectNull() {
        /*
        GIVEN:
        Assert joana@gmail.com doesn't isn't in the projecCollaboratorList.
         */
        assertNull(projectCollaboratorService.getProjectCollaborator("1", "joana@gmail.com"));

         /*
        WHEN:
        Add joana@gmail.com AS projecCollaborator of project "111".
         */
        tarpc.addProjectCollaborator("1", "joana@gmail.com", 10.0);

        /*
        THEN:
        Assert joana@gmail.com is in the projecCollaboratorList.
         */
        String result = "\n" +
                "CAN NOT ADD COLLABORATOR TO THIS PROJECT.";
        assertEquals(result, tarpc.addProjectCollaborator("1", "joana@gmail.com", 10.0));

    }

    @Test
    @Transactional
    public void testAddProjectCollaboratorFailUserIsNotInCompany() {

        /*
        GIVEN:
        Assert "ana@gmail.com" isn't a user.
         */
        assertNull(userService.searchUserByEmail("ana@gmail.com"));
        /*
        WHEN:
        Add "ana@gmail.com" to project "111"
         */
        String message = tarpc.addProjectCollaborator("111", "ana@gmail.com", 10.0);
        /*
        THEN:
         */

        String result = "\n" +
                "CAN NOT ADD COLLABORATOR TO THIS PROJECT.";
        assertEquals(result, message);
        assertEquals(result, tarpc.addProjectCollaborator("111", "ana@gmail.com", 10.0));

    }

    @Test
    @Transactional
    public void testAddProjectCollaboratorFailUserIsAlreadyInProject() {
        tarpc.addProjectCollaborator("111", "joaquim@gmail.com", 10.0);
        String result = "\n" +
                "CAN NOT ADD COLLABORATOR TO THIS PROJECT.";
        assertEquals(result, tarpc.addProjectCollaborator("111", "joaquim@gmail.com", 10.0));

    }

    @Test
    @Transactional
    public void testRemoveProjectCollaboratorSucess() {
        tarpc.addProjectCollaborator("111", "joana@gmail.com", 10.0);

        assertNotNull(projectCollaboratorService.getProjectCollaborator("111", "joana@gmail.com"));
         tarpc.removeProjectCollaborator("111", "joana@gmail.com");
        assertFalse(projectCollaboratorService.getProjectCollaborator("111", "joana@gmail.com").isActiveInProject());

    }

    @Test
    @Transactional
    public void testRemoveProjectCollaboratorFailUserIsNotInCompany() {
        String result = "ROLE_COLLABORATOR not found";

        assertEquals(result, tarpc.removeProjectCollaborator("111", "ana@gmail.com"));

    }

    @Test
    @Transactional
    public void testRemoveProjectCollaboratorFailUserIsNotInProject() {
        String result = "ROLE_COLLABORATOR not found";

        assertEquals(result, tarpc.removeProjectCollaborator("111", "joana@gmail.com"));

    }

    @Test
    @Transactional
    public void testListCollaboratorsNotInProject() {

        projectCollaboratorService.addProjectCollaborator(project1, user2, 3);

        StringBuilder sb = new StringBuilder();
        sb.append("\n[1] ROLE_COLLABORATOR name:  " + user3.getName());
        sb.append("\n    ROLE_COLLABORATOR email: " + user3.getEmail() + "\n");

        String expected = sb.toString();
        String result = tarpc.listCollaboratorsNotInProject("111");

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testListCollaboratorsNotInProjectEmptyList() {

        projectCollaboratorService.addProjectCollaborator(project1, user2, 3);
        projectCollaboratorService.addProjectCollaborator(project1, user3, 3);

        String expected = "THERE ARE NO PROJECTS COLLABORATORS AVAILABLE TO ADD TO THIS PROJECT";
        String result = tarpc.listCollaboratorsNotInProject("111");

        assertEquals(expected, result);
    }

}
