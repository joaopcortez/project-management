package project.controllers.controllertests;

import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.controllers.consolecontrollers.UserRegistryController;
import project.model.UserService;
import project.model.user.User;

import javax.mail.internet.AddressException;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class UserServiceControllerTest {

    UserService userService;
    UserRepositoryClass userRepo;
    UserRegistryController userRegistryController;
    User user1, user2, user3;

    @Before
    public void setUp() throws AddressException {
        userRepo = new UserRepositoryClass();
        userService = new UserService(userRepo);
        userRegistryController = new UserRegistryController(userService);

        userService.addUser("Asdrubal", "123456789", "asdrubal@jj.com", "123456789",
                LocalDate.of(1977, 3, 5), "1", "Rua sem saída", "4250-357", "Porto", "Portugal");
        userService.addUser("Manel", "123456789", "manel@jj.com", "123456789", LocalDate.of(1978, 11, 12),
                "2", "Rua sem saída", "4250-357", "Porto", "Portugal");
        userService.addUser("Joaquim", "123456789", "joaquim@jj.com", "123456789",
                LocalDate.of(1978, 11, 12), "2", "Rua sem saída", "4250-357", "Porto", "Portugal");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user1.setProfileCollaborator();
        user1.setActive();

        user2 = userService.searchUserByEmail("manel@jj.com");
        user2.setProfileDirector();
        user2.setActive();

        user3 = userService.searchUserByEmail("joaquim@jj.com");
        user3.setProfileCollaborator();
        user3.setInactive();

//        userService.updateUser(user1);
//        userService.updateUser(user2);
//        userService.updateUser(user3);
    }

    @Test
    public void testListAllUsers() {
        assertEquals(
                "User name: Asdrubal\n" + "User email: asdrubal@jj.com\n" + "User profile: [ROLE_COLLABORATOR]\n"
                        + "User state: Active\n" + "User name: Joaquim\n" + "User email: joaquim@jj.com\n" +
                        "User profile: [ROLE_COLLABORATOR]\n" + "User state: Inactive\n" + "User name: Manel\n" +
                        "User email: manel@jj.com\n" + "User profile: [ROLE_DIRECTOR]\n" + "User state: Active\n",
                userRegistryController.listAllUsers());
    }

    @Test
    public void testSearchByEmail() {
        assertEquals("User name: Asdrubal\n" + "User email: asdrubal@jj.com\n" + "User profile: [ROLE_COLLABORATOR]\n",
                userRegistryController.searchUserByEmail("asdrubal@jj.com"));
    }

    @Test
    public void testSearchByEmailNotFound() {
        assertEquals("User not found.", userRegistryController.searchUserByEmail("abc@jj.com"));
    }

    @Test
    public void testSearchByProfileDirector() {
        assertEquals("\nUser name: Manel\n" + "User email: manel@jj.com\t\t" + "User profile: [ROLE_DIRECTOR]\n",
                userRegistryController.searchUserByProfile("ROLE_DIRECTOR"));
    }

    @Test
    public void testSearchByProfileCollaborator() {
        assertEquals("\nUser name: Asdrubal\n" + "User email: asdrubal@jj.com\t\t" + "User profile: [ROLE_COLLABORATOR]\n\n"
                        + "User name: Joaquim\n" + "User email: joaquim@jj.com\t\t" + "User profile: [ROLE_COLLABORATOR]\n",
                userRegistryController.searchUserByProfile("ROLE_COLLABORATOR"));
    }

    @Test
    public void testSearchByProfileUserNotFound() {
        assertEquals("There are not users with that profile.",
                userRegistryController.searchUserByProfile("ROLE_ADMINISTRATOR"));
    }

}
