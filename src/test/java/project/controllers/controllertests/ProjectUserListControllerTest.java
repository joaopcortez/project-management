package project.controllers.controllertests;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.controllers.consolecontrollers.ProjectUserListController;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.model.task.Task;
import project.services.TaskService;
import project.model.user.User;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.*;

public class ProjectUserListControllerTest {

    ProjectUserListController ulc;

    UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;
    TaskService taskService;

    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;

    Project project1;
    List<User> expectedUser;

    LocalDate birth;
    User user1;
    User user2;

    ProjectCollaborator projectManager, collaborat2;

    Task task1;

    LocalDateTime d1;
    LocalDateTime d2;

    @Before
    public void setUp() throws AddressException {
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);

        ulc = new ProjectUserListController(userService, projectService, projectCollaboratorService);

        birth = LocalDate.of(1999, 11, 11);

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");

        user1.setProfileCollaborator();
        user2.setProfileCollaborator();

        userService.updateUser(user1);
        userService.updateUser(user2);

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");

        /*
         * Project 1 id= "1"
         *
         * user1 is project manager users 2, 3 and 4 are added to the project and become
         * ProjectCollaborators user2 = projectCollaborator2 and costs 2; user3 =
         * projectCollaborator3 and costs 4; user4 = projectCollaborator3 and costs 5;
         *
         * project 1 has 6 tasks
         *
         * tasks 1 and 2 have reports tasks 1 and 2 are pending
         *
         * tasks 3 and 4 have no collaborators assigned, so they don't have a start date
         * and no end date
         *
         * tasks 5 and 6 have reports and are concluded
         *
         *
         * projectCollaborator2 is in task 1 and 2 in task 1 has one report on 4 hours
         * in task 2 has 4 reports of 4,8,8,4 hours
         *
         * projectCollaborator3 is in task 1, 2, 5 and 6 has reports in task 5 and 6 1
         * report in task5 1 report in task6
         *
         * projectCollaborator4 has no assigned tasks
         *
         *
         */
        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
        d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

        projectService.addProject("1", "Project 1");
        project1 = projectService.getProjectByID("1");

        projectCollaboratorService.setProjectManager(project1, user1);

        project1.setStartDate(d1);
        projectManager = project1.findProjectCollaborator(user1);

        projectCollaboratorService.addProjectCollaborator(project1, user1, 12);
        projectCollaboratorService.addProjectCollaborator(project1, user2, 10);

        taskService.addTask(project1, "task1");

        task1 = project1.findTaskByTitle("task1");
        task1.setPredictedDateOfStart(d1);
        task1.setPredictedDateOfConclusion(d2);
        collaborat2 = project1.findProjectCollaborator(user2);

        projectService.updateProject(project1);

    }

    @Test
    @Transactional
    public void testGetAllProjectCollaboratorEmptyList() {
        projectService.addProject("123", "no colabs");
        String result = ulc.listActiveProjectCollaborators("123");
        String expected = "There are no Collaborators available in this Project";
        assertEquals(expected, result);

    }

    @Test
    @Transactional
    public void testGetAllProjectCollaborator() {

        String result = ulc.listActiveProjectCollaborators("1");
        String expected = "\n[" + "1" + "] Name: " + "Asdrubal" +
                "\n    Email: " + "asdrubal@jj.com" + "\n" +
                "\n" + "[2" + "] Name: " + "Joaquim" +
                "\n    Email: " + "joaquim@gmail.com" + "\n";

        assertEquals(expected, result);

    }

    @Test
    @Transactional
    public void testGetUnassignedProjectCollaboratorList() {

        assertTrue(ulc.getUnassignedProjectCollaboratorList("1").contains("joaquim@gmail.com"));
        task1.addProjectCollaborator(collaborat2);
        taskService.updateTask(task1);
        assertFalse(ulc.getUnassignedProjectCollaboratorList("1").contains("joaquim@gmail.com"));

    }

}
