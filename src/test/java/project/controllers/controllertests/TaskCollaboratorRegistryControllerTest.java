package project.controllers.controllertests;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.controllers.consolecontrollers.TaskCollaboratorRegistryController;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.model.task.Task;
import project.services.TaskService;
import project.model.user.User;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TaskCollaboratorRegistryControllerTest {

    TaskCollaboratorRegistryController tcrc;

    TaskService taskService;
    UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;

    UserRepositoryClass userRepository;
    ProjectRepositoryClass projectRepository;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepository;
    TaskRepositoryClass taskRepository;
    List<User> expectedUser;

    LocalDate birth;
    User user1;
    User user2;
    User user3;
    User user4;
    ProjectCollaborator projectCollaborator2, projectCollaborator3, projectCollaborator4, projectManager;

    Project project1;
    Project project2;

    Task task1, task2, task3, task4, task5, task6, task7;

    LocalDateTime d1;
    LocalDateTime d2;
    LocalDateTime d3;
    LocalDateTime d4;
    int y1;
    Month m1;

    @Before
    public void setUp() throws AddressException {
        userRepository = new UserRepositoryClass();
        taskRepository = new TaskRepositoryClass();
        projectRepository = new ProjectRepositoryClass();
        projectCollaboratorRepository = new ProjectCollaboratorRepositoryClass();

        userService = new UserService(userRepository);
        taskService = new TaskService(taskRepository);
        projectService = new ProjectService(projectRepository);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepository);

        tcrc = new TaskCollaboratorRegistryController(userService, projectService);

        birth = LocalDate.of(1999, 11, 11);
        y1 = LocalDateTime.now().getYear();
        m1 = LocalDateTime.now().getMonth();

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joana", "93 333 33 33", "joana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Lilina", "93 333 33 33", "liliana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");
        user3 = userService.searchUserByEmail("joana@gmail.com");
        user4 = userService.searchUserByEmail("liliana@gmail.com");

        user2.setProfileCollaborator();
        user3.setProfileCollaborator();
        user4.setProfileCollaborator();

        userService.updateUser(user2);
        userService.updateUser(user3);
        userService.updateUser(user4);

        user2 = userService.searchUserByEmail("joaquim@gmail.com");
        user3 = userService.searchUserByEmail("joana@gmail.com");
        user4 = userService.searchUserByEmail("liliana@gmail.com");

        /*
         * Project 1 id= "1"
         *
         * user1 is project manager users 2, 3 and 4 are added to the project and become
         * ProjectCollaborators user2 = projectCollaborator2 and costs 2; user3 =
         * projectCollaborator3 and costs 4; user4 = projectCollaborator3 and costs 5;
         *
         * project 1 has 6 tasks
         *
         * tasks 1 and 2 have reports tasks 1 and 2 are pending
         *
         * tasks 3 and 4 have no collaborators assigned, so they don't have a start date
         * and no end date
         *
         * tasks 5 and 6 have reports and are concluded
         *
         *
         * projectCollaborator2 is in task 1 and 2 in task 1 has one report on 4 hours
         * in task 2 has 4 reports of 4,8,8,4 hours
         *
         * projectCollaborator3 is in task 1, 2, 5 and 6 has reports in task 5 and 6 1
         * report in task5 1 report in task6
         *
         * projectCollaborator4 has no assigned tasks
         *
         *
         */

        projectService.addProject("1", "Project 1");
        project1 = projectService.getProjectByID("1");

        projectService.addProject("2", "teste2");
        project2 = projectService.getProjectByID("2");

        projectCollaboratorService.setProjectManager(project1, user1);
        projectCollaboratorService.addProjectCollaborator(project1, user2, 2);
        projectCollaboratorService.addProjectCollaborator(project1, user3, 4);
        projectCollaboratorService.addProjectCollaborator(project1, user4, 5);

        projectManager = project1.findProjectCollaborator(user1);
        projectCollaborator2 = project1.findProjectCollaborator(user2);
        projectCollaborator3 = project1.findProjectCollaborator(user3);
        projectCollaborator4 = project1.findProjectCollaborator(user4);

        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
        d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

        taskService.addTask(project1, "task1");
        taskService.addTask(project1, "task2");
        taskService.addTask(project1, "task3");
        taskService.addTask(project1, "task4");
        taskService.addTask(project1, "task5");
        taskService.addTask(project1, "task6");

        task1 = taskService.findTaskByTitle("task1");
        task2 = taskService.findTaskByTitle("task2");
        task3 = taskService.findTaskByTitle("task3");
        task4 = taskService.findTaskByTitle("task4");
        task5 = taskService.findTaskByTitle("task5");
        task6 = taskService.findTaskByTitle("task6");

        task1.setPredictedDateOfStart(d1);
        task1.setPredictedDateOfConclusion(d2);
        task2.setPredictedDateOfStart(d1);
        task2.setPredictedDateOfConclusion(d2);
        task3.setPredictedDateOfStart(d1);
        task3.setPredictedDateOfConclusion(d2);
        task4.setPredictedDateOfStart(d1);
        task4.setPredictedDateOfConclusion(d2);
        task5.setPredictedDateOfStart(d1);
        task5.setPredictedDateOfConclusion(d2);
        task6.setPredictedDateOfStart(d1);
        task6.setPredictedDateOfConclusion(d2);

        task7 = new Task(project1, "task7");

        task1.addProjectCollaborator(projectCollaborator2);
        task2.addProjectCollaborator(projectCollaborator2);
        task3.addProjectCollaborator(projectCollaborator2);

        task5.addProjectCollaborator(projectCollaborator2);

        task5.addReport(projectCollaborator2, 5, d1, d1.plusDays(5));
        task3.addReport(projectCollaborator2, 7, d1, d1.plusDays(5));

        taskService.updateTask(task1);
        taskService.updateTask(task2);
        taskService.updateTask(task3);
        taskService.updateTask(task4);
        taskService.updateTask(task5);
        taskService.updateTask(task6);
        taskService.updateTask(task7);

    }

    @Test
    @Transactional
    public void listAssignmentRequestsTest() {

        StringBuilder sb = new StringBuilder();

        task4.requestAddProjectCollaborator(projectCollaborator2);

        sb.append("\nTask Registry ID: "
                + task4.listTaskCollaboratorRegistry(projectCollaborator2).get(0).getTaskCollaboratorRegistryID());
        sb.append("\nProject ROLE_COLLABORATOR" + ": " + projectCollaborator2.getUser().getName());
        sb.append("\nRequest Status: "
                + task4.listTaskCollaboratorRegistry(projectCollaborator2).get(0).getRegistryStatus() + "\n");
        assertEquals(sb.toString(), tcrc.listAssignmentRequests("1"));
    }

    @Test
    @Transactional
    public void listAssignmentRequestsNoRequestsWaitingForAproval() {

        assertEquals("\nNo requests waiting for approval.\n\n", tcrc.listAssignmentRequests(""));
    }

    @Test
    @Transactional
    public void listAssignmentRequestsNoRequestsWaitingForAproval2() {

        assertTrue(project1.listAssignmentRequests().isEmpty());
        assertEquals("\nNo requests waiting for approval.\n\n", tcrc.listAssignmentRequests("1"));
    }

    @Test
    @Transactional
    public void cancelAssignementTest() {

        task4.requestAddProjectCollaborator(projectCollaborator2);
        assertEquals("Assignment cancelled.",
                tcrc.cancelAssignement("1", task4.getId(), projectCollaborator2.getUser().getEmail().toString()));
    }

    @Test
    @Transactional
    public void cancelAssignementTest_failCancelledState() {

        task4.requestAddProjectCollaborator(projectCollaborator2);
        taskService.updateTask(task4);
        tcrc.cancelAssignement("1", task4.getId(), projectCollaborator2.getUser().getEmail().toString());
        assertEquals(
                "Not able to cancel assignmet due to one of the following reasons\n"
                        + "User not found, task not found or task already cancelled ",
                tcrc.cancelAssignement("1", task4.getId(), projectCollaborator2.getUser().getEmail().toString()));
    }

    @Test
    @Transactional
    public void cancelAssignementTest_failUserNull() {

        task4.requestAddProjectCollaborator(projectCollaborator2);
        tcrc.cancelAssignement("1", task4.getId(), projectCollaborator2.getUser().getEmail().toString());
        assertEquals(
                "Not able to cancel assignmet due to one of the following reasons\n"
                        + "User not found, task not found or task already cancelled ",
                tcrc.cancelAssignement("1", task4.getId(), null));
    }

    @Test
    @Transactional
    public void cancelAssignementTest_failTaskNull() {

        task4.requestAddProjectCollaborator(projectCollaborator2);
        tcrc.cancelAssignement("1", task4.getId(), projectCollaborator2.getUser().getEmail().toString());
        assertEquals(
                "Not able to cancel assignmet due to one of the following reasons\n"
                        + "User not found, task not found or task already cancelled ",
                tcrc.cancelAssignement("1", null, projectCollaborator2.getUser().getEmail().toString()));
    }

    @Test
    @Transactional
    public void approveAssignementTest() {

        task4.requestAddProjectCollaborator(projectCollaborator2);
        assertEquals("Assignment approved.",
                tcrc.approveAssignement("1", task4.getId(), projectCollaborator2.getUser().getEmail().toString()));
    }

    @Test
    @Transactional
    public void approveAssignementTest_failCancelledState() {

        task4.requestAddProjectCollaborator(projectCollaborator2);
        taskService.updateTask(task4);
        tcrc.cancelAssignement("1", task4.getId(), projectCollaborator2.getUser().getEmail().toString());
        assertEquals(
                "Not able to approve assignmet due to one of the following reasons\n"
                        + "User not found, task not found or task already cancelled ",
                tcrc.approveAssignement("1", task4.getId(), projectCollaborator2.getUser().getEmail().toString()));
    }

    @Test
    @Transactional
    public void approveAssignementTest_failUserNull() {

        assertEquals(
                "Not able to approve assignmet due to one of the following reasons\n"
                        + "User not found, task not found or task already cancelled ",
                tcrc.approveAssignement("1", task4.getId(), null));
    }

    @Test
    @Transactional
    public void approveAssignementTest_failTaskNull() {

        assertEquals(
                "Not able to approve assignmet due to one of the following reasons\n"
                        + "User not found, task not found or task already cancelled ",
                tcrc.approveAssignement("1", null, projectCollaborator2.getUser().getEmail().toString()));
    }

    @Test
    @Transactional
    public void approveAssignementTest_failed_user_null() {

        assertEquals(
                "Not able to approve assignmet due to one of the following reasons\n"
                        + "User not found, task not found or task already cancelled ",
                tcrc.approveAssignement("1", task4.getId(), "dsa@null.com"));
    }

    @Test
    @Transactional
    public void listRemovalRequestsTest() {

        StringBuilder sb = new StringBuilder();

        task1.requestRemoveProjectCollaborator(projectCollaborator2);

        sb.append("\nTask Registry ID: "
                + task1.listTaskCollaboratorRegistry(projectCollaborator2).get(0).getTaskCollaboratorRegistryID());
        sb.append("\nProject ROLE_COLLABORATOR" + ": " + projectCollaborator2.getUser().getName());
        sb.append("\nRequest Status: "
                + task1.listTaskCollaboratorRegistry(projectCollaborator2).get(0).getRegistryStatus() + "\n");
        assertEquals(sb.toString(), tcrc.listRemovalRequests("1"));
    }

    @Test
    @Transactional
    public void listRemovalRequestsNoRequestsWaitingForAproval() {

        assertEquals("\nNo requests waiting for removal.\n\n", tcrc.listRemovalRequests(""));
    }

    @Test
    @Transactional
    public void cancelRemovalTest() {

        task1.requestRemoveProjectCollaborator(projectCollaborator2);
        assertEquals("Removal cancelled.",
                tcrc.cancelRemoval("1", task1.getId(), projectCollaborator2.getUser().getEmail().toString()));
    }

    @Test
    @Transactional
    public void cancelRemovalTest_failCancelledState() {

        task1.requestRemoveProjectCollaborator(projectCollaborator2);
        taskService.updateTask(task1);
        tcrc.approveRemoval("1", task1.getId(), projectCollaborator2.getUser().getEmail().toString());
        assertEquals(
                "Not able to cancel remove due to one of the following reasons\n"
                        + "User not found, task not found or user already removed from task",
                tcrc.cancelRemoval("1", task1.getId(), projectCollaborator2.getUser().getEmail().toString()));
    }

    @Test
    @Transactional
    public void cancelRemovalTest_failUserNull() {

        task1.requestRemoveProjectCollaborator(projectCollaborator2);

        assertEquals(
                "Not able to cancel remove due to one of the following reasons\n"
                        + "User not found, task not found or user already removed from task",
                tcrc.cancelRemoval("1", task1.getId(), null));
    }

    @Test
    @Transactional
    public void cancelRemovalTest_failUserNull2() {

        task1.requestRemoveProjectCollaborator(projectCollaborator2);

        assertEquals(
                "Not able to cancel remove due to one of the following reasons\n"
                        + "User not found, task not found or user already removed from task",
                tcrc.cancelRemoval("1", task1.getId(), "jj@gmail.com"));
    }

    @Test
    @Transactional
    public void cancelRemovalTest_failTaskNull() {

        task1.requestRemoveProjectCollaborator(projectCollaborator2);

        assertEquals(
                "Not able to cancel remove due to one of the following reasons\n"
                        + "User not found, task not found or user already removed from task",
                tcrc.cancelRemoval("1", null, projectCollaborator2.getUser().getEmail().toString()));
    }

    @Test
    @Transactional
    public void approveRemovalTest() {

        task1.requestRemoveProjectCollaborator(projectCollaborator2);
        assertEquals("Removal approved.",
                tcrc.approveRemoval("1", task1.getId(), projectCollaborator2.getUser().getEmail().toString()));
    }

    @Test
    @Transactional
    public void approveRemovalTest_failCancelledState() {

        task1.requestRemoveProjectCollaborator(projectCollaborator2);
        taskService.updateTask(task1);
        tcrc.approveRemoval("1", task1.getId(), projectCollaborator2.getUser().getEmail().toString());
        assertEquals(
                "Not able to approve removal due to one of the following reasons\n"
                        + "User not found, task not found or user already removed from task ",
                tcrc.approveRemoval("1", task1.getId(), projectCollaborator2.getUser().getEmail().toString()));
    }

    @Test
    @Transactional
    public void approveRemovalTest_failUserNull() {

        assertEquals(
                "Not able to approve removal due to one of the following reasons\n"
                        + "User not found, task not found or user already removed from task ",
                tcrc.approveRemoval("1", task1.getId(), "jj@gmail.com"));
    }

    @Test
    @Transactional
    public void approveRemovalTest_failEmailNull() {

        assertEquals(
                "Not able to approve removal due to one of the following reasons\n"
                        + "User not found, task not found or user already removed from task ",
                tcrc.approveRemoval("1", task1.getId(), null));
    }

    @Test
    @Transactional
    public void approveRemovalTest_failTaskNull() {

        assertEquals(
                "Not able to approve removal due to one of the following reasons\n"
                        + "User not found, task not found or user already removed from task ",
                tcrc.approveRemoval("1", null, projectCollaborator2.getUser().getEmail().toString()));
    }
}
