package project.controllers.controllertests;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.controllers.consolecontrollers.EditTaskListController;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.services.TaskService;
import project.model.user.User;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class EditTaskServiceControllerTest {
    UserService userService;
    ProjectService projectService;
    TaskService taskService;
    ProjectCollaboratorService projectCollaboratorService;

    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;

    EditTaskListController tlc;

    List<User> expectedUser;

    LocalDate birth;
    User user1;
    User user2;
    User user3;
    User user4;
    ProjectCollaborator projectCollaborator2, projectCollaborator3, projectCollaborator4, projectManager;

    Project project1;
    Project project2;
    Task task1, task2, task3, task4, task5, task6, task7;

    LocalDateTime d1;
    LocalDateTime d2;
    LocalDateTime d3;
    LocalDateTime d4;
    int y1;
    Month m1;

    @Before
    public void setUp() throws AddressException {

        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);

        tlc = new EditTaskListController(userService, projectService, taskService);

        birth = LocalDate.of(1999, 11, 11);
        y1 = LocalDateTime.now().getYear();
        m1 = LocalDateTime.now().getMonth();

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joana", "93 333 33 33", "joana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Lilina", "93 333 33 33", "liliana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");
        user3 = userService.searchUserByEmail("joana@gmail.com");
        user4 = userService.searchUserByEmail("liliana@gmail.com");

        user1.setProfileCollaborator();
        user2.setProfileCollaborator();
        user3.setProfileCollaborator();
        user4.setProfileCollaborator();

        userService.updateUser(user1);
        userService.updateUser(user2);
        userService.updateUser(user3);
        userService.updateUser(user4);

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");
        user3 = userService.searchUserByEmail("joana@gmail.com");
        user4 = userService.searchUserByEmail("liliana@gmail.com");
        /*
         * Project 1 id= "1"
         *
         * user1 is project manager users 2, 3 and 4 are added to the project and become
         * ProjectCollaborators user2 = projectCollaborator2 and costs 2; user3 =
         * projectCollaborator3 and costs 4; user4 = projectCollaborator3 and costs 5;
         *
         * project 1 has 6 tasks
         *
         * tasks 1 and 2 have reports tasks 1 and 2 are pending
         *
         * tasks 3 and 4 have no collaborators assigned, so they don't have a start date
         * and no end date
         *
         * tasks 5 and 6 have reports and are concluded
         *
         *
         * projectCollaborator2 is in task 1 and 2 in task 1 has one report on 4 hours
         * in task 2 has 4 reports of 4,8,8,4 hours
         *
         * projectCollaborator3 is in task 1, 2, 5 and 6 has reports in task 5 and 6 1
         * report in task5 1 report in task6
         *
         * projectCollaborator4 has no assigned tasks
         *
         *
         */

        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
        d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

        projectService.addProject("1", "Project 1");
        project1 = projectService.getProjectByID("1");
        projectService.addProject("2", "Project 2");
        project2 = projectService.getProjectByID("2");

        projectCollaboratorService.setProjectManager(project1, user1);

        projectCollaboratorService.addProjectCollaborator(project1, user2, 2);
        projectCollaboratorService.addProjectCollaborator(project1, user3, 4);
        projectCollaboratorService.addProjectCollaborator(project1, user4, 5);

        project1.setStartDate(d1);
        projectManager = project1.findProjectCollaborator(user1);

        projectCollaborator2 = project1.findProjectCollaborator(user2);
        projectCollaborator3 = project1.findProjectCollaborator(user3);
        projectCollaborator4 = project1.findProjectCollaborator(user4);

        taskService.addTask(project1, "task1");
        taskService.addTask(project1, "task2");
        taskService.addTask(project1, "task3");
        taskService.addTask(project1, "task4");
        taskService.addTask(project1, "task5");
        taskService.addTask(project1, "task6");
        taskService.addTask(project2, "task7");

        task1 = project1.findTaskByTitle("task1");
        task2 = project1.findTaskByTitle("task2");
        task3 = project1.findTaskByTitle("task3");
        task4 = project1.findTaskByTitle("task4");
        task5 = project1.findTaskByTitle("task5");
        task6 = project1.findTaskByTitle("task6");
        task7 = project2.findTaskByTitle("task7");

        task1.addProjectCollaborator(projectCollaborator2);
        task2.addProjectCollaborator(projectCollaborator2);
        task3.addProjectCollaborator(projectCollaborator2);
        task5.addProjectCollaborator(projectCollaborator2);

        TaskCollaboratorRegistry col2TaskRegistry = task5.getTaskCollaboratorRegistryByID(task5.getId() + "-" + projectCollaborator2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

        TaskCollaboratorRegistry col1TaskRegistry = task3.getTaskCollaboratorRegistryByID(task3.getId() + "-" + projectCollaborator2.getUser().getEmail());
        col1TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

        task5.addReport(projectCollaborator2, 5, d1, d1.plusDays(5));
        task3.addReport(projectCollaborator2, 7, d1, d1.plusDays(5));

        projectService.updateProject(project1);
        project1 = projectService.getProjectByID("1");

        taskService.updateTask(task1);
        taskService.updateTask(task2);
        taskService.updateTask(task3);
        taskService.updateTask(task5);

    }

    @Test
    @Transactional
    public void testRequestAddTaskToCollaboratorSuccess() {

        String email = "joaquim@gmail.com";

        String expected = "Request successfully sent.";
        String result = tlc.requestAddProjectCollaboratorToTask(email, project1.getId(), task6.getId());

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testRequestAddTaskToCollaboratorFailureCollaboratorWorkingOnTask() {

        String email = "joaquim@gmail.com";

        String expected = "Request denied. You are already working in this task.";
        String result = tlc.requestAddProjectCollaboratorToTask(email, project1.getId(), task5.getId());

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testRequestAddTaskToCollaboratorFailureTaskAlreadyAssigned() {

        String email = "joana@gmail.com";
        tlc.requestAddProjectCollaboratorToTask(email, project1.getId(), task5.getId());

        String expected = " Request already made.";
        String result = tlc.requestAddProjectCollaboratorToTask(email, project1.getId(), task5.getId());

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testRequestAddTaskToCollaboratorFailureNoTaskFound() {

        String email = "joaquim@gmail.com";
        String expected = "No task found.";

        String result = tlc.requestAddProjectCollaboratorToTask(email, project1.getId(), null);

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testSetTaskCompletedsuccess() {

        task1.setEffectiveStartDate(d1);
        String email = "joaquim@gmail.com";
        String id = task1.getId();

        String expected = "\nTask successfully marked as completed.";
        String result = tlc.setTaskCompleted(email, id);

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testSetTaskCompletedFailureTaskNotFound() {

        String email = "joaquim@gmail.com";
        String id = "asd";
        String expected = "\nTask not found!";
        String result = tlc.setTaskCompleted(email, id);

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testSetTaskCompletedfail() {

        String email = "joaquim@gmail.com";
        String id = task6.getId();
        String expected = "\nThis task is not assigned to this collaborator.";
        String result = tlc.setTaskCompleted(email, id);

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testSetTaskCompletedfailSetTaskCompletedFalse() {

        String email = "joaquim@gmail.com";
        String id = task1.getId();
        String expected = "\nThis task is not assigned to this collaborator.";
        String result = tlc.setTaskCompleted(email, id);

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testSetTaskCompletedChangeState() {
        String email = "joaquim@gmail.com";
        String id = task3.getId();

        assertTrue(task1.getStatus().isOnReadyToStartState());
        task1.addTaskDependency(task3);
        assertTrue(task1.getStatus().isOnAssignedState());

        tlc.setTaskCompleted(email, id);

        assertTrue(task1.getStatus().isOnReadyToStartState());
    }

    @Test
    @Transactional
    public void testRequestRemoveTaskFromProjectCollaborator() {

        String email = "joaquim@gmail.com";
        String id = task1.getId();

        String result = tlc.requestRemoveTaskFromProjectCollaborator(email, id);
        String expected = "Request successfully sent.";
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testRequestRemoveTaskFromProjectCollaboratorCollaboratorNotFound() {

        String email = "joana@gmail.com";
        String id = task1.getId();

        String result = tlc.requestRemoveTaskFromProjectCollaborator(email, id);
        String expected = "Request denied.";
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testRequestRemoveTaskFromProjectCollaboratorfailtaskNotFound() {
        String idFalse = "jjkkl";

        String email = "joaquim@gmail.com";
        String result = tlc.requestRemoveTaskFromProjectCollaborator(email, idFalse);
        String expected = "Request denied.";
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testGetAllUserProjects() {

        String email = "joaquim@gmail.com";
        List<Project> expected = new ArrayList<>();
        expected.add(project1);
        List<Project> result = tlc.getAllUserProjects(email);

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testListAllUserProjects() {

        String email = "joaquim@gmail.com";
        String expected = "\nProject ID: " + project1.getId() + "\n" + "Project name: " + project1.getName() + "\n"
                + "Project description: " + project1.getDescription() + "\n" + "\n";
        String result = tlc.listAllUserProjects(email);

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testValidateProjectSuccess() {

        String email = "joaquim@gmail.com";
        String projID = project1.getId();

        String expected = project1.getId();
        String result = tlc.validateProject(projID, email);

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testValidateProjectFailureNoUserFound() {

        String email = "jon@gmail.com";
        String projID = project1.getId();

        String result = tlc.validateProject(projID, email);

        assertNull(result);
    }

    @Test
    @Transactional
    public void testValidateProjectFailureNoProjectFound() {

        String email = "joaquim@gmail.com";
        String projID = "projeto234";

        String result = tlc.validateProject(projID, email);

        assertNull(result);
    }

    @Test
    @Transactional
    public void testListAllProjectPendingTasks() {

        assertTrue(task1.getTaskState().isOnReadyToStartState());
        assertTrue(task2.getTaskState().isOnReadyToStartState());
        assertTrue(task3.getTaskState().isOnInProgressState());
        assertTrue(task4.getTaskState().isOnCreatedState());
        task5.setTaskCompleted();
        assertTrue(task5.getTaskState().isOnCompletedState());
        task6.addProjectCollaborator(projectCollaborator2);

        TaskCollaboratorRegistry col1TaskRegistry = task6.getTaskCollaboratorRegistryByID(task6.getId() + "-" + projectCollaborator2.getUser().getEmail());
        col1TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

        task6.addReport(projectCollaborator2, 4, d1, d1.plusDays(5));
        task6.setTaskCancelled();
        assertTrue(task6.getTaskState().isOnCancelledState());
        taskService.updateTask(task6);
        String expected = "Task ID: " + task1.getId() + "\n" + "Task name: " + task1.getTitle()
                + "\n" + "Task status: " + task1.getStatus() + "\n" + "Task ID: " + task2.getId() + "\n" + "Task name: "
                + task2.getTitle() + "\n" + "Task status: " + task2.getStatus() + "\n" + "Task ID: " + task3.getId()
                + "\n" + "Task name: " + task3.getTitle() + "\n" + "Task status: " + task3.getStatus() + "\n" +
                "Task ID: " + task4.getId() + "\n" + "Task name: " + task4.getTitle() + "\n" + "Task status: "
                + task4.getStatus() + "\n";
        String result = tlc.listAllProjectPendingTasks(project1.getId());
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testValidateTaskIDSuccess() {

        String projID = project1.getId();
        String taskID = task1.getId();

        String expected = task1.getId();
        String result = tlc.validateTaskID(projID, taskID);

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testValidateTaskIDFailureNoTaskIDFound() {

        String projID = project1.getId();
        String taskID = "task45";
        String result = tlc.validateTaskID(projID, taskID);

        assertNull(result);
    }
}