package project.controllers.controllertests;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.controllers.consolecontrollers.TaskStateController;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.services.TaskService;
import project.model.user.User;

import javax.mail.internet.AddressException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

import static org.junit.Assert.*;

public class TaskStateControllerTest {

    TaskStateController tsc;
    UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;
    UserRepositoryClass userRepository;
    ProjectRepositoryClass projectRepository;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepository;
    TaskRepositoryClass taskRepository;
    List<User> expectedUser;
    LocalDate birth;
    User user1;
    User user2;
    User user3;
    User user4;
    ProjectCollaborator projectCollaborator2, projectCollaborator3, projectCollaborator4, projectManager;
    Project project1;
    Task task1, task2, task3, task4, task5, task6;
    LocalDateTime d1;
    LocalDateTime d2;
    LocalDateTime d3;
    LocalDateTime d4;
    int y1;
    Month m1;
    private TaskService taskService;

    @Before
    public void setUp() throws AddressException {
        userRepository = new UserRepositoryClass();
        taskRepository = new TaskRepositoryClass();
        projectRepository = new ProjectRepositoryClass();
        projectCollaboratorRepository = new ProjectCollaboratorRepositoryClass();

        userService = new UserService(userRepository);
        taskService = new TaskService(taskRepository);
        projectService = new ProjectService(projectRepository);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepository);

        tsc = new TaskStateController(projectService, taskService);

        birth = LocalDate.of(1999, 11, 11);
        y1 = LocalDateTime.now().getYear();
        m1 = LocalDateTime.now().getMonth();

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joana", "93 333 33 33", "joana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Lilina", "93 333 33 33", "liliana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");
        user3 = userService.searchUserByEmail("joana@gmail.com");
        user4 = userService.searchUserByEmail("liliana@gmail.com");

        user1.setProfileCollaborator();
        user2.setProfileCollaborator();
        user3.setProfileCollaborator();
        user4.setProfileCollaborator();

        userService.updateUser(user1);
        userService.updateUser(user2);
        userService.updateUser(user3);
        userService.updateUser(user4);

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");
        user3 = userService.searchUserByEmail("joana@gmail.com");
        user4 = userService.searchUserByEmail("liliana@gmail.com");

        /*
         * Project 1 id= "1"
         *
         * user1 is project manager users 2, 3 and 4 are added to the project and become
         * ProjectCollaborators user2 = projectCollaborator2 and costs 2; user3 =
         * projectCollaborator3 and costs 4; user4 = projectCollaborator3 and costs 5;
         *
         * project 1 has 6 tasks
         *
         * tasks 1 and 2 have reports tasks 1 and 2 are pending
         *
         * tasks 3 and 4 have no collaborators assigned, so they don't have a start date
         * and no end date
         *
         * tasks 5 and 6 have reports and are concluded
         *
         *
         * projectCollaborator2 is in task 1 and 2 in task 1 has one report on 4 hours
         * in task 2 has 4 reports of 4,8,8,4 hours
         *
         * projectCollaborator3 is in task 1, 2, 5 and 6 has reports in task 5 and 6 1
         * report in task5 1 report in task6
         *
         * projectCollaborator4 has no assigned tasks
         *
         *
         */
        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
        d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

        projectService.addProject("1", "Project 1");
        project1 = projectService.getProjectByID("1");
        projectCollaboratorService.setProjectManager(project1, user1);

        projectCollaborator2 = new ProjectCollaborator(project1, user2, 2);
        projectCollaborator3 = new ProjectCollaborator(project1, user3, 2);
        projectCollaborator4 = new ProjectCollaborator(project1, user4, 2);

        projectManager = project1.findProjectCollaborator(user1);

        taskService.addTask(project1, "task1");
        taskService.addTask(project1, "task2");
        taskService.addTask(project1, "task3");
        taskService.addTask(project1, "task4");
        taskService.addTask(project1, "task5");
        taskService.addTask(project1, "task6");

        task1 = project1.findTaskByTitle("task1");
        task2 = project1.findTaskByTitle("task2");
        task3 = project1.findTaskByTitle("task3");
        task4 = project1.findTaskByTitle("task4");
        task5 = project1.findTaskByTitle("task5");
        task6 = project1.findTaskByTitle("task6");

        task1.setPredictedDateOfStart(d1);
        task1.setPredictedDateOfConclusion(d2);
        task2.setPredictedDateOfStart(d1);
        task2.setPredictedDateOfConclusion(d2);
        task3.setPredictedDateOfStart(d1);
        task3.setPredictedDateOfConclusion(d2);
        task4.setPredictedDateOfStart(d1);
        task4.setPredictedDateOfConclusion(d2);
        task5.setPredictedDateOfStart(d1);
        task5.setPredictedDateOfConclusion(d2);
        task6.setPredictedDateOfStart(d1);
        task6.setPredictedDateOfConclusion(d2);

        task1.addProjectCollaborator(projectCollaborator2);
        task2.addProjectCollaborator(projectCollaborator2);
        task3.addProjectCollaborator(projectCollaborator2);
        TaskCollaboratorRegistry col1TaskRegistry = task3.getTaskCollaboratorRegistryByID(task3.getId() + "-" + projectCollaborator2.getUser().getEmail());
        col1TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));
        task5.addProjectCollaborator(projectCollaborator2);
        TaskCollaboratorRegistry col2TaskRegistry = task5.getTaskCollaboratorRegistryByID(task5.getId() + "-" + projectCollaborator2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

        task5.addReport(projectCollaborator2, 5, d1, d1.plusDays(5));
        task3.addReport(projectCollaborator2, 7, d1, d1.plusDays(5));

        projectService.updateProject(project1);
        project1 = projectService.getProjectByID("1");

        taskService.updateTask(task1);
        taskService.updateTask(task2);
        taskService.updateTask(task3);
        taskService.updateTask(task4);
        taskService.updateTask(task5);
        taskService.updateTask(task6);
    }

    @Test
    public void testSetTaskCancelledSuccess() {

        String expected = "TASK WAS SUCCESSFULLY CANCELLED";
        assertTrue(task3.getTaskState().isOnInProgressState());
        String result = tsc.setTaskCancelled("1", task3.getId());
        assertTrue(task3.getTaskState().isOnCancelledState());

        assertEquals(expected, result);

    }

    @Test
    public void testSetTaskCancelledFail() {

        String expected = "IS NOT POSSIBLE TO CANCEL THIS TASK";
        assertTrue(task3.getTaskState().isOnInProgressState());
        task3.setTaskCompleted();
        assertFalse(task3.getTaskState().isOnCancelledState());
        assertTrue(task3.getTaskState().isOnCompletedState());
        String result = tsc.setTaskCancelled("1", task3.getId());

        assertEquals(expected, result);

    }

    @Test
    public void testSetTaskCancelledTaskNotFound() {

        String expected = "TASK WAS NOT FOUND";
        String result = tsc.setTaskCancelled("1", null);
        assertFalse(task1.getTaskState().isOnCancelledState());

        assertEquals(expected, result);

    }

    @Test
    public void testSetTaskCompletedSuccess() {

        String expected = "TASK " + task3.getId() + " IS COMPLETED" + "\n";
        assertTrue(task3.getTaskState().isOnInProgressState());
        String result = tsc.setTaskCompleted("1", task3.getId());
        assertTrue(task3.getTaskState().isOnCompletedState());

        assertEquals(expected, result);

    }

    @Test
    public void testSetTaskCompletedFail() {

        String expected = "IT IS NOT POSSIBLE TO SET THIS TASK AS COMPLETED" + "\n";
        String result = tsc.setTaskCompleted("1", task1.getId());
        assertFalse(task1.getTaskState().isOnCompletedState());

        assertEquals(expected, result);

    }

    @Test
    public void testSetTaskCompletedTaskNotFound() {

        String expected = "TASK WAS NOT FOUND";
        String result = tsc.setTaskCompleted("1", null);
        assertFalse(task1.getTaskState().isOnCompletedState());

        assertEquals(expected, result);

    }

}
