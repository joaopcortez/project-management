package project.controllers.controllertests;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.controllers.consolecontrollers.ProjectController;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.services.ProjectCollaboratorService;
import project.model.user.EmailAddress;
import project.model.user.User;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class ProjectControllerTest {

    UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;

    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;

    ProjectController projectController;
    Project project1;
    User user1, user2, user3;

    @Before
    public void setUp() throws AddressException {
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);

        projectController = new ProjectController(userService, projectService, projectCollaboratorService);

        projectService.addProject("1", "project 1");
        project1 = projectService.getProjectByID("1");
        project1.setActive();
        projectService.updateProject(project1);

        userService.addUser("Asdrubal", "123456789", "asdrubal@jj.com", "123456789",
                LocalDate.of(1977, 3, 5), "1", "Rua sem saída", "4250-357", "Porto", "Portugal");
        userService.addUser("Manel", "123456789", "manel@jj.com", "123456789", LocalDate.of(1978, 11, 12),
                "2", "Rua sem saída", "4250-357", "Porto", "Portugal");
        userService.addUser("Joaquim", "123456789", "joaquim@jj.com", "123456789",
                LocalDate.of(1978, 11, 12), "2", "Rua sem saída", "4250-357", "Porto", "Portugal");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user1.setProfileCollaborator();
        user2 = userService.searchUserByEmail("manel@jj.com");
        user2.setProfileCollaborator();
        user3 = userService.searchUserByEmail("joaquim@jj.com");
        user3.setInactive();

        userService.updateUser(user1);
        userService.updateUser(user2);
        userService.updateUser(user3);

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("manel@jj.com");
        user3 = userService.searchUserByEmail("joaquim@jj.com");

        project1 = projectService.getProjectByID("1");
        projectCollaboratorService.setProjectManager(project1, user2);
        projectCollaboratorService.addProjectCollaborator(project1, user1, 2.1);

        projectService.updateProject(project1);
        project1 = projectService.getProjectByID("1");
    }

    @Test
    @Transactional
    public void testChangeProjectManagerSuccess() throws AddressException {
        projectController.changeProjectManager("1", "asdrubal@jj.com");
        assertEquals(new EmailAddress("asdrubal@jj.com").toString(),
                projectService.getProjectByID("1").getProjectManager().getUser().getEmail());
    }

    @Test
    @Transactional
    public void testChangeProjectManagerFailOne() {
        projectController.changeProjectManager("1", "asdrubal@jj.com");

        assertEquals("\nThis user is already the project manager the project with the ID 1.",
                projectController.changeProjectManager("1", "asdrubal@jj.com"));
    }

    @Test
    @Transactional
    public void testChangeProjectManagerFailTwo() {
        assertEquals("\nProject manager did not change because of one of the following reasons:\n"
                + "- the user is not registered.\n" + "- the user is not a project collaborator.\n"
                + "- the user is not active.\n\n", projectController.changeProjectManager("1", "joaquim@jj.com"));
    }

    @Test
    @Transactional
    public void testChangeProjectManagerFailThree() {

        assertEquals("Project manager did not change because entered invalid information",
                projectController.changeProjectManager("", "joaquim@jj.com"));
    }

    @Test
    @Transactional
    public void testChangeProjectManagerFailFour() {
        assertEquals("Project manager did not change because entered invalid information",
                projectController.changeProjectManager("1", ""));
    }

    @Test
    @Transactional
    public void testListAllActiveProjects() {
        assertEquals(
                "\n" +
                        "Project ID: 1	Project Status: START_UP\n" +
                        "		Project name: project 1	Project description: <no project description>\n" +
                        "		Project Cost Calculation Method: LastTimePeriodCost\n" +
                        "		Project Manager: Manel Email: manel@jj.com\n" +
                        "______________________________________________________________________________\n\n\n",
                projectController.listAllActiveProjects());
    }

    @Test
    @Transactional
    public void testListAllProjectManagerProjects() {
        assertEquals(
                "\nProject ID: 1\tProject Status: START_UP\n" + "\t\tProject name: project 1\tProject description: <no project description>\n" +
                        "\t\tProject Manager: Manel Email: manel@jj.com\n",
                projectController.listAllProjectManagerProjects("manel@jj.com"));
    }

    @Test
    @Transactional
    public void testListAllProjectManagerProjectsNoProjects() {
        String expected = "This user has no projects as Project Manager!\n\n";
        assertEquals(expected, projectController.listAllProjectManagerProjects("asdrubal@jj.com"));
    }

    @Test
    @Transactional
    public void testGetProjectInfo() {

        String expected = "\nProject ID: 1	Project Status: START_UP\n" +
                "		Project name: project 1	Project description: <no project description>\n" +
                "		Project Manager: Manel Email: manel@jj.com";
        assertEquals(expected, projectController.getProjectInfo("1"));
    }

    @Test
    @Transactional
    public void testInvalidProjectId() {
        String expected = "\nNo project found with the introduced ID";
        assertEquals(expected, projectController.changeProjectManager("2", "asdrubal@switch.com"));
    }

}
