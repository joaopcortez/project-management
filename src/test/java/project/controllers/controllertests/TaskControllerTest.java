package project.controllers.controllertests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import project.controllers.consolecontrollers.TaskController;
import project.controllers.consolecontrollers.TaskStateController;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.model.task.Task;
import project.services.TaskService;
import project.model.user.User;


public class TaskControllerTest {

    TaskController tc;

    TaskStateController tsc;
    private TaskService taskService;
    UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;
    
    UserRepositoryClass userRepository;
    ProjectRepositoryClass projectRepository;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepository;
    TaskRepositoryClass taskRepository;
   
    Project project1;
  

    List<User> expectedUser;

    LocalDate birth;
    User user1;
    User user2;
    User user3;
    User user4;
    ProjectCollaborator projectCollaborator2, projectCollaborator3, projectCollaborator4, projectManager;

    Task task1, task2, task3, task4, task5, task6;

    LocalDateTime d1;
    LocalDateTime d2;
    LocalDateTime d3;
    LocalDateTime d4;

    List<Task> taskDependencies;

    @Before
    public void setUp() throws AddressException {
    	userRepository = new UserRepositoryClass();
    	taskRepository = new TaskRepositoryClass();
    	projectRepository = new ProjectRepositoryClass();
    	projectCollaboratorRepository = new ProjectCollaboratorRepositoryClass();
    	
    	userService = new UserService(userRepository);
    	taskService = new TaskService(taskRepository);
    	projectService = new ProjectService(projectRepository);
    	projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepository);


        tc = new TaskController(projectService, taskService);

        birth = LocalDate.of(1999, 11, 11);

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joana", "93 333 33 33", "joana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Lilina", "93 333 33 33", "liliana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");
        user3 = userService.searchUserByEmail("joana@gmail.com");
        user4 = userService.searchUserByEmail("liliana@gmail.com");

        user1.setProfileCollaborator();
        user2.setProfileCollaborator();
        user3.setProfileCollaborator();
        user4.setProfileCollaborator();

        userService.updateUser(user1);
        userService.updateUser(user2);
        userService.updateUser(user3);
        userService.updateUser(user4);

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");
        user3 = userService.searchUserByEmail("joana@gmail.com");
        user4 = userService.searchUserByEmail("liliana@gmail.com");

        /*
         * Project 1 id= "1"
         *
         * user1 is project manager users 2, 3 and 4 are added to the project and become
         * ProjectCollaborators user2 = projectCollaborator2 and costs 2; user3 =
         * projectCollaborator3 and costs 4; user4 = projectCollaborator3 and costs 5;
         *
         * project 1 has 6 tasks
         *
         * tasks 1 and 2 have reports tasks 1 and 2 are pending
         *
         * tasks 3 and 4 have no collaborators assigned, so they don't have a start date
         * and no end date
         *
         * tasks 5 and 6 have reports and are concluded
         *
         *
         * projectCollaborator2 is in task 1 and 2 in task 1 has one report on 4 hours
         * in task 2 has 4 reports of 4,8,8,4 hours
         *
         * projectCollaborator3 is in task 1, 2, 5 and 6 has reports in task 5 and 6 1
         * report in task5 1 report in task6
         *
         * projectCollaborator4 has no assigned tasks
         *
         *
         */
        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
        d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

        projectService.addProject("1", "Project 1");
        project1 = projectService.getProjectByID("1");

        projectCollaboratorService.setProjectManager(project1, user1);

        project1.setStartDate(d1);
        projectManager = project1.findProjectCollaborator(user1);

        taskService.addTask(project1, "task1");
        taskService.addTask(project1, "task2");
        taskService.addTask(project1, "task3");
        taskService.addTask(project1, "task4");
        taskService.addTask(project1, "task5");

        task1 = project1.findTaskByTitle("task1");
        task2 = project1.findTaskByTitle("task2");
        task3 = project1.findTaskByTitle("task3");
        task4 = project1.findTaskByTitle("task4");
        task5 = project1.findTaskByTitle("task5");

        task1.setPredictedDateOfStart(d1);
        task1.setPredictedDateOfConclusion(d2);
        task2.setPredictedDateOfStart(d1);
        task2.setPredictedDateOfConclusion(d2);
        task3.setPredictedDateOfStart(d1);
        task3.setPredictedDateOfConclusion(d2);
        task4.setPredictedDateOfStart(d1);
        task4.setPredictedDateOfConclusion(d2);
        task5.setPredictedDateOfStart(d1);
        task5.setPredictedDateOfConclusion(d2);
    }

    @Test
    @Transactional
    public void testAddTaskDependencySuccess() {
        assertEquals(0, task1.getTaskDependencies().size());

        List<Task> taskDependenciesList = new ArrayList<>();

        taskDependenciesList.add(task2);
        taskDependenciesList.add(task3);

        tc.addTaskDependency(project1.getId(), task1.getId(), task2.getId());
        tc.addTaskDependency(project1.getId(), task1.getId(), task3.getId());

        assertEquals(taskDependenciesList, task1.getTaskDependencies());
    }

    @Test
    @Transactional
    public void testAddTaskDependencyNoSuccedd() {

        List<Task> taskDependenciesList = new ArrayList<>();

        taskDependenciesList.add(task2);
        taskDependenciesList.add(task3);

        tc.addTaskDependency(project1.getId(), task1.getId(), task2.getId());
        tc.addTaskDependency(project1.getId(), task1.getId(), task3.getId());
        tc.addTaskDependency(project1.getId(), task1.getId(), null);

        assertEquals(taskDependenciesList, task1.getTaskDependencies());
    }

    @Test
    @Transactional
    public void testTaskCanStart() {

        assertEquals("Task can start\n\n", tc.taskCanStart(task1.getId()));
    }

    @Test
    @Transactional
    public void testTaskCannotStart() {
        tc.addTaskDependency(project1.getId(), task1.getId(), task2.getId());

        assertEquals("Task can not start\n\n", tc.taskCanStart(task1.getId()));
    }

    @Test
    @Transactional
    public void addTaskDependencyTaskDependencexists() {
        task1.addTaskDependency(task2);
        assertEquals("Task dependency already exists\n\n", tc.addTaskDependency(project1.getId(), task1.getId(), task2.getId()));

    }

    @Test
    @Transactional
    public void addTaskDependencyTaskCntDependItself() {
        assertEquals("Task can´t depend on itself\n\n", tc.addTaskDependency(project1.getId(), task2.getId(), task2.getId()));

    }

    @Test
    @Transactional
    public void addTaskDependencyNoTaskAdded() {
        assertEquals("No task dependency added\n\n", tc.addTaskDependency(project1.getId(), null, task2.getId()));

    }

    @Test
    @Transactional
    public void addTaskDependencyOppositeDependencyAlreadyExists() {

        assertFalse(task1.getTaskDependencies().contains(task2));

        tc.addTaskDependency(project1.getId(), task1.getId(), task2.getId());
        assertTrue(task1.getTaskDependencies().contains(task2));

        String result = tc.addTaskDependency(project1.getId(), task2.getId(), task1.getId());
        assertTrue(result.equals(task1.getId() + " already depends on " + task2.getId() + ". No dependency added."));

    }

    @Test
    @Transactional
    public void getProjectUnit() {
        assertEquals("HOURS", tc.getProjectUnit("1"));
    }

    @Test
    @Transactional
    public void getTask() {

        assertEquals(task1.getId(), tc.getTask(task1.getId()));
    }

    @Test
    @Transactional
    public void isValidDay() {
        assertFalse(tc.isValidDay(-5));
        assertTrue(tc.isValidDay(5));
    }

    @Test
    @Transactional
    public void projectIdAndTitle() {
        assertEquals("ID: 1 Title: Project 1", tc.projectIdAndTitle().toString());
    }

}
