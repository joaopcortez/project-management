package project.controllers.controllertests;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;

import project.controllers.consolecontrollers.ProfileController;
import project.model.UserService;
import jparepositoriestest.UserRepositoryClass;
import project.model.user.User;


public class ProfileControllerTest {


    UserService userService;
    UserRepositoryClass userRepo;
    ProfileController profileController;
    User user1, user2;

    @Before
    public void setUp() throws AddressException {
    	userRepo = new UserRepositoryClass();
    	userService = new UserService(userRepo);

        profileController = new ProfileController(userService);

        userService.addUser("Asdrubal", "123456789", "asdrubal@jj.com", "123456789",
                LocalDate.of(1977, 3, 5), "1", "Rua sem saída", "4250-357", "Porto", "Portugal");
        userService.addUser("Manel", "123456789", "manel@jj.com", "123456789", LocalDate.of(1978, 11, 12),
                "2", "Rua sem saída", "4250-357", "Porto", "Portugal");
        userService.addUser("Joaquim", "123456789", "joaquim@jj.com", "123456789",
                LocalDate.of(1978, 11, 12), "2", "Rua sem saída", "4250-357", "Porto", "Portugal");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user1.setProfileCollaborator();
        user2 = userService.searchUserByEmail("manel@jj.com");
        user2.setProfileDirector();
        userService.updateUser(user1);
        userService.updateUser(user2);

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("manel@jj.com");
    }

    @Test
    public void testSetProfileDirectorSucess() {
        assertEquals("User with the email asdrubal@jj.com has been set as ROLE_DIRECTOR profile.",
                profileController.setProfileDirector("asdrubal@jj.com"));
    }

    @Test
    public void testSetProfileDirectorFail1() {
        assertEquals("This user has already the ROLE_DIRECTOR profile.",
                profileController.setProfileDirector("manel@jj.com"));
    }

    @Test
    public void testSetProfileDirectorFail2() {
        assertEquals(
                "User could not be set to ROLE_DIRECTOR profile because he does not exist in the company's user registry.",
                profileController.setProfileDirector("filipa@jj.com"));
    }

    @Test
    public void testSetProfileCollaboratorSucess() {
        assertEquals("User with the email manel@jj.com has been set as ROLE_COLLABORATOR profile.",
                profileController.setProfileCollaborator("manel@jj.com"));
    }

    @Test
    public void testSetProfileCollaboratorFail1() {
        assertEquals("This user has already the ROLE_COLLABORATOR profile.",
                profileController.setProfileCollaborator("asdrubal@jj.com"));
    }

    @Test
    public void testSetProfileCollaboratorFail2() throws AddressException {

        User user3 = new User("Asdrubal", "123456789", "filipa@jj.com", "123456789", LocalDate.of(1977, 3, 5), "1", "Rua sem saída", "4250-357", "Porto", "Portugal");
        assertEquals(
                "User could not be set to ROLE_COLLABORATOR profile because he does not exist in the company's user registry.",
                profileController.setProfileCollaborator(user3.getEmail()));
    }
}
