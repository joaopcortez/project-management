package project.exception;

import org.junit.Test;

import java.io.FileNotFoundException;

import static org.junit.Assert.assertEquals;

public class AppExceptionTest {

    AppException appException = new AppException("Load fail");
    AppException appException2 = new AppException("Load fail", new FileNotFoundException());

    @Test
    public void testAppException() {

        String expected = "Load fail";
        assertEquals(expected, appException.getMessage());
        assertEquals(expected, appException2.getMessage());
        assertEquals(new FileNotFoundException().toString(), appException2.getCause().toString());
    }
}
