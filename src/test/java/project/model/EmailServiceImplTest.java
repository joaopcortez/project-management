package project.model;

import jparepositoriestest.JavaMailSenderClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mail.SimpleMailMessage;
import project.dto.EmailMessageDTO;
import project.services.EmailServiceImpl;

import static org.mockito.Mockito.*;

public class EmailServiceImplTest {
    EmailServiceImpl emailServiceImpl;
    JavaMailSenderClass javaMailSenderClass;
    SimpleMailMessage message;

    @Before
    public void setUp() {
        javaMailSenderClass = mock(JavaMailSenderClass.class);
        emailServiceImpl = new EmailServiceImpl(javaMailSenderClass);
        message = new SimpleMailMessage();

    }

    @Test
    public void sendSimpleMessage() {
        String to, subject, text;
        to = "zezao@gmailcom";
        subject = "Test";
        text = "Texto";

        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailServiceImpl.sendSimpleMessage(to, subject, text);
        verify(javaMailSenderClass, times(1)).send(message);

    }

    @Test
    public void sendSimpleMessageDTO() {

        EmailMessageDTO emailMessageDTO;
        emailMessageDTO = new EmailMessageDTO();

        String to, subject, text;

        to = "zezao@gmailcom";
        subject = "Test";
        text = "Texto";

        emailMessageDTO.setTo(to);
        emailMessageDTO.setSubject(subject);
        emailMessageDTO.setMessage(text);

        message.setTo(emailMessageDTO.getTo());
        message.setSubject(emailMessageDTO.getSubject());
        message.setText(emailMessageDTO.getMessage());

        emailServiceImpl.sendSimpleMessage(emailMessageDTO);
        verify(javaMailSenderClass, times(1)).send(message);

    }
}