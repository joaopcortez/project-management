package project.model.unittests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;

import project.dto.rest.UserRestDTO;
import project.model.project.Project;
import project.model.user.Address;
import project.model.user.EmailAddress;
import project.model.user.User;
import project.model.user.UserIdVO;
import project.model.user.UserLoginData;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import system.dto.LoginTokenDTO;

public class UserTest {

    String name, name2;
    String email, email2;
    String taxPayerId, taxPayerId2;
    String nr, nr2;
    LocalDate birth, birth2;
    User p1, p2, p3, p4, p5;

    @Before
    public void initialize() throws AddressException {
        name = "Joaquim";
        name2 = "Pedro";
        email = "joaquim@gmail.com";
        email2 = "pedro@hotmail.com";
        nr = "4354543";
        birth = LocalDate.of(1999, 9, 11);
        birth2 = LocalDate.of(1975, 12, 5);
        taxPayerId = "132456765";
        taxPayerId2 = "435677899";
        String nr = "914303222";
        String nr2 = "914434111";
        p1 = new User(name, nr, email, taxPayerId, birth, "2", "Rua ", "4433", "cidade", "país");
        p2 = new User(name2, nr2, email, taxPayerId2, birth2, "2", "Rua ", "4433", "cidade", "país");
        p3 = new User("", "", email, "", birth, "2", "Rua ", "4433", "cidade", "país");
        p4 = new User(name, nr, "44@44", taxPayerId, birth, "2", "Rua ", "4433", "cidade", "país");
        p5 = new User("Jony", nr, "jony@mail.com", taxPayerId, birth, "2", "Rua ", "4433", "cidade", "país");
        p1.getRoles().add(new Role(RoleName.ROLE_REGISTEREDUSER));
        p2.getRoles().add(new Role(RoleName.ROLE_REGISTEREDUSER));
        p3.getRoles().add(new Role(RoleName.ROLE_REGISTEREDUSER));
        p4.getRoles().add(new Role(RoleName.ROLE_REGISTEREDUSER));
        p5.getRoles().add(new Role(RoleName.ROLE_ADMINISTRATOR));
    }


    @Test
    public void testGetPassword() {
        // Given
        assertNull(p1.getPassword());
        // When
        String password = "12345";
        p1.setPassword(password);
        // Then
        assertEquals(password, p1.getPassword());

    }

    @Test
    public void testSetAndGetName() {
        // Given
        assertEquals("Joaquim", p1.getName());
        // When
        p2.setName("Pala");
        // Then
        assertEquals("Pala", p2.getName());

    }

    @Test
    public void testSetAndGetPhone() throws AddressException {
        // Given
        assertEquals("914303222", p1.getPhone());
        // When
        p2.setPhone("919999999");
        // Then
        assertEquals("919999999", p2.getPhone());

    }

    @Test
    public void testSetAndGetTaxPayerId() {
        // Given
        assertEquals("132456765", p1.getTaxPayerId());
        // When
        p2.setTaxPayerId("334303222");
        // Then
        assertEquals("334303222", p2.getTaxPayerId());

    }

    @Test
    public void testSetAndGetEmailAddress() throws AddressException {
        // Given
        EmailAddress email = new EmailAddress("joaquim@gmail.com");
        assertEquals(email, p1.getEmailAddress());
        // When
        EmailAddress email2 = new EmailAddress("andre@gmail.com");
        p2.setEmailAddress("andre@gmail.com");
        // Then
        assertEquals(email2, p2.getEmailAddress());

    }

    @Test
    public void testSetandGetUserIdVO() {

        UserIdVO userID = UserIdVO.create("joaquim@gmail.com");
        assertEquals(userID, p1.getUserIdVO());

        UserIdVO userId = UserIdVO.create("asdrubal@switch.com");
        p1.setUserIdVO(userId);
        assertEquals(userId, p1.getUserIdVO());

    }

    @Test
    public void testSetandGetUserLoginData() {

        // Given

        p1.getUserLoginData().setConfirmationToken("12345");

        assertEquals("12345", p1.getUserLoginData().getConfirmationToken());
        // When
        UserLoginData userData = new UserLoginData();
        userData.setConfirmationToken("123456");
        p1.setUserLoginData(userData);
        // Then
        assertEquals(userData, p1.getUserLoginData());

    }

    @Test
    public void testGetId() {

        assertEquals("joaquim@gmail.com", p1.getEmail());
    }

    @Test
    public void testSetAndGetBirth() {
        // Given
        assertEquals(p1.getBirthDate(), birth);
        // When
        p2.setBirthDate(birth);
        // Then
        assertEquals(p2.getBirthDate(), birth);
    }

    @Test
    public void testSetAndGetAddress() {
        // Given
        Address a1 = new Address("1", "Rua do Passal", "Mang", "POR", "3432-123");
        Address a2 = new Address("2", "Rua Fernando Namora", "Maia", "POR", "4425-651");
        // When
        p1.addAddress(a1);
        p1.addAddress(a2);
        p1.getAddressList().get(0).setAddressDescription("00");
        p1.getAddressList().get(0).setStreet("Rua Pinheiro");
        p1.getAddressList().get(0).setCity("Vila Real");
        p1.getAddressList().get(0).setCountry("ING");
        p1.getAddressList().get(0).setPostalCode("4000");
        // then
        assertEquals("00", p1.getAddressList().get(0).getAddressDescription());
        assertEquals("Rua Pinheiro", p1.getAddressList().get(0).getStreet());
        assertEquals("Vila Real", p1.getAddressList().get(0).getCity());
        assertEquals("ING", p1.getAddressList().get(0).getCountry());
        assertEquals("4000", p1.getAddressList().get(0).getPostalCode());

    }

    @Test
    public void testSetAndGetAddress2() {
        // Given
        Address a1 = new Address("1", "Rua do Passal", "Mang", "POR", "3432-123");
        // Then
        assertTrue(p1.addAddress(a1));
    }

    @Test
    public void testAddAddressFalse() {
        // Given
        Address addressFalse = new Address("123", "", "4444-444", "Porto", "PT");
        // When
        p1.addAddress(addressFalse);
        // Then
        assertFalse(p1.addAddress(addressFalse));
    }

    @Test
    public void testAddAddressFalse2() throws AddressException {
        // Given
        Address addressFalse = new Address("123", "Rua", "4444-444", "Porto", "PT");
        LocalDate birth = LocalDate.of(1999, 11, 11);
        User personFalse = new User("Pedro", "919999999", "pedro@gmail.com", "1334324", birth, "123", "Rua", "4444-444",
                "Porto", "PT");
        // Then
        assertFalse(personFalse.addAddress(addressFalse));
    }

    @Test
    public void testEqualsTrue1() throws AddressException {
        // Given
        String name3 = "pedro";
        String email3 = "ool@ds";
        String taxPayerId3 = "333222111";
        LocalDate birth3 = LocalDate.of(1979, 7, 21);
        String nr1 = "334222333";
        String nr2 = "333226333";
        String nr3 = "333222333";
        // When
        User p1 = new User(name, nr1, email, taxPayerId, birth, "2", "Rua ", "4433", "cidade", "país");
        User p2 = new User(name2, nr2, email2, taxPayerId2, birth, "2", "Rua ", "4433", "cidade", "país");
        User p3 = new User(name3, nr3, email3, taxPayerId3, birth3, "2", "Rua ", "4433", "cidade", "país");

        // Then
        assertFalse(p3.equals(p2));
        assertFalse(p1.equals(p2));

    }

    @Test
    public void testEqualsTrue2() throws AddressException {
        // Given
        String name = "pedro";
        String email = "pedro@gmail.com";
        String taxPayerId = "333222111";
        LocalDate birth = LocalDate.of(1979, 7, 21);
        String nr1 = "334222333";
        // When
        User p1 = new User(name, nr1, email, taxPayerId, birth, "2", "Rua ", "4433", "cidade", "país");
        // then
        assertTrue(p1.equals(p1));
    }

    @Test
    public void testEqualsFalse1() {
        // Given
        User p1 = null;
        // Then
        assertFalse(p2.equals(p1));
    }

    @Test
    public void testEqualsFalse2() throws AddressException {
        // Given
        User p1 = new User(name, "5435435", email, taxPayerId, birth, "2", "Rua ", "4433", "cidade", "país");
        User p2 = new User(name2, "5435432", "lisa@gmail.com", taxPayerId2, birth2, "2", "Rua ", "4433", "cidade", "país");
        // Then
        assertFalse(p2.equals(p1));

    }

    @Test
    public void testEqualsFalse3() throws AddressException {
        // Given
        User p1 = new User(name, "4344324", email, taxPayerId, birth, "2", "Rua ", "4433", "cidade", "país");
        // When
        Project p2 = new Project("ww", "ss");
        // Then
        assertFalse(p1.equals(p2));

    }

    @Test
    public void testEqualsNullId() throws AddressException {
        // Given
        User p1 = new User(name, "4354543", email, taxPayerId, birth, "2", "Rua ", "4433", "cidade", "país");

        // When
        Project p2 = new Project("ww", "ss");
        // Then
        assertFalse(p1.equals(p2));

    }

    @Test
    public void testCheckIfUserStatusIsActive() {
        // given
        assertTrue(p3.isActive());
        // When
        p3.setInactive();
        // Then
        assertFalse(p3.isActive());

    }

    @Test
    public void testCheckIfUserStatusIsInactive() {
        // Given
        p3.setInactive();
        assertFalse(p3.isActive());
        // When
        p3.setActive();
        // then
        assertTrue(p3.isActive());

    }

    @Test
    public void testEditUserPersonalDataAddressSucess() throws AddressException {
        // Given
        User user1 = new User("Joaquim", "91739812379", "joaquim@gmail.com", "132456765", birth, "casa",
                "Rua do Bicalho", "4500-200", "Porto", "Portugal");
        Address newAddress2 = new Address("casa", "Rua sem saida", "4600-200", "Lisboa", "Inglaterra");
        // When
        user1.editAddress(newAddress2);
        // Then
        assertEquals(newAddress2.getCity(), user1.getAddressById("casa").getCity());
        assertEquals(newAddress2.getCountry(), user1.getAddressById("casa").getCountry());
        assertEquals(newAddress2.getPostalCode(), user1.getAddressById("casa").getPostalCode());
        assertEquals(newAddress2.getStreet(), user1.getAddressById("casa").getStreet());
        assertEquals(newAddress2, user1.getAddressById("casa"));
    }

    @Test
    public void testEditUserPersonalDataAddressFailAddressNotEquals() throws AddressException {
        // Given
        User user1 = new User("Joaquim", "91739812379", "joaquim@gmail.com", "132456765", birth, "casa",
                "Rua do Bicalho", "4500-200", "Porto", "Portugal");
        Address newAddress2 = new Address("emprego", "Rua sem saida", "4600-200", "Lisboa", "Inglaterra");
        // When
        user1.editAddress(newAddress2);
        // Then
        assertNotEquals(newAddress2, user1.getAddressById("casa"));
    }

    @Test
    public void testEditUserPersonalDataAddressFailNoStreet() throws AddressException {
        // Given
        User user1 = new User("Joaquim", "91739812379", "joaquim@gmail.com", "132456765", birth, "casa",
                "Rua do Bicalho", "4500-200", "Porto", "Portugal");

        Address newAddress2 = new Address("casa", "", "4500-200", "Lisboa", "Inglaterra");
        // When
        user1.editAddress(newAddress2);
        // Then
        assertNotEquals(newAddress2.getCountry(), user1.getAddressById("casa").getCountry());

    }

    @Test
    public void testGetAddressById_noID() throws AddressException {
        // Given
        User user1 = new User("Joaquim", "91739812379", "joaquim@gmail.com", "132456765", birth, "casa",
                "Rua do Bicalho", "4500-200", "Porto", "Portugal");
        // Then
        assertNull(user1.getAddressById("casa1"));
    }

    @Test
    public void testGetAddressById_ID() throws AddressException {
        // Given
        User user1 = new User("Joaquim", "91739812379", "joaquim@gmail.com", "132456765", birth, "casa",
                "Rua do Bicalho", "4500-200", "Porto", "Portugal");
        Address newAddress2 = new Address("casa", "Rua do Bicalho", "4500-200", "Porto", "Portugal");
        // Then
        assertEquals(newAddress2, user1.getAddressById("casa"));
    }

    @Test
    public void testSetProfileDirector() {

        // Given
        assertTrue(p3.hasRoleRegisteredUser());

        // When
        p3.setProfileDirector();
        // Then
        assertTrue(p3.hasRoleDirectorr());

    }

    @Test
    public void testSetProfileRegisteredUser() {
        // Given
        assertTrue(p3.hasRoleRegisteredUser());
        // When
        p3.setProfileRegisteredUser();
        // Then
        assertTrue(p3.hasRoleRegisteredUser());

    }

    @Test
    public void testHasRoleRegisteredUserFail() {
        // Given
        assertFalse(p5.hasRoleRegisteredUser());
    }

    @Test
    public void testSetProfileAdministrator() {
        // Given
        assertTrue(p3.hasRoleRegisteredUser());
        // When
        p3.setProfileAdministrator();
        // Then
        assertTrue(p3.hasRoleAdministrator());

    }

    @Test
    public void testSetProfileAdministratorNotContainsRole() {
        // Given
        assertTrue(p5.hasRoleAdministrator());
        // When
        p5.setProfileAdministrator();
        // Then
        assertTrue(p5.hasRoleAdministrator());

    }

    @Test
    public void testSetProfileAdministratorFail() {
        // Given
        assertTrue(p3.hasRoleRegisteredUser());
        // Then
        assertFalse(p3.hasRoleAdministrator());

    }

    @Test
    public void testSetProfileCollaborator() {
        // Given
        assertTrue(p3.hasRoleRegisteredUser());
        // When
        p3.setProfileCollaborator();
        // Then
        assertTrue(p3.hasRoleCollaborator());

    }

    @Test
    public void testSetActive() {
        // Given
        p3.setActive();
        // Then
        assertTrue(p3.isActive());

    }

    @Test
    public void testSetInactive() {
        // Given
        assertTrue(p3.isActive());
        // When
        p3.setInactive();
        // Then
        assertFalse(p3.isActive());

    }

    @Test
    public void testIsValidSucess() throws AddressException {
        // Given
        String name = "Asdrubal";
        String phone = "913456789";
        String email = "alberto1234@isep.ipp.pt";
        String taxPayerId = "123456789";
        // When
        User u = new User(name, phone, email, taxPayerId, birth, "2", "Rua ", "4433", "cidade", "país");
        // Then
        assertTrue(u.isValid(email));
    }

    @Test//(expected=Exception.class)
    public void testIsValidException() throws AddressException {
        // Given
        String name = "Asdrubal";
        String phone = "913456789";
        String email = "alberto1234@isep.ipp.pt";
        String taxPayerId = "123456789";
        // When
        User u = new User(name, phone, email, taxPayerId, birth, "2", "Rua ", "4433", "cidade", "país");
        // Then

        assertFalse(u.isValid("@@@"));
    }

    @Test
    public void testIsValidFailureNoName() throws AddressException {
        // Given
        String name = "";
        String phone = "913456789";
        String email = "alberto1234@isep.ipp.pt";
        String taxPayerId = "123456789";
        // When
        User u = new User(name, phone, email, taxPayerId, birth, "2", "Rua ", "4433", "cidade", "país");
        // Then
        assertFalse(u.isValid(email));
    }

    @Test
    public void testIsValidFailureNoName2() throws AddressException {
        // Given
        String name = null;
        String phone = "913456789";
        String email = "alberto1234@isep.ipp.pt";
        String taxPayerId = "123456789";
        // When
        User u = new User(name, phone, email, taxPayerId, birth, "2", "Rua ", "4433", "cidade", "país");
        // Then
        assertFalse(u.isValid(email));
    }

    @Test
    public void testIsValidFailureNoPhone() throws AddressException {
        // given
        String name = "Alberto Martins";
        String phone = "";
        String email = "alberto1234@isep.ipp.pt";
        String taxPayerId = "123456789";
        // When
        User u = new User(name, phone, email, taxPayerId, birth, "2", "Rua ", "4433", "cidade", "país");
        // Then
        assertFalse(u.isValid(email));
    }

    @Test
    public void testIsValidFailureNoPhone2() throws AddressException {
        // Given
        String name = "Alberto Martins";
        String phone = null;
        String email = "alberto1234@isep.ipp.pt";
        String taxPayerId = "123456789";
        // When
        User u = new User(name, phone, email, taxPayerId, birth, "2", "Rua ", "4433", "cidade", "país");
        // Then
        assertFalse(u.isValid(email));
    }

    @Test(expected = AddressException.class)
    public void testAddUserFailureEmailTwoAtSigns() throws AddressException {
        // Given
        String name = "Alberto Martins";
        String phone = "913456789";
        String taxPayerId = "123456789";
        // When
        String email = "alberto1234@isep@ipp.pt";
        // Then
        User u = new User(name, phone, email, taxPayerId, birth, "2", "Rua ", "4433", "cidade", "país");
        assertFalse(u.isValid(email));
    }

    @Test
    public void testRemoveAddress() throws AddressException {
        // Given
        User user = new User("Alberto Martins", "913456789", "alberto1234@isep.ipp.pt", "123456789",
                LocalDate.of(1999, 11, 11), "2", "Rua ", "4433", "cidade", "país");
        Address a1 = new Address("1", "Rua do Passal", "Mang", "POR", "3432-123");
        Address a2 = new Address("2", "Rua Fernando Namora", "Maia", "POR", "4425-651");
        // When
        user.addAddress(a1);
        user.addAddress(a2);
        // Then
        assertTrue(user.removeAddress(a1));
        assertFalse(user.removeAddress(a1));
    }

    @Test
    public void createAddressTest() {
        // Given
        Address expected = new Address("1", "Rua do Passal", "Mang", "POR", "3432-123");
        // Then
        assertEquals(expected, p3.createAddress("1", "Rua do Passal", "Mang", "POR", "3432-123"));
    }

    @Test
    public void testValidatePasswordRightPassword() {

        //When
        p1.setPassword("SWitCH");

        // Then
        String expResult = "[ROLE_REGISTEREDUSER] Joaquim SUCCESSFULLY LOGGED";
        LoginTokenDTO result = p1.validatePassword("SWitCH");
        assertEquals(expResult,result.getMessage());
    }

    @Test
    public void testValidatePasswordWrongPassword() {
        //When
        p1.setPassword("SWitCH");
        //Then
        String expResult = "Invalid Email or Password";
        LoginTokenDTO result = p1.validatePassword("e");

        assertTrue(expResult.equals(result.getMessage()));
    }

    @Test
    public void testHashCode() {
        assertEquals(p1.hashCode(), p1.hashCode());
        assertNotEquals(p1.hashCode(), p4.hashCode());

    }

    @Test
    public void testToStringEquals() {
        // Given
        String expected = "User{" + "email='joaquim@gmail.com'" + ", name='Joaquim'" + ", phone='914303222'" + ", profile=[ROLE_REGISTEREDUSER]" + ", taxPayerId='132456765'" + "}";
        // Then
        assertEquals(expected, p1.toString());
    }

    @Test
    public void testToStringNotEquals() {
        // Given
        // Then
        assertNotEquals(p2.toString(), p1.toString());
    }

    @Test
    public void testToDTO() {

        UserRestDTO userDTO = p1.toDTO();

        assertEquals(p1.getName(), userDTO.getName());
        assertEquals(p1.getBirthDate(), userDTO.getBirthDate());
        assertEquals(p1.getEmail(), userDTO.getEmail());
        assertEquals(p1.getPhone(), userDTO.getPhone());
        assertEquals(p1.getTaxPayerId(), userDTO.getTaxPayerId());
        assertEquals(p1.getRoles().toString(), userDTO.getProfile());
    }

    @Test
    public void testIsFirstLoginSuccess() {
        LocalDateTime lastLoginDate = p1.getUserLoginData().getLastLoginDate();
        assertNull(lastLoginDate);
        boolean condition = p1.isFirstLogin();
        assertTrue(condition);
    }

    @Test
    public void testIsFirstLoginLastLoginDateNull() {
        LocalDateTime date;
        date = LocalDateTime.of(2017, 11, 11, 0, 0);
        p1.getUserLoginData().setLastLoginDate(date);
        LocalDateTime lastLoginDate = p1.getUserLoginData().getLastLoginDate();
        assertNotNull(lastLoginDate);
        boolean condition = p1.isFirstLogin();
        assertFalse(condition);
    }





   

    

   

    @Test
    public void setProfileCollaborator() {
        //given
        assertTrue(p1.hasRoleRegisteredUser());

        //when
        p1.setProfileCollaborator();

        //then
        assertTrue(p1.hasRoleCollaborator());
        assertTrue(p1.hasRoleRegisteredUser());

    }

    @Test
    public void setProfileDirector() {
        //given
        assertTrue(p1.hasRoleRegisteredUser());

        //when
        p1.setProfileDirector();

        //then
        assertTrue(p1.hasRoleDirectorr());
        assertTrue(p1.hasRoleRegisteredUser());
    }

    @Test
    public void setProfileRegisteredUser() {
        //given
        assertTrue(p1.hasRoleRegisteredUser());

        //when
        p1.setProfileCollaborator();
        assertTrue(p1.hasRoleCollaborator());
        p1.setProfileRegisteredUser();

        //then
        assertTrue(p1.hasRoleRegisteredUser());
        assertTrue(p1.hasRoleCollaborator());
    }

    @Test
    public void toDTO() {
        UserRestDTO userDTO = new UserRestDTO();
        userDTO.setName(p1.getName());
        userDTO.setEmail(p1.getEmail());
        userDTO.setPhone(p1.getPhone());
        userDTO.setBirthDate(p1.getBirthDate());
        userDTO.setTaxPayerId(p1.getTaxPayerId());
        userDTO.setProfile(p1.getRoles().toString());
        assertEquals(userDTO, p1.toDTO());
    }

    @Test
    public void TestValidatePasswordAndConfirmationCodePasswordsDoesNotMatch() {
        LoginTokenDTO expected = new LoginTokenDTO();
        expected.setMessage("Invalid Data introduced!");
        expected.setStatusCode(1);
        expected.setActive(false);
        assertEquals(expected ,p1.validatePasswordAndConfirmationCode("12345", "1234"));
    }

    @Test
    public void TestValidatePasswordAndConfirmationCodeConfirmationCodeDoesNotMatch() {
        LoginTokenDTO expected = new LoginTokenDTO();
        expected.setMessage("Wrong Confirmation code");
        expected.setStatusCode(3);
        expected.setActive(false);
        p1.setPassword("12345");
        assertEquals(expected ,p1.validatePasswordAndConfirmationCode("12345", "1234"));
    }

    @Test
    public void TestValidatePasswordAndConfirmationCode() {
        LoginTokenDTO exp = new LoginTokenDTO(p1.getName(), p1.getEmail(), "[ROLE_REGISTEREDUSER]", true,
                LocalDateTime.now(), LocalDateTime.now(), "1234", "[ROLE_REGISTEREDUSER]" + " " +
                p1.getName() + " ACCOUNT ACTIVATED, SUCCESSFULLY LOGGED");

        p1.setPassword("12345");
        p1.getUserLoginData().setConfirmationToken("1234");

        assertEquals(exp ,p1.validatePasswordAndConfirmationCode("12345", "1234"));
    }

    @Test
    public void testHasRoleCollaborator() {

        assertFalse(p1.hasRoleCollaborator());
        p1.setProfileCollaborator();
        assertTrue(p1.hasRoleCollaborator());
    }
}