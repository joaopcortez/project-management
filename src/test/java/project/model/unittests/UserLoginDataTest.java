package project.model.unittests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDateTime;

import org.junit.Test;

import project.model.user.UserLoginData;

public class UserLoginDataTest {
	UserLoginData userData= new UserLoginData();
	
	LocalDateTime d1=  LocalDateTime.of(2017, 12, 18, 0, 0);
	LocalDateTime d2=  LocalDateTime.of(2018, 1, 18, 0, 0);
	
	@Test
	public void testGetConfirmationToken() {
		
		UserLoginData userData= new UserLoginData();
		
		assertNotNull(userData.getConfirmationToken());
		
	}
	
	
	@Test
	public void testsetConfirmationToken() {
		
		userData.setConfirmationToken("12345");
		
		assertEquals("12345",userData.getConfirmationToken());
		
	}
	
	@Test
	public void testSetLastLoginDate() {
		
	userData.setLastLoginDate(d1);
		
	assertEquals(d1,userData.getLastLoginDate());
		
		
	}
	
	
	@Test
	public void testSetApprovedDateTime() {
		
		userData.setApprovedDateTime(d2);
		assertEquals(d2,userData.getApprovedDateTime());
	}

}
