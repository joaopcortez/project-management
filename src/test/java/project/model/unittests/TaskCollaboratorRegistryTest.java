package project.model.unittests;

import java.lang.reflect.Constructor;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.mail.internet.AddressException;

import com.google.common.collect.Lists;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import project.dto.rest.TaskCollaboratorRegistryRESTDTO;
import project.model.project.CostCalculator;
import project.model.project.LastTimePeriodCost;
import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;
import project.model.task.Report;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.task.TaskCollaboratorRegistry.RegistryStatus;
import project.model.user.User;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.*;

public class TaskCollaboratorRegistryTest {

    // Dates
    LocalDate birth;
    LocalDateTime d1;
    LocalDateTime d2;
    LocalDateTime d3;

    // Users
    User user1;
    User user2;
    User projectManager;

    // Project Collaborators
    ProjectCollaborator projManager1;
    ProjectCollaborator projManager2;
    ProjectCollaborator collab1;
    ProjectCollaborator collab2;
    ProjectCollaborator collab3;
    ProjectCollaborator collab4;

    // Tasks
    Task t1;
    Task t2;
    Task t3;

    // Task ROLE_COLLABORATOR Registries
    TaskCollaboratorRegistry r1;
    TaskCollaboratorRegistry r2;
    TaskCollaboratorRegistry r3;
    TaskCollaboratorRegistry r4;

    // Reports
    Report report1;
    Report report2;
    // Lists - Project Collaborators and Reports
    List<ProjectCollaborator> expectedCollaboratorList;
    List<Report> reportList;
    // Projects
    private Project project1;
    private Project project2;

    @Before
    public void initialize() throws AddressException {

        project1 = new Project("100", "Project 1");
        project2 = new Project("200", "Project 2");

        expectedCollaboratorList = new ArrayList<>();

        d1 = LocalDateTime.of(2017, 12, 18, 0, 0);
        d2 = LocalDateTime.of(2017, 12, 20, 0, 0);
        d3 = LocalDateTime.of(2017, 12, 23, 0, 0);

        t1 = new Task(project1, "Test 1");
        t2 = new Task(project1, "Test 2");
        t3 = new Task(project2, "test 3");

        birth = LocalDate.of(1999, 11, 11);
        user1 = new User("Joaquim", "91739812379", "jj@jj.com", "132456765", birth, "2", "Rua ", "4433", "cidade",
                "pais");

        user2 = new User("Manel", "666 555 5556", "manel@jj.com", "5656546456", birth, "2", "Rua ", "4433", "cidade",
                "pais");

        projectManager = new User("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "2", "Rua ", "4433",
                "cidade", "pais");

        user1.setProfileCollaborator();
        user2.setProfileCollaborator();

        collab1 = new ProjectCollaborator(project1, user1, 3);
        collab1.getCostAndTimePeriodList().get(0).setStartDate(d1.minusYears(3).toLocalDate());
        collab2 = new ProjectCollaborator(project1, user2, 3);
        collab3 = new ProjectCollaborator(project2, user2, 2);
        collab3.getCostAndTimePeriodList().get(0).setStartDate(d1.minusYears(5).toLocalDate());
        collab4 = new ProjectCollaborator(project2, user1, 4);

        projManager1 = new ProjectCollaborator(project1, projectManager, 0);
        projManager2 = new ProjectCollaborator(project1, projectManager, 0);
        project1.setProjectManager(projManager1);
        project2.setProjectManager(projManager2);
        project1.addProjectCollaborator(collab1);
        project1.addProjectCollaborator(collab2);

        project2.addProjectCollaborator(collab3);
        project2.addProjectCollaborator(collab4);

        t1.addProjectCollaborator(collab1);
        t1.addProjectCollaborator(collab2);
        t3.addProjectCollaborator(collab3);

        r1 = t1.listTaskCollaboratorRegistry(collab1).get(0);
        r1.setCollaboratorAddedToTaskDate(d1.minusYears(20));
        r2 = t1.listTaskCollaboratorRegistry(collab2).get(0);
        r2.setCollaboratorAddedToTaskDate(d1.minusYears(20));
        r3 = t1.listTaskCollaboratorRegistry(collab2).get(0);
        r3.setCollaboratorAddedToTaskDate(d1.minusYears(20));

        r1.addReport(5.0, d1, d1.plusDays(5));
        r1.addReport(8.0, d2, d2.plusDays(5));
        report1 = r1.getReportList().get(0);
        report2 = r1.getReportList().get(1);
    }

    @Test
    public void testReportDefaultConstructorThroughReflection() {
        // Given
        // When
        TaskCollaboratorRegistry taskCollReg = null;

        try {
            Constructor<TaskCollaboratorRegistry> constructor = TaskCollaboratorRegistry.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            taskCollReg = constructor.newInstance();
        } catch (Exception e) {

        }
        // Then
        boolean condition = taskCollReg instanceof TaskCollaboratorRegistry;
        assertTrue(condition);
    }

    @Test
    public void testGetCollaboratorRemovedFromTaskDate() {
        // Given
        // Then
        assertEquals(2, t1.listAllTaskCollaboratorRegistry().size());
        assertEquals(true, t1.hasProjectCollaborator(collab1));
        assertEquals(true, t1.hasProjectCollaborator(collab2));
        assertEquals(r1.getProjectCollaborator(), collab1);
        assertEquals(r2.getProjectCollaborator(), collab2);
        assertEquals(true, t1.removeProjectCollaborator(collab1));
        // When
        r1.setCollaboratorRemovedFromTaskDate(LocalDateTime.of(2017, 12, 14, 1, 0));
        // Then
        assertEquals(1, t1.listActiveCollaborators().size());
        assertEquals(2, t1.listAllTaskCollaboratorRegistry().size());
        assertEquals(LocalDateTime.of(2017, 12, 14, 1, 0), r1.getCollaboratorRemovedFromTaskDate());
    }

    @Test
    public void testGetCollaboratorRemovedFromTaskDateNull() {
        // Given
        // Then
        LocalDateTime result = r1.getCollaboratorRemovedFromTaskDate();
        assertEquals(null, result);
    }

    @Test
    public void testSetCollaboratorRemovedFromTaskDate() {
        // Given
        // When
        t1.removeProjectCollaborator(collab1);
        r1.setCollaboratorRemovedFromTaskDate(d3);
        // Then
        LocalDateTime expected = LocalDateTime.of(2017, 12, 23, 0, 0);
        LocalDateTime result = r1.getCollaboratorRemovedFromTaskDate();
        assertEquals(expected, result);
    }

    @Test
    public void testAddReportValidQuantity() {
        // Given
        // When
        List<Report> expected = new ArrayList<>();
        Report rep1 = new Report("1(1)", 5, d1, d1.plusDays(5));
        Report rep2 = new Report("1(2)", 8, d1, d1.plusDays(5));
        expected.add(rep1);
        expected.add(rep2);
        // Then
        List<Report> result = r1.getReportList();
        assertEquals(expected, result);
        // When
        Report rep3 = new Report("1(3)", 8, d1, d1.plusDays(5));
        expected.add(rep3);
        String message = r1.addReport(8, d1, d1.plusDays(5));
        // Then
        result = r1.getReportList();
        assertEquals(user1.getEmail(), r1.getReportList().get(0).getUserEmail());
        assertEquals(expected, result);
        assertEquals("Report successfully added", message);
    }

    @Test
    public void testAddReportValidStateRemovalRequest() {
        // Given
        // When
        List<Report> expected = new ArrayList<>();
        Report rep1 = new Report("1(1)", 5, d1, d1.plusDays(5));
        Report rep2 = new Report("1(2)", 8, d1, d1.plusDays(5));
        expected.add(rep1);
        expected.add(rep2);
        // Then
        List<Report> result = r1.getReportList();
        assertEquals(expected, result);
        // When
        r1.setRegistryStatus(RegistryStatus.REMOVALREQUEST);
        Report rep3 = new Report("1(3)", 8, d1, d1.plusDays(5));
        expected.add(rep3);
        String message = r1.addReport(8, d1, d1.plusDays(5));
        // Then
        result = r1.getReportList();
        assertEquals(expected, result);
        assertEquals("Report successfully added", message);
    }

    @Test
    public void testAddReportInvalidQuantity() {
        // Given
        // When
        String message = r1.addReport(0, d2, d2.plusDays(5));
        List<Report> expected = new ArrayList<>();
        Report rep1 = new Report("1(1)", 5, d1, d1.plusDays(5));
        Report rep2 = new Report("1(2)", 8, d1, d1.plusDays(5));
        expected.add(rep1);
        expected.add(rep2);
        // Then
        List<Report> result = r1.getReportList();
        assertEquals(expected, result);
        assertEquals("Invalid number of hours. Please try again.", message);
    }

    @Test
    public void testGetReportByIdTrue() {
        // Given
        int reportCount = TaskCollaboratorRegistry.getThisTasksReportCount();
        Report expected = new Report(t1.getId() + "(" + (reportCount - 1) + ")", 5,
                LocalDateTime.of(2017, 12, 14, 1, 0), LocalDateTime.of(2017, 12, 19, 1, 0));
        // When
        r1.setCollaboratorAddedToTaskDate(LocalDateTime.of(2017, 12, 14, 0, 0));
        // Then
        assertEquals(expected, r1.getReportById(t1.getId() + "(" + (reportCount - 1) + ")"));
    }

    @Test
    public void testGetReportByIdNotExist() {
        // Given
        // Then
        Report expected = null;
        Report result = r1.getReportById("4");
        assertEquals(expected, result);
    }

    @Test
    public void testGetReportList() {
        // Given
        List<Report> expected = new ArrayList<>();
        // When
        Report rep1 = new Report("1(1)", 5, LocalDateTime.of(2017, 12, 14, 1, 0), LocalDateTime.of(2017, 12, 19, 1, 0));
        Report rep2 = new Report("1(2)", 8, LocalDateTime.of(2017, 12, 14, 1, 0), LocalDateTime.of(2017, 12, 19, 1, 0));
        expected.add(rep1);
        expected.add(rep2);
        // Then
        List<Report> result = r1.getReportList();
        assertEquals(expected, result);
    }

    @Test
    public void testGetProjectCollaborator() {
        // Given
        // Then
        ProjectCollaborator expected = r1.getProjectCollaborator();
        ProjectCollaborator result = collab1;
        ProjectCollaborator expected2 = r2.getProjectCollaborator();
        ProjectCollaborator result2 = collab2;
        assertEquals(expected, result);
        assertEquals(expected2, result2);
    }

    @Test
    public void testGetCollaboratorAddedToTaskDateTrue() {
        // Given
        TaskCollaboratorRegistry r3 = t3.listTaskCollaboratorRegistry(collab3).get(0);
        LocalDateTime expected = LocalDateTime.of(2017, 12, 14, 1, 0);
        // When
        r3.setCollaboratorAddedToTaskDate(LocalDateTime.of(2017, 12, 14, 1, 0));
        // Then
        LocalDateTime result = r3.getCollaboratorAddedToTaskDate();
        assertEquals(expected, result);
    }

    @Test
    public void testGetCost() {
        // Given
        // Then
        double expected = r1.getCost(project1.getCostCalculator());
        double result = 3 * 5 + 3 * 8;
        assertEquals(expected, result, 0.0);
    }

    @Test
    public void testGetId() {
        // Given
        // Then
        String expected = t1.getId() + "-jj@jj.com";
        String result = r1.getTaskCollaboratorRegistryID();
        assertEquals(expected, result);
    }

    @Test
    public void testGetTaskId() {
        // Given
        // When
        TaskCollaboratorRegistry r4 = new TaskCollaboratorRegistry(collab1, "taskId");
        // Then
        assertEquals("taskId", r4.getTaskId());
    }

    @Test
    public void testHasAssignedTaskTrue() {
        // Given
        // Then
        boolean expected = r2.hasAssignedTask();
        boolean result = true;
        assertEquals(expected, result);
    }

    @Test
    public void testHasAssignedTaskFalse() {
        // Given
        // When
        t1.removeProjectCollaborator(collab2);
        // Then
        boolean expected = r2.hasAssignedTask();
        boolean result = false;
        assertEquals(expected, result);
    }

    @Test
    public void testToString() {
        // Given
        // Then
        int reportCount = TaskCollaboratorRegistry.getThisTasksReportCount();

        String expected = "\nTaskCollaboratorRegistryID = " + t1.getId() + "-jj@jj.com\n" + "\n"
                + "Project ROLE_COLLABORATOR Name=Joaquim, Email=jj@jj.com, cost=3.0\n\n" + "CollaboratorAddedToTaskDate = "
                + r1.getCollaboratorAddedToTaskDate() + "\nCollaboratorRemovedFromTaskDate = null\n" + "\n"
                + "ReportList = [\n" + "Report id = " + t1.getId() + "(" + (reportCount - 1) + ")" + "\n"
                + "Spended Time=5.0\n" + "reportDate=" + report1.getReportCreationDate()
                + "\nstartDate=2017-12-18T00:00\n" + ", \n" + "Report id = " + t1.getId() + "(" + (reportCount) + ")"
                + "\n" + "Spended Time=8.0\n" + "reportDate=" + report2.getReportCreationDate()
                + "\nstartDate=2017-12-20T00:00\n" + "]\n" + "";

        String result = r1.toString();
        assertEquals(expected, result);
    }

    @Test
    public void testGetCostSuccess() {

        // Given
        CostCalculator costMethod = new LastTimePeriodCost();

        ProjectCollaboratorCostAndTimePeriod period1 = new ProjectCollaboratorCostAndTimePeriod(10);
        ProjectCollaboratorCostAndTimePeriod period2 = new ProjectCollaboratorCostAndTimePeriod(15);
        period1.setStartDate(LocalDate.now().minusDays(365));
        period1.setEndDate(LocalDate.now().minusDays(200));
        period2.setStartDate(LocalDate.now().minusDays(200));

        collab1.getCostAndTimePeriodList().clear();
        collab1.getCostAndTimePeriodList().add(period1);
        collab1.getCostAndTimePeriodList().add(period2);

        r1.getReportList().clear();
        r1.addReport(10.0, LocalDateTime.now().minusDays(300), LocalDateTime.now().minusDays(150));
        report1 = r1.getReportList().get(0);

        // When
        double result = r1.getCost(costMethod);

        // Then
        assertEquals(150.0, result, 0.01);
    }

    @Test
    public void testGetCostSuccessOneReportInABoundaryDate() {

        // Given
        CostCalculator costMethod = new LastTimePeriodCost();

        ProjectCollaboratorCostAndTimePeriod period1 = new ProjectCollaboratorCostAndTimePeriod(10);
        ProjectCollaboratorCostAndTimePeriod period2 = new ProjectCollaboratorCostAndTimePeriod(15);
        period1.setStartDate(LocalDate.now().minusDays(365));
        period1.setEndDate(LocalDate.now().minusDays(201));
        period2.setStartDate(LocalDate.now().minusDays(200));

        collab1.getCostAndTimePeriodList().clear();
        collab1.getCostAndTimePeriodList().add(period1);
        collab1.getCostAndTimePeriodList().add(period2);

        r1.getReportList().clear();
        r1.addReport(10.0, LocalDateTime.now().minusDays(300), LocalDateTime.now().minusDays(200));
        r1.addReport(10.0, LocalDateTime.now().minusDays(200), LocalDateTime.now().minusDays(150));

        // When
        double result = r1.getCost(costMethod);

        // Then
        assertEquals(300.0, result, 0.01);
    }

    @Test
    public void testGetAllCostPeriodsFromReportOnePeriodBeforeReportDates() {

        // Given
        ProjectCollaboratorCostAndTimePeriod period1 = new ProjectCollaboratorCostAndTimePeriod(10);
        ProjectCollaboratorCostAndTimePeriod period2 = new ProjectCollaboratorCostAndTimePeriod(15);
        period1.setStartDate(LocalDate.now().minusDays(365));
        period1.setEndDate(LocalDate.now().minusDays(200));
        period2.setStartDate(LocalDate.now().minusDays(200));

        collab1.getCostAndTimePeriodList().clear();
        collab1.getCostAndTimePeriodList().add(period1);
        collab1.getCostAndTimePeriodList().add(period2);

        List<ProjectCollaboratorCostAndTimePeriod> expected = new ArrayList<>();
        expected.add(period2);

        r1.getReportList().clear();
        r1.addReport(10.0, LocalDateTime.now().minusDays(170), LocalDateTime.now().minusDays(150));
        report1 = r1.getReportList().get(0);

        // When
        List<ProjectCollaboratorCostAndTimePeriod> result = r1.getAllCostPeriodsFromReport(report1, collab1);

        // Then
        assertEquals(expected, result);

    }

    @Test
    public void testGetAllCostPeriodsFromReportOnePeriodAfterReportDates() {

        // Given
        ProjectCollaboratorCostAndTimePeriod period1 = new ProjectCollaboratorCostAndTimePeriod(10);
        ProjectCollaboratorCostAndTimePeriod period2 = new ProjectCollaboratorCostAndTimePeriod(15);
        period1.setStartDate(LocalDate.now().minusDays(365));
        period1.setEndDate(LocalDate.now().minusDays(200));
        period2.setStartDate(LocalDate.now().minusDays(200));

        collab1.getCostAndTimePeriodList().clear();
        collab1.getCostAndTimePeriodList().add(period1);
        collab1.getCostAndTimePeriodList().add(period2);

        List<ProjectCollaboratorCostAndTimePeriod> expected = new ArrayList<>();
        expected.add(period1);

        r1.getReportList().clear();
        r1.addReport(10.0, LocalDateTime.now().minusDays(300), LocalDateTime.now().minusDays(250));
        report1 = r1.getReportList().get(0);

        // When
        List<ProjectCollaboratorCostAndTimePeriod> result = r1.getAllCostPeriodsFromReport(report1, collab1);

        // Then
        assertEquals(expected, result);

    }

    @Test
    public void testIsProjectCollaboratorTimePeriodBeforeReportDatesTrue() {

        // Given
        TaskCollaboratorRegistry collab1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + collab1.getUser().getEmail());
        ProjectCollaboratorCostAndTimePeriod projCollabFirstTimePeriod = collab1.getCostAndTimePeriodList().get(0);
        projCollabFirstTimePeriod.setEndDate(LocalDateTime.now().minusDays(500).toLocalDate());
        Report report = new Report("testReport", 45.0, LocalDateTime.now().minusDays(365),
                LocalDateTime.now().minusDays(360));

        // When
        boolean result = collab1TaskRegistry.isProjectCollaboratorTimePeriodBeforeReportDates(report,
                projCollabFirstTimePeriod);

        // Then
        assertTrue(result);
    }

    @Test
    public void testIsProjectCollaboratorTimePeriodBeforeReportDatesFalse() {

        // Given
        TaskCollaboratorRegistry collab1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + collab1.getUser().getEmail());
        ProjectCollaboratorCostAndTimePeriod projCollabFirstTimePeriod = collab1.getCostAndTimePeriodList().get(0);
        projCollabFirstTimePeriod.setStartDate(LocalDateTime.now().minusDays(205).toLocalDate());
        projCollabFirstTimePeriod.setEndDate(LocalDateTime.now().minusDays(200).toLocalDate());
        Report report = new Report("testReport", 45.0, LocalDateTime.now().minusDays(365),
                LocalDateTime.now().minusDays(360));

        // When
        boolean result = collab1TaskRegistry.isProjectCollaboratorTimePeriodBeforeReportDates(report,
                projCollabFirstTimePeriod);

        // Then
        assertFalse(result);
    }

    @Test
    public void testIsProjectCollaboratorTimePeriodBeforeReportDatesTrueEndDateEqualsReportStartDate() {

        // Given
        TaskCollaboratorRegistry collab1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + collab1.getUser().getEmail());
        ProjectCollaboratorCostAndTimePeriod projCollabFirstTimePeriod = collab1.getCostAndTimePeriodList().get(0);
        projCollabFirstTimePeriod.setStartDate(LocalDateTime.now().minusDays(205).toLocalDate());
        projCollabFirstTimePeriod.setEndDate(LocalDateTime.now().minusDays(200).toLocalDate());
        Report report = new Report("testReport", 45.0, LocalDateTime.now().minusDays(200),
                LocalDateTime.now().minusDays(195));

        // When
        boolean result = collab1TaskRegistry.isProjectCollaboratorTimePeriodBeforeReportDates(report,
                projCollabFirstTimePeriod);

        // Then
        assertTrue(result);
    }

    @Test
    public void testIsProjectCollaboratorTimePeriodAfterReportDatesTrue() {

        // Given
        TaskCollaboratorRegistry collab1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + collab1.getUser().getEmail());
        ProjectCollaboratorCostAndTimePeriod projCollabFirstTimePeriod = collab1.getCostAndTimePeriodList().get(0);
        projCollabFirstTimePeriod.setStartDate(LocalDateTime.now().minusDays(365).toLocalDate());
        projCollabFirstTimePeriod.setEndDate(LocalDateTime.now().minusDays(360).toLocalDate());
        Report report = new Report("testReport", 45.0, LocalDateTime.now().minusDays(500),
                LocalDateTime.now().minusDays(495));

        // When
        boolean result = collab1TaskRegistry.isProjectCollaboratorTimePeriodAfterReportDates(report,
                projCollabFirstTimePeriod);

        // Then
        assertTrue(result);
    }

    @Test
    public void testIsProjectCollaboratorTimePeriodCoincidingWithReportDatesTrue() {

        // Given
        TaskCollaboratorRegistry collab1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + collab1.getUser().getEmail());
        ProjectCollaboratorCostAndTimePeriod projCollabFirstTimePeriod = collab1.getCostAndTimePeriodList().get(0);
        projCollabFirstTimePeriod.setStartDate(LocalDateTime.now().minusDays(365).toLocalDate());
        projCollabFirstTimePeriod.setEndDate(LocalDateTime.now().minusDays(360).toLocalDate());
        Report report = new Report("testReport", 45.0, LocalDateTime.now().minusDays(365),
                LocalDateTime.now().minusDays(363));

        // When
        boolean result = collab1TaskRegistry.isProjectCollaboratorTimePeriodAfterReportDates(report,
                projCollabFirstTimePeriod);

        // Then
        assertFalse(result);
    }

    @Test
    public void testIsProjectCollaboratorTimePeriodAfterReportDatesFalse() {

        // Given
        TaskCollaboratorRegistry collab1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + collab1.getUser().getEmail());
        ProjectCollaboratorCostAndTimePeriod projCollabFirstTimePeriod = collab1.getCostAndTimePeriodList().get(0);
        projCollabFirstTimePeriod.setStartDate(LocalDateTime.now().minusDays(365).toLocalDate());
        projCollabFirstTimePeriod.setEndDate(LocalDateTime.now().minusDays(360).toLocalDate());
        Report report = new Report("testReport", 45.0, LocalDateTime.now().minusDays(200),
                LocalDateTime.now().minusDays(195));

        // When
        boolean result = collab1TaskRegistry.isProjectCollaboratorTimePeriodAfterReportDates(report,
                projCollabFirstTimePeriod);

        // Then
        assertFalse(result);
    }

    @Test
    public void testIsProjectCollaboratorTimePeriodAfterReportDatesFalseEqualDates() {

        // Given
        TaskCollaboratorRegistry collab1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + collab1.getUser().getEmail());
        ProjectCollaboratorCostAndTimePeriod projCollabFirstTimePeriod = collab1.getCostAndTimePeriodList().get(0);
        projCollabFirstTimePeriod.setStartDate(LocalDateTime.now().minusDays(365).toLocalDate());
        projCollabFirstTimePeriod.setEndDate(LocalDateTime.now().minusDays(360).toLocalDate());
        Report report = new Report("testReport", 45.0, LocalDateTime.now().minusDays(375),
                LocalDateTime.now().minusDays(365));

        // When
        boolean result = collab1TaskRegistry.isProjectCollaboratorTimePeriodAfterReportDates(report,
                projCollabFirstTimePeriod);

        // Then
        assertFalse(result);
    }

    @Test
    public void testIsProjectCollaboratorTimePeriodCoincidingWithReportDatesFailureTimePeriodEndDateNull() {

        // Given
        TaskCollaboratorRegistry collab1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + collab1.getUser().getEmail());
        ProjectCollaboratorCostAndTimePeriod projCollabFirstTimePeriod = collab1.getCostAndTimePeriodList().get(0);
        projCollabFirstTimePeriod.setStartDate(LocalDateTime.now().minusDays(365).toLocalDate());
        projCollabFirstTimePeriod.setEndDate(null);
        Report report = new Report("testReport", 45.0, LocalDateTime.now().minusDays(375),
                LocalDateTime.now().minusDays(367));

        // When
        boolean result = collab1TaskRegistry.isProjectCollaboratorTimePeriodCoincidingWithReportDates(report,
                projCollabFirstTimePeriod);

        // Then
        assertFalse(result);
    }

    @Test
    public void testIsProjectCollaboratorTimePeriodCoincidingWithReportDatesFailureTimePeriodEndDateNotNullAndStartBefore() {

        // Given
        TaskCollaboratorRegistry collab1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + collab1.getUser().getEmail());
        ProjectCollaboratorCostAndTimePeriod projCollabFirstTimePeriod = collab1.getCostAndTimePeriodList().get(0);
        projCollabFirstTimePeriod.setStartDate(LocalDateTime.now().minusDays(365).toLocalDate());
        projCollabFirstTimePeriod.setEndDate(LocalDateTime.now().minusDays(360).toLocalDate());
        Report report = new Report("testReport", 45.0, LocalDateTime.now().minusDays(375),
                LocalDateTime.now().minusDays(367));

        // When
        boolean result = collab1TaskRegistry.isProjectCollaboratorTimePeriodCoincidingWithReportDates(report,
                projCollabFirstTimePeriod);

        // Then
        assertFalse(result);
    }

    @Test
    public void testIsProjectCollaboratorTimePeriodCoincidingWithReportDatesTrueTimePeriodEndDateNotNullStartBeforeAndEndAfterReportDates() {

        // Given
        TaskCollaboratorRegistry collab1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + collab1.getUser().getEmail());
        ProjectCollaboratorCostAndTimePeriod projCollabFirstTimePeriod = collab1.getCostAndTimePeriodList().get(0);
        projCollabFirstTimePeriod.setStartDate(LocalDateTime.now().minusDays(400).toLocalDate());
        projCollabFirstTimePeriod.setEndDate(LocalDateTime.now().minusDays(300).toLocalDate());
        Report report = new Report("testReport", 45.0, LocalDateTime.now().minusDays(370),
                LocalDateTime.now().minusDays(367));

        // When
        boolean result = collab1TaskRegistry.isProjectCollaboratorTimePeriodCoincidingWithReportDates(report,
                projCollabFirstTimePeriod);

        // Then
        assertTrue(result);
    }

    @Test
    public void testIsProjectCollaboratorTimePeriodCoincidingWithReportDatesTrueTimePeriodEndDateNotNullStartBeforeAndEndDateWithinReportDates() {

        // Given
        TaskCollaboratorRegistry collab1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + collab1.getUser().getEmail());
        ProjectCollaboratorCostAndTimePeriod projCollabFirstTimePeriod = collab1.getCostAndTimePeriodList().get(0);
        projCollabFirstTimePeriod.setStartDate(LocalDateTime.now().minusDays(400).toLocalDate());
        projCollabFirstTimePeriod.setEndDate(LocalDateTime.now().minusDays(368).toLocalDate());
        Report report = new Report("testReport", 45.0, LocalDateTime.now().minusDays(370),
                LocalDateTime.now().minusDays(367));

        // When
        boolean result = collab1TaskRegistry.isProjectCollaboratorTimePeriodCoincidingWithReportDates(report,
                projCollabFirstTimePeriod);

        // Then
        assertTrue(result);
    }

    @Test
    public void testIsProjectCollaboratorTimePeriodCoincidingWithReportDatesTimePeriodNotNullAndWithinReportDates() {

        // Given
        TaskCollaboratorRegistry collab1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + collab1.getUser().getEmail());
        ProjectCollaboratorCostAndTimePeriod projCollabFirstTimePeriod = collab1.getCostAndTimePeriodList().get(0);
        projCollabFirstTimePeriod.setStartDate(LocalDateTime.now().minusDays(365).toLocalDate());
        projCollabFirstTimePeriod.setEndDate(LocalDateTime.now().minusDays(360).toLocalDate());
        Report report = new Report("testReport", 45.0, LocalDateTime.now().minusDays(375),
                LocalDateTime.now().minusDays(350));

        // When
        boolean result = collab1TaskRegistry.isProjectCollaboratorTimePeriodCoincidingWithReportDates(report,
                projCollabFirstTimePeriod);

        // Then
        assertTrue(result);
    }

    @Test
    public void testIsProjectCollaboratorTimePeriodCoincidingWithReportDatesTimePeriodNotNullAndOutsideReportDates() {

        // Given
        TaskCollaboratorRegistry collab1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + collab1.getUser().getEmail());
        ProjectCollaboratorCostAndTimePeriod projCollabFirstTimePeriod = collab1.getCostAndTimePeriodList().get(0);
        projCollabFirstTimePeriod.setStartDate(LocalDateTime.now().minusDays(365).toLocalDate());
        projCollabFirstTimePeriod.setEndDate(LocalDateTime.now().minusDays(300).toLocalDate());
        Report report = new Report("testReport", 45.0, LocalDateTime.now().minusDays(375),
                LocalDateTime.now().minusDays(350));

        // When
        boolean result = collab1TaskRegistry.isProjectCollaboratorTimePeriodCoincidingWithReportDates(report,
                projCollabFirstTimePeriod);

        // Then
        assertTrue(result);
    }

    @Test
    public void testOrderCostAndTimePeriodList() {

        // Given
        ProjectCollaboratorCostAndTimePeriod period1 = new ProjectCollaboratorCostAndTimePeriod(10);
        ProjectCollaboratorCostAndTimePeriod period2 = new ProjectCollaboratorCostAndTimePeriod(15);
        period1.setStartDate(LocalDate.now());
        period2.setStartDate(LocalDate.now().minusDays(5));

        List<ProjectCollaboratorCostAndTimePeriod> expected = new ArrayList<>();
        expected.add(period2);
        expected.add(period1);

        collab1.getCostAndTimePeriodList().clear();
        collab1.getCostAndTimePeriodList().add(period1);
        collab1.getCostAndTimePeriodList().add(period2);

        // When
        List<ProjectCollaboratorCostAndTimePeriod> result = TaskCollaboratorRegistry
                .orderCostAndTimePeriodList(collab1);

        // Then
        assertEquals(expected, result);
    }

    @Test
    public void testIsTimePeriodDateEqualOrBeforeReportDateSucess() {

        // Given
        LocalDate date1 = LocalDate.now();
        LocalDate date2 = LocalDate.now().plusDays(5);

        // When
        boolean result = TaskCollaboratorRegistry.isTimePeriodDateEqualOrBeforeReportDate(date1, date2);

        // Then
        assertTrue(result);
    }

    @Test
    public void testIsTimePeriodDateEqualOrBeforeReportDateSuccessEqualDates() {

        // Given
        LocalDate date1 = LocalDate.now();
        LocalDate date2 = LocalDate.now();

        // When
        boolean result = TaskCollaboratorRegistry.isTimePeriodDateEqualOrBeforeReportDate(date1, date2);

        // Then
        assertTrue(result);
    }

    @Test
    public void testIsTimePeriodDateEqualOrBeforeReportDatesFailure() {

        LocalDate date1 = LocalDate.now();
        LocalDate date2 = LocalDate.now().minusDays(1);

        // When
        boolean result = TaskCollaboratorRegistry.isTimePeriodDateEqualOrBeforeReportDate(date1, date2);

        // Then
        assertFalse(result);
    }

    @Test
    public void testIsTimePeriodDateEqualOrAfterReportDateSuccess() {

        // Given
        LocalDate date1 = LocalDate.now();
        LocalDate date2 = LocalDate.now().minusDays(1);

        // When
        boolean result = TaskCollaboratorRegistry.isTimePeriodDateEqualOrAfterReportDate(date1, date2);

        // Then
        assertTrue(result);
    }

    @Test
    public void testIsTimePeriodDateEqualOrAfterReportDateSuccessEqualDates() {

        // Given
        LocalDate date1 = LocalDate.now();
        LocalDate date2 = LocalDate.now();

        // When
        boolean result = TaskCollaboratorRegistry.isTimePeriodDateEqualOrAfterReportDate(date1, date2);

        // Then
        assertTrue(result);
    }

    @Test
    public void testIsTimePeriodDateEqualOrAfterReportDateFailure() {

        // Given
        LocalDate date1 = LocalDate.now();
        LocalDate date2 = LocalDate.now().plusDays(1);

        // When
        boolean result = TaskCollaboratorRegistry.isTimePeriodDateEqualOrAfterReportDate(date1, date2);

        // Then
        assertFalse(result);
    }

    @Test
    public void testIsWithinRangeSuccess() {
        // Given
        LocalDate dateToBeCompared = LocalDate.now();
        LocalDate earlierDate = LocalDate.now().minusDays(5);
        LocalDate laterDate = LocalDate.now().plusDays(5);

        // When
        boolean result = TaskCollaboratorRegistry.isWithinRange(dateToBeCompared, earlierDate, laterDate);

        // Then
        assertTrue(result);
    }

    @Test
    public void testIsWithinRangeSuccessBoundaryEarlierDateEqual() {
        // Given
        LocalDate dateToBeCompared = LocalDate.now();
        LocalDate earlierDate = LocalDate.now();
        LocalDate laterDate = LocalDate.now().plusDays(5);

        // When
        boolean result = TaskCollaboratorRegistry.isWithinRange(dateToBeCompared, earlierDate, laterDate);

        // Then
        assertTrue(result);
    }

    @Test
    public void testIsWithinRangeSuccessBoundaryLaterDateEqual() {
        // Given
        LocalDate dateToBeCompared = LocalDate.now();
        LocalDate earlierDate = LocalDate.now().minusDays(5);
        LocalDate laterDate = LocalDate.now();

        // When
        boolean result = TaskCollaboratorRegistry.isWithinRange(dateToBeCompared, earlierDate, laterDate);

        // Then
        assertTrue(result);
    }

    @Test
    public void testIsWithinRangeFailureOutOfRangeLater() {
        // Given
        LocalDate dateToBeCompared = LocalDate.now().plusDays(8);
        LocalDate earlierDate = LocalDate.now().minusDays(5);
        LocalDate laterDate = LocalDate.now();

        // When
        boolean result = TaskCollaboratorRegistry.isWithinRange(dateToBeCompared, earlierDate, laterDate);

        // Then
        assertFalse(result);
    }

    @Test
    public void testIsWithinRangeFailureOutOfRangeEarly() {
        // Given
        LocalDate dateToBeCompared = LocalDate.now().minusDays(8);
        LocalDate earlierDate = LocalDate.now().minusDays(5);
        LocalDate laterDate = LocalDate.now();

        // When
        boolean result = TaskCollaboratorRegistry.isWithinRange(dateToBeCompared, earlierDate, laterDate);

        // Then
        assertFalse(result);
    }

    @Test
    public void testIsWithinRangeExcludingBoundariesSuccess() {
        // Given
        LocalDate dateToBeCompared = LocalDate.now();
        LocalDate earlierDate = LocalDate.now().minusDays(5);
        LocalDate laterDate = LocalDate.now().plusDays(5);

        // When
        boolean result = TaskCollaboratorRegistry.isWhithinRangeExcludingBoundaryDates(dateToBeCompared, earlierDate,
                laterDate);

        // Then
        assertTrue(result);
    }

    @Test
    public void testIsWithinRangeExcludingBoundariesFailureBoundaryEarlierDateEqual() {
        // Given
        LocalDate dateToBeCompared = LocalDate.now();
        LocalDate earlierDate = LocalDate.now();
        LocalDate laterDate = LocalDate.now().plusDays(5);

        // When
        boolean result = TaskCollaboratorRegistry.isWhithinRangeExcludingBoundaryDates(dateToBeCompared, earlierDate,
                laterDate);

        // Then
        assertFalse(result);
    }

    @Test
    public void testIsWithinRangeExcludingBoundariesFailureBoundaryLaterDateEqual() {
        // Given
        LocalDate dateToBeCompared = LocalDate.now();
        LocalDate earlierDate = LocalDate.now().minusDays(5);
        LocalDate laterDate = LocalDate.now();

        // When
        boolean result = TaskCollaboratorRegistry.isWhithinRangeExcludingBoundaryDates(dateToBeCompared, earlierDate,
                laterDate);

        // Then
        assertFalse(result);
    }

    @Test
    public void testIsWithinRangeExcludingBoundariesFailure() {
        // Given
        LocalDate dateToBeCompared = LocalDate.now().plusDays(8);
        LocalDate earlierDate = LocalDate.now().minusDays(5);
        LocalDate laterDate = LocalDate.now();

        // When
        boolean result = TaskCollaboratorRegistry.isWhithinRangeExcludingBoundaryDates(dateToBeCompared, earlierDate,
                laterDate);

        // Then
        assertFalse(result);
    }

    // TOSOLVE: remove this method because reports should be made by
    // TaskCollaboratorRegistry
    // @Test
    // @Transactional
    // public void testAddReportFailureCollaboratorNotAssignedToThisTask() {
    //
    // String expected = "This collaborator is not assigned to this task.";
    // String result = collab1.addReport(t1, 6, d1);
    //
    // assertEquals(expected, result);
    // }
    //
    // @Test
    // @Transactional
    // public void testAddReportSuccess() {
    // t1.assignProjectManagerAndAddToProjectCollaboratorList(collab1);
    //
    // String expected = "Report successfully added";
    // String result = collab1.addReport(t1, 6, d1);
    //
    // assertEquals(expected, result);
    // }
    //
    // @Test
    // @Transactional
    // public void testAddReportFailureNegativeHours() {
    //
    // t1.assignProjectManagerAndAddToProjectCollaboratorList(collab1);
    //
    // String expected = "Invalid number of hours. Please try again.";
    // String result = collab1.addReport(t1, -6, d1);
    //
    // assertEquals(expected, result);
    // }

    @Test
    public void addReportRestTest() {
        // Given
        t1.setTaskId("1-1");
        TaskCollaboratorRegistry tcr = new TaskCollaboratorRegistry(collab1, t1.getId());

        // When
        boolean result = tcr.addReportRest(100, d1, d1);

        // Then
        assertEquals(user1.getEmail(), tcr.getReportList().get(0).getUserEmail());
        assertTrue(result);
    }

    @Test
    public void addReportRestTest_InvalisEffort() {
        // Given
        t1.setTaskId("1-1");
        TaskCollaboratorRegistry tcr = new TaskCollaboratorRegistry(collab1, t1.getId());

        // When
        boolean result = tcr.addReportRest(-100, d1, d1);

        // The"
        assertFalse(result);
    }

    @Test
    public void testAddReportValidQuantityRest() {
        // Given
        // When
        List<Report> expected = new ArrayList<>();
        Report rep1 = new Report("1(1)", 5, d1, d1.plusDays(5));
        Report rep2 = new Report("1(2)", 8, d1, d1.plusDays(5));
        expected.add(rep1);
        expected.add(rep2);
        // Then
        List<Report> result = r1.getReportList();
        assertEquals(expected, result);
        // When
        Report rep3 = new Report("1(3)", 8, d1, d1.plusDays(5));
        expected.add(rep3);
        assertEquals(r1.addReportRest(8, d1, d1.plusDays(5)), true);
        // Then
        result = r1.getReportList();
        assertEquals(expected, result);
    }

    @Test
    public void testAddReportValidStateRemovalRequesRest() {
        // Given
        // When
        List<Report> expected = new ArrayList<>();
        Report rep1 = new Report("1(1)", 5, d1, d1.plusDays(5));
        Report rep2 = new Report("1(2)", 8, d1, d1.plusDays(5));
        expected.add(rep1);
        expected.add(rep2);
        // Then
        List<Report> result = r1.getReportList();
        assertEquals(expected, result);
        // When
        r1.setRegistryStatus(RegistryStatus.REMOVALREQUEST);
        Report rep3 = new Report("1(3)", 8, d1, d1.plusDays(5));
        expected.add(rep3);
        assertEquals(r1.addReportRest(8, d1, d1.plusDays(5)), true);
        // Then
        result = r1.getReportList();
        assertEquals(expected, result);
    }

    @Test
    public void testAddReportInvalidQuantityRest() {
        // Given
        // When
        List<Report> expected = new ArrayList<>();
        Report rep1 = new Report("1(1)", 5, d1, d1.plusDays(5));
        Report rep2 = new Report("1(2)", 8, d1, d1.plusDays(5));
        expected.add(rep1);
        expected.add(rep2);
        assertEquals(r1.addReportRest(0, d2, d2.plusDays(5)), false);
        // Then
        List<Report> result = r1.getReportList();
        assertEquals(expected, result);
    }

    @Test
    public void testAddReportSequenceNumber() {
        // Given
        // When

        assertEquals(r1.addReportRest(8, d1, d1.plusDays(5)), true);
        assertEquals(r1.addReportRest(8, d1, d1.plusDays(5)), true);
        assertEquals(r1.addReportRest(8, d1, d1.plusDays(5)), true);
        assertEquals(r1.addReportRest(8, d1, d1.plusDays(5)), true);
        // Then

        int index1;
        int index2;
        String code1 = r1.getReportList().get(1).getCode();
        index1 = code1.indexOf('(');
        index2 = code1.indexOf(')');
        int idCode1 = Integer.parseInt(code1.substring(index1 + 1, index2));

        String code2 = r1.getReportList().get(2).getCode();
        index1 = code2.indexOf('(');
        index2 = code2.indexOf(')');
        int idCode2 = Integer.parseInt(code2.substring(index1 + 1, index2));

        String code3 = r1.getReportList().get(3).getCode();
        index1 = code3.indexOf('(');
        index2 = code3.indexOf(')');
        int idCode3 = Integer.parseInt(code3.substring(index1 + 1, index2));

        assertEquals(1, idCode2 - idCode1);
        assertEquals(1, idCode3 - idCode2);
    }
    
    @Test
    public void testToDTO() {
    
    	// Given
        collab1.setId(2);
        r1.setCollaboratorRemovedFromTaskDate(d1.plusDays(2));
    	TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO= r1.toDTO();

        // When
        String result = taskCollaboratorRegistryRESTDTO.toString();

        // Then
        String expected = "TaskCollaboratorRegistryRESTDTO [taskId=" + t1.getId() + ", projectCollaboratorId=" + collab1.getId()
				+ ", projectCollaboratorName=" + collab1.getUser().getName() + ", projectCollaboratorEmail="
				+ collab1.getUser().getEmail() + ", collaboratorAddedToTaskDate=" + d1.minusYears(20)
				+ ", collaboratorRemovedFromTaskDate=" + d1.plusDays(2) + ", registryStatus="
				+ "ASSIGNMENTAPPROVED"  + "]";
        
        assertEquals(expected, result);
        

    }

    @Test
    public void testCompareTo() {
        assertTrue(r2.getReportList().isEmpty());
        assertTrue(r3.getReportList().isEmpty());
        assertEquals(0, r3.compareTo(r2));
        assertTrue(r3.getReportList().isEmpty());
        assertEquals(-1, r3.compareTo(r1));
        assertTrue(r2.getReportList().isEmpty());
        assertEquals(1 ,r1.compareTo(r2));
        assertEquals(0 ,r1.compareTo(r1));
        r4 = t3.listTaskCollaboratorRegistry(collab3).get(0);
        r4.setCollaboratorAddedToTaskDate(d1.minusYears(20));
        r4.addReport(4, d1, d1);
        r4.getReportList().get(0).setReportCreationDate(d1);
        assertEquals(1, r1.compareTo(r4));
    }

    @Test
    public void testEquals() {
        assertTrue(r1.equals(r1));
        assertFalse(r1.equals(r2));
        assertFalse(r1.equals(user1));
    }

    @Test
    public void testHashCode() {
        assertEquals(r1.hashCode(), r1.hashCode());
        assertNotEquals(r1.hashCode(), r2.hashCode());
    }

    @Test
    public void testGetLastReport() {
        List<Report> reportList = new ArrayList<>();
        reportList.add(report1);
        reportList.add(report2);
        assertEquals(reportList, r1.getReportList());
        assertEquals(report2, r1.getLastReport());
    }

    @Test
    public void testGetLastReportEmptyList() {
        assertNull(r3.getLastReport());
    }

}
