package project.model.unittests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;

import project.dto.rest.ReportRestDTO;
import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Report;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.user.Address;
import project.model.user.User;


public class ReportTest {

  
    // Reports
    Report rep1;
    Report rep2;
    Report rep3;
    Report rep4;
    // Tasks
    Task t1;
    // Users
    User user1;
    User user2;
    // Project Collaborators
    ProjectCollaborator colab1;
    ProjectCollaborator colab2;
    // Dates
    LocalDate d1;
    LocalDateTime dt1;
    LocalDateTime dt2;
    LocalDateTime dt3;
    LocalDateTime dt4;
    String reportDate;
    // Addresses
    Address address;
    // Registry
    TaskCollaboratorRegistry taskCollReg1;
    private Project project;
    int reportCount;

    @Before
    public void initialize() throws AddressException {

        project= new Project("1", "SWitCH");
        
        d1 = LocalDate.of(1990, 1, 1);
        dt1 = LocalDateTime.of(2017, 1, 1, 1, 1, 0);
        dt2 = LocalDateTime.of(2014, 5, 8, 1, 1, 0);
        dt3 = LocalDateTime.of(2017, 12, 13, 1, 1, 0);
        dt4 = null;

        user1 = new User("Diogo", "917813798", "diogo@gmail.com", "wgwqg", d1, "1", "Rua dasminhas terras", "4100",
                "Porto", "Portugal");

        user2 = new User("Tiago", "917813798", "tiago@hotmail.com", "wgwqg", d1, "1", "Rua dasminhas terras", "4100",
                "Porto", "Portugal");
        

        colab1 = new ProjectCollaborator(project, user1, 3);
        colab2 = new ProjectCollaborator(project, user2, 3);
        t1 = new Task(project, "Teste");

        t1.addProjectCollaborator(colab1);
        TaskCollaboratorRegistry col1TaskRegistry = t1.getTaskCollaboratorRegistryByID(t1.getId() + "-" + colab1.getUser().getEmail());
        col1TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(20));
        t1.addProjectCollaborator(colab2);
        TaskCollaboratorRegistry col2TaskRegistry = t1.getTaskCollaboratorRegistryByID(t1.getId() + "-" + colab2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(20));
        t1.addReport(colab1, 2, dt1, dt1.plusDays(5));
        t1.addReport(colab1, 2, dt2, dt2.plusDays(5));
        t1.addReport(colab2, 2, dt3, dt3.plusDays(5));

        reportCount = TaskCollaboratorRegistry.getThisTasksReportCount();
        LocalDateTime reportDate = LocalDateTime.now().minusSeconds(20);

        rep1 = t1.findReportById(t1.getId() + "(" + (reportCount - 2) + ")");
        rep1.setReportDate(reportDate);
        rep2 = t1.findReportById(t1.getId() + "(" + (reportCount - 1) + ")");
        rep2.setReportDate(LocalDateTime.now().minusSeconds(10));
        rep3 = t1.findReportById(t1.getId() + "(" + (reportCount) + ")");
        rep3.setReportDate(LocalDateTime.now());
        rep4 = new Report("1(2)", 2, dt1, dt1.plusDays(6));
        rep4.setReportDate(reportDate);
    }

    @Test
    public void testGetAndSetStartDate() {
        // Given
        // When
        rep1.setStartDate(dt2);
        // Then
        assertEquals(dt2, rep1.getStartDate());
    }

    @Test
    public void testGetAndSetEndDate() {
        // Given
        // When
        rep1.setEndDate(dt2);
        // Then
        assertEquals(dt2, rep1.getEndDate());
    }

    @Test
    public void testReportDefaultConstructorThroughReflection() {
        // Given
        // When
        Report report = null;
        try {
            Constructor<Report> constructor = Report.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            report = constructor.newInstance();
        } catch (Exception e) {
        }
        // Then
        assertTrue(report instanceof Report);
    }

    @Test
    public void testGetHour() {
        // Given
        // When
        rep1.setQuantity(5);
        // Then
        assertEquals(5.0, rep1.getQuantity(), 0.1);
    }

    @Test
    public void testReportDate() {
        // Given
        // When
        rep1.setReportDate(dt2);
        // Then
        assertEquals(dt2, rep1.getReportCreationDate());
    }

    @Test
    public void testEqualsTrue() {
        // Given
        // Then
        assertTrue(rep1.equals(rep1));
    }

    @Test
    public void testEqualsNotEqual() {
        // Given
        // Then
        assertNotEquals(rep1, rep3);
    }

    @Test
    public void testEqualsFalse() {
        // Given
        // Then
        assertFalse(rep1.equals(rep3));
    }

    @Test
    public void testEqualsNull() {
        // Given
        // When
        rep1 = null;
        // Then
        assertFalse(rep2.equals(rep1));

    }

    @Test
    public void testEqualsNullReportDate() {
        // Given
        // When
        rep1.setReportDate(null);
        rep2.setReportDate(null);
        // Then
        assertTrue(rep2.equals(rep1));

    }

    @Test
    public void testEqualsFail() {
        // Given
        // When
        rep1.setReportDate(dt1);
        rep2.setReportDate(dt2);
        // Then
        assertFalse(rep2.equals(rep1));

    }

    @Test
    public void testFalseDifferentClass() {
        // Given
        // Then
        assertFalse(rep1.equals(colab2));
    }

    @Test
    public void testFalseDifferentReportDate() {
        // Given
        // When
        rep1.setReportDate(dt4);
        rep2.setReportDate(dt1);
        // Then
        assertFalse(rep1.equals(rep2));
    }

    @Test
    public void testToString() {
        // Given
        String rep = "\nReport id = " + rep1.getCode() + "\nSpended Time=" + rep1.getQuantity() + "\nreportDate="
                + rep1.getReportCreationDate() + "\nstartDate=" + rep1.getStartDate() + "\n";
        // Then
        assertEquals(rep, rep1.toString());
    }

    @Test
    public void testHashCode() {
        // Given
        // Then
        assertEquals(rep1.hashCode(), rep4.hashCode());
        // When
        rep2.setReportDate(dt3);
        // Then
        assertNotEquals(rep1.hashCode(), rep2.hashCode());
    }

    @Test
    public void testtoDTO() {
        ReportRestDTO reportRestDTO = rep1.toDTO();
        ReportRestDTO expected = new ReportRestDTO();
        assertEquals(2.0 ,reportRestDTO.getEffort(), 0.01);
        assertEquals(dt1 ,reportRestDTO.getStartDate());
        assertEquals(dt1.plusDays(5) ,reportRestDTO.getEndDate());
        assertEquals(user1.getEmail() ,reportRestDTO.getUserEmail());
        assertEquals(t1.getId() + "(" + (reportCount - 2) + ")" ,reportRestDTO.getCode());
        assertEquals(LocalDate.now(),reportRestDTO.getReportCreationDate().toLocalDate());

        expected.setEffort(rep1.getQuantity());
        expected.setStartDate(rep1.getStartDate());
        expected.setEndDate(rep1.getEndDate());
        expected.setUserEmail(user1.getEmail());
        expected.setCode(rep1.getCode());
        expected.setReportCreationDate(rep1.getReportCreationDate());
        assertEquals(expected, reportRestDTO);
    }

    @Test
    public void compareTo() {
        assertTrue(rep3.getReportCreationDate().isAfter(rep2.getReportCreationDate()));
        assertEquals(1, rep3.compareTo(rep2));
        assertTrue(rep1.getReportCreationDate().isBefore(rep2.getReportCreationDate()));
        assertEquals(-1, rep1.compareTo(rep2));
        assertEquals(0, rep1.compareTo(rep1));

    }
}
