package project.model.unittests;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;

import project.dto.rest.ProjectDTO;
import project.model.project.LastTimePeriodCost;
import project.model.project.Project;
import project.model.project.Project.ProjectStatus;
import project.model.project.Project.ProjectUnits;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Report;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.task.taskstate.Cancelled;
import project.model.task.taskstate.TaskState;
import project.model.user.User;

public class ProjectTest {

    // Dates
    LocalDate birth;
    LocalDateTime d1;
    LocalDateTime d2;
    LocalDateTime d3;
    LocalDateTime d4;
    LocalDateTime d9;
    int y1 = LocalDateTime.now().getYear();
    Month m1 = LocalDateTime.now().getMonth();

    // Users
    User user1;
    User user2;
    User user3;
    User user4;
    User projectManager;

    // Project Collaborators
    ProjectCollaborator projManager;
    ProjectCollaborator collaborator1;
    ProjectCollaborator collaborator2;
    ProjectCollaborator collaborator3;
    ProjectCollaborator colab4;
    ProjectCollaborator collaborator5;

    // Projects
    Project project1;
    Project project2;
    Project project3;
    Project project4;
    Project project5;

    double globalBudget;

    // Tasks
    Task t1;
    Task t2;
    Task t3;
    Task t4;
    Task t5;
    Task t6;
    Task t7;
    Task t8;
    // Reports
    Report report1;
    Report report2;
    Report report3;
    Report report4;

    // Lists - ProjectCollaborator and Report
    List<ProjectCollaborator> expectedCollaboratorList;
    List<Report> reportList;

    List<String> calculatorOptions = new ArrayList<>();

    @Before
    public void initialize() throws AddressException {
        expectedCollaboratorList = new ArrayList<>();

        calculatorOptions.add("FirstTimeCostPeriod");

        d1 = LocalDateTime.of(2017, 12, 18, 0, 0);
        d9 = LocalDateTime.of(y1, m1.minus(1), 22, 0, 0);

        birth = LocalDate.of(1999, 11, 11);
        user1 = new User("Joaquim", "91739812379", "jj@jj.com", "132456765", birth, "2", "Rua ", "4433", "cidade",
                "pais");
        user2 = new User("Manel", "666 555 5556", "manel@hotmail.com", "5656546456", birth, "2", "Rua ", "4433",
                "cidade", "pais");
        user3 = new User("Mawsdl", "666 555 5556", "masssl@jj", "5656546456", birth, "2", "Rua ", "4433", "cidade",
                "pais");
        user4 = new User("Joana", "91739812379", "jj@jj.com", "132456765", birth, "2", "Rua ", "4433", "cidade",
                "pais");
        projectManager = new User("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "2", "Rua ", "4433",
                "cidade", "pais");

        project1 = new Project("100", "Project 1");
        project2 = new Project("200", "Project 2");
        project3 = new Project("300", "Project 3");
        project4 = new Project("400", "Project 4");
        project5 = new Project("500", "Project 5");

        // t1 = new Task (project1, "Tarefa1-p1", "task-1", d1, d2, 1.2, 1.3);
        t1 = new Task(project3, "Tarefa1-p3", "task-1", d1, d2, 1.2, 1.3);
        project3.addTask(t1);
        t2 = new Task(project3, "Test 2", "task-2", d1, d2, 1.2, 1.3);
        project3.addTask(t2);
        t3 = new Task(project3, "Test 3", "task-3", d1, d2, 1.2, 1.3);
        project3.addTask(t3);
        t4 = new Task(project3, "Test 4", "task-4", d1, d2, 1.2, 1.3);
        project3.addTask(t4);
        t5 = new Task(project3, "Test 5", "task-5", d1, d2, 1.2, 1.3);
        project3.addTask(t5);
        t6 = new Task(project3, "Test 6", "task-6", d1, d2, 1.2, 1.3);
        project3.addTask(t6);
        t7 = new Task(project1, "Created");

        project5.addTask(t7);
        project1.addTask(t8);

        projManager = new ProjectCollaborator(project3, projectManager, 0.0);
        project3.addProjectCollaborator(projManager);
        project3.setProjectManager(projManager);

        collaborator1 = new ProjectCollaborator(project3, user1, 3);
        project3.addProjectCollaborator(collaborator1);
        collaborator2 = new ProjectCollaborator(project3, user2, 3);
        project3.addProjectCollaborator(collaborator2);
        collaborator3 = new ProjectCollaborator(project3, user3, 3);
        project3.addProjectCollaborator(collaborator3);

        collaborator2.getCostAndTimePeriodList().get(0).setStartDate(d1.minusYears(3).toLocalDate());

        project5.setProjectManager(projManager);
        project5.addProjectCollaborator(projManager);

        t1.addProjectCollaborator(collaborator2);
        t2.addProjectCollaborator(collaborator2);
        t4.addProjectCollaborator(collaborator2);
        t5.addProjectCollaborator(collaborator2);

        reportList = new ArrayList<>();

        reportList.add(report1);

        TaskCollaboratorRegistry col1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + collaborator2.getUser().getEmail());
        col1TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));
        TaskCollaboratorRegistry col2TaskRegistry = t2
                .getTaskCollaboratorRegistryByID(t2.getId() + "-" + collaborator2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));
        TaskCollaboratorRegistry col3TaskRegistry = t4
                .getTaskCollaboratorRegistryByID(t4.getId() + "-" + collaborator2.getUser().getEmail());
        col3TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));
        TaskCollaboratorRegistry col4TaskRegistry = t5
                .getTaskCollaboratorRegistryByID(t5.getId() + "-" + collaborator2.getUser().getEmail());
        col4TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

        t1.addReport(collaborator2, 9, d1, d1.plusDays(5));
        t2.addReport(collaborator2, 9, d1, d1.plusDays(5));
        t4.addReport(collaborator2, 9, d1, d1.plusDays(5));
        t5.addReport(collaborator2, 9, d1, d1.plusDays(5));


    }

    @Test
    public void testConstructorWithListOfCostCalculation() {
        Project proj12 = new Project("1234", "switch", calculatorOptions);

        assertEquals(proj12.getCostCalculationOptions(), calculatorOptions);
        assertTrue(proj12 instanceof Project);

    }

    @Test
    public void testSetProjectManagerSuccess() {
        // Given
        // When
        project1.addProjectCollaborator(projManager);
        boolean result = project1.setProjectManager(projManager);
        // pcl.assignProjectManagerAndAddToProjectCollaboratorList(project1, projectManager);
        // Then
        assertTrue(result);
        assertEquals(project1.findProjectCollaborator(projectManager), project1.getProjectManager());
    }

    @Test
    public void testSetProjectManagerFail() {
        // Given
        // When
        projectManager.setInactive();
        // Then
        assertFalse(project1.setProjectManager(projManager));
    }

    @Test
    public void testEqualsTrue1() {
        // Given
        // Then
        assertTrue(project1.equals(project1));

    }

    @Test
    public void testEqualsTrue2() {
        // Given
        // When
        Project project2 = new Project("A255", "Teste");
        Project project3 = new Project("A255", "Teste ");
        // Then
        assertTrue(project2.equals(project3));
    }

    @Test
    public void testEqualsTrue3() {
        // Given
        // When
        Project projectnull3 = new Project("", "Project 3");
        Project projectnull4 = new Project("", "Project 4");
        // Then
        assertTrue(projectnull3.equals(projectnull4));
    }

    @Test
    public void testEqualsFalse1() {
        // Given
        // Then
        assertFalse(project1.equals(""));
    }

    @Test
    public void testEqualsFalse2() {
        // Given
        // When
        Project project = null;
        // Then
        assertFalse(project1.equals(project));
    }


    @Test
    public void testEqualsFalse4() {
        // Given
        // When
        Project projectId = new Project("ID", "Project ID");
        // Then
        assertFalse(project1.equals(projectId));
    }


    @Test
    public void testGetStatusEquals() {
        // Given
        // When
        project1.setStatus(ProjectStatus.PLANNED);
        // Then
        assertEquals(ProjectStatus.PLANNED, project1.getStatus());
        // When
        project1.setStatus(ProjectStatus.PLANNED);
        // Then
        assertEquals(ProjectStatus.PLANNED, project1.getStatus());
        // When
        project1.setStatus(ProjectStatus.CLOSED);
        // Then
        assertEquals(ProjectStatus.CLOSED, project1.getStatus());
        // When
        project1.setStatus(ProjectStatus.DELIVERY);
        // Then
        assertEquals(ProjectStatus.DELIVERY, project1.getStatus());
        // When
        project1.setStatus(ProjectStatus.EXECUTION);
        // Then
        assertEquals(ProjectStatus.EXECUTION, project1.getStatus());
        // When
        project1.setStatus(ProjectStatus.START_UP);
        // Then
        assertEquals(ProjectStatus.START_UP, project1.getStatus());
    }

    @Test
    public void testGetProjectUnit() {
        // Given
        // When
        project1.setUnit(ProjectUnits.HOURS);
        // Then
        assertEquals(ProjectUnits.HOURS, project1.getUnit());
        // When
        project1.setUnit(ProjectUnits.PEOPLE_MONTH);
        // Then
        assertEquals(ProjectUnits.PEOPLE_MONTH, project1.getUnit());
    }

    @Test
    public void testGetProjectCollaboratorSuccess() {
        // Given
        // Then
        assertEquals(project3.findProjectCollaborator(user1).getUser(), user1);
    }

    @Test
    public void testGetProjectCollaboratorNullValue() {
        // Given
        // Then
        assertNull(project2.findProjectCollaborator(user1));
    }

    @Test
    public void testSetGlobalBudget() {
        // Given
        // When
        project1.setGlobalBudget(globalBudget);
        // Then
        assertEquals(globalBudget, project1.getGlobalBudget(), 0.01);
    }

    @Test
    public void testSetCostCalculator() {
        // Given
        // When
        LastTimePeriodCost lastTimePeriodCost = new LastTimePeriodCost();
        project1.setCostCalculator(lastTimePeriodCost);
        // Then
        assertEquals(lastTimePeriodCost.getClass(), project1.getCostCalculator().getClass());
    }

    @Test
    public void testSetCostCalculatorPersistence() {
        // Given
        // When
        project1.setCostCalculatorPersistence("FirstTimePeriodCost");
        // Then
        assertEquals("FirstTimePeriodCost", project1.getCostCalculatorPersistence());
    }

    @Test
    public void testIsActiveTrue() {
        // Given
        // When
        project1.setActive();
        // Then
        assertTrue(project1.isActive());
    }

    @Test
    public void testIsActiveFalse() {
        // Given
        // When
        project1.setInactive();
        // Then
        assertFalse(project1.isActive());
    }

    @Test
    public void testToStringEquals() {
        // Given
        String expected = "\n" + "Project id=300\n" + "name=Project 3\n" + "description=<no project description>\n"
                + "projectManager=\n" + "Project ROLE_COLLABORATOR Name=Asdrubal, Email=asdrubal@jj.com, cost=0.0\n\n"
                + "status=START_UP\n" + "unit=HOURS\n" + "globalBudget=0.0\n" + "isActive=true\n" + "startDate=null\n"
                + "finalDate=null\n"
                + "projectCollaboratorList=Asdrubal (asdrubal@jj.com); Joaquim (jj@jj.com); Manel (manel@hotmail.com); Mawsdl (masssl@jj); \n"
                + "tasksList=" + t1.getId() + "-Tarefa1-p3 InProgress; " + t2.getId() + "-Test 2 InProgress; "
                + t3.getId() + "-Test 3 Planned; " + t4.getId() + "-Test 4 InProgress; " + t5.getId()
                + "-Test 5 InProgress; " + t6.getId() + "-Test 6 Planned; ";
        // Then
        assertEquals(expected, project3.toString());
    }

    @Test
    public void testToStringNotEquals() {
        // Given
        // Then
        assertNotEquals(project2.toString(), project3.toString());
    }

    @Test
    public void testSetProjectManagerFalse() {
        // Given
        // When
        user3 = null;
        ProjectCollaborator projMana = new ProjectCollaborator(project1, user3, 0.0);
        project1.addProjectCollaborator(projManager);
        // Then
        assertFalse(project1.setProjectManager(projMana));
    }

    @Test
    public void testAddCollaboratorTrue() {
        // Given
        // When
        expectedCollaboratorList.add(projManager);
        expectedCollaboratorList.add(collaborator1);
        expectedCollaboratorList.add(collaborator2);
        expectedCollaboratorList.add(collaborator3);
        // Then
        assertThat(expectedCollaboratorList, containsInAnyOrder(project3.getProjectCollaboratorList().toArray()));
    }

    @Test
    public void testAddCollaboratorFalseProjectCollaboratorAlreadyExist() {
        // Given
        // When
        ProjectCollaborator col = new ProjectCollaborator(project1, user4, 2);
        // Then
        assertTrue(project1.getProjectCollaboratorList().isEmpty());
        project1.addProjectCollaborator(col);
        assertEquals(1, project1.getProjectCollaboratorList().size());
        project1.addProjectCollaborator(col);
        assertEquals(1, project1.getProjectCollaboratorList().size());
    }

    @Test
    public void testRemoveCollaboratorTrue() {
        // Given
        // When
        expectedCollaboratorList.add(collaborator1);
        expectedCollaboratorList.add(collaborator2);
        expectedCollaboratorList.add(projManager);
        // Then
        project3.removeProjectCollaborator(collaborator3);

        assertThat(project3.listActiveProjectCollaborators(), containsInAnyOrder(expectedCollaboratorList.toArray()));
    }

    @Test
    public void testRemoveCollaboratorTrue2() {
        // Given
        // When
        expectedCollaboratorList.add(collaborator1);
        expectedCollaboratorList.add(collaborator2);
        expectedCollaboratorList.add(projManager);
        project3.removeProjectCollaborator(collaborator3);
        project3.removeProjectCollaborator(collaborator3);
        // Then
        assertThat(project3.listActiveProjectCollaborators(), containsInAnyOrder(expectedCollaboratorList.toArray()));
    }

    @Test
    public void testRemoveCollaboratorFalse() {
        // Given
        // When

        expectedCollaboratorList.add(projManager);
        project5.removeProjectCollaborator(collaborator3);
        // Then
        assertThat(project5.listActiveProjectCollaborators(), containsInAnyOrder(expectedCollaboratorList.toArray()));
    }

    @Test
    public void testRemoveTaskInCreatedStateSucess() {
        // Given
        // When
        List<Task> expected = new ArrayList<>();
        // Then
        assertTrue(t7.getTaskState().isOnCreatedState());
        // When
        project1.removeTask(t7);
        // Then
        assertEquals(expected, project4.getTasksList());
    }

    @Test
    public void testRemoveTaskInitializatedStateInProgress() {
        // Given
        assertTrue(t1.getStatus().isOnInProgressState());
        // When
        List<Task> expected = new ArrayList<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t4);
        expected.add(t5);
        expected.add(t3);
        expected.add(t6);
        project3.removeTask(t1);
        List<Task> result = project3.getTasksList();
        // Then
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testRemoveTaskInitializatedStateCancelled() {
        // Given
        // When
        TaskState state = new Cancelled(t1);
        t1.setTaskState(state);
        List<Task> expected = new ArrayList<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t4);
        expected.add(t5);
        expected.add(t3);
        expected.add(t6);
        project3.removeTask(t1);
        List<Task> result = project3.getTasksList();
        // Then
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testRemoveTaskInitializatedStateCompleted() {
        // Given
        // When
        t1.setTaskCompleted();
        List<Task> expected = new ArrayList<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t4);
        expected.add(t5);
        expected.add(t3);
        expected.add(t6);
        project3.removeTask(t1);
        List<Task> result = project3.getTasksList();
        // Then
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testRemoveTaskInitializatedStateSuspended() {
        // Given
        // When
        t1.setPredictedDateOfStart(d1);
        t1.removeProjectCollaborator(collaborator2);
        List<Task> expected = new ArrayList<>();
        expected.add(t1);
        expected.add(t3);
        expected.add(t2);
        expected.add(t4);
        expected.add(t5);
        expected.add(t6);
        project3.removeTask(t1);
        List<Task> result = project3.getTasksList();
        // Then
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testRemoveTaskUninitializatedStatePlanned() {
        // Given
        // When
        List<Task> expected = new ArrayList<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t4);
        expected.add(t5);
        expected.add(t6);
        project3.removeTask(t3);
        List<Task> result = project3.getTasksList();
        // Then
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testRemoveTaskUninitializatedStateAssigned() {
        // Given
        // When
        t3.addProjectCollaborator(collaborator2);
        t3.setTaskId("1-1");
        t3.addTaskDependency(t1);
        assertTrue(t3.getStatus().isOnAssignedState());
        List<Task> expected = new ArrayList<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t4);
        expected.add(t5);
        expected.add(t6);
        project3.removeTask(t3);
        List<Task> result = project3.getTasksList();
        // Then
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testRemoveTaskUninitializatedStateReady() {
        // Given
        // When
        t3.addProjectCollaborator(collaborator2);
        t3.setPredictedDateOfStart(d1);
        List<Task> expected = new ArrayList<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t4);
        expected.add(t5);
        expected.add(t6);
        project3.removeTask(t3);
        List<Task> result = project3.getTasksList();
        // Then
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testRemoveCollaboratorFromTasksConfirm() {
        // Given
        // When
        List<Task> result = project3.listCollaboratorTasks(collaborator2);
        List<Task> expected = new ArrayList<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t4);
        expected.add(t5);
        // Then
        assertEquals(expected, result);
        // When
        t1.removeProjectCollaborator(collaborator2);
        t2.removeProjectCollaborator(collaborator2);
        t4.removeProjectCollaborator(collaborator2);
        t5.removeProjectCollaborator(collaborator2);

        List<Task> result2 = project3.listCollaboratorTasks(collaborator2);
        List<Task> expected2 = new ArrayList<>();
        // Then
        assertThat(result2, containsInAnyOrder(expected2.toArray()));
    }

    @Test
    public void testUnassignedTaskList() {
        // Given
        Set<Task> expected = new LinkedHashSet<>();
        expected.add(t3);
        expected.add(t6);
        // Then
        assertEquals(expected, project3.listUnassignedTasks());
    }

    @Test
    public void testUnassignedTaskListEmptyList() {
        // Given
        // When
        t3.addProjectCollaborator(collaborator2);
        t3.addReport(collaborator2, 9, d1, d1.plusDays(5));
        t6.addProjectCollaborator(collaborator2);
        Set<Task> expected = new LinkedHashSet<>();
        // Then
        assertEquals(expected, project3.listUnassignedTasks());
    }

    @Test
    public void testGetNameEquals() {
        // Given
        // When
        project1.setName("Test");
        // Then
        assertEquals("Test", project1.getName());
    }

    @Test
    public void testGetIdEquals() {
        // Given
        // Then
        assertEquals("100", project1.getId());
    }

    @Test
    public void testGetDescriptionEquals() {
        // Given
        // When
        project1.setDescription("Test description");
        // Then
        assertEquals("Test description", project1.getDescription());
    }

    @Test
    public void testGetStartDateTrue() {
        // Given
        // When
        Project project = new Project("C300", "Test");
        LocalDateTime date = LocalDateTime.of(1999, 11, 11, 10, 50, 30);
        project.setStartDate(date);
        // Then
        assertTrue(date.toLocalDate().equals(project.getStartDate().toLocalDate()));
    }

    @Test
    public void testGetStartDateFalse() {
        // Given
        Project project = new Project("C300", "Test");
        LocalDateTime date = LocalDateTime.of(1999, 11, 11, 10, 50, 30);
        LocalDateTime date2 = LocalDateTime.of(1998, 11, 11, 10, 50, 30);
        // When
        project.setStartDate(date);
        // Then
        assertFalse(date2.toLocalDate().equals(project.getStartDate().toLocalDate()));
    }

    @Test
    public void testGetFinalDateTrue() {
        // Given
        Project project = new Project("C300", "Test");
        LocalDateTime date = LocalDateTime.of(1999, 11, 11, 10, 50, 30);
        // When
        project.setFinalDate(date);
        // Then
        assertTrue(date.toLocalDate().equals(project.getFinalDate().toLocalDate()));
    }

    @Test
    public void testGetFinalDateFalse() {
        // Given
        Project project = new Project("C300", "Test");
        LocalDateTime date = LocalDateTime.of(1999, 11, 11, 10, 50, 30);
        LocalDateTime date2 = LocalDateTime.of(1998, 11, 11, 10, 50, 30);
        // When
        project.setFinalDate(date);
        // Then
        assertFalse(date2.toLocalDate().equals(project.getFinalDate().toLocalDate()));
    }

    @Test
    public void testHasCollaborator() {
        // Given
        // Then
        assertTrue(project3.hasActiveProjectCollaborator(collaborator1));
        assertFalse(project2.hasActiveProjectCollaborator(collaborator2));
    }

    @Test
    public void testGetCostSoFar() {
        // Given
        // Then
        assertEquals(108.0, project3.calculateTotalCostSoFar(), 0.01);
    }

    @Test
    public void testGetCostCalculatorPersistence() {
        // Given
        // Then
        assertEquals("LastTimePeriodCost", project3.getCostCalculatorPersistence());
    }

    @Test
    public void testHasTask() {
        // Given
        // Then
        assertTrue(project3.hasTask(t2));
        assertFalse(project3.hasTask(t7));
    }

    @Test
    public void testGetUserProjectTasks() {
        // Given
        List<Task> result = project3.listCollaboratorTasks(collaborator2);
        List<Task> expected = new ArrayList<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t4);
        expected.add(t5);
        // Then
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testHasUnassignedTasksTrue() {
        // Given
        // Then
        assertTrue(project3.hasAssignedTasks(collaborator2));
    }

    @Test
    public void testHasUnassignedTasksFalse() {
        // Given
        // Then
        assertFalse(project3.hasAssignedTasks(collaborator1));
    }

    @Test
    public void testGetUnassignedUsersList() {
        // Given
        List<ProjectCollaborator> expected = new ArrayList<>();

        List<ProjectCollaborator> result = project3.listUnassignedProjectCollaborator();
        expected.add(collaborator1);
        expected.add(collaborator3);
        // Then
        assertEquals(expected, result);
    }

    @Test
    public void testGetUnassignedUsersListEmptyList() {
        // Given
        List<ProjectCollaborator> expected = new ArrayList<>();
        List<ProjectCollaborator> result = project2.listUnassignedProjectCollaborator();
        // Then
        assertEquals(expected, result);
    }

    @Test
    public void testHashCode() {
        // Given
        // When
        project4 = new Project("100", "Project 4");
        // Then
        assertEquals(project1.hashCode(), project4.hashCode());
        assertNotEquals(project1.hashCode(), project2.hashCode());
    }

    @Test
    public void testListAssignmentRequests() {
        // Given
        // When
        t6.requestAddProjectCollaborator(collaborator2);
        t6.requestAddProjectCollaborator(collaborator3);
        List<TaskCollaboratorRegistry> expected = new ArrayList<>();
        TaskCollaboratorRegistry tcr1 = t6.listTaskCollaboratorRegistry(collaborator2).get(0);
        TaskCollaboratorRegistry tcr2 = t6.listTaskCollaboratorRegistry(collaborator3).get(0);
        expected.add(tcr1);
        expected.add(tcr2);
        // Then
        assertThat(project3.listAssignmentRequests(), containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testListRemovalRequests() {
        // Given
        // When
        t6.addProjectCollaborator(collaborator2);
        t6.addProjectCollaborator(collaborator3);
        List<TaskCollaboratorRegistry> expected = new ArrayList<>();
        t6.requestRemoveProjectCollaborator(collaborator2);
        t6.requestRemoveProjectCollaborator(collaborator3);

        TaskCollaboratorRegistry tcr1 = t6.listTaskCollaboratorRegistry(collaborator2).get(0);
        TaskCollaboratorRegistry tcr2 = t6.listTaskCollaboratorRegistry(collaborator3).get(0);
        expected.add(tcr1);
        expected.add(tcr2);
        // Then
        assertThat(project3.listRemovalRequests(), containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void listAllReportsSucess() {
        // Given
        // When
        int reportCount = TaskCollaboratorRegistry.getThisTasksReportCount();
        List<Report> expected = new ArrayList<>();
        report1 = t1.findReportById(t1.getId() + "(" + (reportCount - 3) + ")");
        report2 = t2.findReportById(t2.getId() + "(" + (reportCount - 2) + ")");
        report3 = t4.findReportById(t4.getId() + "(" + (reportCount - 1) + ")");
        report4 = t5.findReportById(t5.getId() + "(" + (reportCount) + ")");
        expected.add(report1);
        expected.add(report2);
        expected.add(report3);
        expected.add(report4);
        List<Report> result = project3.listAllReports();
        // Then
        assertEquals(expected, result);
    }

    @Test
    public void testRefreshTaskListSuccess() {
        //Given task t7 belonging to project5
        //When we set a new description and refresh taskList
        t7.setDescription("new Description");
        boolean result = project5.refreshTaskList(t7);
        //Then
        assertTrue(result);
    }

    @Test
    public void testRefreshTaskListFail() {
        // Given
        // When
        Task result = new Task(project1, "Test 9");
        // Then
        boolean condition = project1.refreshTaskList(result);
        assertFalse(condition);
    }

    @Test
    public void testListActiveProjectCollaborators() {
        // Given: project1 with 2 collaborators
        assertEquals(project1.getProjectCollaboratorList().size(), 0);
        assertEquals(project1.listActiveProjectCollaborators().size(), 0);
        project1.addProjectCollaborator(collaborator1);
        project1.addProjectCollaborator(collaborator2);
        assertEquals(project1.getProjectCollaboratorList().size(), 2);
        assertEquals(project1.listActiveProjectCollaborators().size(), 2);

        // When: remove collaborator2
        project1.removeProjectCollaborator(collaborator2);

        // Then: collaborator is removed from activeProjectCollaborators list
        assertEquals(project1.getProjectCollaboratorList().size(), 2);
        assertEquals(project1.listActiveProjectCollaborators().size(), 1);
    }

    @Test
    public void testCalculateCostSoFarFromTask() {

        double result = 27.0;
        assertEquals(project1.calculateCostSoFarOfTask(t1), result, 0.00);
    }

    @Test
    public void testListCollaboratorTasksCompleted() {

        // Given
        t1.setEffectiveDateOfConclusion(d1);
        assertTrue(t1.getStatus().isOnCompletedState());
        t2.setEffectiveDateOfConclusion(d1.plusMinutes(1));
        assertTrue(t1.getStatus().isOnCompletedState());

        List<Task> expected = new ArrayList<>();
        expected.add(t2);
        expected.add(t1);
        // When
        List<Task> result = project3.listCollaboratorCompletedTasks(collaborator2);
        // Then

        assertEquals(expected, result);
    }

    @Test
    public void testListCollaboratorTasksCompletedCollaboratorNotInTask() {
        // Given
        // When
        t1.setEffectiveDateOfConclusion(d1);
        assertTrue(t1.getStatus().isOnCompletedState());
        // Then
        List<Task> result = project3.listCollaboratorCompletedTasks(collaborator1);
        List<Task> expected = new ArrayList<>();
        assertTrue(expected.isEmpty());
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testListCollaboratorTasksCompletedLastMonth() {
        // Given
        t1.setEffectiveDateOfConclusion(d9);
        assertTrue(t1.getStatus().isOnCompletedState());
        t2.setEffectiveDateOfConclusion(d9.plusMinutes(1));
        assertTrue(t2.getStatus().isOnCompletedState());

        List<Task> expected = new ArrayList<>();
        expected.add(t2);
        expected.add(t1);

        // When
        List<Task> result = project3.listCollaboratorsCompletedTasksLastMonth(collaborator2);

        // Then
        assertEquals(expected, result);
    }

    @Test
    public void testListCollaboratorTasksCompletedLastMonthFailUserNotInTask() {
        // Given
        // When
        t1.setEffectiveDateOfConclusion(d9);
        assertTrue(t1.getStatus().isOnCompletedState());
        // Then
        List<Task> result = project3.listCollaboratorsCompletedTasksLastMonth(collaborator1);
        List<Task> expected = new ArrayList<>();
        assertTrue(expected.isEmpty());
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testListCollaboratorTasksCompletedLastMonthFailTaskIsNotinList() {
        // Given
        // When
        t1.setEffectiveDateOfConclusion(LocalDateTime.now().minusMonths(2));
        assertTrue(t1.getStatus().isOnCompletedState());
        // Then
        List<Task> result = project3.listCollaboratorsCompletedTasksLastMonth(collaborator2);
        List<Task> expected = new ArrayList<>();
        assertTrue(expected.isEmpty());
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testListNotIniciatedTasksPlannedState() {
        // Given
        // When
        assertTrue(t3.getStatus().isOnPlannedState());
        assertTrue(t6.getStatus().isOnPlannedState());
        // Then
        Set<Task> result = project3.listNotInitiatedTasks();
        Set<Task> expected = new LinkedHashSet<>();
        expected.add(t3);
        expected.add(t6);
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testListNotIniciatedTasksCreatedState() {
        // Given

        Task test = new Task(project3, "TaskTest");
        project3.addTask(test);
        // When
        assertTrue(t3.getStatus().isOnPlannedState());
        assertTrue(t6.getStatus().isOnPlannedState());
        assertTrue(test.getStatus().isOnCreatedState());
        // Then
        Set<Task> result = project3.listNotInitiatedTasks();
        Set<Task> expected = new LinkedHashSet<>();
        expected.add(t3);
        expected.add(t6);
        expected.add(test);
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testListNotIniciatedTasksReadyToStartState() {
        // Given

        Task test = new Task(project3, "TaskTest");
        project3.addTask(test);
        test.addProjectCollaborator(collaborator2);
        // When
        assertTrue(t3.getStatus().isOnPlannedState());
        assertTrue(t6.getStatus().isOnPlannedState());
        assertTrue(test.getStatus().isOnReadyToStartState());
        // Then
        Set<Task> result = project3.listNotInitiatedTasks();
        Set<Task> expected = new LinkedHashSet<>();
        expected.add(t3);
        expected.add(t6);
        expected.add(test);
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testListCompletedTasks() {

        // Given
        t1.setEffectiveDateOfConclusion(d9);
        assertTrue(t1.getStatus().isOnCompletedState());
        t2.setEffectiveDateOfConclusion(d9.plusMinutes(1));
        assertTrue(t2.getStatus().isOnCompletedState());

        List<Task> expected = new ArrayList<>();
        expected.add(t1);
        expected.add(t2);

        // When
        List<Task> result = new ArrayList<>();
        result.addAll(project3.listCompletedTasks());

        // Then
        assertThat(expected, containsInAnyOrder(result.toArray()));
    }

    @Test
    public void testListNotIniciatedTasksAssignedState() {
        // Given

        Task test = new Task(project3, "TaskTest");
        project3.addTask(test);
        test.addProjectCollaborator(collaborator2);
        test.addTaskDependency(t1);
        // When
        assertTrue(t3.getStatus().isOnPlannedState());
        assertTrue(t6.getStatus().isOnPlannedState());
        assertTrue(test.getStatus().isOnAssignedState());
        // Then
        Set<Task> result = project3.listNotInitiatedTasks();
        Set<Task> expected = new LinkedHashSet<>();
        expected.add(t3);
        expected.add(t6);
        expected.add(test);
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testListIniciatedButNotCompletedTasks() {
        // Given

        // When
        assertTrue(t1.getStatus().isOnInProgressState());
        assertTrue(t2.getStatus().isOnInProgressState());
        assertTrue(t4.getStatus().isOnInProgressState());
        assertTrue(t5.getStatus().isOnInProgressState());

        // Then
        Set<Task> result = project3.listInitiatedButNotCompletedTasks();
        Set<Task> expected = new LinkedHashSet<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t4);
        expected.add(t5);

        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testListIniciatedButNotCompletedTasksSuspended() {
        // Given
        assertTrue(t1.getStatus().isOnInProgressState());
        assertTrue(t2.getStatus().isOnInProgressState());
        assertTrue(t4.getStatus().isOnInProgressState());
        assertTrue(t5.getStatus().isOnInProgressState());
        // When
        t2.removeProjectCollaborator(collaborator2);
        assertTrue(t2.getStatus().isOnSuspendedState());

        // Then
        Set<Task> result = project3.listInitiatedButNotCompletedTasks();
        Set<Task> expected = new LinkedHashSet<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t4);
        expected.add(t5);

        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testListNotCompletedTasksAndWithPredictedDateOfConclusionExpired() {
        // Given
        assertFalse(t1.getStatus().isOnCompletedState());
        assertFalse(t2.getStatus().isOnCompletedState());
        // When

        t1.setPredictedDateOfConclusion(LocalDateTime.now().minusDays(5));
        t2.setPredictedDateOfConclusion(LocalDateTime.now().minusDays(5));

        // Then
        Set<Task> result = project3.listNotCompletedTasksAndWithPredictedDateOfConclusionExpired();
        Set<Task> expected = new LinkedHashSet<>();
        expected.add(t1);
        expected.add(t2);

        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testListNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTaskIsCompleted() {
        // Given
        assertFalse(t1.getStatus().isOnCompletedState());
        assertFalse(t2.getStatus().isOnCompletedState());
        // When
        t1.setPredictedDateOfConclusion(LocalDateTime.now().minusDays(5));
        t2.setPredictedDateOfConclusion(LocalDateTime.now().minusDays(5));
        t1.setTaskCompleted();
        assertTrue(t1.getStatus().isOnCompletedState());
        // Then
        Set<Task> result = project3.listNotCompletedTasksAndWithPredictedDateOfConclusionExpired();
        Set<Task> expected = new LinkedHashSet<>();
        expected.add(t2);

        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testListNotCompletedTasksAndWithPredictedDateOfConclusionExpiredDateIsAfter() {
        // Given
        assertFalse(t1.getStatus().isOnCompletedState());
        assertFalse(t2.getStatus().isOnCompletedState());
        // When
        t1.setPredictedDateOfConclusion(LocalDateTime.now().plusDays(5));
        t2.setPredictedDateOfConclusion(LocalDateTime.now().minusDays(5));
        // Then
        Set<Task> result = project3.listNotCompletedTasksAndWithPredictedDateOfConclusionExpired();
        Set<Task> expected = new LinkedHashSet<>();
        expected.add(t2);

        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void testChangeDependentTasksStatus() {

        //Given
        List<Task> expected = new ArrayList<>();

        t3.addProjectCollaborator(collaborator2);
        t3.addTaskDependency(t1);
        assertTrue(t3.getTaskState().isOnAssignedState());

        //When
        t1.setTaskCompleted();
        List<Task> result = project3.changeDependentTasksStatus(t1);

        //Then
        assertEquals(expected, result);
        assertTrue(t3.getTaskState().isOnReadyToStartState());
        assertEquals("ReadyToStart", t3.getState());
    }

    @Test
    public void testChangeDependentTasksStatus2() {

        //Given
        List<Task> expected = new ArrayList<>();
        expected.add(t3);

        t3.addProjectCollaborator(collaborator2);
        t3.addTaskDependency(t1);
        assertTrue(t3.getTaskState().isOnAssignedState());

        //When
        List<Task> result = project3.changeDependentTasksStatus(t1);

        //Then
        assertEquals(expected, result);
        assertTrue(t3.getTaskState().isOnReadyToStartState());
        assertEquals("ReadyToStart", t3.getState());
    }

    /**
     * GIVEN a project with a cost calculator interface
     * WHEN we ask for its absolute path
     * THEN we get a string with its path
     */
    @Test
    public void testGetCostCalculatorAbsolutePath() {

        //When
        String result = project1.getCostCalculatorAbsolutePath();

        //Then
        assertEquals("project.model.project", result);
    }

    @Test
    public void testListAllTaskCollaboratorRegistry() {
        List<TaskCollaboratorRegistry> expected = new ArrayList<>();
        expected.addAll(t1.listTaskCollaboratorRegistry(collaborator2));
        expected.addAll(t2.listTaskCollaboratorRegistry(collaborator2));
        expected.addAll(t3.listTaskCollaboratorRegistry(collaborator2));
        expected.addAll(t4.listTaskCollaboratorRegistry(collaborator2));
        expected.addAll(t5.listTaskCollaboratorRegistry(collaborator2));
        //When
        List<TaskCollaboratorRegistry> result = project3.listAllTaskCollaboratorRegistry(collaborator2);

        //Then
        assertEquals(expected, result);
    }

    /**
     * GIVEN a project
     * WHEN we parse it to a ProjectDTO
     * THEN we get a correctly parsed DTO.
     */
    @Test
    public void testToDTO() {

        //When
        project3.setInactive();
        t4.setTaskCompleted();
        t2.setTaskCancelled();
        ProjectDTO testProj = project3.toDTO();
        //Then
        assertEquals(testProj.getProjectId(), project3.getId());
        assertEquals(testProj.isActive(), project3.isActive());
        assertEquals(testProj.getName(), project3.getName());
        assertEquals(testProj.getDescription(), project3.getDescription());
        assertEquals(testProj.getUnit(), project3.getUnit().toString());
        assertEquals(testProj.getGlobalBudget(), project3.getGlobalBudget(), 0.00001);
        assertEquals(testProj.getUserEmail(), project3.getProjectManager().getUser().getEmail());
        assertEquals(testProj.getStartDate(), project3.getStartDate());
        assertEquals(testProj.getFinalDate(), project3.getFinalDate());
        assertEquals(testProj.getCompletedTasks(), project3.listCompletedTasks().size());
        assertEquals(testProj.getInitiatedButNotCompletedTasks(), project3.listInitiatedButNotCompletedTasks().size());
        assertEquals(testProj.getNotInitiatedTasks(), project3.listNotInitiatedTasks().size());
        assertEquals(testProj.getCanceledTasks(), project3.listCancelledTasks().size());
        assertEquals(testProj.getUnassignedCollaborators(), project3.listUnassignedProjectCollaborator().size());
        assertEquals(testProj.getTasks(), project3.getTasksList().size());
        assertEquals(testProj.getCollaborators(), project3.getProjectCollaboratorList().size());
        assertEquals(testProj.getCurrentCost(), project3.calculateTotalCostSoFar(), 0.01);
        assertEquals(testProj.getCostOptionSelected(), project3.getCostCalculatorPersistence());
    }

    /**
     * GIVEN a project with only LastTimePeriodCost and a list containing new options
     * WHEN we give a new set of options that doesn't contain LastTimePeriodCost
     * THEN the project's cost calculation option will be set to the first element of the list
     */
    @Test
    public void testSetCostCalculatorOptions() {

        //Given
        List<String> testOptions = new ArrayList<>();
        testOptions.add("AverageCost");
        testOptions.add("FirstTimePeriodCost");

        //When
        project1.setCostCalculationOptions(testOptions);

        //Then
        assertEquals("AverageCost", project1.getCostCalculator().getClass().getSimpleName());
        assertThat(project1.getCostCalculationOptions(), containsInAnyOrder(testOptions.toArray()));
    }

    @Test
    public void listCancelledTasksTestSuccess() {
        Set<Task> tasks = new HashSet<>();
        tasks.add(t1);
        t1.setTaskCancelled();
        assertTrue(t1.getTaskState().isOnCancelledState());
        Set<Task> result = project3.listCancelledTasks();
        assertEquals(tasks, result);
    }

    @Test
    public void listCancelledTasksTestFail() {
        Set<Task> tasks = new HashSet<>();
        assertFalse(t1.getTaskState().isOnCancelledState());
        Set<Task> result = project3.listCancelledTasks();
        assertEquals(tasks, result);
    }


}