package project.model.unittests;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import project.model.UserService;
import project.jparepositories.UserRepository;
import project.model.user.User;
import project.model.user.UserIdVO;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class UserServiceMockTest {

    @Mock
    UserRepository userRepository;
    UserService userService;
    User userTest;
    User userTest2;

    @Before
    public void initialize() throws AddressException {
        userRepository = mock(UserRepository.class);
        userService = new UserService(userRepository);

        userTest = new User("Zé", "919999999", "ze@gmail.com", "332432", LocalDate.now(), "2", "Rua ", "4433", "cidade",
                "país");
        userService.addUser(userTest);

        userTest2 = new User("Pedro", "919999999", "pedro@gmail.com", "332432", LocalDate.now(), "2", "Rua ", "4433", "cidade",
                "país");
        userService.addUser(userTest2);

    }

    @Test
    @Transactional
    public void testMockCreation() {
        assertNotNull(userRepository);
    }

    @Test
    @Transactional

    public void testFindAll() {

        List<User> userList = new ArrayList<>();
        userList.add(userTest);

        when(userRepository.findAll()).thenReturn(userList);
        assertEquals(userList, userService.listAllUsers());

        verify(userRepository, times(1)).findAll();

    }

    @Test
    @Transactional

    public void testAddSimpleUserSucess() {

        when(userService.addUser(userTest)).thenReturn(true);
        verify(userRepository).save(userTest);

    }

    @Test
    @Transactional

    public void testAddDetailUserSucess() throws AddressException {

        when(userService.addUser("Zé", "919999999", "ze@gmail.com", "332432", LocalDate.now(), "2", "Rua ", "4433", "cidade",
                "país")).thenReturn(true);
        verify(userRepository).save(userTest);

    }

    @Test
    @Transactional

    public void updateUserSucess()  {
        UserIdVO userIdVO = userTest.getUserIdVO();
        when(userRepository.existsById(userIdVO)).thenReturn(true);

        verify(userRepository, times(1)).save(userTest);

    }

}
