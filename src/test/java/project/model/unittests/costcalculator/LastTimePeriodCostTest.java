package project.model.unittests.costcalculator;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import project.model.project.LastTimePeriodCost;
import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;
import project.model.task.Report;

public class LastTimePeriodCostTest {

	ProjectCollaboratorCostAndTimePeriod colab1 = new ProjectCollaboratorCostAndTimePeriod(5);
	ProjectCollaboratorCostAndTimePeriod colab2 = new ProjectCollaboratorCostAndTimePeriod(7);
	ProjectCollaboratorCostAndTimePeriod colab3 = new ProjectCollaboratorCostAndTimePeriod(3);
	Report report = new Report("1234", 10, LocalDateTime.now().minusDays(10), LocalDateTime.now().minusDays(5));

	
	@Test
	public void testLastTimePeriodCostSuccess() {
		List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();
		
		costAndTimePeriodList.add(colab1);
		costAndTimePeriodList.add(colab2);
		costAndTimePeriodList.add(colab3);
		
		LastTimePeriodCost lastTimePeriodCost = new LastTimePeriodCost();
		
		assertEquals(30.0, lastTimePeriodCost.calculateReportCost(report, costAndTimePeriodList), 0.01);

	}
	
	@Test
	public void testLastTimePeriodCostSuccessDifferentOrder() {
		List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();
		
		costAndTimePeriodList.add(colab3);
		costAndTimePeriodList.add(colab1);
		costAndTimePeriodList.add(colab2);
		
		LastTimePeriodCost lastTimePeriodCost = new LastTimePeriodCost();
		
		assertEquals(70.0, lastTimePeriodCost.calculateReportCost(report, costAndTimePeriodList), 0.01);

	}

	@Test
	public void testLastTimePeriodCostFail() {
		List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();
		
		costAndTimePeriodList.add(colab1);
		costAndTimePeriodList.add(colab2);
		
		LastTimePeriodCost lastTimePeriodCost = new LastTimePeriodCost();
		
		assertNotEquals(50.0, lastTimePeriodCost.calculateReportCost(report, costAndTimePeriodList), 0.01);

	}

}
