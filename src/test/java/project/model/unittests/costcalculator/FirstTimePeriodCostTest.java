package project.model.unittests.costcalculator;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import project.model.project.FirstTimePeriodCost;
import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;
import project.model.task.Report;

public class FirstTimePeriodCostTest {
	
	ProjectCollaboratorCostAndTimePeriod colab1 = new ProjectCollaboratorCostAndTimePeriod(5);
	ProjectCollaboratorCostAndTimePeriod colab2 = new ProjectCollaboratorCostAndTimePeriod(7);
	ProjectCollaboratorCostAndTimePeriod colab3 = new ProjectCollaboratorCostAndTimePeriod(3);
	Report report = new Report("1234", 10, LocalDateTime.now().minusDays(10), LocalDateTime.now().minusDays(5));
	
	@Test
	public void testFirstTimePeriodCostSuccess() {
		List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();
		
		costAndTimePeriodList.add(colab1);
		costAndTimePeriodList.add(colab2);
		costAndTimePeriodList.add(colab3);
		
		FirstTimePeriodCost firstTimePeriodCost = new FirstTimePeriodCost();
		
		assertEquals(50.0, firstTimePeriodCost.calculateReportCost(report, costAndTimePeriodList), 0.01);

	}
	
	@Test
	public void testFirstTimePeriodCostSuccessDifferentOrder() {
		List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();
		
		costAndTimePeriodList.add(colab3);
		costAndTimePeriodList.add(colab1);
		costAndTimePeriodList.add(colab2);
		
		FirstTimePeriodCost firstTimePeriodCost = new FirstTimePeriodCost();
		
		assertEquals(30.0, firstTimePeriodCost.calculateReportCost(report, costAndTimePeriodList), 0.01);

	}

	@Test
	public void testFirstTimePeriodCostFail() {
		List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();
		
		costAndTimePeriodList.add(colab1);
		costAndTimePeriodList.add(colab2);
		
		FirstTimePeriodCost firstTimePeriodCost = new FirstTimePeriodCost();
		
		assertNotEquals(70.0, firstTimePeriodCost.calculateReportCost(report, costAndTimePeriodList), 0.01);

	}
}
