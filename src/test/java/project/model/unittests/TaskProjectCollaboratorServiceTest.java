package project.model.unittests;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.dto.rest.CreateReportRestDTO;
import project.dto.rest.EditReportQuantityDTO;
import project.dto.rest.ReportRestDTO;
import project.dto.rest.TaskProjectCollaboratorRestDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Report;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.task.TaskCollaboratorRegistry.RegistryStatus;
import project.model.user.User;
import project.services.ProjectCollaboratorService;
import project.services.TaskProjectCollaboratorService;
import project.services.TaskService;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static project.model.task.TaskCollaboratorRegistry.RegistryStatus.REMOVALREQUEST;

public class TaskProjectCollaboratorServiceTest {

    // RepositoryClass
    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;

    // Services
    UserService userService;
    ProjectService projectService;
    TaskService taskService;
    ProjectCollaboratorService projectCollaboratorService;
    TaskProjectCollaboratorService taskProjectCollaboratorService;

    LocalDate birth;
    User userProjectManager, userProjectCollaborator1, userProjectCollaborator2;
    ProjectCollaborator projectManager, projectCollaborator1, projectCollaborator2;
    Project project;
    Task task, task2;
    LocalDateTime d1;
    LocalDateTime d2;
    TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO, taskProjectCollaboratorRestDTO2;
    TaskCollaboratorRegistry col2TaskRegistry;

    @Before
    public void setUp() throws AddressException {

        // RepositoryClass
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();

        // Services
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);
        taskProjectCollaboratorService = new TaskProjectCollaboratorService(taskRepositoryClass,
                projectCollaboratorRepositoryClass, projectRepositoryClass);
        ProjectCollaborator.setStartIdGenerator(1);

        birth = LocalDate.of(1999, 11, 11);

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Pedro", "96 452 56 56", "pedro@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");

        userProjectManager = userService.searchUserByEmail("asdrubal@jj.com");
        userProjectCollaborator1 = userService.searchUserByEmail("joaquim@gmail.com");
        userProjectCollaborator2 = userService.searchUserByEmail("pedro@gmail.com");

        userProjectManager.setProfileCollaborator();
        userProjectCollaborator1.setProfileCollaborator();
        userProjectCollaborator2.setProfileCollaborator();

        userService.updateUser(userProjectManager);
        userService.updateUser(userProjectCollaborator1);
        userService.updateUser(userProjectCollaborator2);

        projectService.addProject("1", "Project 1");
        project = projectService.getProjectByID("1");
        projectCollaboratorService.setProjectManager(project, userProjectManager);
        projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator1, 2);
        projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator2, 2);

        projectManager = project.findProjectCollaborator(userProjectManager);
        projectCollaborator1 = project.findProjectCollaborator(userProjectCollaborator1);
        projectCollaborator2 = project.findProjectCollaborator(userProjectCollaborator2);

        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
        d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

        taskService.addTask(project, "task", "task", d1, d2, 1.2, 1.3);
        task = taskService.findTaskByID("1-1");

        taskProjectCollaboratorRestDTO = new TaskProjectCollaboratorRestDTO();
        taskProjectCollaboratorRestDTO.setProjectCollaboratorId(projectCollaborator2.getId());
        taskProjectCollaboratorRestDTO.setTaskId("1-1");
        taskProjectCollaboratorRestDTO.setEmail(projectCollaborator2.getUser().getEmail());

        taskService.addTask(project, "task2", "task2", d1, d2, 1.2, 1.3);
        task2 = taskService.findTaskByID("1-2");
        taskService.addProjectCollaboratorToTask(projectCollaborator1, task2);
        col2TaskRegistry = task2
                .getTaskCollaboratorRegistryByID(task2.getId() + "-" + projectCollaborator1.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));
        task2.addReport(projectCollaborator1, 10, LocalDateTime.now().minusMonths(2), LocalDateTime.now().minusDays(5));
        taskService.updateTask(task2);
        projectCollaboratorService.updateProjectCollaborator(projectCollaborator1);

        taskProjectCollaboratorRestDTO2 = new TaskProjectCollaboratorRestDTO();
        taskProjectCollaboratorRestDTO2.setProjectCollaboratorId(projectCollaborator1.getId());
        taskProjectCollaboratorRestDTO2.setTaskId("1-2");
        taskProjectCollaboratorRestDTO2.setEmail(projectCollaborator1.getUser().getEmail());
    }

    @Test
    @Transactional
    public void addProjectCollaboratorToTaskSuccess() {
        // Given:
        ProjectCollaborator projectCollaborator = projectCollaboratorService.getProjectCollaborator(3);
        assertFalse(task.hasProjectCollaborator(projectCollaborator));
        // When:
        taskProjectCollaboratorService.addProjectCollaboratorToTask(taskProjectCollaboratorRestDTO);
        // Then:
        assertTrue(task.hasProjectCollaborator(projectCollaborator));
    }


    @Test
    @Transactional
    public void addProjectCollaboratorToTask_failProjectCollaboratorNull() {
        // Given:
        ProjectCollaborator projectCollaborator = projectCollaboratorService.getProjectCollaborator(76976);
        assertNull(projectCollaborator);
        // When:
        taskProjectCollaboratorRestDTO.setProjectCollaboratorId(76976);
        taskProjectCollaboratorRestDTO.setEmail("ana");
        assertFalse(taskProjectCollaboratorService.addProjectCollaboratorToTask(taskProjectCollaboratorRestDTO));
        // Then:
        assertFalse(task.hasProjectCollaborator(projectCollaborator));
    }

    @Test
    @Transactional
    public void addProjectCollaboratorToTask_failTaskNull() {
        // Given:
        ProjectCollaborator projectCollaborator = projectCollaboratorService.getProjectCollaborator(3);
        Task t = taskService.findTaskByID("invalid");
        assertNull(t);
        // When:
        taskProjectCollaboratorRestDTO.setTaskId("invalid");
        assertFalse(taskProjectCollaboratorService.addProjectCollaboratorToTask(taskProjectCollaboratorRestDTO));
        // Then:
        assertFalse(task.hasProjectCollaborator(projectCollaborator));
    }


    @Test
    @Transactional
    public void addProjectCollaboratorToTaskFail() {

        // Given:
        assertTrue(taskProjectCollaboratorService.addProjectCollaboratorToTask(taskProjectCollaboratorRestDTO));
        // When:
        taskProjectCollaboratorService.addProjectCollaboratorToTask(taskProjectCollaboratorRestDTO);
        // Then:
        assertFalse(taskProjectCollaboratorService.addProjectCollaboratorToTask(taskProjectCollaboratorRestDTO));
    }

    @Test
    @Transactional
    public void requestAddProjectCollaboratorToTask_Success() {

        TaskCollaboratorRegistry.RegistryStatus expectedRegistryStatus = TaskCollaboratorRegistry.RegistryStatus.ASSIGNMENTREQUEST;

        // Given:
        assertFalse(task.listActiveCollaborators().contains(projectCollaborator2));
        assertTrue(project.hasActiveProjectCollaborator(projectCollaborator2));
        assertNull(taskProjectCollaboratorService.getTaskCollaboratorRegistry(task, projectCollaborator2));
        // When:
        assertTrue(taskProjectCollaboratorService.requestAddProjectCollaboratorToTask(taskProjectCollaboratorRestDTO));
        // Then:
        assertFalse(task.listActiveCollaborators().contains(projectCollaborator2));
        assertEquals(expectedRegistryStatus, taskProjectCollaboratorService
                .getTaskCollaboratorRegistry(task, projectCollaborator2).getRegistryStatus());
    }

    @Test
    @Transactional
    public void requestAddProjectCollaboratorToTaskRemovalApprovedSucess() {

        TaskCollaboratorRegistry.RegistryStatus expectedGivenRegistryStatus = TaskCollaboratorRegistry.RegistryStatus.REMOVALAPPROVED;
        TaskCollaboratorRegistry.RegistryStatus expectedRegistryStatus = TaskCollaboratorRegistry.RegistryStatus.ASSIGNMENTREQUEST;

        // Given:
        task.addProjectCollaborator(projectCollaborator2);
        assertTrue(task.listActiveCollaborators().contains(projectCollaborator2));
        taskProjectCollaboratorService.getTaskCollaboratorRegistry(task, projectCollaborator2)
                .setRegistryStatus(TaskCollaboratorRegistry.RegistryStatus.REMOVALAPPROVED);
        assertEquals(expectedGivenRegistryStatus, taskProjectCollaboratorService
                .getTaskCollaboratorRegistry(task, projectCollaborator2).getRegistryStatus());
        // When:
        assertTrue(taskProjectCollaboratorService.requestAddProjectCollaboratorToTask(taskProjectCollaboratorRestDTO));
        // Then:
        assertFalse(task.listActiveCollaborators().contains(projectCollaborator2));
        assertEquals(expectedRegistryStatus, taskProjectCollaboratorService
                .getTaskCollaboratorRegistry(task, projectCollaborator2).getRegistryStatus());
    }


    @Test
    @Transactional
    public void requestAddProjectCollaboratorToTask_nullProjectCollaborator() {

        //given: an invalid id
        taskProjectCollaboratorRestDTO.setProjectCollaboratorId(0);
        //when:
        boolean result = taskProjectCollaboratorService.requestAddProjectCollaboratorToTask(taskProjectCollaboratorRestDTO);
        //then:
        assertFalse(result);

    }


    @Test
    @Transactional
    public void requestAddProjectCollaboratorToTask_nullTask() {

        //given: an invalid id
        taskProjectCollaboratorRestDTO.setTaskId("invalid");
        //when:
        boolean result = taskProjectCollaboratorService.requestAddProjectCollaboratorToTask(taskProjectCollaboratorRestDTO);
        //then:
        assertFalse(result);

    }

    @Test
    @Transactional
    public void requestAddProjectCollaboratorToTaskFail() {

        TaskCollaboratorRegistry.RegistryStatus expectedRegistryStatus = TaskCollaboratorRegistry.RegistryStatus.ASSIGNMENTREQUEST;
        TaskCollaboratorRegistry.RegistryStatus result = TaskCollaboratorRegistry.RegistryStatus.ASSIGNMENTREQUEST;

        // Given:
        assertFalse(task.listActiveCollaborators().contains(projectCollaborator2));
        assertTrue(project.hasActiveProjectCollaborator(projectCollaborator2));
        assertNull(taskProjectCollaboratorService.getTaskCollaboratorRegistry(task, projectCollaborator2));
        assertTrue(taskProjectCollaboratorService.requestAddProjectCollaboratorToTask(taskProjectCollaboratorRestDTO));

        // When:
        assertFalse(taskProjectCollaboratorService.requestAddProjectCollaboratorToTask(taskProjectCollaboratorRestDTO));

        result = taskProjectCollaboratorService.getTaskCollaboratorRegistry(task, projectCollaborator2)
                .getRegistryStatus();
        // Then:
        assertEquals(expectedRegistryStatus, result);
    }



    @Test
    @Transactional
    public void us208EditReportNullProjCollabTest() {
        // given
        EditReportQuantityDTO editReportQuantityDTO = new EditReportQuantityDTO();
        // when
        editReportQuantityDTO.setTaskID(task.getId());
        editReportQuantityDTO.setReportId(task.getId() + "(1)");
        editReportQuantityDTO.setCollabEmail(projectCollaborator2.getUser().getEmail());

        // then
        assertFalse(taskProjectCollaboratorService.us208EditReport2(editReportQuantityDTO));

    }

    @Test
    @Transactional
    public void us208EditReportSuccessTest() {

        // given
        EditReportQuantityDTO editReportQuantityDTO = new EditReportQuantityDTO();
        // when
        editReportQuantityDTO.setCollabEmail(projectCollaborator1.getUser().getEmail());

        editReportQuantityDTO.setTaskID(task2.getId());
        editReportQuantityDTO.setReportId(task2.getLastTaskCollaboratorRegistryOf(projectCollaborator1).getReportList().get(0).getCode());

        // then
        assertTrue(taskProjectCollaboratorService.us208EditReport2(editReportQuantityDTO));

    }

    @Test
    @Transactional
    public void us208EditInvalidReportIdTest() {

        // given
        EditReportQuantityDTO editReportQuantityDTO = new EditReportQuantityDTO();
        // when
        editReportQuantityDTO.setCollabEmail(projectCollaborator1.getUser().getEmail());

        editReportQuantityDTO.setTaskID(task2.getId());
        editReportQuantityDTO.setReportId(task2.getId() + "(3)");
        // then
        assertFalse(taskProjectCollaboratorService.us208EditReport2(editReportQuantityDTO));

    }

    @Test
    @Transactional
    public void us208EditReportNoReportTest() {

        task.addProjectCollaborator(projectCollaborator1);

        // given
        EditReportQuantityDTO editReportQuantityDTO = new EditReportQuantityDTO();
        // when
        editReportQuantityDTO.setTaskID(task.getId());
        editReportQuantityDTO.setReportId(task.getId() + "(1)");
        editReportQuantityDTO.setCollabEmail(projectCollaborator1.getUser().getEmail());

        // then
        assertFalse(taskProjectCollaboratorService.us208EditReport2(editReportQuantityDTO));

    }

    @Test
    @Transactional
    public void us208EditReportNullProjCollabNotOnTaskTest() {
        // given
        EditReportQuantityDTO editReportQuantityDTO = new EditReportQuantityDTO();
        // when
        editReportQuantityDTO.setTaskID(task.getId());
        editReportQuantityDTO.setReportId(task.getId() + "(1)");
        editReportQuantityDTO.setCollabEmail(projectCollaborator2.getUser().getEmail());

        // then
        assertFalse(taskProjectCollaboratorService.us208EditReport2(editReportQuantityDTO));

    }

    @Test
    @Transactional
    public void us208EditReportNullReportTest() {
        // given
        EditReportQuantityDTO editReportQuantityDTO = new EditReportQuantityDTO();
        // when
        editReportQuantityDTO.setCollabEmail(projectCollaborator2.getUser().getEmail());
        editReportQuantityDTO.setTaskID(task.getId());
        editReportQuantityDTO.setReportId(task.getId() + "(1)");
        editReportQuantityDTO.setReportId("INVALID");
        editReportQuantityDTO.setCollabEmail(projectCollaborator2.getUser().getEmail());

        // then
        assertFalse(taskProjectCollaboratorService.us208EditReport2(editReportQuantityDTO));

    }

    @Test
    @Transactional
    public void testGetTaskCollaboratorRegistrySuccess() {
        // Given
        // Then
        task.addProjectCollaborator(projectCollaborator1);
        TaskCollaboratorRegistry expected = new TaskCollaboratorRegistry(projectCollaborator1, task.getId());

        assertEquals(expected.toString(), taskProjectCollaboratorService.getTaskCollaboratorRegistry(task, projectCollaborator1).toString());
    }

    @Test
    @Transactional
    public void testGetTaskCollaboratorRegistryProjCollabNotInTaskCollabRegistry() {
        // Given
        // Then
        task.addProjectCollaborator(projectCollaborator1);

        assertNull(taskProjectCollaboratorService.getTaskCollaboratorRegistry(task, projectCollaborator2));
    }


    @Test
    @Transactional
    public void testGetTaskCollaboratorRegistryNullFail() {
        // Given
        // Then
        assertNull(taskProjectCollaboratorService.getTaskCollaboratorRegistry(task, projectCollaborator2));
    }

    @Test
    @Transactional
    public void us208EditReportDoesntBellongtoCollabTest() {
        // given
        task.addProjectCollaborator(projectCollaborator1);
        task.getLastTaskCollaboratorRegistryOf(projectCollaborator1).setCollaboratorAddedToTaskDate(d1);
        task.setEffectiveStartDate(d1.minusDays(2));
        task.addReport(projectCollaborator1, 10, d1.plusDays(1), d1.plusDays(2));
        task.addProjectCollaborator(projectCollaborator2);
        task.getLastTaskCollaboratorRegistryOf(projectCollaborator2).setCollaboratorAddedToTaskDate(d1);

        EditReportQuantityDTO editReportQuantityDTO = new EditReportQuantityDTO();
        // when
        editReportQuantityDTO.setTaskID(task.getId());
        editReportQuantityDTO.setReportId(task.getId() + "(1)");
        editReportQuantityDTO.setQuantity(20);
        editReportQuantityDTO.setCollabEmail(projectCollaborator2.getUser().getEmail());


        // then
        assertFalse(taskProjectCollaboratorService.us208EditReport2(editReportQuantityDTO));

    }

    @Test
    @Transactional
    public void addReportDuplicatedTask() {
        // given
        CreateReportRestDTO reportoRestDTO = new CreateReportRestDTO();
        reportoRestDTO.setStartDate(d1);
        reportoRestDTO.setEndDate(d1.plusMonths(300));
        reportoRestDTO.setQuantity(4.0);
        task.addProjectCollaborator(projectCollaborator2);
        task.getLastTaskCollaboratorRegistryOf(projectCollaborator2).setCollaboratorAddedToTaskDate(d1.minusDays(3));
        // when
        reportoRestDTO.setProjectCollaboratorID(projectCollaborator2.getId());
        reportoRestDTO.setProjectCollaboratorEmail(projectCollaborator2.getUser().getEmail());
        reportoRestDTO.setTaskID(task.getId());
        // then


        taskProjectCollaboratorService.addReportRest2(reportoRestDTO);
        assertFalse(taskProjectCollaboratorService.addReportRest2(reportoRestDTO));

    }

    @Test
    @Transactional
    public void addReportSuccess() {
        // given
        CreateReportRestDTO reportoRestDTO = new CreateReportRestDTO();
        reportoRestDTO.setStartDate(d1);
        reportoRestDTO.setEndDate(d1.plusMonths(2));
        reportoRestDTO.setQuantity(4.0);
        task.addProjectCollaborator(projectCollaborator2);
        task.getLastTaskCollaboratorRegistryOf(projectCollaborator2).setCollaboratorAddedToTaskDate(d1.minusDays(3));
        // when
        reportoRestDTO.setProjectCollaboratorID(projectCollaborator2.getId());
        reportoRestDTO.setProjectCollaboratorEmail(projectCollaborator2.getUser().getEmail());
        reportoRestDTO.setTaskID(task.getId());
        // then


        boolean condition = taskProjectCollaboratorService.addReportRest2(reportoRestDTO);

        assertTrue(condition);

    }

    @Test
    @Transactional
    public void addReportProjCollabNotInTaskTestFail() {
        // given
        CreateReportRestDTO reportoRestDTO = new CreateReportRestDTO();
        // when
        reportoRestDTO.setProjectCollaboratorID(projectCollaborator2.getId());
        reportoRestDTO.setTaskID(task.getId());
        reportoRestDTO.setProjectCollaboratorEmail(projectCollaborator2.getUser().getEmail());

        // then
        boolean taskHasProjCollab = task.hasProjectCollaborator(projectCollaborator2);
        boolean condition = taskProjectCollaboratorService.addReportRest2(reportoRestDTO);
        assertFalse(taskHasProjCollab);
        assertFalse(condition);

    }

    @Test
    @Transactional
    public void addReportNullProjCollabTestFail() {
        // given
        task.addProjectCollaborator(projectCollaborator1);
        // when
        CreateReportRestDTO reportoRestDTO = new CreateReportRestDTO();
        reportoRestDTO.setProjectCollaboratorID(789);
        reportoRestDTO.setTaskID(task.getId());
        reportoRestDTO.setProjectCollaboratorEmail("noonebyemail@nomail.com");


        // then
        boolean condition = taskProjectCollaboratorService.addReportRest2(reportoRestDTO);
        assertFalse(condition);

    }

    @Test
    @Transactional
    public void addReportNullTaskTestFail() {
        // given
        task.addProjectCollaborator(projectCollaborator1);
        // when
        CreateReportRestDTO reportoRestDTO = new CreateReportRestDTO();
        reportoRestDTO.setProjectCollaboratorID(projectCollaborator1.getId());
        reportoRestDTO.setTaskID("134");
        reportoRestDTO.setProjectCollaboratorEmail(projectCollaborator1.getUser().getEmail());


        // then
        boolean condition = taskProjectCollaboratorService.addReportRest2(reportoRestDTO);
        assertFalse(condition);

    }

    @Test
    @Transactional
    public void testSendCompletedRequestSuccess() {
        // Given an initiated task with a project collaborator associated
        projectRepositoryClass.save(project);

        assertTrue(task2.getStatus().isOnInProgressState());
        assertTrue(task2.hasProjectCollaborator(projectCollaborator1));

        // When a collaborator send a request to mark task as completed
        assertTrue(taskProjectCollaboratorService.sendCompletedRequest(taskProjectCollaboratorRestDTO2));

        // Then the task receive the request and save the project collaborator email

        String result = taskService.findTaskByID(task2.getId()).getRequestTaskCompleted();
        String expected = projectCollaborator1.getUser().getEmail();
        assertEquals(expected, result);

    }

    @Test
    @Transactional
    public void testSendCompletedRequestFail() {
        // Given an initiated task and a project collaborator not associated with that
        // task

        assertTrue(task2.getStatus().isOnInProgressState());
        assertFalse(task2.hasProjectCollaborator(projectCollaborator2));
        taskProjectCollaboratorRestDTO2.setEmail(projectCollaborator2.getUser().getEmail());

        // When a collaborator send a request to mark task as completed

        assertFalse(taskProjectCollaboratorService.sendCompletedRequest(taskProjectCollaboratorRestDTO2));

        // Then the task doesn't receive the request

        String result = taskService.findTaskByID(task2.getId()).getRequestTaskCompleted();
        String expected = null;
        assertEquals(expected, result);

    }

    /**
     * GIVEN: a task that doesn't exist
     * WHEN: we try to make a request to remove task from collaborator
     * THEN: we receive a taskProjectCollaboratorRestDTO with its taskID set to null, signifying a task
     * not found.
     */
    @Test
    @Transactional
    public void testRequestRemoveProjectCollaboratorFromTaskFailureTaskNull() {

        //Given
        taskProjectCollaboratorRestDTO.setTaskId(null);
        assertNull(taskProjectCollaboratorRestDTO.getTaskId());

        //When
        boolean result = taskProjectCollaboratorService.requestRemoveProjectCollaboratorFromTask(taskProjectCollaboratorRestDTO);

        //Then
        assertFalse(result);
    }

    /**
     * GIVEN: a projectCollaborator whose registry status is not ASSIGNMENTAPPROVED
     * WHEN: we try to make a request to remove task from collaborator
     * THEN: we can't remove him and get a false result
     */
    @Test
    @Transactional
    public void testRequestRemoveProjectCollaboratorFromTaskFailureRegistryStatusNotASSIGNMENTAPPROVED() {

        //Given
        RegistryStatus status = TaskCollaboratorRegistry.RegistryStatus.ASSIGNMENTCANCELLED;
        col2TaskRegistry.setRegistryStatus(status);

        //When
        boolean result = taskProjectCollaboratorService.requestRemoveProjectCollaboratorFromTask(taskProjectCollaboratorRestDTO);

        //Then
        assertFalse(result);
    }

    @Test
    @Transactional
    public void testRequestRemoveProjectCollaboratorFromTaskSuccess() {

        //Given
        TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO2 = new TaskProjectCollaboratorRestDTO();
        taskProjectCollaboratorRestDTO2.setProjectCollaboratorId(projectCollaborator2.getId());
        taskProjectCollaboratorRestDTO2.setTaskId(task.getId());
        task.addProjectCollaborator(projectCollaborator2);
        //When
        boolean result = taskProjectCollaboratorService.requestRemoveProjectCollaboratorFromTask(taskProjectCollaboratorRestDTO);

        //Then
        assertTrue(result);
    }

    @Test
    @Transactional
    public void testRequestRemoveProjectCollaboratorFromTaskFailNotSameStatus() {

        //Given
        TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO2 = new TaskProjectCollaboratorRestDTO();
        taskProjectCollaboratorRestDTO2.setProjectCollaboratorId(projectCollaborator2.getId());
        taskProjectCollaboratorRestDTO2.setTaskId(task.getId());
        task.addProjectCollaborator(projectCollaborator2);
        taskProjectCollaboratorService.getTaskCollaboratorRegistry(task, projectCollaborator2).setRegistryStatus(REMOVALREQUEST);

        //When
        boolean result = taskProjectCollaboratorService.requestRemoveProjectCollaboratorFromTask(taskProjectCollaboratorRestDTO);

        //Then
        assertFalse(result);
    }

    @Test
    @Transactional
    public void testAddReportRest2FSuccess() {
        CreateReportRestDTO createReportRestDTO = new CreateReportRestDTO();
        createReportRestDTO.setTaskID(task.getId());
        createReportRestDTO.setQuantity(5);
        createReportRestDTO.setEndDate(d1);
        createReportRestDTO.setStartDate(d2);
        createReportRestDTO.setProjectCollaboratorID(projectCollaborator1.getId());
        createReportRestDTO.setProjectCollaboratorEmail(userProjectCollaborator1.getEmail());
        task.addProjectCollaborator(projectCollaborator1);
        task.getLastTaskCollaboratorRegistryOf(projectCollaborator1).setCollaboratorAddedToTaskDate(d2.minusDays(4));
        assertTrue(task.hasProjectCollaborator(projectCollaborator1));
        assertTrue(taskProjectCollaboratorService.addReportRest2(createReportRestDTO));
    }

    @Test
    @Transactional
    public void testAddReportRest2FailTaskHasNoProjectCollaborator() {
        CreateReportRestDTO createReportRestDTO = new CreateReportRestDTO();
        createReportRestDTO.setTaskID(task.getId());
        createReportRestDTO.setQuantity(5);
        createReportRestDTO.setEndDate(d1);
        createReportRestDTO.setStartDate(d2);
        createReportRestDTO.setProjectCollaboratorID(projectCollaborator1.getId());
        createReportRestDTO.setProjectCollaboratorEmail(userProjectCollaborator1.getEmail());
        assertFalse(taskProjectCollaboratorService.addReportRest2(createReportRestDTO));
    }

    @Test
    @Transactional
    public void testAddReportRest2FailNotExistsTaskAndProjectCollaborator() {
        CreateReportRestDTO createReportRestDTO = new CreateReportRestDTO();
        createReportRestDTO.setTaskID(task.getId());
        createReportRestDTO.setQuantity(5);
        createReportRestDTO.setEndDate(d1);
        createReportRestDTO.setStartDate(d2);
        createReportRestDTO.setProjectCollaboratorID(projectCollaborator1.getId());
        createReportRestDTO.setProjectCollaboratorEmail("ds@mail.com");
        assertFalse(taskProjectCollaboratorService.addReportRest2(createReportRestDTO));
    }

    @Test
    @Transactional
    public void testListReportsByTaskAndCollaboratorSuccess() {
        List<ReportRestDTO> reports = new ArrayList<>();
        reports.add(task2.listAllReports().get(0).toDTO());
        assertEquals(reports, taskProjectCollaboratorService.listReportsByTaskAndCollaborator(task2.getId(), userProjectCollaborator1.getEmail()));
    }

    @Test
    @Transactional
    public void testListReportsByTaskAndCollaboratorFailEmailNotMatch() {
        List<Report> reports = new ArrayList<>();
        assertEquals(reports, taskProjectCollaboratorService.listReportsByTaskAndCollaborator(task2.getId(), userProjectCollaborator2.getEmail()));
    }

    @Test
    @Transactional
    public void testListReportsByCollaboratorSuccess() {
        List<ReportRestDTO> reports = new ArrayList<>();
        reports.add(task2.listAllReports().get(0).toDTO());
        assertEquals(reports, taskProjectCollaboratorService.listReportsByCollaborator(projectCollaborator1.getUser().getEmail()));
    }

    @Test
    @Transactional
    public void testListReportsByCollaboratorFailEmailDoesNotMatch() {
        List<ReportRestDTO> reports = new ArrayList<>();
        assertEquals(reports, taskProjectCollaboratorService.listReportsByCollaborator(projectCollaborator2.getUser().getEmail()));
    }

    @Test
    @Transactional
    public void testUs208EditReport2Success() {
        EditReportQuantityDTO editReportQuantityDTO = new EditReportQuantityDTO();
        editReportQuantityDTO.setTaskID(task2.getId());
        editReportQuantityDTO.setQuantity(6);
        editReportQuantityDTO.setCollabEmail(projectCollaborator1.getUser().getEmail());
        editReportQuantityDTO.setReportId(task2.getLastTaskCollaboratorRegistryOf(projectCollaborator1).getReportList().get(0).getCode());
        task2.addProjectCollaborator(projectCollaborator1);
        assertTrue(taskProjectCollaboratorService.us208EditReport2(editReportQuantityDTO));
    }

    @Test
    @Transactional
    public void testUs208EditReport2FailNoReportsAndReportNull() {
        EditReportQuantityDTO editReportQuantityDTO = new EditReportQuantityDTO();
        editReportQuantityDTO.setTaskID(task.getId());
        editReportQuantityDTO.setQuantity(6);
        editReportQuantityDTO.setCollabEmail(projectCollaborator1.getUser().getEmail());
        editReportQuantityDTO.setReportId("2");
        task.addProjectCollaborator(projectCollaborator1);
        assertFalse(taskProjectCollaboratorService.us208EditReport2(editReportQuantityDTO));
    }

    @Test
    @Transactional
    public void testUs208EditReport2FailTaskHasNoProjectCollaborator() {
        EditReportQuantityDTO editReportQuantityDTO = new EditReportQuantityDTO();
        editReportQuantityDTO.setTaskID(task.getId());
        editReportQuantityDTO.setQuantity(6);
        editReportQuantityDTO.setCollabEmail(projectCollaborator1.getUser().getEmail());
        editReportQuantityDTO.setReportId("2");
        assertFalse(taskProjectCollaboratorService.us208EditReport2(editReportQuantityDTO));
    }

    @Test
    @Transactional
    public void testUs208EditReport2FailNotExistsTaskAndProjectCollaborator() {
        EditReportQuantityDTO editReportQuantityDTO = new EditReportQuantityDTO();
        editReportQuantityDTO.setTaskID(task.getId());
        editReportQuantityDTO.setQuantity(6);
        editReportQuantityDTO.setCollabEmail("fail@mail.com");
        editReportQuantityDTO.setReportId("2");
        assertFalse(taskProjectCollaboratorService.us208EditReport2(editReportQuantityDTO));
    }

    @Test
    @Transactional
    public void testGetProjectCollaboratorSuccess() {
        assertEquals(projectCollaborator1, taskProjectCollaboratorService.getProjectCollaborator(project.getId(),
                projectCollaborator1.getUser().getEmail()));
    }

    @Test
    @Transactional
    public void testGetProjectCollaboratorFail() {
        String wrongMail = "jony@mail.com";
        assertNull(taskProjectCollaboratorService.getProjectCollaborator(project.getId(),
                wrongMail));
    }

    @Test
    @Transactional
    public void testGetProjectCollaboratorFail2() {
        Project otherProject = new Project("7", "name");
        assertNull(taskProjectCollaboratorService.getProjectCollaborator(otherProject.getId(),
                projectCollaborator1.getUser().getEmail()));
    }

    @Test
    @Transactional
    public void testRemoveProjectCollaboratorFromTaskRestSuccess() {
        TaskProjectCollaboratorRestDTO expected = new TaskProjectCollaboratorRestDTO();
        expected.setProjectCollaboratorId(projectCollaborator2.getId());
        expected.setTaskId("1-1");
        expected.setEmail(projectCollaborator2.getUser().getEmail());
        assertEquals(expected, taskProjectCollaboratorService.removeProjectCollaboratorFromTaskRest(taskProjectCollaboratorRestDTO, task.getId()));
    }

    @Test
    @Transactional
    public void testRemoveProjectCollaboratorFromTaskRestFailExistsTaskAndProjCollab() {
        TaskProjectCollaboratorRestDTO actual = new TaskProjectCollaboratorRestDTO();
        TaskProjectCollaboratorRestDTO expected = new TaskProjectCollaboratorRestDTO();

        actual.setProjectCollaboratorId(9);
        actual.setTaskId("fail");
        actual.setEmail("fail@fail.com");

        expected.setProjectCollaboratorId(9);
        expected.setTaskId("fail");
        assertEquals(expected, taskProjectCollaboratorService.removeProjectCollaboratorFromTaskRest(actual, task.getId()));
    }

    @Test
    @Transactional
    public void testRemoveProjectCollaboratorFromTaskRestFailTaskHasProjCollab() {
        TaskProjectCollaboratorRestDTO expected = new TaskProjectCollaboratorRestDTO();
        expected.setProjectCollaboratorId(projectCollaborator2.getId());
        expected.setTaskId("1-1");
        expected.setEmail(projectCollaborator2.getUser().getEmail());
        task.addProjectCollaborator(projectCollaborator2);
        assertEquals(expected, taskProjectCollaboratorService.removeProjectCollaboratorFromTaskRest(taskProjectCollaboratorRestDTO, task.getId()));
    }

}
