package project.model.unittests.taskstatetests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;

import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.task.taskstate.Suspended;
import project.model.task.taskstate.TaskState;
import project.model.user.User;


public class SuspendedTest {

   
    LocalDate birth;
    User user1;
    User user2;
    ProjectCollaborator projectCollaborator2;
    ProjectCollaborator projectManager;
    Task task1;
    Task task2;
    TaskState suspended;
    LocalDateTime d1;
    LocalDateTime d2;
    private Project project;
    

    @Before
    public void setUp() throws AddressException {
        birth = LocalDate.of(1999, 11, 11);

    	d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
		d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

		project = new Project("1", "SWitCH");
		task1 = new Task(project, "From Stupid to Solid");
		task2 = new Task(project, "Keep it Simple Stupid");
		task1.setPredictedDateOfStart(d1);
		task1.setPredictedDateOfConclusion(d2);
		task2.setTaskId("1-2");
		project.addTask(task1);
		project.addTask(task2);
		
		User user = new User("name", "phone", "email@mail.com", "tin", birth, "1", "street", "postalCode", "city",
				"country");
		projectCollaborator2 = new ProjectCollaborator(project, user, 10);
		project.addProjectCollaborator(projectCollaborator2);
		task1.addProjectCollaborator(projectCollaborator2);

		TaskCollaboratorRegistry col1TaskRegistry = task1
				.getTaskCollaboratorRegistryByID(task1.getId() + "-" + projectCollaborator2.getUser().getEmail());
		col1TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));

		task1.addReport(projectCollaborator2, 300, LocalDateTime.now().minusMonths(1),
				LocalDateTime.now().minusDays(25));

		task1.setEffectiveStartDate(d1);


        task1.removeProjectCollaborator(projectCollaborator2);
        suspended = new Suspended(task1);
    }

    @Test
    public void testSuspended() {
        assertTrue(suspended.isOnSuspendedState());
    }

    @Test
    public void testIsOnAssignedState() {
        assertFalse(task1.getTaskState().isOnAssignedState());
    }

    @Test
    public void testIsOnCompletedState() {

        assertFalse(task1.getTaskState().isOnCompletedState());
    }

    @Test
    public void testIsOnCancelledState() {

        assertFalse(task1.getTaskState().isOnCancelledState());
    }

    @Test
    public void testIsOnCreatedState() {

        assertFalse(task1.getTaskState().isOnCreatedState());
    }

    @Test
    public void testIsOnInProgressState() {

        assertFalse(task1.getTaskState().isOnInProgressState());
    }

    @Test
    public void testIsOnReadyToStartState() {

        assertFalse(task1.getTaskState().isOnReadyToStartState());
    }

    @Test
    public void testIsOnSuspendedState() {
        suspended.isValid();
        suspended.changeTo();

        assertTrue(task1.getTaskState().isOnSuspendedState());
    }

    @Test
    public void testIsOnPlannedState() {

        assertFalse(task1.getTaskState().isOnPlannedState());
    }

    @Test
    public void testIsValid() {

        assertTrue(task1.getEffectiveStartDate() != null);
        assertTrue(suspended.isValid());
    }

    @Test
    public void testIsValidNotValidStartDateNull() {

        task1.setEffectiveStartDate(null);
        assertTrue(task1.getEffectiveStartDate() == null);
        assertFalse(suspended.isValid());
    }

    @Test
    public void testIsValid2() {
        assertTrue(task1.addProjectCollaborator(projectCollaborator2));
        assertFalse(suspended.isValid());
    }

    @Test
    public void testChangeTo() {

        suspended.changeTo();
        assertTrue(task1.getTaskState().isOnSuspendedState());
    }

    @Test
    public void testChangeToCompleted() {

        task1.setTaskCompleted();
        assertTrue(task1.getTaskState().isOnCompletedState());
    }

    @Test
    public void testChangeToInProgress() {
    	
    	task1.addProjectCollaborator(projectCollaborator2);
        assertTrue(task1.getTaskState().isOnInProgressState());
    }

    @Test
    public void testChangeToCancelled() {

        task1.setTaskCancelled();
        assertTrue(task1.getTaskState().isOnCancelledState());
    }

    @Test
    public void testToString() {

        String expected = "Suspended";
        String result = suspended.toString();

        assertEquals(expected, result);
    }

    /**
	 * GIVEN: a task which is on Suspended state and a list of possible actions
	 * WHEN: we ask for the list of available actions,
	 * THEN: we obtain that list
	 */
	@Test
	public void testListAvailableActions() {
		//Given
		assertTrue(task1.getTaskState().isOnSuspendedState());
		String nameOfTaskState = task1.getTaskState().getClass().getSimpleName();
		List<String> expected = new ArrayList<>();
		expected.add(nameOfTaskState + ".requestAddProjectCollaborator");
		expected.add(nameOfTaskState + ".addProjectCollaborator");
		expected.add(nameOfTaskState + ".setTaskCancelled");	
		expected.add(nameOfTaskState + ".setTaskCompleted");
		
		//When
		List<String> result = task1.getTaskState().listAvailableActions();
		
		//Then
		assertEquals(expected, result);
	}
}
