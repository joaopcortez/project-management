package project.model.unittests.taskstatetests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.task.taskstate.InProgress;
import project.model.task.taskstate.TaskState;
import project.model.user.User;

public class InProgressTest {

	private Project project;
	private LocalDate birth;
	private User user;
	private ProjectCollaborator projectCollaborator;
	private Task task1;
	private Task task2;
	private TaskState inProgressState, inProgressState2;
	private LocalDateTime d1;
	private LocalDateTime d2;

	@Before
	public void setUp() throws AddressException {
		d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
		d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

		project = new Project("1", "SWitCH");
		task1 = new Task(project, "From Stupid to Solid");
		task2 = new Task(project, "Keep it Simple Stupid");
		project.addTask(task1);
		project.addTask(task2);
		user = new User("name", "phone", "email@mail.com", "tin", birth, "1", "street", "postalCode", "city",
				"country");
		projectCollaborator = new ProjectCollaborator(project, user, 10);
		project.addProjectCollaborator(projectCollaborator);
		task1.setPredictedDateOfStart(d1);
		task1.setPredictedDateOfConclusion(d2);
		task1.addProjectCollaborator(projectCollaborator);
		
		task2.setPredictedDateOfStart(d1);
		task2.setPredictedDateOfConclusion(d2);
		
		task2.setTaskId("1-2");

		TaskCollaboratorRegistry col1TaskRegistry = task1
				.getTaskCollaboratorRegistryByID(task1.getId() + "-" + projectCollaborator.getUser().getEmail());
		col1TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));

		inProgressState = new InProgress(task1);
		inProgressState2 = new InProgress(task2);
	}

	@After
	public void tearDown() {

		task1 = null;
	}

	@Test
	public void testInProgress() {

		assertTrue(inProgressState.isOnInProgressState());
	}

	@Test
	public void testIsOnAssignedState() {

		assertFalse(task1.getTaskState().isOnAssignedState());
	}

	@Test
	public void testIsOnCancelledState() {

		assertFalse(task1.getTaskState().isOnCancelledState());
	}

	@Test
	public void testIsOnCompletedState() {

		assertFalse(task1.getTaskState().isOnCompletedState());
	}

	@Test
	public void testIsOnInProgressState() {

		task1.addReport(projectCollaborator, 300, LocalDateTime.now().minusMonths(1),LocalDateTime.now().minusDays(25));
		
		assertTrue(task1.getTaskState().isOnInProgressState());
	}

	@Test
	public void testIsOnCreatedState() {

		assertFalse(task1.getTaskState().isOnCreatedState());
	}

	@Test
	public void testIsOnPlannedState() {

		assertFalse(task1.getTaskState().isOnPlannedState());
	}

	@Test
	public void testIsOnReadyToStartState() {
		
		task1.addReport(projectCollaborator, 300, LocalDateTime.now().minusMonths(1),LocalDateTime.now().minusDays(25));

		assertFalse(task1.getTaskState().isOnReadyToStartState());
	}

	@Test
	public void testIsOnSuspendedState() {

		assertFalse(task1.getTaskState().isOnSuspendedState());
	}

	@Test
	public void testIsValid() {
		
		task1.addReport(projectCollaborator, 300, LocalDateTime.now().minusMonths(1),LocalDateTime.now().minusDays(25));

		assertTrue(inProgressState.isValid());
	}
	
	@Test
	public void testIsValidFailurehasAssignedProjectCollaboratorNotNull() {
		
		task2.setTaskCompleted();
		assertFalse(inProgressState2.isValid());
	}
	
	@Test
	public void testIsValidFailureTaskEffectiveDateOfConclusionNotNull() {
		
		task1.setEffectiveDateOfConclusion(d2);
		assertFalse(inProgressState.isValid());
	}

	@Test
	public void testIsValidFailureTaskEffectiveDateOfConclusionNull() {
		
		task1.setTaskCompleted();
		task1.setEffectiveStartDate(d1);
		task1.setEffectiveDateOfConclusion(null);
		assertTrue(inProgressState.isValid());
	}
	
	@Test
	public void testIsValidFailureTaskEffectiveDateOfStartNull() {

		task1.setTaskCompleted();
		task1.setEffectiveStartDate(null);
		assertFalse(inProgressState.isValid());
	}

	@Test
	public void testIsValidFailureTaskCannotStartDueToDependency() {

		task1.setEffectiveStartDate(null);
		task1.addTaskDependency(task2);

		assertFalse(inProgressState.isValid());
	}

	@Test
	public void testChangeTo() {

		task1.addReport(projectCollaborator, 300, LocalDateTime.now().minusMonths(1),LocalDateTime.now().minusDays(25));
		inProgressState.changeTo();
		assertTrue(task1.getTaskState().isOnInProgressState());
	}

	@Test
	public void testChangeToSuspended() {

		task1.addReport(projectCollaborator, 4, LocalDateTime.now().minusMonths(5), LocalDateTime.now().minusDays(25));
		task1.removeProjectCollaborator(projectCollaborator);
		assertTrue(task1.getTaskState().isOnSuspendedState());
	}

	@Test
	public void testChangeToCancelled() {

		task1.addReport(projectCollaborator, 300, LocalDateTime.now().minusMonths(1),LocalDateTime.now().minusDays(25));
		assertTrue(task1.getTaskState().isOnInProgressState());
		task1.setTaskCancelled();
		assertTrue(task1.getTaskState().isOnCancelledState());
	}

	@Test
	public void testChangeToCompleted() {

		task1.addReport(projectCollaborator, 300, LocalDateTime.now().minusMonths(1),LocalDateTime.now().minusDays(25));
		task1.setTaskCompleted();
		assertTrue(task1.getTaskState().isOnCompletedState());
	}

	@Test
	public void testToString() {

		String expected = "InProgress";
		String result = inProgressState.toString();
		assertEquals(expected, result);
	}
	
	/**
	 * GIVEN: a task which is on InProgress state and a list of possible actions
	 * WHEN: we ask for the list of available actions,
	 * THEN: we obtain that list
	 */
	@Test
	public void testListAvailableActions() {
		//Given
		task1.addReport(projectCollaborator, 5, LocalDateTime.now().minusDays(300), LocalDateTime.now().minusDays(290));
		assertTrue(task1.getTaskState().isOnInProgressState());
		String nameOfTaskState = task1.getTaskState().getClass().getSimpleName();
		List<String> expected = new ArrayList<>();
		expected.add(nameOfTaskState + ".requestAddProjectCollaborator");
		expected.add(nameOfTaskState + ".addProjectCollaborator");
		expected.add(nameOfTaskState + ".requestRemoveProjectCollaborator");
		expected.add(nameOfTaskState + ".removeProjectCollaborator");
		expected.add(nameOfTaskState + ".addReport");
		expected.add(nameOfTaskState + ".setTaskCancelled");	
		expected.add(nameOfTaskState + ".requestTaskCompleted");
		expected.add(nameOfTaskState + ".setTaskCompleted");
		
		//When
		List<String> result = task1.getTaskState().listAvailableActions();
		
		//Then
		assertEquals(expected, result);
	}
}
