package project.model.unittests;

import org.junit.Before;
import org.junit.Test;
import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ProjectCollaboratorCostAndTimePeriodTest {

    // ProjectCollaborators Cost and Time Period
    ProjectCollaboratorCostAndTimePeriod projCollabCostForPeriod1;
    ProjectCollaboratorCostAndTimePeriod projCollabCostForPeriod2;
    ProjectCollaboratorCostAndTimePeriod projCollabCostForPeriod3;

    @Before
    public void setUp() {
        projCollabCostForPeriod1 = new ProjectCollaboratorCostAndTimePeriod(10.0);
        projCollabCostForPeriod2 = new ProjectCollaboratorCostAndTimePeriod(10.0);
        projCollabCostForPeriod3 = new ProjectCollaboratorCostAndTimePeriod(20.0);
    }

    @Test
    public void getSetStartDateTest() {
        // Given
        assertEquals(LocalDate.now(), projCollabCostForPeriod1.getStartDate());
        // When
        projCollabCostForPeriod1.setStartDate(LocalDate.now().minusDays(4));
        // Then
        assertEquals(LocalDate.now().minusDays(4), projCollabCostForPeriod1.getStartDate());
    }

    @Test
    public void getSetEndDateTest() {
        // Given
        assertNull(projCollabCostForPeriod1.getEndDate());
        // When
        projCollabCostForPeriod1.setEndDate(LocalDate.now().minusDays(2));
        // Then
        assertEquals(LocalDate.now().minusDays(2), projCollabCostForPeriod1.getEndDate());
    }

    @Test
    public void getSetCostTest() {
        // Given
        assertEquals(10.0, projCollabCostForPeriod1.getCost(), 0.2);
        // When
        projCollabCostForPeriod1.setCost(20.0);
        // Then
        assertEquals(20.0, projCollabCostForPeriod1.getCost(), 0.2);
    }
}
