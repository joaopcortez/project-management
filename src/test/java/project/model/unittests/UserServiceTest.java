package project.model.unittests;

import jparepositoriestest.UserRepositoryClass;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import project.dto.rest.LoadUserDataDTO;
import project.dto.rest.LoginRestDTO;
import project.dto.rest.UserRegistryRestDTO;
import project.dto.rest.UserRestDTO;
import project.model.UserService;
import project.model.user.User;
import project.model.user.UserIdVO;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import system.dto.LoginTokenDTO;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.*;

public class UserServiceTest {

    UserService userService;
    UserRepositoryClass userRepositoryClass;

    User user1;
    User user2;
    User user3;
    User user4;
    User user5;
    User user6;
    User user7;

    LocalDate birth1;
    LocalDate birth2;
    LocalDate birth3;
    LocalDate birth4;

    LocalDateTime approvedDateTime;

    List<User> expectedUserList;
    LoginRestDTO loginRestDTO;
    LoginTokenDTO loginTokenDTOExpected;
    LoginTokenDTO loginTokenDTOActual;
    String confirmationCode;
    UserIdVO userIdVO;

    Role roleCollaborator;

    @Before
    public void initialize() throws AddressException {
        userRepositoryClass = new UserRepositoryClass();
        userService = new UserService(userRepositoryClass);

        Role roleUser = new Role(RoleName.ROLE_REGISTEREDUSER);
        roleCollaborator = new Role(RoleName.ROLE_COLLABORATOR);

        birth1 = LocalDate.of(1999, 11, 11);
        birth2 = LocalDate.of(1999, 12, 12);
        birth3 = LocalDate.of(1999, 10, 11);
        birth4 = LocalDate.of(1999, 10, 11);

        userService.addUser("Pedro", "919999999", "pedro@gmail.com", "332432", birth1, "2", "Rua ", "4433", "cidade",
                "país");
        userService.addUser("Joana", "929999999", "joana@gmail.com", "432893", birth2, "2", "Rua ", "4433", "cidade",
                "país");
        userService.addUser("Catarina", "12", "ac@bb.com", "159753", birth3, "2", "Rua ", "4433", "cidade", "país");
        userService.addUser("Alexandra", "243", "a4@bb.com", "1553", birth4, "2", "Rua ", "4433", "cidade", "país");
        userService.addUser("Josefina", "91 231 67 84", "josefina@switch.com", "207 207 207", LocalDate.now(), "casa", "rua", "cp", "cidade",
                "pais");

        user1 = userService.searchUserByEmail("pedro@gmail.com");
        user2 = userService.searchUserByEmail("joana@gmail.com");
        user3 = userService.searchUserByEmail("ac@bb.com");
        user4 = userService.searchUserByEmail("a4@bb.com");
        user5 = userService.searchUserByEmail("josefina@switch.com");

        userService.updateUser(user1);
        expectedUserList = new ArrayList<>();

        loginRestDTO = new LoginRestDTO();

        for (User u : userService.listAllUsers()) {
            u.getRoles().add(roleUser);
            userService.updateUser(u);

        }

    }

    // list all users test

    @Test
    @Transactional
    public void testListAllUsersEquals1() throws AddressException {
        expectedUserList.add(user1);
        expectedUserList.add(user2);
        expectedUserList.add(user3);
        expectedUserList.add(user4);
        expectedUserList.add(user5);

        assertThat(expectedUserList, containsInAnyOrder(userService.listAllUsers().toArray()));
    }

    @Test
    @Transactional
    public void testListAllUsersNotEquals() throws AddressException {
        expectedUserList.add(user1);
        expectedUserList.add(user2);

        assertNotEquals(expectedUserList, userService.listAllUsers());
    }

    @Test
    @Transactional
    public void testSearchUserByEmailSucess() throws AddressException {

        assertEquals(user1, userService.searchUserByEmail("pedro@gmail.com"));
    }

    @Test
    @Transactional
    public void testSearchUserByEmailNotFound() throws AddressException {

        assertNull(userService.searchUserByEmail("ghr@hgd.com"));
    }

    @Test
    @Transactional
    public void testAddUserFailureAlreadyRegistered() throws AddressException {

        assertTrue(userService.addUser("Pedro", "919999999", "zemanel@gmail.com", "332432", birth1, "2", "Rua ", "4433",
                "cidade", "país"));
        assertFalse(userService.addUser("Manel", "919123456", "zemanel@gmail.com", "33632", birth2, "2", "Rua ", "4433",
                "cidade", "país"));
    }

    // Tests to validate addUser method
    @Test
    @Transactional
    public void testAddUserSuccess() throws AddressException {
        assertTrue(userService.addUser("Pedro", "919999999", "asdrubal@gmail.com", "332432", birth1, "2", "Rua ", "4433",
                "cidade", "país"));
    }

    @Test
    @Transactional
    public void testAddUserFail() throws AddressException {

        User userInvalid = new User(null, "919999999", "asdrubal@gmail.com", "332432", birth1, "2", "Rua ", "",
                "cidade", "país");
        assertFalse(userService.addUser(userInvalid));
    }

    @Test
    @Transactional
    public void testAddUserFail2() throws AddressException {

        assertFalse(userService.addUser(null, "919999999", "asdrubal@gmail.com", "332432", birth1, "2", "Rua ", "",
                "cidade", "país"));
    }

    @Test
    @Transactional
    public void testAddUserSimpleConstructorFailureAlreadyRegistered() throws AddressException {

        User userFail = new User("Joana", "929999999", "gertulio@gmail.com", "432893", birth2, "2", "Rua ", "4433", "cidade",
                "país");
        assertTrue(userService.addUser(userFail));

        assertFalse(userService.addUser(userFail));
    }

    @Test
    @Transactional
    public void testAddUserDTO() throws AddressException {
        //GIVEN: A user that not exist on application
        userIdVO = UserIdVO.create("luis@switch.com");
        assertFalse(userRepositoryClass.existsById(userIdVO));

        //WHEN: Add data to user above to do registration and add user on application

        UserRegistryRestDTO userDTO = new UserRegistryRestDTO();
        userDTO.setName("Luis");
        userDTO.setEmail("luis@switch.com");
        userDTO.setPhone("123456789");
        userDTO.setTin("123456789");
        userDTO.setBirthDate(LocalDate.of(1977, 3, 5));
        userDTO.setAddressId("1");
        userDTO.setStreet("Rua sem saida");
        userDTO.setPostalCode("3700");
        userDTO.setCity("Porto");
        userDTO.setCountry("Portugal");
        userDTO.setPassword("12345");

        UserRegistryRestDTO result = userService.addUserDTO(userDTO);

        //THEN: The new user was added with success

        UserRegistryRestDTO expected = userDTO;

        assertEquals(expected, result);
        assertTrue(userRepositoryClass.existsById(userIdVO));
        assertEquals(userService.searchUserByEmail("luis@switch.com").validatePassword("12345").getMessage(), "[" + RoleName.ROLE_REGISTEREDUSER.toString() + "]" + " " + "Luis" + " SUCCESSFULLY LOGGED");

    }

    @Test
    @Transactional
    public void testAddUserDTOUserAlreadyExist() throws AddressException {
        //GIVEN: A user that not exist on application
        userIdVO = UserIdVO.create("pedro@gmail.com");
        assertTrue(userRepositoryClass.existsById(userIdVO));
        //WHEN: Add data to user above to do registration and add user on application

        UserRegistryRestDTO userDTO = new UserRegistryRestDTO();
        userDTO.setName("Pedro");
        userDTO.setEmail("pedro@gmail.com");
        userDTO.setPhone("123456789");
        userDTO.setTin("123456789");
        userDTO.setBirthDate(LocalDate.of(1977, 3, 5));
        userDTO.setAddressId("1");
        userDTO.setStreet("Rua sem saida");
        userDTO.setPostalCode("3700");
        userDTO.setCity("Porto");
        userDTO.setCountry("Portugal");

        UserRegistryRestDTO result = userService.addUserDTO(userDTO);

        //THEN: The new user was added with success

        UserRegistryRestDTO expected = null;

        assertEquals(expected, result);
        assertTrue(userRepositoryClass.existsById(userIdVO));
    }

    @Test
    @Transactional
    public void testAddUserDTOInvalidEmail() throws AddressException {
        //GIVEN: A user that not exist on application
        userIdVO = UserIdVO.create("rita@gmail.com");
        assertFalse(userRepositoryClass.existsById(userIdVO));

        //WHEN: Add data to user above to do registration and add user on application

        UserRegistryRestDTO userDTO = new UserRegistryRestDTO();
        userDTO.setName(null);
        userDTO.setEmail("rita@gmail.com");
        userDTO.setPhone("123456789");
        userDTO.setTin("123456789");
        userDTO.setBirthDate(LocalDate.of(1977, 3, 5));
        userDTO.setAddressId("1");
        userDTO.setStreet("Rua sem saida");
        userDTO.setPostalCode("3700");
        userDTO.setCity("Porto");
        userDTO.setCountry("Portugal");

        UserRegistryRestDTO result = userService.addUserDTO(userDTO);

        //THEN: The new user was added with success

        UserRegistryRestDTO expected = null;

        assertEquals(expected, result);
        assertFalse(userRepositoryClass.existsById(userIdVO));
    }

    @Test
    @Transactional
    public void testSearchUsersByProfileDifferentProfiles() {

        userService.setUserProfileDirector(user1);
        userService.setUserProfileDirector(user2);

        expectedUserList.add(user1);
        expectedUserList.add(user2);
        List<User> result = userService.searchUserByProfile(new Role(RoleName.ROLE_DIRECTOR));

        assertThat(expectedUserList, containsInAnyOrder(result.toArray()));
    }

    // Test set user profile to director

    @Test
    @Transactional
    public void testSetUserProfileDirectorSuccess() {

        String expected = "User with the email " + user1.getEmail() + " has been set as ROLE_DIRECTOR profile.";
        String result = userService.setUserProfileDirector(user1);

        assertEquals(expected, result);
        assertTrue(user1.getRoles().contains(new Role(RoleName.ROLE_DIRECTOR)));
    }

    @Test
    @Transactional
    public void testSetUserProfileDirectorFailUserNull() {
        String expected = "User with the email " + user1.getEmail() + " has been set as ROLE_DIRECTOR profile.";
        String result = userService.setUserProfileDirector(user1);

        assertEquals(expected, result);
        assertTrue(user1.getRoles().contains(new Role(RoleName.ROLE_DIRECTOR)));
    }

    @Test
    @Transactional
    public void testSetUserProfileDirectorFailureNotInList() throws AddressException {

        User user5 = new User("Pedro", "919999999", "zemanel@gmail.com", "332432", birth1, "2", "Rua ", "4433",
                "cidade", "país");
        String expected = "User could not be set to ROLE_DIRECTOR profile because he does not exist in the company's user registry.";

        assertEquals(expected, userService.setUserProfileDirector(user5));
    }

    @Test
    @Transactional
    public void testSetUserProfileDirectorFailureAlreadyDirector() {
        userService.setUserProfileDirector(user1);

        String expected = "This user has already the ROLE_DIRECTOR profile.";
        userService.setUserProfileDirector(user1);

        assertTrue(user1.hasRoleDirectorr());
        String result = userService.setUserProfileDirector(user1);

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testSetUserProfileDirectorFailureUserNull() {

        User user5 = null;
        String result = userService.setUserProfileDirector(user5);
        String expected = "User could not be set to ROLE_DIRECTOR profile because he does not exist in the company's user registry.";

        assertEquals(expected, result);
    }

    // Test set user profile to collaborator

    @Test
    @Transactional
    public void testSetUserProfileCollaboratorSuccess() {
        String expected = "User with the email " + user1.getEmail() + " has been set as ROLE_COLLABORATOR profile.";

        user1.getRoles().remove(roleCollaborator);
        assertEquals(expected, userService.setUserProfileCollaborator(user1));

    }

    @Test
    @Transactional
    public void testSetUserProfileCollaboratorFailureNotInList() throws AddressException {

        User user5 = new User("Pedro", "919999999", "zemanel@gmail.com", "332432", birth1, "2", "Rua ", "4433",
                "cidade", "país");

        String expected = "User could not be set to ROLE_COLLABORATOR profile because he does not exist in the company's user registry.";

        assertEquals(expected, userService.setUserProfileCollaborator(user5));
    }

    @Test
    @Transactional
    public void testSetUserProfileCollaboratorFailureAlreadyCollaborator() {

        String expected = "This user has already the ROLE_COLLABORATOR profile.";
        userService.setUserProfileCollaborator(userService.searchUserByEmail(user1.getEmail()));

        assertTrue(userService.searchUserByEmail(user1.getEmail()).getRoles().contains(new Role(RoleName.ROLE_COLLABORATOR)));
        String result = userService.setUserProfileCollaborator(userService.searchUserByEmail(user1.getEmail()));

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testSetUserProfileCollaboratorFailureUserNull() {

        User user5 = null;
        String result = userService.setUserProfileCollaborator(user5);
        String expected = "User could not be set to ROLE_COLLABORATOR profile because he does not exist in the company's user registry.";

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testSetUserInactive() {
        userService.updateUser(user1);
        assertTrue(userService.setUserInactive(user1));

    }

    @Test
    @Transactional
    public void testSetUserInactiveFailNotInRepo() throws AddressException {

        User user5 = new User("Jorge", "919999999", "jorge@gmail.com", "332432", birth1, "2", "Rua ", "4433", "cidade",
                "país");
        assertFalse(userService.setUserInactive(user5));
    }

    @Test
    @Transactional
    public void testSetUserInactiveFailNotActive() {

        userService.setUserInactive(user1);
        assertFalse(userService.setUserInactive(user1));
    }

    @Test
    @Transactional
    public void testSetUserActive() {
        //Given
        userService.setUserInactive(user1);

        //When
        boolean result = userService.setUserActive(user1);

        //Then
        assertTrue(result);
        assertTrue(user1.isActive());

    }

    @Test
    @Transactional
    public void testSetUserActiveFailNotInRepo() throws AddressException {
        User user5 = new User("Jorge", "919999999", "jorge@gmail.com", "332432", birth1, "2", "Rua ", "4433", "cidade",
                "país");
        assertFalse(userService.setUserActive(user5));

    }

    @Test
    @Transactional
    public void testSetUserActiveFailNotActive() {
        userService.setUserActive(user1);
        assertFalse(userService.setUserActive(user1));

    }

    @Test
    @Transactional
    public void testSearchUsersByProfileDirectorSuccess() {

        userService.setUserProfileDirector(user1);
        userService.setUserProfileDirector(user3);

        expectedUserList.add(user1);
        expectedUserList.add(user3);

        //FIXME  List<User> result = userService.searchUserByProfile(User.UserProfiles.DIRECTOR);

        //FIXME  assertThat(expectedUserList, containsInAnyOrder(result.toArray()));
    }

    @Test
    @Transactional
    public void testSearchUsersByProfileNoDirectorElements() {

        assertEquals(expectedUserList, userService.searchUserByProfile(new Role(RoleName.ROLE_DIRECTOR)));
    }

    @Test
    @Transactional
    public void testCreateUser() throws AddressException {

        User u1 = userService.createUser("pedro", "919999999", "pedro@gmail.com", "332432", birth1, "2", "Rua ", "4433", "cidade",
                "país");
        User u2 = userService.createUser("joao", "919760409", "joao@gmail.com", "332567", birth2, "2", "Rua ", "4433", "cidade",
                "país");

        assertNotEquals(u2, u1);

    }

    @Test
    @Transactional
    public void testValidateDataSuccessUserFoundInUsersList() {

        user1.setPassword("ss");
        userService.addUser(user1);

        String expResult = "Invalid Email or Password";
        LoginTokenDTO result = userService.validateData("pedro@gmail.com", "SWitCH");

        assertTrue(expResult.equals(result.getMessage()));
    }

    @Test
    @Transactional
    public void testValidateDataNoUserWithThatEmailInUsersList() {

        String expResult = "Invalid Email or Password";
        LoginTokenDTO result = userService.validateData("sergio@isep", "SWitCH");

        assertTrue(expResult.equals(result.getMessage()));
    }

    @Test
    @Transactional
    public void testValidateDataRestNoUserWithThatEmailInUsersList() {

        LoginRestDTO loginRestDTO = new LoginRestDTO();
        String password = "12345";
        user1.setPassword(password);
        loginRestDTO.setEmail("theresNoOne@withThis.email");
        loginRestDTO.setPassword(password);

        assertNull(userService.validateDataRest(loginRestDTO));

    }

    @Test
    @Transactional
    public void testValidateDataRestEmailNull() {

        loginRestDTO.setEmail(null);
        loginRestDTO.setPassword("12345");
        loginRestDTO.setConfirmationCode("54321");

        assertNull(userService.validateDataRest(loginRestDTO));

    }

    @Test
    @Transactional
    public void testValidateDataRestPasswordNull() {

        loginRestDTO.setEmail("pedro@gmail.com");
        loginRestDTO.setPassword(null);
        loginRestDTO.setConfirmationCode("54321");

        assertNull(userService.validateDataRest(loginRestDTO));

    }

    @Test
    @Transactional
    public void testValidateDataRestWrongPassword() {

        LoginRestDTO loginRestDTO = new LoginRestDTO();
        String password = "12345";
        user1.setPassword(password);
        loginRestDTO.setEmail(user1.getEmail());
        loginRestDTO.setPassword("this pass is incorrect");

        assertNull(userService.validateDataRest(loginRestDTO));

    }

    @Test
    @Transactional
    public void validateDataFirstLoginSuccess() {
        //GIVEN
        LocalDateTime localDateTimeExpected;
        user5.setPassword("12345");

        user5.getUserLoginData().setConfirmationToken("54321");
        userService.updateUser(user5);

        //WHEN
        loginTokenDTOActual = userService.validateDataFirstLogin("josefina@switch.com", "12345", "54321");

        localDateTimeExpected = loginTokenDTOActual.getApprovedDateTime();
        LoginTokenDTO loginTokenDTOExpected = new LoginTokenDTO("Josefina", "josefina@switch.com", "[ROLE_REGISTEREDUSER]", true, localDateTimeExpected, localDateTimeExpected, "54321", "[ROLE_REGISTEREDUSER] Josefina ACCOUNT ACTIVATED, SUCCESSFULLY LOGGED");

        //THEN
        assertEquals(loginTokenDTOExpected.toString(), loginTokenDTOActual.toString());
        assertTrue(user5.isActive());

    }

    @Test
    @Transactional
    public void testValidateDataRest() {
        LoginRestDTO loginRestDTO = new LoginRestDTO();
        String password = "12345";
        user1.setPassword(password);
        loginRestDTO.setEmail(user1.getEmail());
        loginRestDTO.setPassword(password);

        //FIXME   LoginTokenDTO loginTokenDTOexpected = new LoginTokenDTO(user1.getName(), user1.getEmail(), user1.getProfile().toString(), user1.isActive(), user1.getUserLoginData().getApprovedDateTime(),
        //FIXME       user1.getUserLoginData().getLastLoginDate(), user1.getUserLoginData().getConfirmationToken(),
        //FIXME       user1.getProfile().toString() + " " + user1.getName() + " SUCCESSFULLY LOGGED");

        assertNotNull(userService.validateDataRest(loginRestDTO));
        //FIXME assertEquals(loginTokenDTOexpected, userService.validateDataRest(loginRestDTO));
    }

    @Test
    @Transactional
    public void testUpdateUser() throws AddressException {

        userService.addUser("Miguel", "99999999", "miguel@gmail.com", "23456", birth1, "6", "rua", "5555555", "porto", "portugal");
        User user5 = userService.searchUserByEmail("miguel@gmail.com");
        user5.setPhone("88888888");
        boolean result = userService.updateUser(user5);

        assertTrue(result);
        assertEquals("88888888", userService.searchUserByEmail("miguel@gmail.com").getPhone());
    }

    @Test
    @Transactional
    public void testUpdateUserDoenstExistRepo() throws AddressException {

        User user5 = new User("Miguel", "99999999", "miguel@gmail.com", "23456", birth1, "6", "rua", "5555555", "porto", "portugal");
        assertFalse(userService.updateUser(user5));
    }

    @Test
    @Transactional
    public void testUpdateUserInvalid() throws AddressException {

        userService.addUser("Pedro", "99999999", "miguel@gmail.com", "23456", birth1, "6", "rua", "5555555", "porto", "portugal");
        User user5 = userService.searchUserByEmail("miguel@gmail.com");
        user5.setName("");
        assertFalse(userService.updateUser(user5));
    }

    @Test
    @Transactional
    public void testSearchUserDTObyEmailTrueCase() {

        UserRestDTO result = userService.searchUserDTOByEmail("joana@gmail.com");
        UserRestDTO expected = userService.searchUserByEmail("joana@gmail.com").toDTO();

        assertEquals(result, expected);

    }

    @Test
    @Transactional
    public void testSearchUserDTObyEmailNullCase() {

        UserRestDTO result = userService.searchUserDTOByEmail("joa@hotmail.com");
        UserRestDTO expected = null;

        assertEquals(result, expected);

    }

    @Test
    @Transactional
    public void testActivateAccountSuccess() {
        /*
         GIVEN: An LoginRestDTO with data user data of a inactivate account (user, password, codeConfirmation).
         WHEN: We post these LoginRestDTO in HTTP client.
         THEN We get HttpStatus.OK and the DTO with information.
         */

        //GIVEN:
        user5 = userService.searchUserByEmail("josefina@switch.com");
        user5.setPassword("12345");
        confirmationCode = user5.getUserLoginData().getConfirmationToken();
        userService.updateUser(user5);

        loginRestDTO.setConfirmationCode(confirmationCode);
        loginRestDTO.setEmail("josefina@switch.com");
        loginRestDTO.setPassword("12345");

        //WHEN
        loginTokenDTOActual = userService.activateAccount(loginRestDTO);
        user5 = userService.searchUserByEmail("josefina@switch.com");

        approvedDateTime = user5.getUserLoginData().getApprovedDateTime();
        loginTokenDTOExpected = new LoginTokenDTO("Josefina", "josefina@switch.com", "REGISTEREDUSER", true, approvedDateTime,
                approvedDateTime, confirmationCode,
                user5.getRoles().toString() + " " + "Josefina" + " ACCOUNT ACTIVATED, SUCCESSFULLY LOGGED");

        //THEN
        assertEquals(loginTokenDTOExpected, loginTokenDTOActual);
        assertTrue(user5.isActive());
        assertNotNull(user5.getUserLoginData().getLastLoginDate());
        assertNotNull(user5.getUserLoginData().getApprovedDateTime());
        assertNotNull(user5.getUserLoginData().getConfirmationToken());
        assertTrue(user5.isActive());

    }

    @Test
    @Transactional
    public void testActivateAccountFailNullEmail() {
        /*
         GIVEN: An LoginRestDTO with data user data of a inactivate account (user, password, codeConfirmation).
         WHEN: We post these LoginRestDTO in HTTP client.
         THEN We get HttpStatus.OK and the DTO with information.
         */

        //GIVEN:
        user5 = userService.searchUserByEmail("josefina@switch.com");
        user5.setPassword("12345");
        confirmationCode = user5.getUserLoginData().getConfirmationToken();
        userService.updateUser(user5);

        loginRestDTO.setConfirmationCode(confirmationCode);
        loginRestDTO.setEmail(null);
        loginRestDTO.setPassword("12345");

        //WHEN
        loginTokenDTOActual = userService.activateAccount(loginRestDTO);
        loginTokenDTOExpected = new LoginTokenDTO("Invalid Data introduced!", 1);

        //THEN
        assertEquals(loginTokenDTOExpected, loginTokenDTOActual);

    }

    @Test
    @Transactional
    public void testActivateAccountFailNullPassword() {
        /*
         GIVEN: An LoginRestDTO with data user data of a inactivate account (user, password, codeConfirmation).
         WHEN: We post these LoginRestDTO in HTTP client.
         THEN We get HttpStatus.OK and the DTO with information.
         */

        //GIVEN:
        user5 = userService.searchUserByEmail("josefina@switch.com");
        user5.setPassword("12345");
        confirmationCode = user5.getUserLoginData().getConfirmationToken();
        userService.updateUser(user5);

        loginRestDTO.setConfirmationCode(confirmationCode);
        loginRestDTO.setEmail("josefina@switch.com");
        loginRestDTO.setPassword(null);

        //WHEN
        loginTokenDTOActual = userService.activateAccount(loginRestDTO);
        loginTokenDTOExpected = new LoginTokenDTO("Invalid Data introduced!", 1);

        //THEN
        assertEquals(loginTokenDTOExpected, loginTokenDTOActual);

    }

    @Test
    @Transactional
    public void testActivateAccountFailNullConfirmationCode() {
        /*
         GIVEN: An LoginRestDTO with data user data of a inactivate account (user, password, codeConfirmation).
         WHEN: We post these LoginRestDTO in HTTP client.
         THEN We get HttpStatus.OK and the DTO with information.
         */

        //GIVEN:
        user5 = userService.searchUserByEmail("josefina@switch.com");
        user5.setPassword("12345");
        confirmationCode = user5.getUserLoginData().getConfirmationToken();
        userService.updateUser(user5);

        loginRestDTO.setConfirmationCode(null);
        loginRestDTO.setEmail("josefina@switch.com");
        loginRestDTO.setPassword("12345");

        //WHEN
        loginTokenDTOActual = userService.activateAccount(loginRestDTO);
        loginTokenDTOExpected = new LoginTokenDTO("Invalid Data introduced!", 1);

        //THEN
        assertEquals(loginTokenDTOExpected, loginTokenDTOActual);

    }

    @Test
    @Transactional
    public void testActivateAccountFailWrongPassword() {

        /*
        GIVEN: An LoginRestDTO with wrong password.
        WHEN: We post these LoginRestDTO in HTTP client.
        THEN We get HttpStatus.BAD_REQUEST and the DTO with message information.
         */

        //GIVEN:

        user5.setPassword("12345");
        confirmationCode = user5.getUserLoginData().getConfirmationToken();
        loginRestDTO.setConfirmationCode(confirmationCode);
        loginRestDTO.setEmail("josefina@switch.com");
        loginRestDTO.setPassword("1234");
        approvedDateTime = user5.getUserLoginData().getApprovedDateTime();
        loginTokenDTOExpected = new LoginTokenDTO("Invalid Data introduced!", 1);

        //WHEN
        loginTokenDTOActual = userService.activateAccount(loginRestDTO);

        //THEN
        assertEquals(loginTokenDTOExpected, loginTokenDTOActual);
    }

    @Test
    @Transactional
    public void testActivateAccountFailWrongEmail() {
        /*
        GIVEN: An LoginRestDTO with wrong email.
        WHEN: We post these LoginRestDTO in HTTP client.
        THEN We get HttpStatus.BAD_REQUEST and the DTO with message information.
         */

        //GIVEN:
        user5.setPassword("12345");
        confirmationCode = user5.getUserLoginData().getConfirmationToken();

        loginRestDTO.setConfirmationCode(confirmationCode);
        loginRestDTO.setEmail("XXXX@gmail.com");
        loginRestDTO.setPassword("12345");
        userService.addUser(user5);
        loginTokenDTOExpected = new LoginTokenDTO("Invalid Email or Password", 1);

        //WHEN
        loginTokenDTOActual = userService.activateAccount(loginRestDTO);

        //THEN
        assertEquals(loginTokenDTOExpected, loginTokenDTOActual);
    }

    @Test
    @Transactional
    public void testActivateAccountFailNotFirstLogin() {
        /*
        GIVEN: An LoginRestDTO with activated user.
        WHEN: We post these LoginRestDTO in HTTP client.
        THEN We get HttpStatus.BAD_REQUEST and the DTO with message information.
         */

        //GIVEN:
        user5.setPassword("12345");
        confirmationCode = user5.getUserLoginData().getConfirmationToken();

        loginRestDTO.setConfirmationCode(confirmationCode);
        loginRestDTO.setEmail("josefina@switch.com");
        loginRestDTO.setPassword("12345");
        userService.updateUser(user5);
        approvedDateTime = user5.getUserLoginData().getApprovedDateTime();
        loginTokenDTOExpected = new LoginTokenDTO("Account already activated, log in with the same credentials", 2);

        //WHEN
        userService.activateAccount(loginRestDTO);
        loginTokenDTOActual = userService.activateAccount(loginRestDTO);

        //THEN
        assertEquals(loginTokenDTOExpected, loginTokenDTOActual);

    }

    @Test
    @Transactional
    public void setProfileDirectorToSuccess() {
        /*
        GIVEN: An activated user in company.
        WHEN: set Profile DirectorTo.
        THEN true, is assigned director.
         */

        //GIVEN:
        user1.setActive();

        //WHEN
        boolean result = userService.setProfileDirectorTo(user1);

        //THEN
        assertTrue(user1.hasRoleDirectorr());
        assertTrue(result);
        //FIXME assertEquals(User.UserProfiles.DIRECTOR, user1.getProfile());
    }

    @Test
    @Transactional
    public void setProfileDirectorToFail() throws AddressException {
        /*
        GIVEN: An activated user in company.
        WHEN: set Profile DirectorTo.
        THEN true, is assigned director.
         */

        //GIVEN:
        User userTest = new User("Pedro", "919999999", "zemaneltratas@gmail.com", "332432", birth1, "2", "Rua ", "4433", "cidade",
                "país");
        //WHEN
        boolean result = userService.setProfileDirectorTo(userTest);

        //THEN
        assertFalse(result);
        assertFalse(user1.hasRoleDirectorr());

    }

    @Test
    @Transactional
    public void setProfileCollaboratorToSuccess() {
        /*
        GIVEN: An activated user in company.
        WHEN: set Profile DirectorTo.
        THEN true, is assigned director.
         */

        //GIVEN:
        user1.setActive();

        //WHEN
        boolean result = userService.setProfileCollaboratorTo(user1);

        //THEN
        assertTrue(result);
        assertTrue(user1.getRoles().contains(new Role(RoleName.ROLE_COLLABORATOR)));
    }

    @Test
    @Transactional
    public void setProfileCollaboratorToFail() throws AddressException {
        /*
        GIVEN: An activated user in company.
        WHEN: set Profile DirectorTo.
        THEN true, is assigned director.
         */

        //GIVEN:
        User userTest = new User("Pedro", "919999999", "zemaneltratas@gmail.com", "332432", birth1, "2", "Rua ", "4433", "cidade",
                "país");
        //WHEN
        boolean result = userService.setProfileCollaboratorTo((userTest));

        //THEN
        assertFalse(result);
        assertFalse(user1.hasRoleCollaborator());

    }

    @Test
    @Transactional
    public void usersPopulateDatabaseFromFileTest() throws Exception {
        String filename = "{\n " + "\t\"inputFile\" : \"firstUsers.xml\"\n" + "}";
        LoadUserDataDTO loadUserDataDTO = new LoadUserDataDTO();
        JSONObject obj = new JSONObject(filename);
        loadUserDataDTO.setFileName(obj.getString("inputFile"));
        List<User> result;
        result = userService.usersPopulateDatabaseFromFile(loadUserDataDTO.getFileName());

        user6 = userService.searchUserByEmail("administrator@mymail.com");
        user7 = userService.searchUserByEmail("director@mymail.com");

        List<User> expected = new ArrayList<>();
        expected.add(user1);
        expected.add(user2);
        expected.add(user3);
        expected.add(user4);
        expected.add(user5);
        expected.add(user6);
        expected.add(user7);

        assertTrue(result.get(2).hasRoleCollaborator());
        assertEquals(expected, result);
    }

    @Test
    public void profileToRoleFromDatabase() throws Exception {

        String filename = "{\n " + "\t\"inputFile\" : \"firstUsers.xml\"\n" + "}";
        LoadUserDataDTO loadUserDataDTO = new LoadUserDataDTO();
        JSONObject obj = new JSONObject(filename);
        loadUserDataDTO.setFileName(obj.getString("inputFile"));

        userService.usersPopulateDatabaseFromFile(loadUserDataDTO.getFileName());

        User user = userService.searchUserByEmail("administrator@mymail.com");
        assertTrue(user.hasRoleCollaborator());

    }
}
