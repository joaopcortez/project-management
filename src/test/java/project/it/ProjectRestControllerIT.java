package project.it;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import project.dto.rest.ProjectDTO;
import project.jparepositories.ProjectRepository;
import project.jparepositories.RoleRepository;
import project.jparepositories.UserRepository;
import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.payload.JwtAuthenticationResponse;
import project.payload.LoginRequest;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class ProjectRestControllerIT {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	UserRepository userRepository;
	@Autowired
	ProjectRepository projectRepository;

	User user;
	Project project1;
	Project project2;
	ProjectCollaborator projCollab1;
	ProjectCollaborator projCollab2;

	@Autowired
	PasswordEncoder passwordEncoder;
	@Autowired
	RoleRepository roleRepository;

	private JwtAuthenticationResponse jwt;

	@Before
	public void setUp() throws Exception {
		
		projectRepository.deleteAll();
		
		Role rolesDirector = roleRepository.getOneByName(RoleName.ROLE_DIRECTOR);

		if (!roleRepository.existsByName(RoleName.ROLE_DIRECTOR)) {
			rolesDirector = new Role(RoleName.ROLE_DIRECTOR);
			roleRepository.save(rolesDirector);
		}

		user = new User("Ana", "123", "asdrubal@jj.com", "2345", LocalDate.of(2018, 5, 31), "2", "Rua ", "4433",
				"cidade", "país");

		String pass = passwordEncoder.encode("12345");
		user.setPassword(pass);

		user.setRoles(Collections.singleton(rolesDirector));
		user.setActive();
		userRepository.save(user);

		project1 = new Project("1", "switch1");
		projectRepository.save(project1);

		project2 = new Project("2", "switch2");
		projectRepository.save(project2);

		projCollab1 = new ProjectCollaborator(project1, user, 5);
		projCollab2 = new ProjectCollaborator(project2, user, 5);

		project1.addProjectCollaborator(projCollab1);
		project1.setProjectManager(projCollab1);

		project2.addProjectCollaborator(projCollab2);
		project2.setProjectManager(projCollab2);

		projectRepository.save(project1);
		projectRepository.save(project2);

		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("asdrubal@jj.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();
	}

	/**
	 * US320
	 * 
	 * GIVEN all of the company's projects WHEN we ask for all active projects using
	 * appropriate URI THEN we get an HTTP status of OK and a body with the active
	 * projects.
	 */
	@Test
	public void testListProjectsByActiveStatusActiveTrue() {

		// Given
		ResponseEntity<List<ProjectDTO>> result = null;
		boolean isActive = true;
		List<ProjectDTO> expectedList = new ArrayList<>();
		project2.setInactive();
		projectRepository.save(project2);
		expectedList.add(project1.toDTO());

		// When
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		
		result = restTemplate.exchange("http://localhost:" + port + "/projects?isActive=" + isActive, HttpMethod.GET,
				entity, new ParameterizedTypeReference<List<ProjectDTO>>() {
				});

		// Then
		assertEquals(HttpStatus.OK, result.getStatusCode());
		assertEquals(expectedList.toString(), result.getBody().toString());
	}

	/**
	 * US320
	 * 
	 * GIVEN all of the company's projects WHEN we ask for all inactive projects
	 * using appropriate URI THEN we get an HTTP status of OK and a body with the
	 * inactive projects.
	 */
	// @Test
	// public void testListProjectsByActiveStatusActiveFalse() {
	//
	// //Given
	// ResponseEntity<List<ProjectDTO>> result = null;
	// boolean isActive = false;
	// project2.setInactive();
	// projectRepository.save(project2);
	// List<ProjectDTO> expectedList = new ArrayList<>();
	// expectedList.add(project2.toDTO());
	//
	// //When
	// result = restTemplate.exchange("http://localhost:" + port +
	// "/projects?isActive=" + isActive,
	// HttpMethod.GET, null, new ParameterizedTypeReference<List<ProjectDTO>>() {});
	//
	// //Then
	// assertEquals(HttpStatus.OK, result.getStatusCode());
	// assertEquals(expectedList.toString(), result.getBody().toString());
	// }

	/**
	 * US320
	 * 
	 * GIVEN all of the company's projects (they're all active) WHEN we ask for all
	 * inactive projects using appropriate URI THEN we get an HTTP status of OK and
	 * an empty body.
	 */
	@Test
	public void testListProjectsByActiveStatusEmptyList() {

		// Given
		ResponseEntity<List<ProjectDTO>> result = null;
		boolean isActive = false;

		// When
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		
		result = restTemplate.exchange("http://localhost:" + port + "/projects?isActive=" + isActive, HttpMethod.GET,
				entity, new ParameterizedTypeReference<List<ProjectDTO>>() {
				});

		// Then
		assertEquals(HttpStatus.OK, result.getStatusCode());
		assertTrue(result.getBody().isEmpty());
	}

}