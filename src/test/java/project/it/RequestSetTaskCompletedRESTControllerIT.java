package project.it;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import project.dto.rest.TaskProjectCollaboratorRestDTO;
import project.dto.rest.TaskRestDTO;
import project.jparepositories.RoleRepository;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.payload.JwtAuthenticationResponse;
import project.payload.LoginRequest;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class RequestSetTaskCompletedRESTControllerIT {

	User user1;
	User user2;
	@Autowired
	ProjectCollaboratorService projectCollaboratorService;
	Project project;
	Task task1, task2;
	ProjectCollaborator col1, col2;
	@LocalServerPort
	private int port;
	@Autowired
	private TestRestTemplate restTemplate;
	@Autowired
	private ProjectService projectService;
	@Autowired
	private TaskService taskService;
	@Autowired
	private UserService userService;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	BCryptPasswordEncoder passwordEncoder;
	JwtAuthenticationResponse jwt;
	
	@Before
	public void setUp() throws AddressException {

		Role rolesCollaborator = roleRepository.getOneByName(RoleName.ROLE_COLLABORATOR);

		if (!roleRepository.existsByName(RoleName.ROLE_COLLABORATOR)) {
			rolesCollaborator = new Role(RoleName.ROLE_COLLABORATOR);
			roleRepository.save(rolesCollaborator);
		}

		userService.addUser("Asdrubal", "123456789", "asdrubal@jj.com", "123456789", LocalDate.of(1977, 3, 5), "1",
				"Rua sem saída", "4250-357", "Porto", "Portugal");
		userService.addUser("Manel", "123456789", "manel@jj.com", "123456789", LocalDate.of(1978, 11, 12), "2",
				"Rua sem saída", "4250-357", "Porto", "Portugal");

		user1 = userService.searchUserByEmail("asdrubal@jj.com");
		user2 = userService.searchUserByEmail("manel@jj.com");
		
		String pass = passwordEncoder.encode("12345");

		user1.setPassword(pass);
		user2.setPassword(pass);
		user1.setRoles(Collections.singleton(rolesCollaborator));
		user2.setRoles(Collections.singleton(rolesCollaborator));
		
		user1.setRoles(Collections.singleton(rolesCollaborator));
		user2.setRoles(Collections.singleton(rolesCollaborator));
		user1.setActive();
		user2.setActive();

		userService.updateUser(user1);
		userService.updateUser(user2);

		projectService.addProject("1", "Project1");
		project = projectService.getProjectByID("1");

		projectCollaboratorService.setProjectManager(project, user1);
		projectService.updateProject(project);
		projectCollaboratorService.addProjectCollaborator(project, user2, 6);

		col1 = projectCollaboratorService.getProjectCollaborator("1", "asdrubal@jj.com");
		col2 = projectCollaboratorService.getProjectCollaborator("1", "manel@jj.com");

		taskService.addTask(project, "task1");
		taskService.addTask(project, "task2");

		task1 = taskService.findTaskByID("1-1");
		task2 = taskService.findTaskByID("1-2");

		taskService.addProjectCollaboratorToTask(col2, task1);

		projectCollaboratorService.updateProjectCollaborator(col2);

		taskService.updateTask(task1);
		taskService.updateTask(task2);

		TaskCollaboratorRegistry colTaskRegistry = task1
				.getTaskCollaboratorRegistryByID(task1.getId() + "-" + col2.getUser().getEmail());
		colTaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));

		task1.addReport(col2, 3, LocalDateTime.now().minusDays(10), LocalDateTime.now().minusDays(2));
		taskService.updateTask(task1);
		
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("manel@jj.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();

	}

	/**
	 * US250
	 * <p>
	 * GIVEN a task in progress with a collaborator associated WHEN the collaborator
	 * send a request for set the task completed, using the appropriate URI THEN we
	 * get an HTTP status of OK and the task save the collaborator email .
	 */

	@Test
	public void requestCompletedTaskFromCollaborator() {
		// GIVEN

		String taskId = task1.getId();
		assertTrue(task1.getStatus().isOnInProgressState());
		assertTrue(task1.hasProjectCollaborator(col2));
		TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO = new TaskProjectCollaboratorRestDTO();
		taskProjectCollaboratorRestDTO.setProjectCollaboratorId(col2.getId());
		taskProjectCollaboratorRestDTO.setEmail(col2.getUser().getEmail());

		Map<String, String> uriParams = new HashMap<String, String>();
		uriParams.put("id", task1.getId());

		// WHEN

		String url = "http://localhost:" + port + "tasks/" + taskId + "/request-completed";
		String requestBody = "{\"email\":\"" + taskProjectCollaboratorRestDTO.getEmail()
				+ "\"}";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);
		
		ResponseEntity<TaskRestDTO> responsePut = restTemplate.exchange(url, HttpMethod.PUT, entity, TaskRestDTO.class,
				taskId);

		ResponseEntity<TaskRestDTO> responseGet = restTemplate.exchange("/tasks/{id}", HttpMethod.GET, entity, TaskRestDTO.class,
				uriParams);

		// THEN

		assertThat(responsePut.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertTrue(responsePut.getBody().getCompletedRequest().equals(col2.getUser().getEmail()));
		assertThat(responseGet.getStatusCode()).isEqualTo(HttpStatus.OK);

		assertTrue(responseGet.getBody().getCompletedRequest().equals(col2.getUser().getEmail()));
	}

	/**
	 * US250
	 * <p>
	 * GIVEN a task not in progress and a collaborator that is not associated with
	 * that task WHEN the collaborator send a request for set the task completed,
	 * using the appropriate URI THEN we get an HTTP status of FORBIDDEN and the
	 * task don't save the collaborator email .
	 */
	@Test
	public void requestCompletedTaskFromCollaboratorFail() {
		// GIVEN
		String taskId2 = task2.getId();
		assertFalse(task2.getStatus().isOnInProgressState());
		assertFalse(task2.hasProjectCollaborator(col2));
		TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO = new TaskProjectCollaboratorRestDTO();
		taskProjectCollaboratorRestDTO.setProjectCollaboratorId(col2.getId());
		taskProjectCollaboratorRestDTO.setEmail(col2.getUser().getEmail());

		Map<String, String> uriParams = new HashMap<String, String>();
		uriParams.put("id", task2.getId());

		// WHEN

		String url = "http://localhost:" + port + "tasks/" + taskId2 + "/request-completed";
		String requestBody = "{\"projectCollaboratorId\":\"" + taskProjectCollaboratorRestDTO.getProjectCollaboratorId()
				+ "\"}";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);
		ResponseEntity<TaskRestDTO> responsePut = restTemplate.exchange(url, HttpMethod.PUT, entity, TaskRestDTO.class,
				taskId2);

		ResponseEntity<TaskRestDTO> responseGet = restTemplate.exchange("/tasks/{id}", HttpMethod.GET, entity, TaskRestDTO.class,
				uriParams);

		// THEN

		assertThat(responsePut.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
		assertThat(responseGet.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertNull(responseGet.getBody().getCompletedRequest());
	}

}
