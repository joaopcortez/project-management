package project.it;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import project.controllers.rest.AddOrRemoveProjectCollaboratorControllerREST;
import project.dto.rest.ProjectCollaboratorRestDTO;
import project.jparepositories.RoleRepository;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.payload.JwtAuthenticationResponse;
import project.payload.LoginRequest;
import project.services.ProjectCollaboratorService;
import project.services.ProjectUserService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class AddOrRemoveProjectCollaboratorRESTControllerIT {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private ProjectService projectService;
	@Autowired
	private ProjectCollaboratorService projectCollaboratorService;
	@Autowired
	private UserService userService;
	@Autowired
	private ProjectUserService projectUserService;

	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	BCryptPasswordEncoder passwordEncoder;

	AddOrRemoveProjectCollaboratorControllerREST addOrRemoveProjectCollaboratorRESTControllerTest;

	LocalDate birth;

	User userProjectManager, userDirector;
	User userProjectCollaborator, userProjectCollaborator2, userProjectCollaborator3;
	ProjectCollaborator projectCollaborator, projectCollaborator2, projectCollaborator3, projectManager,
			projectDirector;
	Project project;
	String projectId;

	private JwtAuthenticationResponse jwt;
	
	@Before
	public void setUp() throws AddressException {

		Role rolesAdmin = roleRepository.getOneByName(RoleName.ROLE_ADMINISTRATOR);
		Role rolesUser = roleRepository.getOneByName(RoleName.ROLE_REGISTEREDUSER);
		Role rolesDirector = roleRepository.getOneByName(RoleName.ROLE_DIRECTOR);
		Role rolesCollaborator = roleRepository.getOneByName(RoleName.ROLE_COLLABORATOR);

		if (!roleRepository.existsByName(RoleName.ROLE_ADMINISTRATOR)) {
			rolesAdmin = new Role(RoleName.ROLE_ADMINISTRATOR);
			roleRepository.save(rolesAdmin);
		}
		if (!roleRepository.existsByName(RoleName.ROLE_REGISTEREDUSER)) {
			rolesUser = new Role(RoleName.ROLE_REGISTEREDUSER);
			roleRepository.save(rolesUser);
		}

		if (!roleRepository.existsByName(RoleName.ROLE_DIRECTOR)) {
			rolesDirector = new Role(RoleName.ROLE_DIRECTOR);
			roleRepository.save(rolesDirector);
		}
		if (!roleRepository.existsByName(RoleName.ROLE_COLLABORATOR)) {
			rolesCollaborator = new Role(RoleName.ROLE_COLLABORATOR);
			roleRepository.save(rolesCollaborator);
		}

		addOrRemoveProjectCollaboratorRESTControllerTest = new AddOrRemoveProjectCollaboratorControllerREST(
				projectUserService);

		birth = LocalDate.of(1999, 11, 11);

		userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "5555555", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Manuel", "96 452 56 56", "manuel@gmail.com", "114622", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Josefina", "96 452 56 56", "josefina@gmail.com", "114622", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Bebiana", "96 452 56 56", "bebiana@gmail.com", "114622", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		
		String pass = passwordEncoder.encode("12345");

		userProjectManager = userService.searchUserByEmail("asdrubal@jj.com");
		userProjectCollaborator = userService.searchUserByEmail("joaquim@gmail.com");
		userProjectCollaborator2 = userService.searchUserByEmail("manuel@gmail.com");
		userProjectCollaborator3 = userService.searchUserByEmail("josefina@gmail.com");
		userDirector = userService.searchUserByEmail("bebiana@gmail.com");

		userProjectCollaborator.setPassword(pass);
		userProjectCollaborator2.setPassword(pass);
		userProjectCollaborator3.setPassword(pass);
		userProjectManager.setPassword(pass);
		userDirector.setPassword(pass);
		
		userProjectManager.setRoles(Collections.singleton(rolesCollaborator));
		userDirector.setRoles(Collections.singleton(rolesDirector));
		userProjectCollaborator.setRoles(Collections.singleton(rolesCollaborator));
		userProjectCollaborator2.setRoles(Collections.singleton(rolesCollaborator));
		userProjectCollaborator3.setInactive();
		
		
		 userService.updateUser(userProjectManager);
		 userService.updateUser(userDirector);
		 userService.updateUser(userProjectCollaborator);
		 userService.updateUser(userProjectCollaborator2);
		 userService.updateUser(userProjectCollaborator3);

		projectService.addProject("1", "Project 1");
		project = projectService.getProjectByID("1");
		projectCollaboratorService.setProjectManager(project, userProjectManager);
		projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator, 2);

		projectManager = project.findProjectCollaborator(userProjectManager);
		projectCollaborator = project.findProjectCollaborator(userProjectCollaborator);
		projectCollaborator2 = project.findProjectCollaborator(userProjectCollaborator2);
		projectCollaborator3 = project.findProjectCollaborator(userProjectCollaborator3);

		projectService.updateProject(project);

		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("asdrubal@jj.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();
		
		
	}

	/**
	 * GIVEN: A project without a project collaborator(projectCollaborator2) WHEN:
	 * The project collaborator(projectCollaborator2) was added to the project THEN:
	 * We get an HTTP status of CREATED, the project collaborator was added to the
	 * project collaborator list
	 * 
	 */

	@Test
	public void testAddCollaboratorToProjectSucess() {
		// Given
		ProjectCollaboratorRestDTO projectCollaboratorRestDTO = new ProjectCollaboratorRestDTO();
		projectCollaboratorRestDTO.setUserId(userProjectCollaborator2.getEmail());
		projectCollaboratorRestDTO.setCost(2.5);
		projectId = project.getId();
		projectCollaboratorRestDTO.setProjectId(projectId);

		// When
		
		Map<String, String> uriParams = new HashMap<String, String>();
		uriParams.put("id", project.getId());
		
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer "+jwt.getAccessToken());
        HttpEntity<ProjectCollaboratorRestDTO> entity = new HttpEntity<>(projectCollaboratorRestDTO, headers);
        
		ResponseEntity<ProjectCollaboratorRestDTO> response = restTemplate.exchange("/projects/{id}", HttpMethod.POST,
						entity, ProjectCollaboratorRestDTO.class, uriParams);

		// Then
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
	}

	/**
	 * GIVEN: A project with a project collaborator WHEN: The same project
	 * collaborator was added again to the project THEN: We get an HTTP status of
	 * BAD_REQUEST, the project collaborator wan't added to the project collaborator
	 * list
	 */

	@Test
	public void testAddCollaboratorToProjectFail() {

		// Given
		ProjectCollaboratorRestDTO projectCollaboratorRestDTO = new ProjectCollaboratorRestDTO();
		projectCollaboratorRestDTO.setUserId(userProjectCollaborator.getEmail());
		projectCollaboratorRestDTO.setCost(2.5);
		projectId = project.getId();

		// When

		Map<String, String> uriParams = new HashMap<String, String>();
		uriParams.put("id", project.getId());
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer "+jwt.getAccessToken());
        HttpEntity<ProjectCollaboratorRestDTO> entity = new HttpEntity<>(projectCollaboratorRestDTO, headers);
        
		ResponseEntity<ProjectCollaboratorRestDTO> response = restTemplate.exchange("/projects/{id}", HttpMethod.POST,
				entity, ProjectCollaboratorRestDTO.class, uriParams);

		// Then
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
	}

}
