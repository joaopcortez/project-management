package project.it;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.AddressException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import project.controllers.rest.AddOrRemoveTaskRESTController;
import project.dto.rest.TaskRestDTO;
import project.jparepositories.RoleRepository;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.payload.JwtAuthenticationResponse;
import project.payload.LoginRequest;
import project.security.UserPrincipal;
import project.services.ProjectCollaboratorService;
import project.services.TaskProjectService;
import project.services.TaskService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class AddOrRemoveTaskRESTControllerIT {

	ProjectCollaborator col1, col2;
	User user1, user2;
	AddOrRemoveTaskRESTController addOrRemoveTaskRESTController;
	@Autowired
	private TestRestTemplate restTemplate;
	@Autowired
	private ProjectService projectService;
	@Autowired
	private TaskService taskService;
	@Autowired
	private TaskProjectService taskProjectService;
	@Autowired
	private UserService userService;
	@Autowired
	private ProjectCollaboratorService projectCollaboratorService;
	private Project project;
	private Task task1;
	private Task task2;
	private Task task3;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	BCryptPasswordEncoder passwordEncoder;
	JwtAuthenticationResponse jwt;

	UserPrincipal userPrincipal;

	@Before
	public void setUp() throws AddressException {

		Role rolesAdmin = roleRepository.getOneByName(RoleName.ROLE_ADMINISTRATOR);
		Role rolesUser = roleRepository.getOneByName(RoleName.ROLE_REGISTEREDUSER);
		Role rolesDirector = roleRepository.getOneByName(RoleName.ROLE_DIRECTOR);
		Role rolesCollaborator = roleRepository.getOneByName(RoleName.ROLE_COLLABORATOR);

		if (!roleRepository.existsByName(RoleName.ROLE_ADMINISTRATOR)) {
			rolesAdmin = new Role(RoleName.ROLE_ADMINISTRATOR);
			roleRepository.save(rolesAdmin);
		}
		if (!roleRepository.existsByName(RoleName.ROLE_REGISTEREDUSER)) {
			rolesUser = new Role(RoleName.ROLE_REGISTEREDUSER);
			roleRepository.save(rolesUser);
		}

		if (!roleRepository.existsByName(RoleName.ROLE_DIRECTOR)) {
			rolesDirector = new Role(RoleName.ROLE_DIRECTOR);
			roleRepository.save(rolesDirector);
		}
		if (!roleRepository.existsByName(RoleName.ROLE_COLLABORATOR)) {
			rolesCollaborator = new Role(RoleName.ROLE_COLLABORATOR);
			roleRepository.save(rolesCollaborator);
		}

		addOrRemoveTaskRESTController = new AddOrRemoveTaskRESTController(taskService, taskProjectService);
		userService.addUser("Asdrubal", "123456789", "asdrubal@jj.com", "123456789", LocalDate.of(1977, 3, 5), "1",
				"Rua sem saída", "4250-357", "Porto", "Portugal");
		userService.addUser("Manel", "123456789", "manel@jj.com", "123456789", LocalDate.of(1978, 11, 12), "2",
				"Rua sem saída", "4250-357", "Porto", "Portugal");

		user1 = userService.searchUserByEmail("asdrubal@jj.com");
		user2 = userService.searchUserByEmail("manel@jj.com");

		String pass = passwordEncoder.encode("12345");

		user1.setPassword(pass);
		user2.setPassword(pass);

		user1.setRoles(Collections.singleton(rolesCollaborator));
		user2.setRoles(Collections.singleton(rolesCollaborator));
		user1.setActive();
		user2.setActive();

		List<String> listProjectWhereProjectManager = new ArrayList<>();
		List<String> listTasksWhereIsProjectManager = new ArrayList<>();
		List<String> listTasksWhereIsTaskCollaborator = new ArrayList<>();
		userPrincipal = UserPrincipal.create(user1, listProjectWhereProjectManager, listTasksWhereIsTaskCollaborator,
				listTasksWhereIsProjectManager);

		userService.updateUser(user1);
		userService.updateUser(user2);

		projectService.addProject("1", "Project1");
		project = projectService.getProjectByID("1");

		projectCollaboratorService.setProjectManager(project, user1);
		projectService.updateProject(project);
		projectCollaboratorService.addProjectCollaborator(project, user2, 6);

		col1 = projectCollaboratorService.getProjectCollaborator("1", "asdrubal@jj.com");
		col2 = projectCollaboratorService.getProjectCollaborator("1", "manel@jj.com");

		taskService.addTask(project, "task1");
		taskService.addTask(project, "task2");
		taskService.addTask(project, "task3");

		task1 = taskService.findTaskByID("1-1");
		task2 = taskService.findTaskByID("1-2");
		task3 = taskService.findTaskByID("1-3");

		taskService.addProjectCollaboratorToTask(col2, task2);

		taskService.updateTask(task1);
		taskService.updateTask(task2);
		taskService.updateTask(task3);

		TaskCollaboratorRegistry colTaskRegistry = task2
				.getTaskCollaboratorRegistryByID(task2.getId() + "-" + col2.getUser().getEmail());
		colTaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));

		task2.addReport(col2, 3, LocalDateTime.now().minusDays(10), LocalDateTime.now().minusDays(2));
		taskService.updateTask(task2);

		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("asdrubal@jj.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();
	}

	@After
	public void tearDown() {
		projectService.deleteAll();
		taskService.deleteAll();
	}

	/**
	 * US345
	 * <p>
	 * GIVEN: A project with three tasks not initiated and in Planned State WHEN:
	 * The project Manager remove a not initiated task named task1 THEN: We get an
	 * HTTP status of OK and the task was deleted and removed from the project task
	 * list
	 */

	@Test
	public void removeTask() {

		// given
		String projectid = "1";
		TaskRestDTO taskRestDTO = new TaskRestDTO();
		taskRestDTO.setTitle("task1");
		taskRestDTO.setProjectId(project.getId());
		taskRestDTO.setDateOfCreation(LocalDateTime.now());
		taskRestDTO.setTaskId("1-1");
		taskRestDTO.setState("PLANNED");

		// when
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<TaskRestDTO> entity = new HttpEntity<>(taskRestDTO, headers);

		ResponseEntity<String> responseDelete = restTemplate.exchange("/tasks/" + taskRestDTO.getTaskId(),
				HttpMethod.DELETE, entity, new ParameterizedTypeReference<String>() {
				});

		ResponseEntity<List<TaskRestDTO>> responseGet = restTemplate.exchange("/projects/" + projectid + "/tasks",
				HttpMethod.GET, entity, new ParameterizedTypeReference<List<TaskRestDTO>>() {
				});
		// then

		assertThat(responseDelete.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(responseGet.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertFalse(responseGet.getBody().contains(taskRestDTO));

	}

	/**
	 * US345
	 * <p>
	 * GIVEN: A project with three tasks and one of them is already initiated WHEN:
	 * The project Manager remove a initiated task named task1 THEN: We get an HTTP
	 * status of OK but the task could not be removed and is still on the project
	 * task list
	 */
	@Test
	public void removeTaskFail() {

		// given

		// assertTrue(task2.getStatus().isOnInProgressState());
		String projectid = "1";
		TaskRestDTO taskRestDTO = new TaskRestDTO();
		taskRestDTO.setTitle("task2");
		taskRestDTO.setProjectId(project.getId());
		taskRestDTO.setDateOfCreation(LocalDateTime.now());
		taskRestDTO.setTaskId("1-2");

		// when
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		ResponseEntity<String> responseDelete = restTemplate.exchange("/tasks/" + taskRestDTO.getTaskId(),
				HttpMethod.DELETE, entity, new ParameterizedTypeReference<String>() {
				});

		ResponseEntity<List<TaskRestDTO>> responseGet = restTemplate.exchange("/projects/" + projectid + "/tasks",
				HttpMethod.GET, entity, new ParameterizedTypeReference<List<TaskRestDTO>>() {
				});
		// then

		assertThat(responseDelete.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
		assertThat(responseGet.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertFalse(responseGet.getBody().toString().contains(taskRestDTO.toString()));

	}

	/**
	 * GIVEN: A project with 3 tasks WHEN: the method create task is run with valid
	 * project id data THEN: the task created in the created state
	 */
	@Test
	public void createTask() {

		// given

		// when
		TaskRestDTO taskRestDTO = new TaskRestDTO();
		taskRestDTO.setTitle("HATEOAS");
		taskRestDTO.setProjectId(project.getId());

		Map<String, String> uriParams = new HashMap<>();
		uriParams.put("id", project.getId());

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<TaskRestDTO> entity = new HttpEntity<>(taskRestDTO, headers);

		ResponseEntity<TaskRestDTO> response = restTemplate.exchange("/projects/{id}/tasks", HttpMethod.POST, entity,
				TaskRestDTO.class, uriParams);

		// then
		Task task = taskService.findTaskByID(response.getBody().getTaskId());

		TaskRestDTO taskRestDTOOut;
		taskRestDTOOut = task.toDTO();

		ResponseEntity<List<TaskRestDTO>> responseGet = restTemplate.exchange("/projects/" + project.getId() + "/tasks",
				HttpMethod.GET, entity, new ParameterizedTypeReference<List<TaskRestDTO>>() {
				});
		taskRestDTOOut.setDateOfCreation(taskRestDTOOut.getDateOfCreation().truncatedTo(ChronoUnit.SECONDS));

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		assertEquals(response.getBody().getTaskId(), (taskRestDTOOut.getTaskId()));
		assertNotNull(taskService.findTaskByID(response.getBody().getTaskId()));
		// assertEquals(responseGet.getBody().get(3).toString(),
		// taskRestDTOOut.toString());

	}

	/**
	 * GIVEN: A project with 3 tasks WHEN: the method create task is run with valid
	 * project id data THEN: the task created in the planned state
	 */
	@Test
	public void createTask_plannedState() {

		// given

		// when
		TaskRestDTO taskRestDTO = new TaskRestDTO();
		taskRestDTO.setTitle("HATEOAS");
		taskRestDTO.setProjectId(project.getId());
		taskRestDTO.setPredictedDateOfStart(LocalDateTime.of(2017, 10, 30, 0, 0));
		taskRestDTO.setPredictedDateOfConclusion(LocalDateTime.of(2017, 10, 30, 0, 0));

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<TaskRestDTO> entity = new HttpEntity<>(taskRestDTO, headers);

		Map<String, String> uriParams = new HashMap<String, String>();
		uriParams.put("id", project.getId());

		ResponseEntity<TaskRestDTO> response = restTemplate.exchange("/projects/{id}/tasks", HttpMethod.POST, entity,
				TaskRestDTO.class, uriParams);

		// then

		Task task = taskService.findTaskByID(response.getBody().getTaskId());

		TaskRestDTO taskRestDTOOut;
		taskRestDTOOut = task.toDTO();

		ResponseEntity<List<TaskRestDTO>> responseGet = restTemplate.exchange("/projects/" + project.getId() + "/tasks",
				HttpMethod.GET, entity, new ParameterizedTypeReference<List<TaskRestDTO>>() {
				});
		taskRestDTOOut.setDateOfCreation(taskRestDTOOut.getDateOfCreation().truncatedTo(ChronoUnit.SECONDS));

		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertEquals(taskRestDTOOut.getTaskId(), response.getBody().getTaskId());
		assertNotNull(taskService.findTaskByID(response.getBody().getTaskId()));
		// assertEquals(responseGet.getBody().get(3).toString(),
		// taskRestDTOOut.toString());

	}

	/**
	 * GIVEN: A project with 3 tasks WHEN: the method create task is run with
	 * invalid project id data THEN: the task is not created and the response is
	 * BAD_REQUEST
	 */
	@Test
	public void createTask_fail() {

		// given

		// when
		TaskRestDTO taskRestDTO = new TaskRestDTO();
		taskRestDTO.setTitle("HATEOAS");
		taskRestDTO.setProjectId("tobeornottobe");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<TaskRestDTO> entity = new HttpEntity<>(taskRestDTO, headers);

		Map<String, String> uriParams = new HashMap<>();
		uriParams.put("id", "2");
		ResponseEntity<TaskRestDTO> response = restTemplate.exchange("/projects/{id}/tasks", HttpMethod.POST, entity,
				TaskRestDTO.class, uriParams);

		// Then
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);

	}

	@Test
	public void getProjectTasks() {

		// Given
		String projectid = "1";
		List<String> expectedList = new ArrayList<>();
		
		expectedList.add(task3.getId());
		expectedList.add(task2.getId());
		expectedList.add(task1.getId());

		// When
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);

		ResponseEntity<List<TaskRestDTO>> result = restTemplate.exchange("/projects/" + projectid + "/tasks",
				HttpMethod.GET, entity, new ParameterizedTypeReference<List<TaskRestDTO>>() {
				});
		List<String> resultList = new ArrayList<>();
		for (TaskRestDTO taskDTO: result.getBody()) {
			resultList.add(taskDTO.getTaskId());
		}
		// Then
		assertEquals(HttpStatus.OK, result.getStatusCode());	
		for (String taskId: expectedList) {
			assertTrue(resultList.contains(taskId));
		}

	}
}