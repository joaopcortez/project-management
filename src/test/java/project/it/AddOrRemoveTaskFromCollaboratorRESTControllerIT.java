package project.it;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;
import project.controllers.rest.AddOrRemoveTaskFromCollaboratorRESTController;
import project.dto.rest.TaskProjectCollaboratorRestDTO;
import project.model.UserService;
import project.jparepositories.RoleRepository;
import project.payload.JwtAuthenticationResponse;
import project.payload.LoginRequest;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.model.task.Task;
import project.services.TaskService;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.services.TaskProjectCollaboratorService;

import javax.mail.internet.AddressException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class AddOrRemoveTaskFromCollaboratorRESTControllerIT {

	@Autowired
	private TestRestTemplate restTemplate;
	@LocalServerPort
	private int port;
	@Autowired
	RoleRepository roleRepository;
	@Autowired
	private TaskProjectCollaboratorService taskProjectCollaboratorService;
	@Autowired
	private UserService userService;
	@Autowired
	private ProjectService projectService;
	@Autowired
	private ProjectCollaboratorService projectCollaboratorService;
	@Autowired
	private TaskService taskService;

	AddOrRemoveTaskFromCollaboratorRESTController addOrRemoveTaskFromCollaboratorRESTController;

	LocalDate birth;

	User userProjectManager;
	User userProjectCollaborator, userProjectCollaborator2;
	ProjectCollaborator projectCollaborator, projectCollaborator2, projectManager;
	Project project;
	Task task;
	LocalDateTime d1;
	LocalDateTime d2;
	TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO;
	String taskId;
	
	@Autowired
	BCryptPasswordEncoder passwordEncoder;
	JwtAuthenticationResponse jwt;

	@Before
	public void setUp() throws AddressException {

		Role rolesAdmin = roleRepository.getOneByName(RoleName.ROLE_ADMINISTRATOR);
		Role rolesUser = roleRepository.getOneByName(RoleName.ROLE_REGISTEREDUSER);
		Role rolesDirector = roleRepository.getOneByName(RoleName.ROLE_DIRECTOR);
		Role rolesCollaborator = roleRepository.getOneByName(RoleName.ROLE_COLLABORATOR);

		if (!roleRepository.existsByName(RoleName.ROLE_ADMINISTRATOR)) {
			rolesAdmin = new Role(RoleName.ROLE_ADMINISTRATOR);
			roleRepository.save(rolesAdmin);
		}
		if (!roleRepository.existsByName(RoleName.ROLE_REGISTEREDUSER)) {
			rolesUser = new Role(RoleName.ROLE_REGISTEREDUSER);
			roleRepository.save(rolesUser);
		}

		if (!roleRepository.existsByName(RoleName.ROLE_DIRECTOR)) {
			rolesDirector = new Role(RoleName.ROLE_DIRECTOR);
			roleRepository.save(rolesDirector);
		}
		if (!roleRepository.existsByName(RoleName.ROLE_COLLABORATOR)) {
			rolesCollaborator = new Role(RoleName.ROLE_COLLABORATOR);
			roleRepository.save(rolesCollaborator);
		}

		addOrRemoveTaskFromCollaboratorRESTController = new AddOrRemoveTaskFromCollaboratorRESTController(
				taskProjectCollaboratorService);

		birth = LocalDate.of(1999, 11, 11);

		userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Josefina", "96 452 56 56", "josefina@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");

		String pass = passwordEncoder.encode("12345");
		
		userProjectManager = userService.searchUserByEmail("asdrubal@jj.com");
		userProjectCollaborator = userService.searchUserByEmail("joaquim@gmail.com");
		userProjectCollaborator2 = userService.searchUserByEmail("josefina@gmail.com");

		userProjectManager.setPassword(pass);
		userProjectCollaborator.setPassword(pass);
		userProjectCollaborator2.setPassword(pass);

		userProjectManager.setRoles(Collections.singleton(rolesCollaborator));
		userProjectCollaborator.setRoles(Collections.singleton(rolesCollaborator));
		userProjectCollaborator2.setRoles(Collections.singleton(rolesCollaborator));

		userService.updateUser(userProjectManager);
		userService.updateUser(userProjectCollaborator);
		userService.updateUser(userProjectCollaborator2);

		projectService.addProject("1", "Project 1");
		project = projectService.getProjectByID("1");
		projectCollaboratorService.setProjectManager(project, userProjectManager);
		projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator, 2);
		projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator2, 2);

		projectManager = project.findProjectCollaborator(userProjectManager);
		projectCollaborator = project.findProjectCollaborator(userProjectCollaborator);
		projectCollaborator2 = project.findProjectCollaborator(userProjectCollaborator2);

		d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
		d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

		taskService.addTask(project, "task", "task", d1, d2, 1.2, 1.3);
		task = taskService.findTaskByID("1-1");
		taskService.addProjectCollaboratorToTask(projectCollaborator2, task);

		userService.updateUser(userProjectManager);
		userService.updateUser(userProjectCollaborator);
		userService.updateUser(userProjectCollaborator2);

		projectService.updateProject(project);

		taskService.updateTask(task);

		projectCollaboratorService.updateProjectCollaborator(projectCollaborator);
		projectCollaboratorService.updateProjectCollaborator(projectCollaborator2);

		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("asdrubal@jj.com");
		loginRequest.setPassword("12345");
		
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();
	}

	@After
	public void tearDown() {
		projectService.deleteAll();
		taskService.deleteAll();
	}

	/**
	 * GIVEN: A task with a project collaborator WHEN: A project collaborator is
	 * remove from task THEN: We get an HTTP status of CREATED, the project
	 * collaborator was remove from the task active collaborator list
	 */

	@Test
	public void removeProjectCollaboratorFromTaskSucess() {

		// Given
		taskProjectCollaboratorRestDTO = new TaskProjectCollaboratorRestDTO();
		taskProjectCollaboratorRestDTO.setProjectCollaboratorId(projectCollaborator2.getId());
		taskProjectCollaboratorRestDTO.setEmail(projectCollaborator2.getUser().getEmail());
		taskId = task.getId();
		taskProjectCollaboratorRestDTO.setTaskId(taskId);

		// When
		Map<String, String> uriParams = new HashMap<String, String>();
		uriParams.put("id", task.getId());

		String url = "http://localhost:" + port + "/tasks/" + taskId;

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());

		HttpEntity<TaskProjectCollaboratorRestDTO> entity = new HttpEntity<>(taskProjectCollaboratorRestDTO, headers);
		ResponseEntity<String> responsePut = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, taskId);

		// Then
		assertThat(responsePut.getStatusCode()).isEqualTo(HttpStatus.CREATED);
	}

	/**
	 * GIVEN: A task without a project collaborator WHEN: A project collaborator was
	 * remove from task THEN: We get an HTTP status of BAD_REQUEST, the project
	 * collaborator can not be remove from the task active collaborator list
	 */

	@Test
	public void removeProjectCollaboratorFromTaskFail() {

		// Given
		taskProjectCollaboratorRestDTO = new TaskProjectCollaboratorRestDTO();
		taskProjectCollaboratorRestDTO.setEmail(projectCollaborator.getUser().getEmail());
		taskId = task.getId();

		// When
		Map<String, String> uriParams = new HashMap<String, String>();
		uriParams.put("id", task.getId());

		String url = "http://localhost:" + port + "tasks/" + taskId;

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());

		HttpEntity<TaskProjectCollaboratorRestDTO> entity = new HttpEntity<>(taskProjectCollaboratorRestDTO, headers);
		ResponseEntity<String> responsePut = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, taskId);

		// Then
		assertThat(responsePut.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

	}

}
