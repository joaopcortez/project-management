package project.it;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import project.controllers.rest.CreateProjectRestController;
import project.dto.rest.ProjectDTO;
import project.jparepositories.RoleRepository;
import project.jparepositories.UserRepository;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.payload.JwtAuthenticationResponse;
import project.payload.LoginRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext
public class CreateProjectRestControllerIT {

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	ProjectDTO projectDTO;
	ProjectDTO projectExpectedDTO;
	User user;
	ResponseEntity<ProjectDTO> creationResponse;
	@Autowired
	private TestRestTemplate testRestTemplate;

	@Autowired
	BCryptPasswordEncoder passwordEncoder;
	private JwtAuthenticationResponse jwt;
	
	@Before
	public void setUp() throws Exception {
		user = new User("Ana", "123", "asdrubal@switch.com", "2345", LocalDate.of(2018, 5, 31), "2", "Rua ", "4433",
				"cidade", "país");

		Role rolesDirector = roleRepository.getOneByName(RoleName.ROLE_DIRECTOR);
		
		if (!roleRepository.existsByName(RoleName.ROLE_DIRECTOR)) {
			rolesDirector = new Role(RoleName.ROLE_DIRECTOR);
			roleRepository.save(rolesDirector);
		}

		String pass = passwordEncoder.encode("12345");
		user.setPassword(pass);
		user.setRoles(Collections.singleton(rolesDirector));
		user.setActive();
		userRepository.save(user);

		projectDTO = new ProjectDTO();
		projectDTO.setName("Project name");
		projectDTO.setDescription("Project description");
		projectDTO.setStartDate(LocalDateTime.of(2018, 5, 1, 0, 0));
		projectDTO.setFinalDate(LocalDateTime.of(2018, 8, 1, 0, 0));
		projectDTO.setUserEmail("asdrubal@switch.com");
		projectDTO.setUnit("HOURS");
		projectDTO.setGlobalBudget(90.0);

		projectExpectedDTO = projectDTO;
		projectExpectedDTO
				.add(linkTo(methodOn(CreateProjectRestController.class).createProject(projectDTO)).withSelfRel());
		
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("asdrubal@switch.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = testRestTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();
	}

	@Test
	public void testCreateProjectSuccess() {

		/*
		 * //GIVEN: An projectDTO with correct data to create project. //WHEN: We post
		 * these projectDTO in HTTP client. //THEN We get HttpStatus.CREATED and the
		 * created project DTO.
		 */

		// GIVEN
		projectDTO.setProjectId("Project Created");

		// WHEN
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<ProjectDTO> entity = new HttpEntity<>(projectDTO, headers);
		creationResponse = testRestTemplate.exchange("/projects", HttpMethod.POST, entity, ProjectDTO.class);

		// THEN
		assertEquals(HttpStatus.CREATED, creationResponse.getStatusCode());
		assertEquals(projectExpectedDTO.toString(), creationResponse.getBody().toString());

	}

	@Test
	public void testCreateProjectFail() {

		/*
		 * //GIVEN: An projectDTO with incorrect data to create project. //WHEN: We post
		 * these projectDTO in HTTP client. //THEN We get HttpStatus.BAD_REQUEST.
		 */

		// GIVEN
		projectDTO.setProjectId(null);

		// WHEN
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<ProjectDTO> entity = new HttpEntity<>(projectDTO, headers);
		creationResponse = testRestTemplate.exchange("/projects", HttpMethod.POST, entity, ProjectDTO.class);

		// THEN
		projectDTO.add(linkTo(methodOn(CreateProjectRestController.class).createProject(projectDTO)).withSelfRel());
		assertEquals(HttpStatus.BAD_REQUEST, creationResponse.getStatusCode());

	}

}