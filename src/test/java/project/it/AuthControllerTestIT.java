package project.it;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import project.jparepositories.RoleRepository;
import project.model.UserService;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.payload.ApiAuthResponse;
import project.payload.JwtAuthenticationResponse;
import project.payload.LoginRequest;
import project.payload.SignUpRequest;

import javax.mail.internet.AddressException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext
public class AuthControllerTestIT {
    @Autowired
    TestRestTemplate restTemplate;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    private UserService userService;
    @Autowired
    private RoleRepository roleRepository;
    private User user;

    @Before
    public void setUp() throws AddressException {
        Role roleCollab = roleRepository.getOneByName(RoleName.ROLE_COLLABORATOR);

        if (!roleRepository.existsByName(RoleName.ROLE_COLLABORATOR)) {
            roleCollab = new Role(RoleName.ROLE_COLLABORATOR);
            roleRepository.save(roleCollab);
        }

        userService.addUser("Moon", "919898967", "Universe@stars.moon",
                "123321123", LocalDate.of(1980, 12, 18),
                "Casa", "Street", "4100-100", "NY", "Mars");

        String pass = passwordEncoder.encode("222");
        user = userService.searchUserByEmail("Universe@stars.moon");
        user.setPassword(pass);
        user.setRoles(Collections.singleton(roleCollab));
        user.getUserLoginData().setApprovedDateTime(LocalDateTime.now());

        userService.updateUser(user);
    }

    @Test
    public void activateUser_success() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsernameOrEmail(user.getEmail());
        loginRequest.setPassword("222");

        ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
                loginRequest, JwtAuthenticationResponse.class);

        JwtAuthenticationResponse jwt = responseEntity.getBody();
        String body = user.getUserLoginData().getConfirmationToken();
        //WHEN
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + jwt.getAccessToken());
        HttpEntity<String> entity = new HttpEntity<>(body, headers);

        ResponseEntity responseActivate = restTemplate.exchange("/api/auth/activate",
                HttpMethod.POST, entity, String.class);

        assertEquals(HttpStatus.OK, responseActivate.getStatusCode());

    }

    @Test
    public void activateUser_fail() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsernameOrEmail(user.getEmail());
        loginRequest.setPassword("222");

        ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
                loginRequest, JwtAuthenticationResponse.class);

        JwtAuthenticationResponse jwt = responseEntity.getBody();
        String body = "sdfds2";
        //WHEN
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + jwt.getAccessToken());
        HttpEntity<String> entity = new HttpEntity<>(body, headers);

        ResponseEntity responseActivate = restTemplate.exchange("/api/auth/activate",
                HttpMethod.POST, entity, String.class);

        assertEquals(HttpStatus.UNAUTHORIZED, responseActivate.getStatusCode());

    }

    @Test
    public void whenAuthenticating() {
        //Given
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsernameOrEmail("Universe@stars.moon");
        loginRequest.setPassword("222");
        //When
        ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin", loginRequest, JwtAuthenticationResponse.class);

        //Then
        assertEquals(HttpStatus.ACCEPTED, responseEntity.getStatusCode());
    }

    @Test
    public void whenRegistering() {
        //Given
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setEmail("adela22@gmail.com");
        signUpRequest.setPassword("12345456");
        signUpRequest.setPhone("917393727");
        signUpRequest.setName("LisaAdelaide");
        signUpRequest.setBirthDate("1980-12-18");
        signUpRequest.setTaxPayerId("12345678");
        signUpRequest.setAddressId("casara");
        signUpRequest.setStreet("anyhjhjstreet");
        signUpRequest.setCity("Awaykjhkjh");
        signUpRequest.setCountry("fakjhkjhaway");
        signUpRequest.setPostalCode("4100-001");

        //When
        ResponseEntity<ApiAuthResponse> responseEntity = restTemplate.postForEntity("/api/auth/signup", signUpRequest, ApiAuthResponse.class);

        //Then
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
    }
    
    @Test
    public void testResendEmailSuccess() {
    	
    		//Given
    		LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsernameOrEmail(user.getEmail());
        loginRequest.setPassword("222");

        ResponseEntity<JwtAuthenticationResponse> responseLogin = restTemplate.postForEntity("/api/auth/signin",
                loginRequest, JwtAuthenticationResponse.class);
        JwtAuthenticationResponse jwt = responseLogin.getBody();
        
        //When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + jwt.getAccessToken());
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
    		ResponseEntity<ApiAuthResponse> responseEntity = restTemplate.exchange("/api/auth/activate/resendmail", HttpMethod.GET, entity, ApiAuthResponse.class);
    		
    		//Then
    		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    		assertTrue(responseEntity.getBody().getSuccess());
    		assertEquals("Confirmation code resent to your email.", responseEntity.getBody().getMessage());
    		
    }

}