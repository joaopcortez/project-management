package project.Services;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.DirtiesContext;
import project.dto.rest.TaskRestDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.services.ProjectCollaboratorService;
import project.model.task.Task;
import project.services.TaskService;
import project.model.user.User;
import project.services.TaskProjectService;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Double.NaN;
import static org.junit.Assert.*;

@DirtiesContext
public class TaskProjectServiceTest {
    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;

    UserService userService;
    ProjectService projectService;
    TaskService taskService;
    ProjectCollaboratorService projectCollaboratorService;
    TaskProjectService taskProjectService;

    User user1;
    Project project1, project2;
    Task task, task1, task2,task3;
    TaskRestDTO taskRestDTO;

    @Before
    public void setUp() throws AddressException {
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        taskProjectService = new TaskProjectService(projectRepositoryClass, taskRepositoryClass);

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);

        LocalDate birth = LocalDate.of(1999, 11, 11);
        userService.addUser("Pedro", "919999999", "pedro@gmail.com", "332432", birth, "2", "Rua ", "4433", "cidade",
                "país");
        user1 = userService.searchUserByEmail("pedro@gmail.com");
        projectService.addProject("1", "Don't fall asleep");
        projectService.addProject("2", "Don't wake up");
        project1 = projectService.getProjectByID("1");
        project2 = projectService.getProjectByID("2");
        projectCollaboratorService.setProjectManager(project1, user1);
        projectCollaboratorService.setProjectManager(project2, user1);
        projectRepositoryClass.save(project1);
        projectRepositoryClass.save(project2);
        taskService.addTask(project2, "Task 1");

    }

    @Test
    @Transactional
    public void testListTasksThatCanBeAddedAsDependencyStateConditions() throws Exception {
        String METHOD_NAME = "listTasksThatCanBeAddedAsDependencyStateConditions";
        Class[] parameterTypes = new Class[1];
        parameterTypes[0] = Task.class;
        Method method = taskProjectService.getClass().getDeclaredMethod(METHOD_NAME, parameterTypes);
        method.setAccessible(true);
        Object[] parameters = new Object[1];

        task1 = taskService.findTaskByTitle("Task 1");
        parameters[0] = task1;
        Boolean result = (Boolean) method.invoke(taskProjectService, parameters);

        assertTrue(result);

    }

    /**
     * Given: A project without tasks
     * When: It's given valid information to create a task
     * Then: The task is created
     */
    @Test
    @Transactional
    public void addTaskRestTest() {
        taskRestDTO = new TaskRestDTO();
        taskRestDTO.setProjectId(project1.getId());
        taskRestDTO.setTitle("wake up!");
        TaskRestDTO result = taskProjectService.createTaskRest(taskRestDTO);
        assertNotNull(result);
    }

    /**
     * Given: A project without tasks
     * When: It's given valid information to create a task
     * Then: The task is created
     */
    @Test
    @Transactional
    public void createTaskRestTest() {
        //Given:
        assertTrue(project1.getTasksList().isEmpty());

        //When:
        taskRestDTO = new TaskRestDTO();
        taskRestDTO.setProjectId(project1.getId());
        taskRestDTO.setTitle("wake up!");
        TaskRestDTO taskRestDTO1 = taskProjectService.createTaskRest(taskRestDTO);

        //Then
        Task task = taskService.findTaskByID(taskRestDTO1.getTaskId());
        TaskRestDTO expected = task.toDTO();
        assertNotNull(taskRestDTO1);
        assertEquals(expected, taskRestDTO1);
    }

    @Test
    @Transactional
    public void createTaskRestTest2() {
        //Given:
        assertTrue(project1.getTasksList().isEmpty());

        //When:
        taskRestDTO = new TaskRestDTO();
        taskRestDTO.setProjectId(project1.getId());
        taskRestDTO.setTitle("wake up!");
        taskRestDTO.setPredictedDateOfConclusion(LocalDateTime.now());
        TaskRestDTO taskRestDTO1 = taskProjectService.createTaskRest(taskRestDTO);

        //Then
        Task task = taskService.findTaskByID(taskRestDTO1.getTaskId());
        TaskRestDTO expected = task.toDTO();
        assertNotNull(taskRestDTO1);
        assertEquals(expected, taskRestDTO1);
    }

    /**
     * Given: A project without tasks
     * When: It's given invalid information to create a task
     * Then: The task is not created
     */
    @Test
    @Transactional
    public void addTaskRestTestFail_projectDoesntExists() {
        //Given:
        assertTrue(project1.getTasksList().isEmpty());

        //When:
        taskRestDTO = new TaskRestDTO();
        taskRestDTO.setProjectId("invalid");
        taskRestDTO.setTitle("wake up!");

        //Then
        assertNull(taskProjectService.createTaskRest(taskRestDTO));
    }

    /**
     * Given: A project without tasks
     * When: It's given invalid information to create a task
     * Then: The task is not created
     */
    @Test
    @Transactional
    public void addTaskRestTestFail_NoTitle() {
        //Given:
        assertTrue(project1.getTasksList().isEmpty());

        //When:
        taskRestDTO = new TaskRestDTO();
        taskRestDTO.setProjectId("1");

        //Then
        assertNull(taskProjectService.createTaskRest(taskRestDTO));
        assertTrue(project1.getTasksList().isEmpty());
    }

    /**
     * Given: A project without tasks
     * When: It's given full valid information to create a task
     * Then: The task is created
     */
    @Test
    @Transactional
    public void addTaskRestTestSuccess_fullInfo() {
        //Given:
        assertTrue(project1.getTasksList().isEmpty());

        //When:
        taskRestDTO = new TaskRestDTO();
        taskRestDTO.setProjectId("1");
        taskRestDTO.setTitle("wake up!");
        taskRestDTO.setUnitCost(2.2);
        taskRestDTO.setEstimatedEffort(100);
        taskRestDTO.setDescription("Be Happy");
        taskRestDTO.setPredictedDateOfStart(LocalDateTime.now().plusDays(2));
        taskRestDTO.setPredictedDateOfStart(LocalDateTime.now().plusMonths(6));

        //Then
        TaskRestDTO taskRestDTO1 = taskProjectService.createTaskRest(taskRestDTO);
        Task task = taskService.findTaskByID(taskRestDTO1.getTaskId());
        assertEquals(task.toDTO(), taskRestDTO1);
    }

    @Test
    @Transactional
    public void addTaskRestTestSuccess_Nanparameters() {
        //Given:
        assertTrue(project1.getTasksList().isEmpty());

        //When:
        taskRestDTO = new TaskRestDTO();
        taskRestDTO.setProjectId("1");
        taskRestDTO.setTitle("wake up!");
        taskRestDTO.setUnitCost(NaN);
        taskRestDTO.setEstimatedEffort(NaN);
        taskRestDTO.setDescription("Be Happy");
        taskRestDTO.setPredictedDateOfStart(LocalDateTime.now().plusDays(2));
        taskRestDTO.setPredictedDateOfStart(LocalDateTime.now().plusMonths(6));

        //Then
        TaskRestDTO taskRestDTO1 = taskProjectService.createTaskRest(taskRestDTO);
        Task task = taskService.findTaskByID(taskRestDTO1.getTaskId());
        assertEquals(task.toDTO(), taskRestDTO1);
    }


    @Test
    @Transactional
    public void addTaskRestTestFail_nullProjectId() {
        //Given:
        assertTrue(project1.getTasksList().isEmpty());

        //When:
        taskRestDTO = new TaskRestDTO();
        taskRestDTO.setProjectId(null);

        //Then
        assertNull(taskProjectService.createTaskRest(taskRestDTO));
        assertTrue(project1.getTasksList().isEmpty());
    }

    @Test
    @Transactional
    public void addDependenciesTest() {
        List<String> taskIdependencyList = new ArrayList<>();
        taskIdependencyList.add("task1");
        taskIdependencyList.add("task2");
        taskService.addTask(project1, "task1", "task1", LocalDateTime.now(), LocalDateTime.now().plusMonths(2), 1.2, 1.3);
        taskService.addTask(project1, "task2", "task2", LocalDateTime.now().plusMonths(2), LocalDateTime.now().plusMonths(6), 1.2, 1.3);

        task = project1.findTaskByTitle("task1");
        task2 = project1.findTaskByTitle("task2");
        assertTrue(taskProjectService.addDependencies(task.getId(), taskIdependencyList));
    }

    @Test
    @Transactional
    public void addDependenciesTestFail() {
        List<String> taskIdependencyList = new ArrayList<>();
        taskIdependencyList.add("task1");
        taskIdependencyList.add("task2");

        assertFalse(taskProjectService.addDependencies("task3", taskIdependencyList));
    }

    @Test
    @Transactional
    public void listTasksThatCanBeAddedAsDependency() {
        List<TaskRestDTO> expected = new ArrayList<>();
        TaskRestDTO taskRestDTO = new TaskRestDTO();
        taskService.addTask(project1, "title");
        taskService.addTask(project1, "title2");
        task = taskService.getProjectTasks(project1).get(0);
        task2 = taskService.getProjectTasks(project1).get(1);
        taskService.updateTask(task);
        taskService.updateTask(task2);
        taskRestDTO.setProjectId(project1.getId());
        taskRestDTO.setTaskId(task2.getId());
        taskRestDTO.setDateOfCreation(LocalDateTime.now());
        taskRestDTO.setTitle(task2.getTitle());
        taskRestDTO.setState("Created");
        List<TaskRestDTO> result = taskProjectService.listTasksThatCanBeAddedAsDependency(task.getId());
        expected.add(taskRestDTO);
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void listTasksThatCanBeAddedAsDependencyTaskToAddDependencyNull() {
        List<TaskRestDTO> expected = new ArrayList<>();
        taskService.addTask(project1, "title");
        taskService.addTask(project1, "title2");
        task = taskService.getProjectTasks(project1).get(0);
        task2 = taskService.getProjectTasks(project1).get(1);
        taskService.updateTask(task);
        taskService.updateTask(task2);
        List<TaskRestDTO> result = taskProjectService.listTasksThatCanBeAddedAsDependency(null);
        assertEquals(expected, result);
    }


    @Test
    @Transactional
    public void setTaskCanceles() {


        assertFalse(taskProjectService.setTaskCanceles("Not exist"));

    }


}
