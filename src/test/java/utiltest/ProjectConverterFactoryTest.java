package utiltest;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

import project.util.ProjectConverter;
import project.util.ProjectConverterFactory;
import project.util.ProjectXmlConverter;

public class ProjectConverterFactoryTest {
	String inputFile;

	ProjectConverterFactory converterFactory;

	@Before
	public void setUp() throws Exception {
		inputFile = System.getProperty("user.dir") + "/src/main/resources/project/util/data/Projeto_v00.xml";

	}

	@Test
	public void createProjectConverter() {
		assertTrue(ProjectConverterFactory.createProjectConverter(inputFile) instanceof ProjectConverter);

	}

	@Test
	public void createProjectConverterWrongFilename() {
		assertTrue(ProjectConverterFactory
				.createProjectConverter(System.getProperty("user.dir")) instanceof ProjectXmlConverter);

	}

	@Test
	public void testConstructorReflection() {

		// Given
		ProjectConverterFactory factory = null;

		// When
		try {
			Constructor<ProjectConverterFactory> constructor = ProjectConverterFactory.class.getDeclaredConstructor();
			constructor.setAccessible(true);
			factory = constructor.newInstance();
		} catch (Exception e) {
		}

		// Then
		assertTrue(factory instanceof ProjectConverterFactory);
	}

	@Test
	public void testReadConverterClassNoVersion() throws Exception {

		// Given
		String extension = "xml";
		String version = "";
		String path = System.getProperty("user.dir") + "/src/main/resources/application.properties";

		Class<?>[] parameterTypes = new Class<?>[3];
		parameterTypes[0] = String.class;
		parameterTypes[1] = String.class;
		parameterTypes[2] = String.class;

		Method method = ProjectConverterFactory.class.getDeclaredMethod("readConverterClass", parameterTypes);
		method.setAccessible(true);

		// When
		String result = (String) method.invoke(converterFactory, path, extension, version);

		// Then
		assertEquals("UserXmlConverter", result);
	}

	@Test
	public void testReadConverterClassVersionPresent() throws Exception {

		// Given
		String extension = "xml";
		String version = "Projeto_v00";
		String path = System.getProperty("user.dir") + "/src/main/resources/application.properties";

		Class<?>[] parameterTypes = new Class<?>[3];
		parameterTypes[0] = String.class;
		parameterTypes[1] = String.class;
		parameterTypes[2] = String.class;

		Method method = ProjectConverterFactory.class.getDeclaredMethod("readConverterClass", parameterTypes);
		method.setAccessible(true);

		// When
		String result = (String) method.invoke(converterFactory, path, extension, version);

		// Then
		assertEquals("ProjectXmlConverter", result);
	}

	/**
	 * GIVEN a wrong path to mapper file, WHEN we call createProjectConverter THEN
	 * we receive a default projectConverter.
	 */
	@Test
	public void testCreateProjectConverterWrongPathToMapper() {
		// Given
		ProjectConverterFactory.setPathToMapperFile("WrongPath");

		// When
		ProjectConverter result = ProjectConverterFactory.createProjectConverter(inputFile);

		// Then
		assertTrue(result instanceof ProjectXmlConverter);
	}

	/**
	 * GIVEN a wrong class name WHEN we call contructClassFromString THEN an
	 * exception is thrown and we receive a default projectConverter
	 * 
	 * @throws Exception
	 */
	@Test(expected = Exception.class)
	public void testContructClassFromStringFailureWrongClassName() throws Exception {

		// Given
		String wrongClassName = "wrongClassName";
		Method method = ProjectConverterFactory.class.getDeclaredMethod("contructClassFromString", String.class);
		method.setAccessible(true);

		// When
		method.invoke(converterFactory, wrongClassName);
	}
}