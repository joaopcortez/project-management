package utiltest;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.hateoas.Link;

import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.task.Task;
import project.model.user.User;
import project.security.UserPrincipal;
import project.services.TaskService;
import project.util.TaskLinkBuilderFactory;


public class TaskLinkBuilderFactoryTest {
	
	
	TaskService taskService;
	ProjectService projectService;
	
	ProjectRepositoryClass projectRepository;
	TaskRepositoryClass taskRepository;
	
	
	private TaskLinkBuilderFactory linkBuilder;
	private Project project;
	private Task task1;
	
	UserPrincipal userPrincipal;
    List<String>listTasksWhereIsProjectManager;
    List<String>listTasksWhereIsTaskCollaborator;
	
	
	@Before
	public void setUp() throws Exception {
		
		projectRepository = new ProjectRepositoryClass();
		taskRepository = new TaskRepositoryClass();
		projectService = new ProjectService(projectRepository);
		taskService = new TaskService(taskRepository);
		linkBuilder = new TaskLinkBuilderFactory(taskService);
		
		User user1 = new User("Asdrubal", "123456789", "asdrubal@switch.com", "123456789", LocalDate.of(1977, 3, 5), "1",
				"Rua sem saída", "4250-357", "Porto", "Portugal");
		
		List<String>listProjectWhereProjectManager = new ArrayList<>();
        List<String>listTasksWhereIsProjectManager = new ArrayList<>();
        List<String>listTasksWhereIsTaskCollaborator = new ArrayList<>();
        userPrincipal = UserPrincipal.create(user1, listProjectWhereProjectManager, listTasksWhereIsTaskCollaborator, listTasksWhereIsProjectManager);
        
		projectService.addProject("1", "switch1");
		project = projectService.getProjectByID("1");
		
		taskService.addTask(project, "transactional", "1-1");	
		task1 = taskService.findTaskByID("1-1");
	}

	@Test
	public void testTaskLinkBuilderFactory() {
		assertTrue (linkBuilder instanceof TaskLinkBuilderFactory);
	}

	/**
	 * GIVEN: a valid filename and a taskId pertaining to a task in Created state
	 * WHEN: we ask for available links for that task
	 * THEN: we get a list of valid links
	 */
	@Test
	public void testGetAvailableLinksSuccess() {
		
		//Given
		List<String>listTasksWhereIsProjectManager = new ArrayList<>();
        List<String>listTasksWhereIsTaskCollaborator = new ArrayList<>();
        listTasksWhereIsProjectManager.add(task1.getId());
                
		List<String> expectedRel = new ArrayList<>();
		expectedRel.add("Add Collaborator");
		expectedRel.add("Add Task Dependency");
		
		String filename = "taskactionstestfile.config";
		String taskId = task1.getId();
		
		//When
		List<Link> result = linkBuilder.getAvailableLinks(taskId, filename, listTasksWhereIsProjectManager, listTasksWhereIsTaskCollaborator);
		List<String> resultRel = new ArrayList<>();
		for(Link link: result) {
			resultRel.add(link.getRel());
		}
		
		//Then
		assertEquals(2, result.size());
		assertThat(expectedRel, containsInAnyOrder(resultRel.toArray()));	
	}
	
	/**
	 * GIVEN: an invalid filename
	 * WHEN: we ask for available links for that task
	 * THEN: we get an IOException and the result link list is empty
	 */
	@Test
	public void testGetAvailableLinksFailureIOException() {
		//Given
		String filename = "invalidFilename.config";
		String taskId = task1.getId();
		
		
		//When
		List<Link> result = linkBuilder.getAvailableLinks(taskId, filename, listTasksWhereIsProjectManager, listTasksWhereIsTaskCollaborator);
		assertTrue(result.isEmpty());
	}
	
	/**
	 * GIVEN: an invalid class name in file
	 * WHEN: we ask for available links for that task
	 * THEN: we get a ClassNotFoundException and the result link list is empty
	 */
	@Test
	public void testGetAvailableLinksFailureClassNotFoundException() {
		//Given
		String filename = "taskactionstestfile.config";
		String taskId = task1.getId();
		List<String>listTasksWhereIsProjectManager = new ArrayList<>();
		listTasksWhereIsProjectManager.add("1-1");
        List<String>listTasksWhereIsTaskCollaborator = new ArrayList<>();
        listTasksWhereIsTaskCollaborator.add("1-1");
		
		//When
		List<Link> result = linkBuilder.getAvailableLinks(taskId, filename, listTasksWhereIsProjectManager, listTasksWhereIsTaskCollaborator);
		assertTrue(result.isEmpty());
	}
	
	/**
	 * GIVEN: an invalid method name in file
	 * WHEN: we ask for available links for that task
	 * THEN: we get a NoSuchMethodException and the result link list is empty
	 */
	@Test
	public void testGetAvailableLinksFailureNoSuchMethodException() {
		//Given
		List<String>listTasksWhereIsProjectManager = new ArrayList<>();
		listTasksWhereIsProjectManager.add("1-1");
        List<String>listTasksWhereIsTaskCollaborator = new ArrayList<>();
        listTasksWhereIsTaskCollaborator.add("1-1");
		String filename = "taskactionstestfile.config";
		String taskId = task1.getId();
		task1.setPredictedDateOfStart(LocalDateTime.now());
		task1.setPredictedDateOfConclusion(LocalDateTime.now().plusDays(5));
		
		//When
		List<Link> result = linkBuilder.getAvailableLinks(taskId, filename, listTasksWhereIsProjectManager, listTasksWhereIsTaskCollaborator);
		assertTrue(result.isEmpty());
	}
	
	/**
	 * GIVEN: a list with possible actions from a task in created state
	 * WHEN: we ask for its available actions through method getTaskAvailableActions
	 * THEN: we receive a list of strings with available actions for that state that's equal
	 * to expected.
	 */
	@Test
	public void testGetTaskAvailableActionsThroughReflection() throws Exception {
		
		//Given
		List<String> expected = new ArrayList<>();		
		expected.add("Created.setPredictedDateOfStart");
		expected.add("Created.setPredictedDateOfConclusion");
		expected.add("Created.requestAddProjectCollaborator");
		expected.add("Created.addProjectCollaborator");
		expected.add("Created.addTaskDependency");
		expected.add("Created.removeTask");
		
		//When
		Method getTaskAvailableActions = TaskLinkBuilderFactory.class.getDeclaredMethod("getTaskAvailableActions", String.class);
		getTaskAvailableActions.setAccessible(true);
		@SuppressWarnings("unchecked")
		List<String> result = (List<String>) getTaskAvailableActions.invoke(linkBuilder, task1.getId());
		
		//Then
		assertEquals(expected, result);
	}
	
	/**
	 * GIVEN a task ID and two lists containing task IDs, both containing the task ID
	 * WHEN we call for method getPertinentRoleFromTaskId
	 * THEN we get the string "COLLABORATOR"
	 */
	@Test
	public void testGetPertinentRoleFromTaskId() {
		
		//Given
		String taskId = "1-1";
		String expected = "BOTH";
		
		List<String> listTasksWhereIsProjectManager = new ArrayList<>();
		listTasksWhereIsProjectManager.add("1-1");
		listTasksWhereIsProjectManager.add("1-3");
		List<String> listTasksWhereIsTaskCollaborator = new ArrayList<>();
		listTasksWhereIsTaskCollaborator.add("1-1");
		listTasksWhereIsTaskCollaborator.add("1-2");
		
		//When
		String result = linkBuilder.getPertinentRoleFromTaskId(taskId, listTasksWhereIsProjectManager, listTasksWhereIsTaskCollaborator);
		
		//Then
		assertEquals(expected, result);
	}
	
	/**
	 * GIVEN a task ID and two lists containing task IDs, both containing the task ID
	 * WHEN we call for method getPertinentRoleFromTaskId
	 * THEN we get the string "COLLABORATOR"
	 */
	@Test
	public void testGetPertinentRoleFromTaskIdSuccessBothListsContainTaskId() {
		
		//Given
		String taskId = "1-1";
		String expected = "BOTH";
		
		List<String> listTasksWhereIsProjectManager = new ArrayList<>();
		listTasksWhereIsProjectManager.add("1-1");
		listTasksWhereIsProjectManager.add("1-3");
		List<String> listTasksWhereIsTaskCollaborator = new ArrayList<>();
		listTasksWhereIsTaskCollaborator.add("1-1");
		listTasksWhereIsTaskCollaborator.add("1-2");
		
		//When
		String result = linkBuilder.getPertinentRoleFromTaskId(taskId, listTasksWhereIsProjectManager, listTasksWhereIsTaskCollaborator);
		
		//Then
		assertEquals(expected, result);
	}
	
	/**
	 * GIVEN a task ID and two lists containing task IDs, only listTasksWhereIsProjectManager contains the task ID
	 * WHEN we call for method getPertinentRoleFromTaskId
	 * THEN we get the string "MANAGER"
	 */
	@Test
	public void testGetPertinentRoleFromTaskIdSuccessOnlyListTasksWhereProjectManagerHasTask() {
		
		//Given
		String taskId = "1-3";
		String expected = "MANAGER";
		
		List<String> listTasksWhereIsProjectManager = new ArrayList<>();
		listTasksWhereIsProjectManager.add("1-1");
		listTasksWhereIsProjectManager.add("1-3");
		List<String> listTasksWhereIsTaskCollaborator = new ArrayList<>();
		listTasksWhereIsTaskCollaborator.add("1-1");
		listTasksWhereIsTaskCollaborator.add("1-2");
		
		//When
		String result = linkBuilder.getPertinentRoleFromTaskId(taskId, listTasksWhereIsProjectManager, listTasksWhereIsTaskCollaborator);
		
		//Then
		assertEquals(expected, result);
	}
	
	/**
	 * GIVEN a task ID and two lists containing task IDs, only listTasksWhereIsTaskCollaborator contains the task ID
	 * WHEN we call for method getPertinentRoleFromTaskId
	 * THEN we get the string "COLLABORATOR"
	 */
	@Test
	public void testGetPertinentRoleFromTaskIdSuccessOnlyListTasksWhereTaskCollaboratorHasTask() {
		
		//Given
		String taskId = "1-2";
		String expected = "COLLABORATOR";
		
		List<String> listTasksWhereIsProjectManager = new ArrayList<>();
		listTasksWhereIsProjectManager.add("1-1");
		listTasksWhereIsProjectManager.add("1-3");
		List<String> listTasksWhereIsTaskCollaborator = new ArrayList<>();
		listTasksWhereIsTaskCollaborator.add("1-1");
		listTasksWhereIsTaskCollaborator.add("1-2");
		
		//When
		String result = linkBuilder.getPertinentRoleFromTaskId(taskId, listTasksWhereIsProjectManager, listTasksWhereIsTaskCollaborator);
		
		//Then
		assertEquals(expected, result);
	}
	
	/**
	 * GIVEN a task ID and two lists containing task IDs, none of the lists contains the task ID
	 * WHEN we call for method getPertinentRoleFromTaskId
	 * THEN we get an empty string
	 */
	@Test
	public void testGetPertinentRoleFromTaskIdSuccessNoneHasThatTaskId() {
		
		//Given
		String taskId = "1-4";
		String expected = "PROJECTCOLLABORATOR";
		
		List<String> listTasksWhereIsProjectManager = new ArrayList<>();
		listTasksWhereIsProjectManager.add("1-1");
		listTasksWhereIsProjectManager.add("1-3");
		List<String> listTasksWhereIsTaskCollaborator = new ArrayList<>();
		listTasksWhereIsTaskCollaborator.add("1-1");
		listTasksWhereIsTaskCollaborator.add("1-2");
		
		//When
		String result = linkBuilder.getPertinentRoleFromTaskId(taskId, listTasksWhereIsProjectManager, listTasksWhereIsTaskCollaborator);
		
		//Then
		assertEquals(expected, result);
	}
}
