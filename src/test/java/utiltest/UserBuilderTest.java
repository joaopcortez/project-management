package utiltest;

import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.dto.AddressDTO;
import project.dto.UserDTO;
import project.dto.UserRegistryDTO;
import project.model.UserService;
import project.util.UserBuilder;
import project.util.UserXmlConverter;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UserBuilderTest {

    UserRepositoryClass userRepositoryClass;
    private UserBuilder userBuilder;
    private UserService userService;
    private UserDTO userDTO;
    private UserXmlConverter userXmlConverter;
    private UserRegistryDTO userRegistryDTO;

    @Before
    public void setUp() {

        userRepositoryClass = new UserRepositoryClass();
        userService = new UserService(userRepositoryClass);

        String inputXml = System.getProperty("user.dir")
                + "/src/main/resources/project/util/Utilizador_v00.xml";

        userXmlConverter = new UserXmlConverter();
        userBuilder = new UserBuilder(userService);
        userRegistryDTO = userXmlConverter.parseToUser(inputXml);
    }

    @Test
    @Transactional
    public void testProtectedDefaultConstructor() throws Exception {

        Constructor<UserBuilder> defaultConstructor = UserBuilder.class.getDeclaredConstructor();
        defaultConstructor.setAccessible(true);
        UserBuilder userBuilder = defaultConstructor.newInstance();

        assertTrue(userBuilder instanceof UserBuilder);
    }

    @Test
    @Transactional
    public void createAndPersistUserTest_UserData() throws AddressException {
        //Given
        userDTO = userRegistryDTO.getListUsers().get(0);
        //When
        userBuilder.createAndPersistUser(userDTO);
        //Then
        assertEquals("Asdrubal", userDTO.getName());
        assertEquals("asdrubal@switch.com", userDTO.getEmail());
        assertEquals("123456789", userDTO.getPhone());
        assertEquals("qwerty", userDTO.getPassword());
    }

    @Test
    @Transactional
    public void createAndPersistUserTest_UserAddress() throws AddressException {
        //Given
        userDTO = userRegistryDTO.getListUsers().get(0);
        //When
        userBuilder.createAndPersistUser(userDTO);
        //Then
        AddressDTO addressDTO = userDTO.getAddressList().get(0);
        assertEquals("Porto", addressDTO.getCity());
        assertEquals("Portugal", addressDTO.getCountry());
        assertEquals("541", addressDTO.getPostalCodeAddress());
        assertEquals("4250", addressDTO.getPostalCodeCity());
        assertEquals("Rua do Amial", addressDTO.getStreet());
    }

    @Test
    @Transactional
    public void userRegistryDTOToStringTest()  {
        //Given
        String result = "[lista_utilizadores=[[utilizador=[nome_utilizador=Asdrubal, email_utilizador=asdrubal@switch.com, "
                + "lista_enderecos=[[lista_enderecos=[endereco_postal=Porto, pais=Portugal, "
                + "cp_loc=4250, cp_num=541, rua=Rua do Amial]], telefone=123456789, password=qwerty], "
                + "[utilizador=[nome_utilizador=Paulo, email_utilizador=paulo@switch.com, "
                + "lista_enderecos=[[lista_enderecos=[endereco_postal=Porto, pais=Portugal, cp_loc=4250, "
                + "cp_num=541, rua=Avenida de França], [lista_enderecos=[endereco_postal=Lisboa, "
                + "pais=Portugal, cp_loc=4444, cp_num=251, rua=Rua do Francos]], telefone=123456789, "
                + "password=qwerty], [utilizador=[nome_utilizador=Joana, email_utilizador=joana@switch.com,"
                + " lista_enderecos=[[lista_enderecos=[endereco_postal=Porto, pais=Portugal, cp_loc=4226, "
                + "cp_num=155, rua=Estrada da Circunvalação]], telefone=123456789, password=qwerty],"
                + " [utilizador=[nome_utilizador=Maria, email_utilizador=maria@switch.com,"
                + " lista_enderecos=[[lista_enderecos=[endereco_postal=Porto, pais=Portugal,"
                + " cp_loc=4250, cp_num=500, rua=Avenida de Boavista], "
                + "[lista_enderecos=[endereco_postal=Porto, pais=Portugal, "
                + "cp_loc=4350, cp_num=580, rua=Rua do Almada]], telefone=123456789, password=qwerty]]]";
        //Then
        assertEquals(result, userRegistryDTO.toString());

    }

    @Test
    @Transactional
    public void userDTOToStringTest() throws AddressException {
        //Given
        userDTO = userRegistryDTO.getListUsers().get(0);
        //When
        userBuilder.createAndPersistUser(userDTO);
        String result = "[utilizador=[nome_utilizador=Asdrubal, email_utilizador=asdrubal@switch.com, "
                + "lista_enderecos=[[lista_enderecos=[endereco_postal=Porto, pais=Portugal, cp_loc=4250, "
                + "cp_num=541, rua=Rua do Amial]], telefone=123456789, password=qwerty]";
        //Then
        assertEquals(result, userDTO.toString());
    }

    @Test
    @Transactional
    public void createAndPersistUserTest_UserAddressTwo() throws AddressException {
        //Given
        userDTO = userRegistryDTO.getListUsers().get(1);
        //When
        userBuilder.createAndPersistUser(userDTO);
        //Then
        List<AddressDTO> result = userDTO.getAddressList();
        List<AddressDTO> expected = new ArrayList<>();
        expected.add(userDTO.getAddressList().get(0));
        expected.add(userDTO.getAddressList().get(1));
        assertEquals(expected, result);
    }
}
