package utiltest;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

import project.util.UserConverter;
import project.util.UserConverterFactory;
import project.util.UserXmlConverter;

public class UserConverterFactoryTest {
	String inputFile;

	UserConverterFactory converterFactory;

	@Before
	public void setUp() throws Exception {
		inputFile = System.getProperty("user.dir") + "/src/main/resources/project/util/data/Utilizador_v00.xml";

	}

	@Test
	public void createUserConverter() {
		assertTrue(UserConverterFactory.createUserConverter(inputFile) instanceof UserConverter);

	}

	@Test
	public void createUserConverterWrongFileName() {
		assertTrue(
				UserConverterFactory.createUserConverter(System.getProperty("user.dir")) instanceof UserXmlConverter);

	}

	@Test
	public void testConstructorReflection() {

		// Given
		UserConverterFactory factory = null;

		// When
		try {
			Constructor<UserConverterFactory> constructor = UserConverterFactory.class.getDeclaredConstructor();
			constructor.setAccessible(true);
			factory = constructor.newInstance();
		} catch (Exception e) {
		}

		// Then
		assertTrue(factory instanceof UserConverterFactory);
	}

	@Test
	public void testReadConverterClassNoVersion() throws Exception {

		// Given
		String extension = "xml";
		String version = "";
		String path = System.getProperty("user.dir") + "/src/main/resources/application.properties";

		Class<?>[] parameterTypes = new Class<?>[3];
		parameterTypes[0] = String.class;
		parameterTypes[1] = String.class;
		parameterTypes[2] = String.class;

		Method method = UserConverterFactory.class.getDeclaredMethod("readConverterClass", parameterTypes);
		method.setAccessible(true);

		// When
		String result = (String) method.invoke(converterFactory, path, extension, version);

		// Then
		assertEquals("UserXmlConverter", result);
	}

	@Test
	public void testReadConverterClassVersionPresent() throws Exception {
		// Given
		String extension = "xml";
		String version = "Utilizador_v00";
		String path = System.getProperty("user.dir") + "/src/main/resources/application.properties";

		Class<?>[] parameterTypes = new Class<?>[3];
		parameterTypes[0] = String.class;
		parameterTypes[1] = String.class;
		parameterTypes[2] = String.class;

		Method method = UserConverterFactory.class.getDeclaredMethod("readConverterClass", parameterTypes);
		method.setAccessible(true);

		// When
		String result = (String) method.invoke(converterFactory, path, extension, version);

		// Then
		assertEquals("UserXmlConverter", result);
	}

	/**
	 * GIVEN a wrong path to mapper file, WHEN we call createUserConverter THEN we
	 * receive a default userConverter.
	 */
	@Test
	public void testCreateUserConverterWrongPathToMapper() {
		// Given
		UserConverterFactory.setPathToMapperFile("WrongPath");

		// When
		UserConverter result = UserConverterFactory.createUserConverter(inputFile);

		// Then
		assertTrue(result instanceof UserXmlConverter);
	}

	/**
	 * GIVEN a wrong class name WHEN we call contructClassFromString THEN an
	 * exception is thrown and we receive a default userConverter
	 * 
	 * @throws Exception
	 */
	@Test(expected = Exception.class)
	public void testContructClassFromStringFailureWrongClassName() throws Exception {

		// Given
		String wrongClassName = "wrongClassName";
		Method method = UserConverterFactory.class.getDeclaredMethod("contructClassFromString", String.class);
		method.setAccessible(true);

		// When
		method.invoke(converterFactory, wrongClassName);
	}
}