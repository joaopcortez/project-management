package utiltest;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.dto.*;
import project.model.UserService;
import project.model.project.ProjectService;
import project.model.user.User;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;
import project.util.ProjectBuilder;
import project.util.ProjectXmlConverter;
import project.util.UserBuilder;
import project.util.UserXmlConverter;

import javax.transaction.Transactional;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.List;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;

public class ProjectBuilderTest {

    String userDirectory = System.getProperty("user.dir");
    String inputXml = userDirectory + "/src/main/resources/project/util/ProjectTest.xml";
    String inputXml2 = userDirectory + "/src/main/resources/project/util/ProjectTest2.xml";
    String inputXmlCI = userDirectory + "/src/main/resources/project/util/ProjectTestCostCI.xml";
    String inputXmlFail = userDirectory + "/src/main/resources/project/util/ProjectTestCostFail.xml";
    String inputXmlCM = userDirectory + "/src/main/resources/project/util/ProjectTestCostCM.xml";
    String inputXmlLTP = userDirectory + "/src/main/resources/project/util/ProjectTestCostLTP.xml";
    String inputFirstUsersXml = userDirectory + "/src/main/resources/project/util/firstUsers.xml";
    String inputUserXml = userDirectory + "/src/main/resources/project/util/data/Utilizador_v00_Dt1.xml";
    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;
    private ProjectXmlConverter projectXmlConverter;
    private UserXmlConverter userXmlConverter;
    private UserBuilder userBuilder;
    private ProjectBuilder projectBuilder;
    private ProjectRegistryDTO projectRegistryDTO;
    private ProjectDTO projectDTO;
    private UserRegistryDTO userRegistryDTO;
    private UserService userService;
    private ProjectService projectService;
    private ProjectCollaboratorService projectCollaboratorService;
    private TaskService taskService;

    @Before
    public void setUp() throws Exception {
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);

        userXmlConverter = new UserXmlConverter();
        projectXmlConverter = new ProjectXmlConverter();
        userBuilder = new UserBuilder(userService);
        projectBuilder = new ProjectBuilder(projectService, userService, projectCollaboratorService, taskService);

        //First initialization of users
        userRegistryDTO = userXmlConverter.parseToUser(inputFirstUsersXml);
        for (UserDTO userDTO : userRegistryDTO.getListUsers()) {
            userBuilder.createAndPersistUser(userDTO);
        }
        userRegistryDTO = userXmlConverter.parseToUser(inputUserXml);
        for (UserDTO userDTO : userRegistryDTO.getListUsers()) {
            userBuilder.createAndPersistUser(userDTO);
        }

        // Get all users from database
        List<User> users = userService.listAllUsers();

        // Assign profile ROLE_ADMINISTRATOR to the first user in the users list
        User user1 = users.get(0);
        user1.setProfileAdministrator();
        userService.updateUser(user1);

        // Assign profile ROLE_DIRECTOR to the second user in the users list
        User user2 = users.get(1);
        user2.setProfileDirector();
        userService.updateUser(user2);

        // Assign profile ROLE_COLLABORATOR to all, if any, remaining users in the users list
        for (int i = 2; i < users.size(); i++) {
            User user = users.get(i);
            user.setProfileCollaborator();
            userService.updateUser(user);
        }

        projectDTO = projectXmlConverter.parseToProject(inputXml);
    }

    @Test
    @Transactional
    public void validateDateFail() {
        // Given
        // When

        ProjectBuilder projectBuilderReflected = null;

        try {
            Constructor<ProjectBuilder> constructor = ProjectBuilder.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            projectBuilderReflected = constructor.newInstance();

            Class<?>[] parameterTypes = new Class<?>[1];
            parameterTypes[0] = String.class;
            Method method = ProjectBuilder.class.getDeclaredMethod("validateDate", parameterTypes);
            method.setAccessible(true);
            // Then
            assertNull((boolean) method.invoke(projectBuilderReflected, ""));
            assertNull((boolean) method.invoke(projectBuilderReflected, "N/A"));
            assertNull((boolean) method.invoke(projectBuilderReflected, "NA"));

        } catch (Exception e) {
        }
    }

    @Test
    @Transactional
    public void createAndPersistProjectTestCI() {
        projectDTO = projectXmlConverter.parseToProject(inputXmlCI);
        projectBuilder.createAndPersistProject(projectDTO);

        assertEquals("TestProj01", projectDTO.getId());
        assertEquals("Projeto de teste inicial", projectDTO.getDescription());
        assertEquals("Projeto de teste 1", projectDTO.getName());
        assertEquals("01/10/2017", projectDTO.getStartDate());
        assertEquals("30/06/2018", projectDTO.getFinalDate());
        assertEquals(50.0, projectDTO.getGlobalBudget(), 0.001);
        assertEquals("Inativo", projectDTO.getStatus());
        assertEquals("tc@mymail.com", projectDTO.getProjectManager());
        assertEquals("HOURS", projectDTO.getUnit());
        assertEquals("CI", projectDTO.getCostCalculatorPersistence());

        ProjectCollaboratorDTO projCollabDTO = projectDTO.getProjectCollaboratorList().get(0);
        assertEquals("js@mymail.com", projCollabDTO.getId());

        ProjectCollaboratorCostAndTimePeriodDTO projCollabCostTime = projCollabDTO.getCostAndTimePeriodList().get(0);
        assertEquals("01/10/2017", projCollabCostTime.getStartDate());
        assertEquals("", projCollabCostTime.getEndDate());
        assertEquals("1", projCollabCostTime.getCost());

        TaskDTO taskDTO = projectDTO.getTasks().get(0);

        assertEquals("Gestão do Projeto", taskDTO.getTitle());
        assertEquals("Gestão do projeto, bla, bla, bla...", taskDTO.getDescription());
        assertEquals("WP1.T01", taskDTO.getTaskId());
        assertEquals("Ativo", taskDTO.getState());
        assertEquals(3.0, taskDTO.getEstimatedEffort(), 0.01);
        assertEquals(2.0, taskDTO.getUnitCost(), 0.01);
        assertEquals("01/10/2017", taskDTO.getPredictedDateOfStart());
        assertEquals("30/06/2018", taskDTO.getPredictedDateOfConclusion());
        assertEquals("N/A", taskDTO.getEffectiveDateOfConclusion());

        List<String> dependencies = taskDTO.getTaskDependencies();
        assertEquals("N/A", dependencies.get(0));

        TaskCollaboratorDTO taskCollaborators = taskDTO.getTaskCollaborators().get(0);
        assertEquals("tc@mymail.com", taskCollaborators.getId());

        TaskCollaboratorRegistryDTO taskCollReg = taskCollaborators.getTaskCollaboratorLink().get(0);
        assertEquals("01/10/2017", taskCollReg.getStartDate());
        assertEquals("N/A", taskCollReg.getEndDate());

        ReportDTO report = taskCollReg.getReports().get(0);
        assertEquals("01/10/2017", report.getStartDate());
        assertEquals("31/12/2017", report.getEndDate());
        assertEquals(1.5, report.getEffort(), 0.01);

    }

    @Test
    @Transactional
    public void createAndPersistProjectTestFail() {
        projectDTO = projectXmlConverter.parseToProject(inputXmlFail);
        projectBuilder.createAndPersistProject(projectDTO);

        assertEquals("TestProj01", projectDTO.getId());
        assertEquals("Projeto de teste inicial", projectDTO.getDescription());
        assertEquals("Projeto de teste 1", projectDTO.getName());
        assertEquals("01/10/2017", projectDTO.getStartDate());
        assertEquals("30/06/2018", projectDTO.getFinalDate());
        assertEquals(50.0, projectDTO.getGlobalBudget(), 0.001);
        assertEquals("Inativo", projectDTO.getStatus());
        assertEquals("tc@mymail.com", projectDTO.getProjectManager());
        assertEquals("HOURS", projectDTO.getUnit());
        assertEquals("CI", projectDTO.getCostCalculatorPersistence());

        ProjectCollaboratorDTO projCollabDTO = projectDTO.getProjectCollaboratorList().get(0);
        assertEquals("j", projCollabDTO.getId());

        ProjectCollaboratorCostAndTimePeriodDTO projCollabCostTime = projCollabDTO.getCostAndTimePeriodList().get(0);
        assertEquals("01/10/2017", projCollabCostTime.getStartDate());
        assertEquals("", projCollabCostTime.getEndDate());
        assertEquals("1", projCollabCostTime.getCost());

        TaskDTO taskDTO = projectDTO.getTasks().get(0);

        assertEquals("Gestão do Projeto", taskDTO.getTitle());
        assertEquals("Gestão do projeto, bla, bla, bla...", taskDTO.getDescription());
        assertEquals("WP1.T01", taskDTO.getTaskId());
        assertEquals("Ativo", taskDTO.getState());
        assertEquals(3.0, taskDTO.getEstimatedEffort(), 0.01);
        assertEquals(2.0, taskDTO.getUnitCost(), 0.01);
        assertEquals("01/10/2017", taskDTO.getPredictedDateOfStart());
        assertEquals("30/06/2018", taskDTO.getPredictedDateOfConclusion());
        assertEquals("N/A", taskDTO.getEffectiveDateOfConclusion());

        List<String> dependencies = taskDTO.getTaskDependencies();
        assertEquals("N/A", dependencies.get(0));

        TaskCollaboratorDTO taskCollaborators = taskDTO.getTaskCollaborators().get(0);
        assertEquals("tc@mymail.com", taskCollaborators.getId());

        TaskCollaboratorRegistryDTO taskCollReg = taskCollaborators.getTaskCollaboratorLink().get(0);
        assertEquals("01/10/2017", taskCollReg.getStartDate());
        assertEquals("N/A", taskCollReg.getEndDate());

        ReportDTO report = taskCollReg.getReports().get(0);
        assertEquals("01/10/2017", report.getStartDate());
        assertEquals("31/12/2017", report.getEndDate());
        assertEquals(1.5, report.getEffort(), 0.01);

    }


    @Test
    @Transactional
    public void createAndPersistProjectTestLTP() {
        projectDTO = projectXmlConverter.parseToProject(inputXmlLTP);
        projectBuilder.createAndPersistProject(projectDTO);

        assertEquals("Projeto de teste inicial", projectDTO.getDescription());
        assertEquals("TestProj01", projectDTO.getId());
        assertEquals("Projeto de teste 1", projectDTO.getName());
        assertEquals("01/10/2017", projectDTO.getStartDate());
        assertEquals("30/06/2018", projectDTO.getFinalDate());
        assertEquals(50.0, projectDTO.getGlobalBudget(), 0.001);
        assertEquals("Inativo", projectDTO.getStatus());
        assertEquals("tc@mymail.com", projectDTO.getProjectManager());
        assertEquals("LTP", projectDTO.getCostCalculatorPersistence());
        assertEquals("HOURS", projectDTO.getUnit());

        ProjectCollaboratorDTO projCollabDTO = projectDTO.getProjectCollaboratorList().get(0);
        assertEquals("js@mymail.com", projCollabDTO.getId());

        ProjectCollaboratorCostAndTimePeriodDTO projCollabCostTime = projCollabDTO.getCostAndTimePeriodList().get(0);
        assertEquals("01/10/2017", projCollabCostTime.getStartDate());
        assertEquals("", projCollabCostTime.getEndDate());
        assertEquals("1", projCollabCostTime.getCost());

        TaskDTO taskDTO = projectDTO.getTasks().get(0);

        assertEquals("Gestão do Projeto", taskDTO.getTitle());
        assertEquals("Gestão do projeto, bla, bla, bla...", taskDTO.getDescription());
        assertEquals("WP1.T01", taskDTO.getTaskId());
        assertEquals("Ativo", taskDTO.getState());
        assertEquals(3.0, taskDTO.getEstimatedEffort(), 0.01);
        assertEquals(2.0, taskDTO.getUnitCost(), 0.01);
        assertEquals("01/10/2017", taskDTO.getPredictedDateOfStart());
        assertEquals("30/06/2018", taskDTO.getPredictedDateOfConclusion());
        assertEquals("N/A", taskDTO.getEffectiveDateOfConclusion());

        List<String> dependencies = taskDTO.getTaskDependencies();
        assertEquals("N/A", dependencies.get(0));

        TaskCollaboratorDTO taskCollaborators = taskDTO.getTaskCollaborators().get(0);
        assertEquals("tc@mymail.com", taskCollaborators.getId());

        TaskCollaboratorRegistryDTO taskCollReg = taskCollaborators.getTaskCollaboratorLink().get(0);
        assertEquals("01/10/2017", taskCollReg.getStartDate());
        assertEquals("N/A", taskCollReg.getEndDate());

        ReportDTO report = taskCollReg.getReports().get(0);
        assertEquals("01/10/2017", report.getStartDate());
        assertEquals("31/12/2017", report.getEndDate());
        assertEquals(1.5, report.getEffort(), 0.01);

    }

    @Test
    @Transactional
    public void createAndPersistProjectTestCM() {
        projectDTO = projectXmlConverter.parseToProject(inputXmlCM);
        projectBuilder.createAndPersistProject(projectDTO);

        assertEquals("Projeto de teste 1", projectDTO.getName());
        assertEquals("TestProj01", projectDTO.getId());
        assertEquals("Projeto de teste inicial", projectDTO.getDescription());
        assertEquals("01/10/2017", projectDTO.getStartDate());
        assertEquals("30/06/2018", projectDTO.getFinalDate());
        assertEquals(50.0, projectDTO.getGlobalBudget(), 0.001);
        assertEquals("tc@mymail.com", projectDTO.getProjectManager());
        assertEquals("Inativo", projectDTO.getStatus());
        assertEquals("HOURS", projectDTO.getUnit());
        assertEquals("CM", projectDTO.getCostCalculatorPersistence());

        ProjectCollaboratorDTO projCollabDTO = projectDTO.getProjectCollaboratorList().get(0);
        assertEquals("js@mymail.com", projCollabDTO.getId());

        ProjectCollaboratorCostAndTimePeriodDTO projCollabCostTime = projCollabDTO.getCostAndTimePeriodList().get(0);
        assertEquals("01/10/2017", projCollabCostTime.getStartDate());
        assertEquals("", projCollabCostTime.getEndDate());
        assertEquals("1", projCollabCostTime.getCost());

        TaskDTO taskDTO = projectDTO.getTasks().get(0);

        assertEquals("Gestão do Projeto", taskDTO.getTitle());
        assertEquals("Gestão do projeto, bla, bla, bla...", taskDTO.getDescription());
        assertEquals("WP1.T01", taskDTO.getTaskId());
        assertEquals("Ativo", taskDTO.getState());
        assertEquals(3.0, taskDTO.getEstimatedEffort(), 0.01);
        assertEquals(2.0, taskDTO.getUnitCost(), 0.01);
        assertEquals("01/10/2017", taskDTO.getPredictedDateOfStart());
        assertEquals("30/06/2018", taskDTO.getPredictedDateOfConclusion());
        assertEquals("N/A", taskDTO.getEffectiveDateOfConclusion());

        List<String> dependencies = taskDTO.getTaskDependencies();
        assertEquals("N/A", dependencies.get(0));

        TaskCollaboratorDTO taskCollaborators = taskDTO.getTaskCollaborators().get(0);
        assertEquals("tc@mymail.com", taskCollaborators.getId());

        TaskCollaboratorRegistryDTO taskCollReg = taskCollaborators.getTaskCollaboratorLink().get(0);
        assertEquals("01/10/2017", taskCollReg.getStartDate());
        assertEquals("N/A", taskCollReg.getEndDate());

        ReportDTO report = taskCollReg.getReports().get(0);
        assertEquals("01/10/2017", report.getStartDate());
        assertEquals("31/12/2017", report.getEndDate());
        assertEquals(1.5, report.getEffort(), 0.01);

    }

    @Test
    @Transactional
    public void createAndPersistProjectTest3() {
        projectDTO = projectXmlConverter.parseToProject(inputXml2);
        projectBuilder.createAndPersistProject(projectDTO);

        assertEquals("Projeto de teste 1", projectDTO.getName());
        assertEquals("TestProj01", projectDTO.getId());
        assertEquals("Projeto de teste inicial", projectDTO.getDescription());
        assertEquals("01/10/2017", projectDTO.getStartDate());
        assertEquals(50.0, projectDTO.getGlobalBudget(), 0.001);
        assertEquals("30/06/2018", projectDTO.getFinalDate());
        assertEquals("Inativo", projectDTO.getStatus());
        assertEquals("tc@mymail.com", projectDTO.getProjectManager());
        assertEquals("HOURS", projectDTO.getUnit());
        assertEquals("CI", projectDTO.getCostCalculatorPersistence());

        ProjectCollaboratorDTO projCollabDTO = projectDTO.getProjectCollaboratorList().get(0);
        assertEquals("js@mymail.com", projCollabDTO.getId());

        ProjectCollaboratorCostAndTimePeriodDTO projCollabCostTime = projCollabDTO.getCostAndTimePeriodList().get(0);
        assertEquals("", projCollabCostTime.getStartDate());
        assertEquals("", projCollabCostTime.getEndDate());
        assertEquals("1", projCollabCostTime.getCost());

        TaskDTO taskDTO = projectDTO.getTasks().get(0);

        assertEquals("Gestão do Projeto", taskDTO.getTitle());
        assertEquals("Gestão do projeto, bla, bla, bla...", taskDTO.getDescription());
        assertEquals("WP1.T01", taskDTO.getTaskId());
        assertEquals("Ativo", taskDTO.getState());
        assertEquals(3.0, taskDTO.getEstimatedEffort(), 0.01);
        assertEquals(2.0, taskDTO.getUnitCost(), 0.01);
        assertEquals("01/10/2017", taskDTO.getPredictedDateOfStart());
        assertEquals("30/06/2018", taskDTO.getPredictedDateOfConclusion());
        assertEquals("N/A", taskDTO.getEffectiveDateOfConclusion());

        List<String> dependencies = taskDTO.getTaskDependencies();
        assertEquals("N/A", dependencies.get(0));

        TaskCollaboratorDTO taskCollaborators = taskDTO.getTaskCollaborators().get(0);
        assertEquals("tc@mymail.com", taskCollaborators.getId());

        TaskCollaboratorRegistryDTO taskCollReg = taskCollaborators.getTaskCollaboratorLink().get(0);
        assertEquals("01/10/2017", taskCollReg.getStartDate());
        assertEquals("N/A", taskCollReg.getEndDate());

        ReportDTO report = taskCollReg.getReports().get(0);
        assertEquals("", report.getStartDate());
        assertEquals("", report.getEndDate());
        assertEquals(1.5, report.getEffort(), 0.01);

    }

    @Test
    @Transactional
    public void createAndPersistProjectTest() {

        projectBuilder.createAndPersistProject(projectDTO);

        assertEquals("Projeto de teste 1", projectDTO.getName());
        assertEquals("TestProj01", projectDTO.getId());
        assertEquals("Projeto de teste inicial", projectDTO.getDescription());
        assertEquals("01/10/2017", projectDTO.getStartDate());
        assertEquals("30/06/2018", projectDTO.getFinalDate());
        assertEquals(50.0, projectDTO.getGlobalBudget(), 0.001);
        assertEquals("Ativo", projectDTO.getStatus());
        assertEquals("tc@mymail.com", projectDTO.getProjectManager());
        assertEquals("PM", projectDTO.getUnit());
        assertEquals("CI", projectDTO.getCostCalculatorPersistence());

        ProjectCollaboratorDTO projCollabDTO = projectDTO.getProjectCollaboratorList().get(0);
        assertEquals("js@mymail.com", projCollabDTO.getId());

        ProjectCollaboratorCostAndTimePeriodDTO projCollabCostTime = projCollabDTO.getCostAndTimePeriodList().get(0);
        assertEquals("01/10/2017", projCollabCostTime.getStartDate());
        assertEquals("", projCollabCostTime.getEndDate());
        assertEquals("1", projCollabCostTime.getCost());

        TaskDTO taskDTO = projectDTO.getTasks().get(0);

        assertEquals("Gestão do Projeto", taskDTO.getTitle());
        assertEquals("Gestão do projeto, bla, bla, bla...", taskDTO.getDescription());
        assertEquals("WP1.T01", taskDTO.getTaskId());
        assertEquals("Ativo", taskDTO.getState());
        assertEquals(3.0, taskDTO.getEstimatedEffort(), 0.01);
        assertEquals(2.0, taskDTO.getUnitCost(), 0.01);
        assertEquals("01/10/2017", taskDTO.getPredictedDateOfStart());
        assertEquals("30/06/2018", taskDTO.getPredictedDateOfConclusion());
        assertEquals("N/A", taskDTO.getEffectiveDateOfConclusion());

        List<String> dependencies = taskDTO.getTaskDependencies();
        assertEquals("N/A", dependencies.get(0));

        TaskCollaboratorDTO taskCollaborators = taskDTO.getTaskCollaborators().get(0);
        assertEquals("tc@mymail.com", taskCollaborators.getId());

        TaskCollaboratorRegistryDTO taskCollReg = taskCollaborators.getTaskCollaboratorLink().get(0);
        assertEquals("01/10/2017", taskCollReg.getStartDate());
        assertEquals("N/A", taskCollReg.getEndDate());

        ReportDTO report = taskCollReg.getReports().get(0);
        assertEquals("01/10/2017", report.getStartDate());
        assertEquals("31/12/2017", report.getEndDate());
        assertEquals(1.5, report.getEffort(), 0.01);

    }

    @Test
    @Transactional
    public void createAndPersistProjectTest2() {

        projectBuilder.createAndPersistProject(projectDTO);

        assertEquals("Projeto de teste 1", projectDTO.getName());
        assertEquals("TestProj01", projectDTO.getId());
        assertEquals("Projeto de teste inicial", projectDTO.getDescription());
        assertEquals("01/10/2017", projectDTO.getStartDate());
        assertEquals("30/06/2018", projectDTO.getFinalDate());
        assertEquals(50.0, projectDTO.getGlobalBudget(), 0.001);
        assertEquals("Ativo", projectDTO.getStatus());
        assertEquals("tc@mymail.com", projectDTO.getProjectManager());
        assertEquals("PM", projectDTO.getUnit());
        assertEquals("CI", projectDTO.getCostCalculatorPersistence());

        StringBuilder builder = new StringBuilder();
        builder.append("[lista_projetos=[");
        builder.append(projectDTO);
        builder.append("]]");

        ProjectCollaboratorDTO projCollabDTO = projectDTO.getProjectCollaboratorList().get(0);
        assertEquals("js@mymail.com", projCollabDTO.getId());

        ProjectCollaboratorCostAndTimePeriodDTO projCollabCostTime = projCollabDTO.getCostAndTimePeriodList().get(0);
        assertEquals("01/10/2017", projCollabCostTime.getStartDate());
        assertEquals("", projCollabCostTime.getEndDate());
        assertEquals("1", projCollabCostTime.getCost());

        TaskDTO taskDTO = projectDTO.getTasks().get(0);

        assertEquals("Gestão do Projeto", taskDTO.getTitle());
        assertEquals("Gestão do projeto, bla, bla, bla...", taskDTO.getDescription());
        assertEquals("WP1.T01", taskDTO.getTaskId());
        assertEquals("Ativo", taskDTO.getState());
        assertEquals(3.0, taskDTO.getEstimatedEffort(), 0.01);
        assertEquals(2.0, taskDTO.getUnitCost(), 0.01);
        assertEquals("01/10/2017", taskDTO.getPredictedDateOfStart());
        assertEquals("30/06/2018", taskDTO.getPredictedDateOfConclusion());
        assertEquals("N/A", taskDTO.getEffectiveDateOfConclusion());

        List<String> dependencies = taskDTO.getTaskDependencies();
        assertEquals("N/A", dependencies.get(0));

        TaskCollaboratorDTO taskCollaborators = taskDTO.getTaskCollaborators().get(0);
        assertEquals("tc@mymail.com", taskCollaborators.getId());

        TaskCollaboratorRegistryDTO taskCollReg = taskCollaborators.getTaskCollaboratorLink().get(0);
        assertEquals("01/10/2017", taskCollReg.getStartDate());
        assertEquals("N/A", taskCollReg.getEndDate());

        ReportDTO report = taskCollReg.getReports().get(0);
        assertEquals("01/10/2017", report.getStartDate());
        assertEquals("31/12/2017", report.getEndDate());
        assertEquals(1.5, report.getEffort(), 0.01);

    }

    @Test
    @Transactional
    public void createAndPersistProjectTestFailEmptyDependencies() {

        projectBuilder.createAndPersistProject(projectDTO);

        assertEquals("Projeto de teste 1", projectDTO.getName());
        assertEquals("TestProj01", projectDTO.getId());
        assertEquals("Projeto de teste inicial", projectDTO.getDescription());
        assertEquals("01/10/2017", projectDTO.getStartDate());
        assertEquals("30/06/2018", projectDTO.getFinalDate());
        assertEquals(50.0, projectDTO.getGlobalBudget(), 0.001);
        assertEquals("Ativo", projectDTO.getStatus());
        assertEquals("tc@mymail.com", projectDTO.getProjectManager());
        assertEquals("PM", projectDTO.getUnit());
        assertEquals("CI", projectDTO.getCostCalculatorPersistence());

        StringBuilder builder = new StringBuilder();
        builder.append("[lista_projetos=[");
        builder.append(projectDTO);
        builder.append("]]");

        ProjectCollaboratorDTO projCollabDTO = projectDTO.getProjectCollaboratorList().get(0);
        assertEquals("js@mymail.com", projCollabDTO.getId());

        ProjectCollaboratorCostAndTimePeriodDTO projCollabCostTime = projCollabDTO.getCostAndTimePeriodList().get(0);
        assertEquals("01/10/2017", projCollabCostTime.getStartDate());
        assertEquals("", projCollabCostTime.getEndDate());
        assertEquals("1", projCollabCostTime.getCost());

        TaskDTO taskDTO = projectDTO.getTasks().get(0);

        assertEquals("Gestão do Projeto", taskDTO.getTitle());
        assertEquals("Gestão do projeto, bla, bla, bla...", taskDTO.getDescription());
        assertEquals("WP1.T01", taskDTO.getTaskId());
        assertEquals("Ativo", taskDTO.getState());
        assertEquals(3.0, taskDTO.getEstimatedEffort(), 0.01);
        assertEquals(2.0, taskDTO.getUnitCost(), 0.01);
        assertEquals("01/10/2017", taskDTO.getPredictedDateOfStart());
        assertEquals("30/06/2018", taskDTO.getPredictedDateOfConclusion());
        assertEquals("N/A", taskDTO.getEffectiveDateOfConclusion());

        TaskCollaboratorDTO taskCollaborators = taskDTO.getTaskCollaborators().get(0);
        assertEquals("tc@mymail.com", taskCollaborators.getId());

        TaskCollaboratorRegistryDTO taskCollReg = taskCollaborators.getTaskCollaboratorLink().get(0);
        assertEquals("01/10/2017", taskCollReg.getStartDate());
        assertEquals("N/A", taskCollReg.getEndDate());

        ReportDTO report = taskCollReg.getReports().get(0);
        assertEquals("01/10/2017", report.getStartDate());
        assertEquals("31/12/2017", report.getEndDate());
        assertEquals(1.5, report.getEffort(), 0.01);
    }

    @Test
    @Transactional
    public void createAndPersistProjectFailTaskCollaboratorsList() {

        projectBuilder.createAndPersistProject(projectDTO);

        assertEquals("Projeto de teste 1", projectDTO.getName());
        assertEquals("TestProj01", projectDTO.getId());
        assertEquals("Projeto de teste inicial", projectDTO.getDescription());
        assertEquals("01/10/2017", projectDTO.getStartDate());
        assertEquals("30/06/2018", projectDTO.getFinalDate());
        assertEquals(50.0, projectDTO.getGlobalBudget(), 0.001);
        assertEquals("Ativo", projectDTO.getStatus());
        assertEquals("tc@mymail.com", projectDTO.getProjectManager());
        assertEquals("PM", projectDTO.getUnit());
        assertEquals("CI", projectDTO.getCostCalculatorPersistence());

        ProjectCollaboratorDTO projCollabDTO = projectDTO.getProjectCollaboratorList().get(0);
        assertEquals("js@mymail.com", projCollabDTO.getId());

        ProjectCollaboratorCostAndTimePeriodDTO projCollabCostTime = projCollabDTO.getCostAndTimePeriodList().get(0);
        assertEquals("01/10/2017", projCollabCostTime.getStartDate());
        assertEquals("", projCollabCostTime.getEndDate());
        assertEquals("1", projCollabCostTime.getCost());

        TaskDTO taskDTO = projectDTO.getTasks().get(0);

        assertEquals("Gestão do Projeto", taskDTO.getTitle());
        assertEquals("Gestão do projeto, bla, bla, bla...", taskDTO.getDescription());
        assertEquals("WP1.T01", taskDTO.getTaskId());
        assertEquals("Ativo", taskDTO.getState());
        assertEquals(3.0, taskDTO.getEstimatedEffort(), 0.01);
        assertEquals(2.0, taskDTO.getUnitCost(), 0.01);
        assertEquals("01/10/2017", taskDTO.getPredictedDateOfStart());
        assertEquals("30/06/2018", taskDTO.getPredictedDateOfConclusion());
        assertEquals("N/A", taskDTO.getEffectiveDateOfConclusion());

        List<String> dependencies = taskDTO.getTaskDependencies();
        assertEquals("N/A", dependencies.get(0));

    }

}
