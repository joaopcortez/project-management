package project.util;

import project.dto.ProjectDTO;

public interface ProjectConverter {

    ProjectDTO parseToProject(String inputFile);
}
