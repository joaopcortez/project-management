package project.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

public final class ProjectConverterFactory {

	private static String version = null;
	private static Logger logger = Logger.getAnonymousLogger();
	private static String pathToMapperFile = System.getProperty("user.dir")
			+ "/src/main/resources/application.properties";

	private ProjectConverterFactory() {
	}

	public static ProjectConverter createProjectConverter(String inputFileComplete) {
		String extension;
		String projectConverterClassName;
		String inputFile;
		ProjectConverter projectDefaultXmlConverter = new ProjectXmlConverter();

		inputFile = inputFileComplete.substring(inputFileComplete.lastIndexOf('/') + 1);
		extension = inputFile.substring(inputFile.lastIndexOf('.') + 1);

		Pattern pattern = Pattern.compile("Projeto" + ".*v[0-9]{2}");
		Matcher matcher = pattern.matcher(inputFile);
		if (matcher.find()) {
			version = matcher.group();
		}

		try {
			projectConverterClassName = readConverterClass(pathToMapperFile, extension, version);

		} catch (Exception ioe) {
			logger.log(Level.WARNING, "Wrong filename, returning default converter.", ioe);
			return projectDefaultXmlConverter;
		}
		try {
			projectDefaultXmlConverter = contructClassFromString(projectConverterClassName);
		} catch (Exception e) {
			logger.log(Level.WARNING, "Wrong class name, returning default converter.", e);
			return projectDefaultXmlConverter;
		}

		return projectDefaultXmlConverter;
	}

	private static ProjectConverter contructClassFromString(String projectConverterClassName) throws Exception {

		Class<?> clazz;

		clazz = Class.forName(ProjectConverter.class.getPackage().getName() + "." + projectConverterClassName);
		Constructor<?> construct = clazz.getConstructor();
		return (ProjectConverter) construct.newInstance();

	}

	private static String readConverterClass(String pathToMapperFile, String extension, String version)
			throws IOException {
		String versionAndExtensionName;
		String converterProjectClassName;

		versionAndExtensionName = (StringUtils.isEmpty(version) ? extension : extension + "." + version);

		BufferedReader brProject = getBufferedReader(pathToMapperFile);

		String strProject;
		while ((strProject = brProject.readLine()) != null) {
			if (strProject.contains(versionAndExtensionName)) {
				converterProjectClassName = strProject.substring(strProject.lastIndexOf('=') + 1);
				return converterProjectClassName;
			}
		}
		brProject.close();
		return null;
	}

	private static BufferedReader getBufferedReader(String pathToFile) throws IOException {

		Path path = Paths.get(pathToFile);

		return Files.newBufferedReader(path);
	}

	public static void setPathToMapperFile(String pathToMapperFile) {
		ProjectConverterFactory.pathToMapperFile = pathToMapperFile;
	}
}
