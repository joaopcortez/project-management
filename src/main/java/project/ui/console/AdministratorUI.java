package project.ui.console;

import org.springframework.stereotype.Component;
import project.controllers.consolecontrollers.*;
import project.services.EmailService;
import project.model.UserService;

import javax.mail.internet.AddressException;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.Scanner;

@Component
public class AdministratorUI {

	static Scanner scan = new Scanner(System.in);
	static PrintStream writer = new PrintStream(System.out);
	static boolean leaveUI = false;
	private PopulateFileDataUI populateFileDataUI;
	UserService userService;
	EmailService emailService;
	ProfileController profileController;
	UserActivationController userActivationController;
	UserRegistryController userRegistryController;
	RegistryController registryController;
	private String name;
	private String phone;
	private String email;
	private String tin;
	private String date;
	private String addId;
	private String street;
	private String postalCode;
	private String city;
	private String country;
	private String pwd;
	private LocalDate birth;
	String fourLinesFeed = "\n\n\n\n";
	String separator = "______________________________________";

	String separator2 = "_______________________________________________________________";


	public AdministratorUI(UserService userService, EmailService emailService,  PopulateFileDataUI populateFileDataUI) {
		this.userService = userService;
		this.emailService = emailService;
		this.populateFileDataUI= populateFileDataUI;
	}

	private void parseUsersBirthDate() {

		String[] dateSplited = date.split("/");
		int day = Integer.parseInt(dateSplited[0]);
		int month = Integer.parseInt(dateSplited[1]);
		int year = Integer.parseInt(dateSplited[2]);

		birth = LocalDate.of(year, month, day);
	}

	private void printUserInformation() {

		String userInformation = ("\nName: " + name + "\nPhone: " + phone + "\nEmail: " + email
				+ "\nTax identification number (tin): " + tin + "\nBirthdate: " + date + "\nAddress identifier id: "
				+ addId + "\nStreet: " + street + "\nPostal code: " + postalCode + "\nCity: " + city + "\nCountry: "
				+ country);
		writer.println(userInformation);
	}

	private void validateUser() {
		writer.println("\nEnter your name:");
		name = scan.nextLine();

		writer.println("\nEnter password:");
		pwd = scan.nextLine();

		writer.println("\nEnter your phone number:");
		phone = scan.nextLine();
		phone = isValidPhoneNumber(phone);

		writer.println("\nEnter your email:");
		email = scan.nextLine();
		email = isValidEmailString(email);

		writer.println("\nEnter your tax identification number (tin):");
		tin = scan.nextLine();
		tin = isValidTinNumber(tin);

		writer.println("\nEnter your birthdate(dd/mm/yyyy):");
		date = scan.nextLine();
		date = isValidDateString(date);

		parseUsersBirthDate();

	}

	private void validateAddress() {

		writer.println("\nWrite an identifier id for your Address: ");
		addId = scan.nextLine();

		writer.println("\nEnter your Street name:");
		street = scan.nextLine();

		writer.println("\nEnter your postal code:");
		postalCode = scan.nextLine();

		writer.println("\nEnter your city:");
		city = scan.nextLine();
		city = isValidCityString(city);

		writer.println("\nEnter your country:");
		country = scan.nextLine();
		country = isValidCountryString(country);
	}

	private void validateUserInformation() throws AddressException {

		writer.println("\nIs your registration information correct? Press [1] for yes or [2] for no.");
		String acceptance = scan.nextLine();

		switch (acceptance) {

			case "1":
				registryController = new RegistryController(userService, emailService);
				registerUser();

				break;

			case "2":

				writer.println("\n\nRegistry cancelled.\n\n");
				break;

			default:
				break;
		}
	}

	private String isValidCountryString(String country) {
		String auxCountry = country;
		registryController = new RegistryController(userService, emailService);
		while (!registryController.isValidString(auxCountry)) {
			writer.println("\nInvalid city, enter again:");
			auxCountry = scan.nextLine();
		}
		return auxCountry;
	}

	private String isValidCityString(String city) {
		String auxCity = city;
		while (!registryController.isValidString(auxCity)) {
			writer.println("\nInvalid city, enter again:");
			auxCity = scan.nextLine();
		}
		return auxCity;
	}

	private String isValidDateString(String date) {
		String auxDate = date;
		while (!registryController.isValidDate(auxDate)) {
			writer.println("\nInvalid date format, enter again (dd/mm/yyyy):");
			auxDate = scan.nextLine();
		}
		return auxDate;
	}

	private String isValidTinNumber(String tin) {
		String auxTin = tin;
		while (!registryController.isValidNumber(auxTin)) {
			writer.println("\nInvalid tax identification number, enter again:");
			auxTin = scan.nextLine();
		}
		return auxTin;
	}

	private String isValidEmailString(String email) {
		String auxEmail = email;
		while (!registryController.isValidEmail(auxEmail)) {
			writer.println("\nInvalid email, enter again:");
			auxEmail = scan.nextLine();
		}
		return auxEmail;
	}

	private String isValidPhoneNumber(String phone) {
		String auxPhone = phone;
		registryController = new RegistryController(userService, emailService);
		while (!registryController.isValidNumber(auxPhone)) {
			writer.println("\nInvalid phone number, enter again:");
			auxPhone = scan.nextLine();
		}
		return auxPhone;
	}

	private void registerUser() throws AddressException {
		registryController = new RegistryController(userService, emailService);

		if (registryController.registerUser(name, phone, email, tin, birth, addId, street, postalCode, city, country,
				pwd)) {

			writer.println("\nA confirmation e-mail was sent to " + email
					+ " with a confirmation code. On your first login the confirmation code will be asked.\n\n");
		} else {
			writer.println("\n\nWrong data. Please re enter your data.\n\n");
		}
	}

	private String isValidSProfile(String userProfile) {
		String profile = userProfile;
		while (!registryController.isValidProfile(profile)) {
			writer.println("\nInvalid profile, enter again:");
			profile = scan.nextLine();
		}
		return profile;
	}

	public void showMenu() {
		String separator3 = "__________________________________";
		writer.println(separator3);
		writer.println("\n       ADMINISTRATOR MENU");
		writer.println(separator3);
		writer.println("\n[1] - ASSIGN DIRECTOR PROFILE");
		writer.println("[2] - ASSIGN COLLABORATOR PROFILE");
		writer.println("[3] - USER REACTIVATION");
		writer.println("[4] - USER DEACTIVATION");
		writer.println("[5] - LIST OF ALL USERS");
		writer.println("[6] - SEARCH USER BY EMAIL");
		writer.println("[7] - SEARCH USER BY PROFILE");
		writer.println("[8] - POPULATE DATABASE FROM FILE");
		writer.println("[ ] - PRESS OTHER KEY TO LOGOUT");
		writer.println(separator3);
		writer.println("\n\n");
	}

	public synchronized void chooseOption() throws Exception {
		profileController = new ProfileController(userService);
		userActivationController = new UserActivationController(userService);
		userRegistryController = new UserRegistryController(userService);
		String userEmail;

		do {
			showMenu();
			String choice = scan.nextLine();
			String message = "\nProfiles: ADMINISTRATOR, DIRECTOR, COLLABORATOR, REGISTEREDUSER\nEnter the profile to search users:\n";

			switch (choice) {
				case "1":
					writer.println("\nEnter the user email to assign the director profile:");
					userEmail = scan.nextLine();
					userEmail = isValidEmailString(userEmail);
					writer.println(profileController.setProfileDirector(userEmail));
					break;
				case "2":
					writer.println("\nEnter the user email to assign the collaborator profile:");
					userEmail = scan.nextLine();
					userEmail = isValidEmailString(userEmail);
					writer.println(profileController.setProfileCollaborator(userEmail));
					break;
				case "3":
					writer.println("\nEnter the user email to reactivate:");
					userEmail = scan.nextLine();
					userEmail = isValidEmailString(userEmail);
					writer.println(userActivationController.reactivateUser(userEmail));
					break;
				case "4":
					writer.println("\nEnter the user email to desactivate:");
					userEmail = scan.nextLine();
					userEmail = isValidEmailString(userEmail);
					writer.println(userActivationController.inactivateUser(userEmail));
					break;
				case "5":
					writer.println(separator + "\n   LIST OF ALL USERS OF THE COMPANY" + separator + "\n");
					writer.println(userRegistryController.listAllUsers());
					writer.println(separator);
					writer.println(fourLinesFeed);
					break;
				case "6":
					writer.println("\nEnter the user email to search in user registry:");
					userEmail = scan.nextLine();
					userEmail = isValidEmailString(userEmail);
					printSearchUserByEmail(userEmail);
					break;
				case "7":
					String userProfile = getUserProfile(message);
					userProfile = isValidSProfile(userProfile.toUpperCase());
					writer.println(separator2 + "\n               LIST OF USERS WITH CHOSEN PROFILE");
					printSearchUserByProfile(userProfile);
					break;
				case "8":
					populateFileDataUI.chooseFileToPopulateDatabase();
					break;
				default:
					leaveUI = true;
					break;
			}
		} while (!leaveUI);
	}

	private void printSearchUserByEmail(String userEmail) {
		writer.println(separator + "\n");
		writer.println(userRegistryController.searchUserByEmail(userEmail));
		writer.println(separator + fourLinesFeed);
	}

	private void printSearchUserByProfile(String userProfile) {
		writer.println(separator2 + "\n");
		writer.println(userRegistryController.searchUserByProfile(userProfile));
		writer.println(separator2 + fourLinesFeed);
	}

	private static String getUserProfile(String message) {
		writer.println(message);
		return scan.nextLine();
	}

	public synchronized void register() throws AddressException {

		registryController = new RegistryController(userService, emailService);

		writer.println("To register, the application needs to access your personal data.\n"
				+ "Do you accept these access conditions? Press [1] for yes or [2] for no.");
		String conditions = scan.nextLine();

		switch (conditions) {
			case "1":
				validateUser();
				validateAddress();
				printUserInformation();
				validateUserInformation();
				break;
			case "2":
				writer.println("\n\nRegistry failed because you didn't accept the conditions.\n\n");
				break;
			default:
				break;

		}

	}


}
