package project.ui.console;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import project.controllers.consolecontrollers.AccountController;
import project.controllers.consolecontrollers.UserDataController;
import project.model.UserService;
import system.dto.LoginTokenDTO;

import java.io.PrintStream;
import java.util.Scanner;
@Component
@Profile("console")

public class LoginUI {

    private Scanner scan = new Scanner(System.in);
    private PrintStream writer = new PrintStream(System.out);

    boolean isApprovedAccount = true;
    boolean leaveUI = false;
    String separator = "_______________________________________________________";

    @Autowired
    UserService userService;

    @Autowired
    private ApplicationContext context;
    @Autowired
    private AccountController accountController;
    @Autowired
    private AdministratorUI administratorUi;
    @Autowired
    private DirectorUI directorUi;
    @Autowired
    private CollaboratorUI collaboratorUi;
    @Autowired
    private RegisteredUserUI regUserUi;
    @Autowired
    private UserDataController userDataController;


    public static void clearScreen() {
        for (int clear = 0; clear < 20; clear++) {
            System.out.println("\b");
        }
    }

    @Bean
    public CommandLineRunner demo() {
        return (String... args) -> {


            clearScreen();
            writer.println("	███████╗██╗    ██╗██╗████████╗ ██████╗██╗  ██╗  ");
            writer.println("	██╔════╝██║    ██║██║╚══██╔══╝██╔════╝██║  ██║  ");
            writer.println("	███████╗██║ █╗ ██║██║   ██║   ██║     ███████║ ");
            writer.println("	╚════██║██║███╗██║██║   ██║   ██║     ██╔══██║  ");
            writer.println("	███████║╚███╔███╔╝██║   ██║   ╚██████╗██║  ██║   ");
            writer.println("	╚══════╝ ╚══╝╚══╝ ╚═╝   ╚═╝    ╚═════╝╚═╝  ╚═╝   ");
            writer.println("	PROJECT MANAGEMENT APP- GROUP1 ");
            writer.println(" ");
            writer.println(" ");

            // http://patorjk.com/software/taag/#p=testall&f=Alphabet&t=Project%0AManagement

            do {
                showMenu();
                chooseOption();
            } while (!leaveUI);
        };
    }



    public void showMenu() {

        writer.println(separator);
        writer.println("\n            WELCOME, CHOOSE AN OPTION:");
        writer.println(separator);
        writer.println("\n[1] - REGISTER");
        writer.println("[2] - LOGIN");
        writer.println("\n[ ] - PRESS ANY KEY TO QUIT");
        writer.println(separator);
        writer.println("\n\n");
    }

    private void chooseOption() throws Exception {
        String choice = scan.nextLine();
        if ("1".equals(choice)) {

            administratorUi.register();
        } else if ("2".equals(choice)) {

            LoginTokenDTO loginDto = doLogin();

            LoginTokenDTO service = context.getBean(LoginTokenDTO.class);

            service.setName(loginDto.getName());
            service.setEmail(loginDto.getEmail());
            service.setMessage(loginDto.getMessage());
            service.setProfile(loginDto.getProfile());
            service.setActive(loginDto.isActive());
            service.setApprovedDateTime(loginDto.getApprovedDateTime());
            service.setConfirmationCode(loginDto.getConfirmationCode());
            service.setLastLoginDate(loginDto.getLastLoginDate());

            if (isFirstLogin(loginDto)) {
                if (loginDto.isActive())
                    firstLoginScreen(loginDto);
                isApprovedAccount = confirmationCode(loginDto);
            }

            if (loginDto.getProfile() != null && isApprovedAccount) {
                accountController.updateLastLoginDate(loginDto.getEmail());
                switch (loginDto.getProfile()) {
                    case "[ROLE_DIRECTOR]":
                        directorUi.chooseOption();
                        break;
                    case "[ROLE_ADMINISTRATOR]":
                        administratorUi.chooseOption();
                        break;
                    case "[ROLE_COLLABORATOR]":
                        collaboratorUi.home();
                        break;
                    case "[ROLE_REGISTEREDUSER]":
                        regUserUi.home();
                        break;
                    default:
                        break;
                }
            }

        } else {
            writer.println("\n\nThank you for your attention. We'll be back...");
            leaveUI = true;
        }
    }

    public synchronized LoginTokenDTO doLogin() {
        String email;
        writer.println("Email: ");
        email = scan.nextLine();
        writer.println("\nPassword: ");
        String password = scan.nextLine();
        LoginTokenDTO loginDTO = accountController.login(email, password);
        writer.println("\n\n");
        writer.println(loginDTO.getMessage());
        return loginDTO;
    }

    public boolean confirmationCode(LoginTokenDTO loginDto) {
        writer.println("\n\n______________________________________________\n");
        writer.println("     YOUR EMAIL ACCOUNT IS NOT VERIFIED");
        writer.println("______________________________________________");
        writer.println("\nPlease, enter confirmation code: ");
        String confirmationCode = scan.nextLine();
        if (confirmationCode.equals(loginDto.getConfirmationCode())) {
            writer.println("\n\nACCOUNT SUCCESSFULLY APPROVED!");
            accountController.approveAccount(loginDto.getEmail());
            return true;
        } else {
            writer.println("\n\nCONFIRMATION CODE IS INCORRECT\n\nYOUR ACCOUNT WILL BE LOGGED OFF...\n\n\n");
            return false;
        }

    }

    public void firstLoginScreen(LoginTokenDTO loginDto) {
        writer.println("\n\n______________________________________________\n");
        writer.println("        COMPLETE ACCOUNT VALIDATION");
        writer.println("______________________________________________\n");
        writer.println("Please insert a new password to complete account validation.\n");
        writer.println("New Password:");
        String newPassword = scan.nextLine();
        writer.println("\n\nPASSWORD WAS SUCCESSFULLY CHANGED\n");

        userDataController.setNewPassword(loginDto.getEmail(), newPassword);
    }

    public boolean isFirstLogin(LoginTokenDTO loginDto) {
        return (loginDto.getProfile() != null && loginDto.getLastLoginDate() == null);
    }


}
