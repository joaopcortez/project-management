package project.ui.console.projectmanagerui;

import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import project.controllers.consolecontrollers.ChooseCostCalculationController;
import project.controllers.consolecontrollers.DirectorSetProjectCostCalculationOptions;
import project.controllers.consolecontrollers.ProjectController;
import project.controllers.consolecontrollers.ReportCostController;
import project.model.UserService;
import project.model.project.ProjectService;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;
import project.ui.console.CollaboratorUI;
import system.dto.LoginTokenDTO;

@Component
public class ProjectManagerUI {

	static PrintStream writer = new PrintStream(System.out);
	static String goBackMessage = "\n[ ] - OTHER KEY TO GO BACK TO PREVIOUS MENU\n";
	LoginTokenDTO loginDto;
	UserService userService;
	ProjectCollaboratorService projCollabRegistry;
	ProjectService projectService;
	@Autowired
	TaskService taskService;
	ReportCostController reportCostController;
	ProjectController projectController;
	ChooseCostCalculationController chooseCostCalculationController;
	DirectorSetProjectCostCalculationOptions setProjectCalculationOptions;
	@Autowired
	CollaboratorUI collaboratorUi;
	ProjectCollaboratorListAddRemoveUi projCollabListAddRemoveUi;
	CreateEditTaskOptionsUI createEditTaskUi;
	AddRemoveTaskFromProjCollabUI addRemoveTaskFromProjCollabUi;
	ListTasksUi listTasksUi;
	String projectId;
	Scanner scan = new Scanner(System.in);
	boolean leaveUi = false;
	static final String FOURLINESFEED = "\n\n\n\n";

	public ProjectManagerUI(LoginTokenDTO ltdto, UserService userService, ProjectService projectService,
			ProjectCollaboratorService projectCollaboratorService, TaskService taskService) {
		this.loginDto = ltdto;
		this.userService = userService;
		this.projectService = projectService;
		this.projCollabRegistry = projectCollaboratorService;
		this.taskService = taskService;
	}

	static void showProjManagerTaskOptionsMenu() {
		String separatorManageTasksMenu = "____________________________________________________";
		writer.println(FOURLINESFEED + separatorManageTasksMenu);
		writer.println("\n               MANAGE TASKS");
		writer.println(separatorManageTasksMenu + "\n");
		writer.println("[1] - CREATE, EDIT TASKS");
		writer.println("[2] - LIST TASKS BY STATUS");
		writer.println("[3] - MANAGE TASK ASSIGNMENTS");
		writer.println(goBackMessage);
	}

	static void showProjManagerCostOptionsMenu() {
		String separatorManageCostsMenu = "____________________________________________________";
		writer.println(FOURLINESFEED + separatorManageCostsMenu);
		writer.println("\n                MANAGE COSTS");
		writer.println(separatorManageCostsMenu);
		writer.println("\n[1] - GET COST SO FAR");
		writer.println("[2] - CHANGE COST CALCULATION TYPE");
		writer.println(goBackMessage);
	}

	private static void listCostCalculationOptions(List<String> calcOptions) {

		String separatorCalculationOptions = "_____________________________";
		System.out.println(FOURLINESFEED + separatorCalculationOptions);
		System.out.println("\nAVAILABLE CALCULATION OPTIONS");
		System.out.println(separatorCalculationOptions + "\n");

		for (int i = 0; i < calcOptions.size(); i++) {
			String[] costOption = calcOptions.get(i).split("Cost");
			System.out.println("[" + (i + 1) + "] - " + costOption[0].toUpperCase() + " COST");
		}
		System.out.println("\n[0] - LEAVE MENU\n");
	}

	private static boolean validateOption(String choice, int listSize) {

		for (int i = 0; i <= (listSize); i++) {
			try {
				if (Integer.parseInt(choice) == i) {
					return true;
				}
			} catch (NumberFormatException nfe) {
				return false;
			}
		}
		return false;
	}

	public synchronized void showProjects() {
		String separatorMyProjectsPMMenu = "_____________________________________________________________________________";
		projectController = new ProjectController(userService, projectService, projCollabRegistry);
		String email = loginDto.getEmail();
		String projects = projectController.listAllProjectManagerProjects(email);
		writer.println(separatorMyProjectsPMMenu);
		writer.println("\n                       MY PROJECTS AS PROJECT MANAGER");
		writer.println(separatorMyProjectsPMMenu + "\n");
		writer.println(projects);

		if ("THIS USER HAS NO PROJECTS AS PROJECT MANAGER.\n\n".equals(projects)) {
			collaboratorUi.setGoBackToLevel1(true);
			return;

		}

		writer.println("\nChoose a Project:\n");
		projectId = scan.nextLine();

		while (!projects.contains("Project ID: " + projectId) || "".equals(projectId)) {
			writer.println("\nTHE PROJECT ID IS NOT VALID. ENTER AGAIN:\n");
			projectId = scan.nextLine();
		}

		chooseOptionProjectManagerOptionsMenu(projectId);

	}

	public void showProjectManagerOptionsmenu(String projectId) {
		String separatorMyProjectsPMMenu = "_____________________________________________________________________________";
		projectController = new ProjectController(userService, projectService, projCollabRegistry);
		writer.println("\n\n" + separatorMyProjectsPMMenu);
		writer.println(projectController.getProjectInfo(projectId));
		writer.println(separatorMyProjectsPMMenu);
		writer.println("\n\n[1] - MANAGE COLLABORATORS MENU");
		writer.println("[2] - MANAGE TASK LIST MENU");
		writer.println("[3] - MANAGE INFO: TOTAL COST SO FAR");
		writer.println(goBackMessage);
	}

	public synchronized void chooseOptionProjectManagerOptionsMenu(String projectId) {

		reportCostController = new ReportCostController(projectService, taskService);
		projCollabListAddRemoveUi = new ProjectCollaboratorListAddRemoveUi(projectId, userService, projectService,
				projCollabRegistry, taskService);
		boolean loop = true;
		do {

			showProjectManagerOptionsmenu(projectId);
			String choice = scan.nextLine();

			switch (choice) {
			case "1":

				projCollabListAddRemoveUi.optionslistInsertAndRemoveProjCollaborator();
				break;
			case "2":
				tasksChooseOptionsMenu();
				break;
			case "3":
				costsChooseOptionsMenu();
				break;
			default:
				loop = false;
			}
		} while (loop);

	}

	private void costsChooseOptionsMenu() {
		chooseCostCalculationController = new ChooseCostCalculationController(projectService);
		boolean loop = true;
		do {
			showProjManagerCostOptionsMenu();
			String choice = scan.nextLine();

			switch (choice) {
			case "1":
				String[] costTypeSelected = chooseCostCalculationController
						.getSelectedProjectCostCalculationMechanism(projectId).split("Cost");
				writer.println(reportCostController.listCostSoFarDetails(projectId));
				printCostSoFar(costTypeSelected[0]);
				break;
			case "2":
				String choiceCost = chooseCostCalculationOptions();
				if ("0".equals(choiceCost)) 
					break;
				else chooseCostCalculationController.setProjectCostCalculationMechanism(projectId, choiceCost);
					break;
			default:
				loop = false;
			}
		} while (loop);
	}

	private void printCostSoFar(String costTypeSelected) {
		writer.println("Calculation type selected: " + costTypeSelected.toUpperCase() + " COST");
		writer.println(
				"\nTOTAL COST SO FAR IS [ " + String.format("%.2f", reportCostController.getProjectCostSoFar(projectId))
						+ " " + reportCostController.getUnit(projectId) + " ]\n");
	}

	private void tasksChooseOptionsMenu() {
		addRemoveTaskFromProjCollabUi = new AddRemoveTaskFromProjCollabUI(projectId, userService, projectService,
				taskService, projCollabRegistry);
		createEditTaskUi = new CreateEditTaskOptionsUI(projectId, userService, projectService, taskService);
		listTasksUi = new ListTasksUi(projectId, projectService, taskService);

		boolean loop = true;
		do {
			showProjManagerTaskOptionsMenu();
			String choice = scan.nextLine();

			switch (choice) {
			case "1":

				createEditTaskUi.tasksOptions();
				break;
			case "2":

				listTasksUi.tasksOptions();
				break;
			case "3":
				addRemoveTaskFromProjCollabUi.optionAssignmentRequests();
				break;
			default:
				loop = false;
			}
		} while (loop);
	}

	private String chooseCostCalculationOptions() {

		setProjectCalculationOptions = new DirectorSetProjectCostCalculationOptions(projectService);

		List<String> calcOptions = setProjectCalculationOptions.listProjectCalculationOptions(projectId);
		String choiceCost;

		listCostCalculationOptions(calcOptions);

		choiceCost = scan.nextLine();

		if ("0".equals(choiceCost)) {
			return choiceCost;
		}

		if (!(validateOption(choiceCost, calcOptions.size()))) {
			System.out.println("INVALID CHOICE. PLEASE TRY AGAIN.");
			chooseCostCalculationOptions();
		}

		choiceCost = setProjectCalculationOptions.listProjectCalculationOptions(projectId)
				.get(Integer.parseInt(choiceCost) - 1);
		System.out.println("CALCULATION TYPE: " + choiceCost.toUpperCase() + ". PRESS [1] TO CALCULATE.");
		return choiceCost;

	}

}