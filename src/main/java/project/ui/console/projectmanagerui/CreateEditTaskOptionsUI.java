package project.ui.console.projectmanagerui;

import project.controllers.consolecontrollers.CreateTaskController;
import project.controllers.consolecontrollers.ProjectTaskListController;
import project.controllers.consolecontrollers.TaskController;
import project.controllers.consolecontrollers.TaskStateController;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.task.Task;
import project.services.TaskService;
import project.ui.console.TaskListUI;

import java.io.PrintStream;
import java.time.LocalDateTime;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CreateEditTaskOptionsUI {

    String projectId;
    UserService userService;
    ProjectService projectService;
    TaskService taskService;
    Logger logger = Logger.getAnonymousLogger();
    ProjectTaskListController projTaskListController;
    TaskStateController taskStateController;
    String insertTaskId = "Insert task ID: ";
    PrintStream writer = new PrintStream(System.out);
    Scanner scan = new Scanner(System.in);
    CreateTaskController createTaskController;
    TaskController taskController;

    public CreateEditTaskOptionsUI(String projectId, UserService userService, ProjectService projectService, TaskService taskService) {
        this.projectId = projectId;
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    private double validateDoublePositiveValues(String hours) {
        double parseValue = -1;
        String message = "INVALID NUMBER!\n";
        try {
            parseValue = Double.parseDouble(hours);
            if (parseValue <= 0.0) {
                logger.log(Level.INFO, message);
                return -1;
            }
        } catch (NumberFormatException nfe) {
            logger.log(Level.INFO, message);
        }
        return parseValue;
    }

    public void showCreteEditTaskOptions() {
        String separatorEditTasksMenu = "___________________________________________";
        String fourLinesFeed = "\n\n\n\n";
        writer.println(fourLinesFeed + separatorEditTasksMenu);
        writer.println("\n           CREATE/EDIT TASKS");
        writer.println(separatorEditTasksMenu + "\n");
        writer.println("[1] - CREATE TASK");
        writer.println("[2] - CANCEL TASK");
        writer.println("[3] - SET TASK COMPLETED");
        writer.println("[4] - REMOVE TASK");
        writer.println("[5] - REVERT COMPLETED TASK TO IN PROGRESS");
        writer.println("[6] - SET TASK DEPENDENCIES");
        writer.println("\n[ ] - OTHER KEY TO GO BACK TO PREVIOUS MENU\n");
    }

    public void tasksOptions() {
        boolean loop = true;
        do {
            showCreteEditTaskOptions();
            String choice = scan.nextLine();

            switch (choice) {
                case "1":
                    addTask();
                    break;
                case "2":
                    cancelTask();
                    break;
                case "3":
                    setTaskCompletedOpt();
                    break;
                case "4":
                    removeTask();
                    break;
                case "5":
                    revertTaskOpt();
                    break;
                case "6":
                    showDependenciesOptions();
                    break;
                default:
                    loop = false;
            }
        } while (loop);

    }

    private void revertTaskOpt() {
        projTaskListController = new ProjectTaskListController(projectService,taskService);
        String separatorRevertTasks = "_____________________________________________________________";
        writer.println(separatorRevertTasks);
        writer.println(projTaskListController.listProjectManagerCompletedTasks(projectId));
        writer.println("Enter task id to revert:");
        String taskId = scan.nextLine();
        writer.println(
                "\n" + projTaskListController.changeCompletedTaskToSuspendedState(taskId, projectId) + "\n");
    }

    private void removeTask() {
        projTaskListController = new ProjectTaskListController(projectService,taskService);
        String separatorListNotInitiatedTasks = "_____________________________________________________________";
        writer.println(separatorListNotInitiatedTasks);
        writer.println(projTaskListController.listNotInitiatedTask(projectId));
        writer.println("Enter task id to remove:");
        String taskId = scan.nextLine();
        writer.println("\n" + projTaskListController.removeNotInitiatedTask(taskId, projectId) + "\n");
    }

    private void setTaskCompletedOpt() {
        projTaskListController = new ProjectTaskListController(projectService,taskService);
        taskStateController = new TaskStateController(projectService, taskService);
        writer.println(projTaskListController.listProjectTasksList(projectId) + "\n\n");
        writer.print(insertTaskId);
        String idTask2 = scan.next();
        writer.println("\n" + taskStateController.setTaskCompleted(projectId, idTask2) + "\n");
        scan.nextLine();
    }

    private void cancelTask() {
        projTaskListController = new ProjectTaskListController(projectService,taskService);
        taskStateController = new TaskStateController(projectService, taskService);
        writer.println(projTaskListController.listProjectTasksList(projectId) + "\n\n");
        writer.println(insertTaskId);
        String idTask = scan.next();
        writer.println("\n" + taskStateController.setTaskCancelled(projectId, idTask) + "\n");
        scan.nextLine();
    }

    public void addTask() {
        String title;
        createTaskController = new CreateTaskController(userService, projectService, taskService);
        do {
            writer.println("\nInsert Task title:\n");
            title = scan.nextLine();
            if (title.isEmpty()) {
                writer.println("\n*** Invalid title (empty field) ***\n");
            }
        } while (title.isEmpty());

        writer.println(
                "\nDo you want to add more information?\n\n[1] - INSERT PREDICTED DATES\n[2] - INSERT PREDICTED DATES RELATED TO PROJECT START DATE\n[3] - SAVE TASK WITHOUT DATES\n");
        String choice;
        do {
            writer.println("\nChoose an option:");
            choice = scan.nextLine();
            if ("1".equals(choice)) {
                addTaskEffectiveDates(title);
            } else if ("2".equals(choice)) {
                addTaskRelativeDates(title);
            } else if ("3".equals(choice)) {
                writer.println(createTaskController.createTaskWithOutDates(projectId, title));
            } else {
                choice = "invalid";
                writer.println("\nINVALID OPTION!\n");
            }
        } while (("invalid").equals(choice));

    }

    private void addTaskEffectiveDates(String title) {
        LocalDateTime predictedDateOfStart;
        LocalDateTime predictedDateOfConclusion;
        createTaskController = new CreateTaskController(userService, projectService, taskService);

        writer.println("\nInsert task description:\n");
        String description = scan.nextLine();

        writer.println("\nInsert task predicted date of start(dd/mm/yyyy):\n");
        String predictedStart = scan.nextLine();

        try {
            predictedDateOfStart = TaskListUI.validateReportsDate(predictedStart).atTime(0, 0);
        } catch (Exception e) {
            logger.log(Level.INFO, "an exception was thrown", e);
            writer.println("\nOPERATION ABORTED!\n INVALID DATE FORMAT!\n");
            return;
        }
        if (predictedDateOfStart.getYear() <= 1950) {
            writer.println("\nOPERATION ABORTED!\n INVALID YEAR!\n");
            return;
        }

        writer.println("\nInsert task predicted date of conclusion (dd/mm/yyyy):\n");
        String predictedConclusion = scan.nextLine();

        try {
            predictedDateOfConclusion = TaskListUI.validateReportsDate(predictedConclusion).atTime(0, 0);
        } catch (Exception e) {
            logger.log(Level.INFO, "an exception was thrown", e);
            writer.println("\nOPERATION ABORTED!\nINVALID DATE FORMAT!\n");

            return;
        }
        if (predictedDateOfConclusion.getYear() <= 1950) {
            writer.println("\nOPERATION ABORTED!\n INVALID YEAR!\n");
            return;
        }

        writer.println("\nInsert task unit cost:\n");
        String uCost = scan.nextLine();
        double unitCost = Double.parseDouble(uCost);

        writer.println("\nInsert task estimated effort:\n");
        String estEffort = scan.nextLine();
        double estimatedEffort = Double.parseDouble(estEffort);

        writer.println(createTaskController.createTaskWithDateEffectiveDate(projectId, title, description,
                predictedDateOfStart, predictedDateOfConclusion, unitCost, estimatedEffort));

    }

    private void addTaskRelativeDates(String title) {
        createTaskController = new CreateTaskController(userService, projectService, taskService);
        writer.println("\nInsert task description:\n");
        String description = scan.nextLine();

        writer.println("Insert number of days to start task related to project start date: ");
        int days = Integer.parseInt(scan.nextLine());

        writer.println("Insert number of days the task will last: ");
        int duration = Integer.parseInt(scan.nextLine());

        writer.println("Insert task unit cost:");
        double unitCost3 = Double.parseDouble(scan.nextLine());

        double estimatedEffort3;
        do {
            writer.println("Insert task estimated effort:\n");
            String estimatedEffStr = scan.nextLine();
            estimatedEffort3 = validateDoublePositiveValues(estimatedEffStr);
        } while (estimatedEffort3 < 0);

        writer.println(createTaskController.createTaskWithDateRelatedToProjectStartDate(projectId, title, description,
                days, duration, unitCost3, estimatedEffort3));

    }

    public void showDependenciesOptions() {

        taskController = new TaskController(projectService, taskService);
        String separatorTasksDependencies = "____________________________________";
        String separatorTasksToBeDependency = "________________________________________";
        writer.println(separatorTasksDependencies);
        writer.println("\nPOSSIBLE TASKS TO ADD DEPENDENCY TO ");
        writer.println(separatorTasksDependencies + "\n");

        Project project = projectService.getProjectByID(projectId);

        for (Task task : project.listNotInitiatedTasks()) {

            writer.println(task.getId());
        }

        writer.println("\nChoose task to add dependency to: ");

        String taskid = scan.next();
        writer.println("\n" + separatorTasksToBeDependency);
        writer.println("\nPOSSIBLE TASKS TO BE A DEPENDENCY OF " + taskid);
        writer.println(separatorTasksToBeDependency + "\n");

        for (Task task : taskService.listTasksThatCanBeDependency(project)) {
            if (!(task.getId().equals(taskid))) {

                writer.println(task.getId());
            }
        }

        writer.println("Enter the task ID dependency for the task: ");

        String task2id = scan.next();

        writer.println(taskController.addTaskDependency(projectId, taskid, task2id));

    }

    public void showTaskCanStart() {

        taskController = new TaskController(projectService, taskService);

        Project project = projectService.getProjectByID(projectId);
        writer.println("List of uninitiated tasks: ");

        for (Task task : project.listNotInitiatedTasks()) {

            writer.println(task.getId());
        }

        writer.println("Please choose task to check if it can start: ");
        String taskid = scan.next();

        writer.println(taskController.taskCanStart(taskid));

    }
}