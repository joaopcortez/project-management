package project.ui.console;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import project.dto.ProjectDTO;
import project.dto.UserDTO;
import project.dto.UserRegistryDTO;
import project.jparepositories.RoleRepository;
import project.model.UserService;
import project.model.project.ProjectService;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;
import project.util.*;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

@Component
public class PopulateFileDataUI implements CommandLineRunner {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PopulateFirstUsersUI populateFirstUsers;

    static PrintStream writer = new PrintStream(System.out);
    String separator = "______________________________________";

    private UserBuilder userBuilder;
    private ProjectBuilder projectBuilder;
    private static final String PATH = "/src/main/resources/project/util/data/";

    private UserService userService;
    private ProjectService projectService;
    private TaskService taskService;
    private ProjectCollaboratorService projectCollaboratorService;

    private String userDirectory = System.getProperty("user.dir");


    public PopulateFileDataUI(UserService userService, ProjectService projectService, TaskService taskService,
                              ProjectCollaboratorService projectCollaboratorService) {
        this.userService = userService;
        this.projectCollaboratorService = projectCollaboratorService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void run(String... args) throws Exception {
        populateFirstUsers.firstUsersPopulateDatabase(userDirectory + "/src/main/resources/project/util/firstUsers.xml");
    }

    public void usersPopulateDatabaseFromFile(String choice) throws Exception {

        UserRegistryDTO userRegistryDto;

        Role roleCollaborator = roleRepository.getOneByName(RoleName.ROLE_COLLABORATOR);
        Role roleUser;
        if (!roleRepository.existsByName(RoleName.ROLE_COLLABORATOR)) {
            roleCollaborator = new Role(RoleName.ROLE_COLLABORATOR);
            roleRepository.save(roleCollaborator);
        }
        if (!roleRepository.existsByName(RoleName.ROLE_REGISTEREDUSER)) {
            roleUser = new Role(RoleName.ROLE_REGISTEREDUSER);
            roleRepository.save(roleUser);
        }

        UserConverter userConverter;

        String inputUserFile = userDirectory + PATH + choice;
        userBuilder = new UserBuilder(userService);
        projectBuilder = new ProjectBuilder(projectService, userService, projectCollaboratorService, taskService);

        // Convert info from Utilizador_v00.xml to User object and persist in database
        userConverter = UserConverterFactory.createUserConverter(inputUserFile);
        userRegistryDto = userConverter.parseToUser(inputUserFile);
        for (UserDTO userDTO : userRegistryDto.getListUsers()) {
            userBuilder.createAndPersistUser(userDTO);
        }

        // Get all users from database
        List<User> users = userService.listAllUsers();

        // Assign profile ROLE_COLLABORATOR to all, if any, remaining users in the users list
        for (int i = 0; i < users.size(); i++) {
            User user = users.get(i);
            if (i >= 2) {
                user.setRoles(Collections.singleton(roleCollaborator));
                user.setPassword(passwordEncoder.encode(user.getPassword()));
                user.getUserLoginData().setApprovedDateTime(LocalDateTime.now());
            }
            userService.updateUser(user);
        }
    }

    public boolean projectPopulateDatabaseFromFile(String choice) {
        ProjectConverter projectConverter;
        ProjectDTO projectDTO;
        
        String inputProjectFile = (choice.indexOf('/') != -1 ? choice: userDirectory + PATH + choice);
        userBuilder = new UserBuilder(userService);
        projectBuilder = new ProjectBuilder(projectService, userService, projectCollaboratorService, taskService);

        // Convert info from file to Project object and persist in database
        projectConverter = ProjectConverterFactory.createProjectConverter(inputProjectFile);
        projectDTO = projectConverter.parseToProject(inputProjectFile);
        return projectBuilder.createAndPersistProject(projectDTO);

    }


    public void chooseFileToPopulateDatabase() throws Exception {

        String fileChoice;
        Scanner scanner = new Scanner(System.in); //do not close!!!!!!!!!!!
        boolean choiceAccepted = false;
        File[] fileList = getListOfFiles();

        printHeadline();

        do {

            listAllFileOptions(fileList);
            fileChoice = scanner.nextLine();

            if (!isValidChoice(fileChoice, fileList.length)) {
                writer.println("Invalid choice. Please try again.");
                continue;
            }

            choiceAccepted = true;
            fileChoice = fileList[Integer.parseInt(fileChoice) - 1].getName();
            populateDatabaseFromUserFile(fileChoice);

            writer.println("\n\nSuccessfully loaded database users. Do you want to load the corresponding project data? [Y/N]");
            String loadProject = scanner.nextLine();
            if ("Y".equalsIgnoreCase(loadProject)) {
                String version = fileChoice.substring(fileChoice.indexOf('_'));
                projectPopulateDatabaseFromFile("Projeto" + version);
                writer.println("Successfully loaded project data.\n\n\n");
            }
        } while (!choiceAccepted);

    }

    private static File[] getListOfFiles() {

        String userDirectory = System.getProperty("user.dir");
        File folder = new File(userDirectory + PATH);
        return folder.listFiles();
    }

    private void printHeadline() {

        writer.println("\n\n" + separator);
        writer.println("\n      PLEASE CHOOSE FILE TO POPULATE DATABASE: ");
        writer.println(separator + "\n");
    }

    private static boolean isValidChoice(String choice, int listSize) {

        for (int i = 1; i <= (listSize); i++) {
            try {
                if (Integer.parseInt(choice) == i) {
                    return true;
                }
            } catch (NumberFormatException nfe) {
                return false;
            }
        }
        return false;
    }

    private static void listAllFileOptions(File[] fileList) {

        for (int i = 0; i < fileList.length; i++) {
            writer.println("[" + (i + 1) + "] - " + fileList[i].getName());
        }
    }

    public void populateDatabaseFromUserFile(String choice) throws Exception {
        usersPopulateDatabaseFromFile(choice);
    }


}
