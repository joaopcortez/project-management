package project.ui.console;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.mail.internet.AddressException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import project.jparepositories.RoleRepository;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;
import project.ui.console.populate.ProjectOne;
import project.ui.console.populate.ProjectTwo;

@Component
public class PopulateMockDataUI implements CommandLineRunner {

    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    UserService userService;
    ProjectService projectService;
    TaskService taskService;
    ProjectCollaboratorService projectCollaboratorService;
    ProjectOne projectOne;
    ProjectTwo projectTwo;
    
    String phoneNumber = "91 231 68 84";

    public PopulateMockDataUI( ProjectTwo projectTwo, ProjectOne projectOne, UserService userService, ProjectService projectService, TaskService taskService, ProjectCollaboratorService projectCollaboratorService) {
        this.userService = userService;
        this.projectCollaboratorService = projectCollaboratorService;
        this.projectService = projectService;
        this.taskService = taskService;
        this.projectOne = projectOne;
        this.projectTwo= projectTwo;
    }

    @Override
    public void run(String... args) throws Exception {
        populateDatabaseWithMockData();
        projectOne.projectOneData();
        projectTwo.projectTwoData();


    }

    public void populateDatabaseWithMockData() throws AddressException {

        Role roleAdmin = roleRepository.getOneByName(RoleName.ROLE_ADMINISTRATOR);
        Role roleDirector = roleRepository.getOneByName(RoleName.ROLE_DIRECTOR);
        Role roleCollaborator = roleRepository.getOneByName(RoleName.ROLE_COLLABORATOR);
        Role roleUser = roleRepository.getOneByName(RoleName.ROLE_REGISTEREDUSER);

        if (!roleRepository.existsByName(RoleName.ROLE_ADMINISTRATOR)) {
            roleAdmin = new Role(RoleName.ROLE_ADMINISTRATOR);
            roleRepository.save(roleAdmin);
        }
        if (!roleRepository.existsByName(RoleName.ROLE_REGISTEREDUSER)) {
            roleUser = new Role(RoleName.ROLE_REGISTEREDUSER);
            roleRepository.save(roleUser);
        }

        if (!roleRepository.existsByName(RoleName.ROLE_DIRECTOR)) {
            roleDirector = new Role(RoleName.ROLE_DIRECTOR);
            roleRepository.save(roleDirector);
        }
        if (!roleRepository.existsByName(RoleName.ROLE_COLLABORATOR)) {
            roleCollaborator = new Role(RoleName.ROLE_COLLABORATOR);
            roleRepository.save(roleCollaborator);
        }

        /* Users initialization and profile assignment **/
        String rua = "Rua do Amial";
        String cp = "4250-444";
        String cidade = "Porto";
        String pais = "Portugal";
        String tin = "123456789";
        LocalDate birth = LocalDate.of(1999, 11, 11);
        LocalDateTime lastLogin = LocalDateTime.of(2018, 1, 1, 10, 30);
        LocalDateTime approveDate = LocalDateTime.of(2018, 1, 1, 9, 30);
        String pass = passwordEncoder.encode("qwerty1!");



        // Create users and persist in database
        userService.addUser("Asdrubal", "92 555 66 22", "asdrubal@switch.com", tin, birth, "casa", rua, cp, cidade,
                pais);
        User asdrubal = userService.searchUserByEmail("asdrubal@switch.com");
        asdrubal.setPassword(pass);
        asdrubal.setRoles(Collections.singleton(roleCollaborator));
        asdrubal.getUserLoginData().setApprovedDateTime(approveDate);
        asdrubal.getUserLoginData().setLastLoginDate(lastLogin);


        userService.addUser("Joaquim", "96 452 56 56", "joaquim@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User joaquim = userService.searchUserByEmail("joaquim@switch.com");
        joaquim.setPassword(pass);
        joaquim.setRoles(Collections.singleton(roleCollaborator));
        joaquim.getUserLoginData().setApprovedDateTime(approveDate);
        joaquim.getUserLoginData().setLastLoginDate(lastLogin);



        userService.addUser("Joana", "93 333 38 33", "joana@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User joana = userService.searchUserByEmail("joana@switch.com");
        joana.setPassword(pass);
        joana.setRoles(Collections.singleton(roleDirector));
        joana.getUserLoginData().setApprovedDateTime(approveDate);
        joana.getUserLoginData().setLastLoginDate(lastLogin);



        userService.addUser("Lilina", "93 353 33 33", "liliana@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User liliana = userService.searchUserByEmail("liliana@switch.com");
        liliana.setPassword(pass);
        liliana.setRoles(Collections.singleton(roleAdmin));
        liliana.getUserLoginData().setApprovedDateTime(approveDate);
        liliana.getUserLoginData().setLastLoginDate(lastLogin);



        userService.addUser("Paulo", "91 997 54 72", "paulo@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User paulo = userService.searchUserByEmail("paulo@switch.com");
        paulo.setPassword(pass);
        paulo.setRoles(Collections.singleton(roleCollaborator));
        paulo.getUserLoginData().setApprovedDateTime(approveDate);
        paulo.getUserLoginData().setLastLoginDate(lastLogin);


        userService.addUser("Dali", "91 897 34 44", "dali@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User dali = userService.searchUserByEmail("dali@switch.com");
        dali.setPassword(pass);
        dali.setRoles(Collections.singleton(roleCollaborator));
        dali.setInactive();
        dali.getUserLoginData().setApprovedDateTime(approveDate);
        dali.getUserLoginData().setLastLoginDate(lastLogin);



        userService.addUser("Maria", "91 241 57 84", "maria@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User maria = userService.searchUserByEmail("maria@switch.com");
        maria.setPassword(pass);
        maria.setRoles(Collections.singleton(roleCollaborator));
        maria.getUserLoginData().setApprovedDateTime(approveDate);
        maria.getUserLoginData().setLastLoginDate(lastLogin);



        userService.addUser("Manuel", phoneNumber, "manuel@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User manuel = userService.searchUserByEmail("manuel@switch.com");
        manuel.setPassword(pass);
        manuel.setRoles(Collections.singleton(roleCollaborator));
        manuel.getUserLoginData().setApprovedDateTime(approveDate);
        manuel.getUserLoginData().setLastLoginDate(lastLogin);



        userService.addUser("Josefina", "91 231 67 84", "josefina@switch.com", tin, birth, "casa", rua, cp, cidade,
                pais);
        User josefina = userService.searchUserByEmail("josefina@switch.com");
        josefina.setPassword(pass);
        josefina.setRoles(Collections.singleton(roleCollaborator));



        userService.addUser("Rogerio", "91 231 87 82", "rogerio@switch.com", tin, birth, "casa", rua, cp, cidade,
                pais);
        User rogerio = userService.searchUserByEmail("rogerio@switch.com");
        rogerio.setPassword(pass);
        rogerio.setRoles(Collections.singleton(roleUser));


        userService.addUser("Diogo Martins", phoneNumber, "diogo@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User diogo = userService.searchUserByEmail("diogo@switch.com");
        diogo.setPassword(pass);
        diogo.setRoles(Collections.singleton(roleCollaborator));
        diogo.getUserLoginData().setApprovedDateTime(approveDate);
        diogo.getUserLoginData().setLastLoginDate(lastLogin);

        userService.addUser("Tiago Martins", phoneNumber, "tiago@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User tiago = userService.searchUserByEmail("tiago@switch.com");
        tiago.setPassword(pass);
        tiago.setRoles(Collections.singleton(roleCollaborator));
        tiago.getUserLoginData().setApprovedDateTime(approveDate);
        tiago.getUserLoginData().setLastLoginDate(lastLogin);

        userService.addUser("Lisa Silva", phoneNumber, "lisa@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User lisa = userService.searchUserByEmail("lisa@switch.com");
        lisa.setPassword(pass);
        lisa.setRoles(Collections.singleton(roleCollaborator));
        lisa.getUserLoginData().setApprovedDateTime(approveDate);
        lisa.getUserLoginData().setLastLoginDate(lastLogin);

        userService.addUser("Ana Catarina", phoneNumber, "ana@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User ana = userService.searchUserByEmail("ana@switch.com");
        ana.setPassword(pass);
        ana.setRoles(Collections.singleton(roleCollaborator));
        ana.getUserLoginData().setApprovedDateTime(approveDate);
        ana.getUserLoginData().setLastLoginDate(lastLogin);

        userService.addUser("Filipa Oliveira", phoneNumber, "filipa@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User filipa = userService.searchUserByEmail("filipa@switch.com");
        filipa.setPassword(pass);
        filipa.setRoles(Collections.singleton(roleCollaborator));
        filipa.getUserLoginData().setApprovedDateTime(approveDate);
        filipa.getUserLoginData().setLastLoginDate(lastLogin);

        userService.addUser("Bebiana Mendonça", phoneNumber, "bebiana@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User bebiana = userService.searchUserByEmail("bebiana@switch.com");
        bebiana.setPassword(pass);
        bebiana.setRoles(Collections.singleton(roleCollaborator));
        bebiana.getUserLoginData().setApprovedDateTime(approveDate);
        bebiana.getUserLoginData().setLastLoginDate(lastLogin);


        userService.addUser("Pedro Cortez", phoneNumber, "pedro@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User pedro = userService.searchUserByEmail("pedro@switch.com");
        pedro.setPassword(pass);
        pedro.setRoles(Collections.singleton(roleCollaborator));
        pedro.getUserLoginData().setApprovedDateTime(approveDate);
        pedro.getUserLoginData().setLastLoginDate(lastLogin);

        userService.addUser("Joao Gomes", phoneNumber, "joao@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User joao = userService.searchUserByEmail("joao@switch.com");
        joao.setPassword(pass);
        joao.setRoles(Collections.singleton(roleCollaborator));
        joao.getUserLoginData().setApprovedDateTime(approveDate);
        joao.getUserLoginData().setLastLoginDate(lastLogin);


        userService.addUser("Daniel Barbosa", phoneNumber, "daniel@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User daniel = userService.searchUserByEmail("daniel@switch.com");
        daniel.setPassword(pass);
        daniel.setRoles(Collections.singleton(roleCollaborator));
        daniel.getUserLoginData().setApprovedDateTime(approveDate);
        daniel.getUserLoginData().setLastLoginDate(lastLogin);

        userService.addUser("Antonio Canelas", phoneNumber, "antonio@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User antonio = userService.searchUserByEmail("antonio@switch.com");
        antonio.setPassword(pass);
        antonio.setRoles(Collections.singleton(roleCollaborator));
        antonio.getUserLoginData().setApprovedDateTime(approveDate);
        antonio.getUserLoginData().setLastLoginDate(lastLogin);


        userService.addUser("Sergio Valente", phoneNumber, "sergio@switch.com", tin, birth, "casa", rua, cp, cidade, pais);
        User sergio = userService.searchUserByEmail("sergio@switch.com");
        sergio.setPassword(pass);
        sergio.setRoles(Collections.singleton(roleCollaborator));
        sergio.getUserLoginData().setApprovedDateTime(approveDate);
        sergio.getUserLoginData().setLastLoginDate(lastLogin);







        // Update userRegistry and persist in database
        userService.updateUser(joaquim);
        userService.updateUser(joana);
        userService.updateUser(liliana);
        userService.updateUser(dali);
        userService.updateUser(rogerio);
        userService.updateUser(asdrubal);
        userService.updateUser(paulo);
        userService.updateUser(maria);
        userService.updateUser(manuel);
        userService.updateUser(josefina);


        userService.updateUser(diogo);
        userService.updateUser(tiago);
        userService.updateUser(lisa);
        userService.updateUser(ana);
        userService.updateUser(filipa);
        userService.updateUser(bebiana);
        userService.updateUser(joao);
        userService.updateUser(daniel);

        userService.updateUser(pedro);
        userService.updateUser(antonio);
        userService.updateUser(sergio);




        /**
         * Projects initialization, project collaborators assignment and task related
         * work
         **/
        LocalDateTime d1 = LocalDateTime.of(2018, 1, 5, 0, 0);
        LocalDateTime d2 = LocalDateTime.of(2018, 10, 11, 0, 0);
        LocalDateTime d3 = LocalDateTime.of(2017, 12, 18, 0, 0);

        // Create Project 1 and persist in database
        projectService.addProject("1", "API documentation - Architectural view model");
        Project project1 = projectService.getProjectByID("1");
        project1.setDescription(
                "At vero eos et accusamus et iusto odio dignissimos Temporibus autem quibusdam" +
                " et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates r" +
                        "epudiandae sint et molestiae non recusandae.");
        project1.setGlobalBudget(550);
        List<String> option = new ArrayList<>();
        option.add("AverageCost");
        option.add("MinimumTimePeriodCost");
        option.add("MaximumTimePeriodCost");
        project1.setCostCalculationOptions(option);
        project1.setActive();
        project1.setStartDate(d3);
        projectCollaboratorService.setProjectManager(project1, joaquim);
        projectCollaboratorService.addProjectCollaborator(project1, manuel, 3);
        projectCollaboratorService.addProjectCollaborator(project1, paulo, 4);
        projectCollaboratorService.addProjectCollaborator(project1, maria, 5);

        ProjectCollaborator projectCollaborator1 = project1.findProjectCollaborator(manuel);
        ProjectCollaborator projectCollaborator2 = project1.findProjectCollaborator(paulo);
        ProjectCollaborator projectCollaborator3 = project1.findProjectCollaborator(maria);

        projectCollaborator1.getCostAndTimePeriodList().get(0).setStartDate(LocalDateTime.now().minusDays(20).toLocalDate());
        projectCollaborator1.getCostAndTimePeriodList().get(0).setEndDate(LocalDateTime.now().minusDays(13).toLocalDate());
        projectCollaborator1.getCostAndTimePeriodList().add(new ProjectCollaboratorCostAndTimePeriod(10));
        projectCollaborator1.getCostAndTimePeriodList().get(1).setStartDate(LocalDateTime.now().minusDays(12).toLocalDate());

        projectCollaborator2.getCostAndTimePeriodList().get(0).setStartDate(LocalDateTime.now().minusDays(15).toLocalDate());
        projectCollaborator2.getCostAndTimePeriodList().get(0).setStartDate(LocalDateTime.now().minusDays(10).toLocalDate());
        projectCollaborator2.getCostAndTimePeriodList().add(new ProjectCollaboratorCostAndTimePeriod(20));
        projectCollaborator2.getCostAndTimePeriodList().get(1).setStartDate(LocalDateTime.now().minusDays(10).toLocalDate());

        projectCollaborator3.getCostAndTimePeriodList().get(0).setStartDate(LocalDateTime.now().minusDays(5).toLocalDate());
        projectCollaborator3.getCostAndTimePeriodList().get(0).setStartDate(LocalDateTime.now().minusDays(10).toLocalDate());
        projectCollaborator3.getCostAndTimePeriodList().add(new ProjectCollaboratorCostAndTimePeriod(20));
        projectCollaborator3.getCostAndTimePeriodList().get(1).setStartDate(LocalDateTime.now().minusDays(15).toLocalDate());


        // Create tasks
        taskService.addTask(project1, "Document Development view",
                "The development view illustrates a system from a programmer's perspective and is concerned with software management. " +
                        "This view is also known as the implementation view. It uses the UML Component diagram to describe system components. UML " +
                        "Diagrams used to represent the development view include the Package diagram.", d2.minusMonths(4), d2.minusMonths(2), 1.2, 1.3);

        taskService.addTask(project1, "Document Logical view", " The logical view is concerned with the functionality that the system provides to end-users. " +
                "UML diagrams used to represent the logical view include, class diagrams, and state diagrams.", d1.plusMonths(1), d2, 1.2, 1.3);
        taskService.addTask(project1, "Document Physical view", "The physical view depicts the system from a system engineer's point of view. It is concerned with the topology of software components on the physical layer as well as the physical connections between these components. This view is also known as the deployment view. UML diagrams used to represent the physical view include the deployment diagram", d2.minusMonths(2), d2.plusMonths(1), 1.4, 1.2);
        taskService.addTask(project1, "Document Process view",
                "" +
                        "" +
                        "", d1.plusDays(10), d1.plusMonths(4), 1.3, 1.3);
        taskService.addTask(project1, "Document Scenarios", "The description of an architecture is illustrated using a small set of use cases, or scenarios, which become a fifth view. The scenarios describe sequences of interactions between objects and between processes. They are used to identify architectural elements and to illustrate and validate the architecture design. They also serve as a starting point for tests of an architecture prototype. This view is also known as the use case view.", d1.plusDays(12), d2.minusMonths(5), 1.3, 1.7);
        taskService.addTask(project1, "Deployment diagram", "A deployment diagram in the Unified Modeling Language models the physical deployment of artifacts on nodes.[1] To describe a web site, for example, a deployment diagram would show what hardware components (\"nodes\") exist (e.g., a web server, an application server, and a database server), what software components (\"artifacts\") run on each node (e.g., web application, database), and how the different pieces are connected ", d1.plusDays(17), d2, 1.6, 1.1);
        taskService.addTask(project1, "Package diagram", "A package diagram in the Unified Modeling Language depicts the dependencies between the packages that make up a model", d1.plusMonths(2), d2.minusDays(5), 1.2, 1.4);

        Task task1 = project1.findTaskByTitle("Document Development view");
        Task task2 = project1.findTaskByTitle("Document Logical view");
        Task task3 = project1.findTaskByTitle("Document Physical view");
        Task task4 = project1.findTaskByTitle("Document Process view");
        Task task5 = project1.findTaskByTitle("Document Scenarios");
        Task task6 = project1.findTaskByTitle("Deployment diagram");
        Task task7 = project1.findTaskByTitle("Package diagram");

        // task1
        taskService.addProjectCollaboratorToTask(projectCollaborator2, task1);
        taskService.addProjectCollaboratorToTask(projectCollaborator3, task1);

        TaskCollaboratorRegistry col2Task1Registry = task1.getTaskCollaboratorRegistryByID(task1.getId() + "-" + projectCollaborator2.getUser().getEmail());
        col2Task1Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusDays(17));
        TaskCollaboratorRegistry col3Task1Registry = task1.getTaskCollaboratorRegistryByID(task1.getId() + "-" + projectCollaborator3.getUser().getEmail());
        col3Task1Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusDays(10));

        task1.addReport(projectCollaborator2, 4, LocalDateTime.now(), LocalDateTime.now().plusDays(1));
        task1.addReport(projectCollaborator2, 7, LocalDateTime.now(), LocalDateTime.now().plusDays(5));
        task1.addReport(projectCollaborator3, 15, LocalDateTime.now(), LocalDateTime.now().plusDays(11));
        task1.addReport(projectCollaborator3, 8, LocalDateTime.now(), LocalDateTime.now().plusDays(7));

        // task2
        taskService.addProjectCollaboratorToTask(projectCollaborator1, task2);
        taskService.addProjectCollaboratorToTask(projectCollaborator3, task2);

        TaskCollaboratorRegistry col1Task2Registry = task2.getTaskCollaboratorRegistryByID(task2.getId() + "-" + projectCollaborator1.getUser().getEmail());
        col1Task2Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusDays(20));
        TaskCollaboratorRegistry col3Task2Registry = task2.getTaskCollaboratorRegistryByID(task2.getId() + "-" + projectCollaborator3.getUser().getEmail());
        col3Task2Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusDays(18));

        task2.addReport(projectCollaborator1, 8, LocalDateTime.now().minusDays(7), LocalDateTime.now().minusDays(1));
        task2.addReport(projectCollaborator1, 4, LocalDateTime.now().minusDays(10), LocalDateTime.now().minusDays(5));
        task2.addReport(projectCollaborator1, 4, LocalDateTime.now().minusDays(20), LocalDateTime.now().minusDays(10));
        task2.addReport(projectCollaborator3, 8, LocalDateTime.now().minusMonths(1), LocalDateTime.now().minusDays(20));
        task2.setTaskCompleted();
        task2.setEffectiveDateOfConclusion(LocalDateTime.now().minusDays(1));

        // task3
        taskService.addProjectCollaboratorToTask(projectCollaborator1, task3);

        TaskCollaboratorRegistry col1Task3Registry = task3.getTaskCollaboratorRegistryByID(task3.getId() + "-" + projectCollaborator1.getUser().getEmail());
        col1Task3Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusMonths(2));

        task3.requestAddProjectCollaborator(projectCollaborator2);
        task3.addTaskDependency(task1);

        // task4

        // task5
        taskService.addProjectCollaboratorToTask(projectCollaborator3, task5);

        TaskCollaboratorRegistry col3Task5Registry = task5.getTaskCollaboratorRegistryByID(task5.getId() + "-" + projectCollaborator3.getUser().getEmail());
        col3Task5Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusMonths(2));

        task5.addReport(projectCollaborator3, 4, LocalDateTime.now().minusDays(20), LocalDateTime.now().minusDays(7));
        task5.addReport(projectCollaborator3, 6, LocalDateTime.now().minusDays(10), LocalDateTime.now().minusDays(7));

        // task6
        taskService.addProjectCollaboratorToTask(projectCollaborator3, task6);

        TaskCollaboratorRegistry col3Task6Registry = task6.getTaskCollaboratorRegistryByID(task6.getId() + "-" + projectCollaborator3.getUser().getEmail());
        col3Task6Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusMonths(1));

        task6.addReport(projectCollaborator3, 4, LocalDateTime.now().minusDays(16), LocalDateTime.now().minusDays(7));
        task6.setTaskCompleted();
        task6.setEffectiveDateOfConclusion(LocalDateTime.now().minusDays(5));

        // task7
        taskService.addProjectCollaboratorToTask(projectCollaborator1, task7);

        TaskCollaboratorRegistry col1Task7Registry = task7.getTaskCollaboratorRegistryByID(task7.getId() + "-" + projectCollaborator1.getUser().getEmail());
        col1Task7Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusMonths(1));

        task7.addReport(projectCollaborator1, 4, LocalDateTime.now().minusDays(10), LocalDateTime.now().minusDays(7));
        task7.requestRemoveProjectCollaborator(projectCollaborator1);

        // Create Project 2 and persist in database
        projectService.addProject("2", "Frontend with React-Redux");
        Project project2 = projectService.getProjectByID("2");
        project2.setDescription("In computing, React (also known as React.js or ReactJS) is a JavaScript library[2] for building user interfaces. It is maintained by Facebook and a community of individual developers and companies.\n" +
                "\n" +
                "React can be used as a base in the development of single-page or mobile applications. Complex React applications usually require the use of additional libraries for state management, routing, and interaction with an API.");
        project2.setGlobalBudget(700);
        List<String> optionProject2 = new ArrayList<>();
        optionProject2.add("MinimumTimePeriodCost");
        optionProject2.add("MaximumTimePeriodCost");
        optionProject2.add("AverageCost");
        project2.setCostCalculationOptions(optionProject2);

        project2.setActive();
        project2.setStartDate(d3.plusDays(40));
        projectCollaboratorService.setProjectManager(project2, joaquim);
        projectCollaboratorService.addProjectCollaborator(project2, asdrubal, 2);
        projectCollaboratorService.addProjectCollaborator(project2, paulo, 5);
        projectCollaboratorService.addProjectCollaborator(project2, josefina, 5);

        ProjectCollaborator projectCollaborator4 = project2.findProjectCollaborator(asdrubal);
        ProjectCollaborator projectCollaborator5 = project2.findProjectCollaborator(paulo);
        ProjectCollaborator projectCollaborator6 = project2.findProjectCollaborator(josefina);

        projectCollaborator4.getCostAndTimePeriodList().get(0).setStartDate(LocalDateTime.now().minusDays(20).toLocalDate());
        projectCollaborator4.getCostAndTimePeriodList().get(0).setEndDate(LocalDateTime.now().minusDays(13).toLocalDate());
        projectCollaborator4.getCostAndTimePeriodList().add(new ProjectCollaboratorCostAndTimePeriod(10));
        projectCollaborator4.getCostAndTimePeriodList().get(1).setStartDate(LocalDateTime.now().minusDays(12).toLocalDate());

        projectCollaborator5.getCostAndTimePeriodList().get(0).setStartDate(LocalDateTime.now().minusDays(35).toLocalDate());
        projectCollaborator5.getCostAndTimePeriodList().get(0).setStartDate(LocalDateTime.now().minusDays(30).toLocalDate());
        projectCollaborator5.getCostAndTimePeriodList().add(new ProjectCollaboratorCostAndTimePeriod(20));
        projectCollaborator5.getCostAndTimePeriodList().get(1).setStartDate(LocalDateTime.now().minusDays(30).toLocalDate());

        projectCollaborator6.getCostAndTimePeriodList().get(0).setStartDate(LocalDateTime.now().minusDays(5).toLocalDate());
        projectCollaborator6.getCostAndTimePeriodList().get(0).setStartDate(LocalDateTime.now().minusDays(10).toLocalDate());
        projectCollaborator6.getCostAndTimePeriodList().add(new ProjectCollaboratorCostAndTimePeriod(20));
        projectCollaborator6.getCostAndTimePeriodList().get(1).setStartDate(LocalDateTime.now().minusDays(15).toLocalDate());


        String taskDescription="Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
                "Fusce mollis ligula diam, id mollis arcu rhoncus et. Vivamus molestie eget mauris sed vehicula. " +
                "Sed nec consectetur est. Phasellus sem leo, bibendum et lobortis placerat, molestie id nibh. In hac habitasse platea dictumst. " +
                "Nunc dignissim mattis sagittis. Vivamus quis erat pulvinar massa rutrum posuere. Sed sed maximus quam. Praesent sapien ligul";




        // Create tasks
        taskService.addTask(project2, "create-react-app", taskDescription, d1, d2, 1.5, 1.3);
        taskService.addTask(project2, "Install Dependencies", taskDescription, d1.plusDays(5), d2.plusDays(20), 1.5, 1.3);
        taskService.addTask(project2, "Implement react redux", taskDescription, d1.plusDays(10), d2.plusDays(15), 1.6, 1.4);
        taskService.addTask(project2, "Create Store", taskDescription, d1.plusDays(7), d2.plusDays(30), 1.3, 1.4);
        taskService.addTask(project2, "Create reducers", taskDescription, d1.plusDays(10), d2.plusDays(20), 1.8, 1.7);
        taskService.addTask(project2, "Create actions", taskDescription, d1.plusDays(20), d2.plusDays(45), 2.0, 1.5);
        taskService.addTask(project2, "Components and containers", taskDescription, d1.plusDays(25), d2.plusDays(30), 1.5, 1.6);

        Task task8 = taskService.findTaskByTitle("create-react-app");
        Task task9 = taskService.findTaskByTitle("Install Dependencies");
        Task task10 = taskService.findTaskByTitle("Implement react redux");
        Task task11 = taskService.findTaskByTitle("Create Store");
        Task task12 = taskService.findTaskByTitle("Create reducers");
        Task task13 = taskService.findTaskByTitle("Create actions");
        Task task14 = taskService.findTaskByTitle("Components and containers");

        // task8
        taskService.addProjectCollaboratorToTask(projectCollaborator4, task8);
        taskService.addProjectCollaboratorToTask(projectCollaborator5, task8);

        TaskCollaboratorRegistry col4Task8Registry = task8.getTaskCollaboratorRegistryByID(task8.getId() + "-" + projectCollaborator4.getUser().getEmail());
        col4Task8Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusDays(30));
        TaskCollaboratorRegistry col5Task8Registry = task8.getTaskCollaboratorRegistryByID(task8.getId() + "-" + projectCollaborator5.getUser().getEmail());
        col5Task8Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusDays(15));

        task8.addReport(projectCollaborator4, 6, LocalDateTime.now().minusDays(3), LocalDateTime.now().minusDays(2));
        task8.addReport(projectCollaborator4, 4, LocalDateTime.now().minusDays(6), LocalDateTime.now().minusDays(4));
        task8.addReport(projectCollaborator4, 8, LocalDateTime.now().minusDays(8), LocalDateTime.now().minusDays(8));
        task8.setEffectiveStartDate(LocalDateTime.now().minusMonths(3));
        task8.setPredictedDateOfConclusion(LocalDateTime.of(2018, 8, 15, 0, 0));

        // task9
        taskService.addProjectCollaboratorToTask(projectCollaborator4, task9);
        taskService.addProjectCollaboratorToTask(projectCollaborator5, task9);

        TaskCollaboratorRegistry col4Task9Registry = task9.getTaskCollaboratorRegistryByID(task9.getId() + "-" + projectCollaborator4.getUser().getEmail());
        col4Task9Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusDays(40));
        TaskCollaboratorRegistry col5Task9Registry = task9.getTaskCollaboratorRegistryByID(task9.getId() + "-" + projectCollaborator5.getUser().getEmail());
        col5Task9Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusDays(35));

        task9.addReport(projectCollaborator4, 8, LocalDateTime.now().minusDays(7), LocalDateTime.now().minusDays(3));
        task9.addReport(projectCollaborator4, 4, LocalDateTime.now().minusDays(30), LocalDateTime.now().minusDays(25));
        task9.addReport(projectCollaborator4, 4, LocalDateTime.now().minusDays(20), LocalDateTime.now().minusDays(10));
        task9.addReport(projectCollaborator4, 8, LocalDateTime.now().minusMonths(1), LocalDateTime.now().minusDays(25));
        task9.setTaskCompleted();
        task9.setEffectiveDateOfConclusion(LocalDateTime.now().minusMonths(1));

        // task10
        taskService.addProjectCollaboratorToTask(projectCollaborator4, task10);

        TaskCollaboratorRegistry col4Task10Registry = task10.getTaskCollaboratorRegistryByID(task10.getId() + "-" + projectCollaborator4.getUser().getEmail());
        col4Task10Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusDays(60));

        task10.requestAddProjectCollaborator(projectCollaborator5);
        task10.setPredictedDateOfConclusion(LocalDateTime.now().plusDays(125));

        // task11

        // task12
        taskService.addProjectCollaboratorToTask(projectCollaborator5, task12);

        TaskCollaboratorRegistry col5Task12Registry = task12.getTaskCollaboratorRegistryByID(task12.getId() + "-" + projectCollaborator5.getUser().getEmail());
        col5Task12Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusDays(60));

        task12.addReport(projectCollaborator5, 5, LocalDateTime.now().minusDays(30), LocalDateTime.now().minusDays(20));
        task12.setTaskCancelled();

        task12.addReport(projectCollaborator5, 8, LocalDateTime.now().minusDays(21), LocalDateTime.now().minusDays(15));
        task12.setTaskCompleted();
        task12.setEffectiveDateOfConclusion(LocalDateTime.now().minusDays(10));

        // task13
        taskService.addProjectCollaboratorToTask(projectCollaborator6, task13);

        TaskCollaboratorRegistry col6Task13Registry = task13.getTaskCollaboratorRegistryByID(task13.getId() + "-" + projectCollaborator6.getUser().getEmail());
        col6Task13Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusDays(30));

        task13.addReport(projectCollaborator6, 10, LocalDateTime.now().minusDays(16), LocalDateTime.now().minusDays(7));
        task13.setTaskCompleted();
        task13.setEffectiveDateOfConclusion(LocalDateTime.now().minusDays(4));

        // task14
        taskService.addProjectCollaboratorToTask(projectCollaborator6, task14);

        TaskCollaboratorRegistry col6Task14Registry = task14.getTaskCollaboratorRegistryByID(task14.getId() + "-" + projectCollaborator6.getUser().getEmail());
        col6Task14Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusDays(15));

        task14.addReport(projectCollaborator6, 4, LocalDateTime.now().minusDays(10), LocalDateTime.now().minusDays(5));

        // Update projectRegistry and persist in database
        projectService.updateProject(project1);
        projectService.updateProject(project2);

        projectCollaboratorService.updateProjectCollaborator(projectCollaborator1);
        projectCollaboratorService.updateProjectCollaborator(projectCollaborator2);
        projectCollaboratorService.updateProjectCollaborator(projectCollaborator3);
        projectCollaboratorService.updateProjectCollaborator(projectCollaborator4);
        projectCollaboratorService.updateProjectCollaborator(projectCollaborator5);
        projectCollaboratorService.updateProjectCollaborator(projectCollaborator6);

        taskService.updateTask(task1);
        taskService.updateTask(task2);
        taskService.updateTask(task3);
        taskService.updateTask(task4);
        taskService.updateTask(task5);
        taskService.updateTask(task6);
        taskService.updateTask(task7);
        taskService.updateTask(task8);
        taskService.updateTask(task9);
        taskService.updateTask(task10);
        taskService.updateTask(task11);
        taskService.updateTask(task12);
        taskService.updateTask(task13);
        taskService.updateTask(task14);

    }

}
