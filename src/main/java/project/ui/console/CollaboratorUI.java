package project.ui.console;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import project.controllers.consolecontrollers.ProjectTaskListController;
import project.model.UserService;
import project.model.project.ProjectService;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;
import project.ui.console.projectmanagerui.ProjectManagerUI;
import system.dto.LoginTokenDTO;

import java.io.PrintStream;
import java.util.Scanner;

@Component
public class CollaboratorUI {

    static Scanner scan = new Scanner(System.in);
    static boolean goBackToLevel2 = false;
    PrintStream writer = new PrintStream(System.out);
    boolean leaveUI = false;
    boolean goBackToLevel1 = false;
    LoginTokenDTO logintDto;
    @Autowired
    UserDataUI userDataUi;
    @Autowired
    TaskListUI taskListUi;
    @Autowired
    ProjectManagerUI projectManagerUi;
    ProjectTaskListController projectTaskListController;
    UserService userService;
    ProjectService projectService;
    TaskService taskService;
    ProjectCollaboratorService projectCollabService;
    String goBackMessage = "[ ] - OTHER KEY TO GO BACK TO PREVIOUS MENU";
    String fourLinesFeed = "\n\n\n\n";

    public CollaboratorUI(LoginTokenDTO ltdto, UserService userService, ProjectService projectService, TaskService taskService, ProjectCollaboratorService projectCollabService) {
        logintDto = ltdto;
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
        this.projectCollabService = projectCollabService;
    }

    public boolean isGoBackToLevel1() {
        return goBackToLevel1;
    }

    public void setGoBackToLevel1(boolean goBackToLevel1) {
        this.goBackToLevel1 = goBackToLevel1;
    }

    public LoginTokenDTO getLogintdto() {
        return logintDto;
    }

    public void home() {

        leaveUI = false;
        do {
            String separatorCollaboratorMenu = "____________________________________";
            writer.println("\n\n\n\n" + separatorCollaboratorMenu);
            writer.println("\n         COLLABORATOR MENU");
            writer.println(separatorCollaboratorMenu + "\n");
            showMainCollaboratorMenu();
            writer.println(separatorCollaboratorMenu);
            writer.println(fourLinesFeed);
            chooseFirstOption();
        } while (!leaveUI);
    }

    public void showMainCollaboratorMenu() {
        writer.println("[1] - MY PERSONAL DATA");
        writer.println("[2] - MY TASKS");
        writer.println("[3] - MY PROJECTS AS COLLABORATOR");
        writer.println("[4] - MY PROJECTS AS PROJECT MANAGER");
        writer.println("[ ] - OTHER KEY TO LOGOUT");
    }

    public synchronized void chooseFirstOption() {

        String choice = scan.nextLine();

        if ("1".equals(choice)) {
            managePersonalData();
        } else if ("2".equals(choice)) {

            manageMyTasks();
        } else if ("3".equals(choice)) {
            manageMyProjectsAsACollaborator();
        } else if ("4".equals(choice)) {

            projectManagerUi.showProjects();
        } else {
            logout();
        }
    }

    public synchronized void managePersonalData() {

        do {
            String separatorMyPersonalDataMenu = "___________________________________________";
            writer.println(separatorMyPersonalDataMenu);
            writer.println("\n             MY PERSONAL DATA");
            writer.println(separatorMyPersonalDataMenu + "\n");
            userDataUi.showMenu();
            writer.println(separatorMyPersonalDataMenu);
            userDataUi.chooseOption();
            writer.println("\n\n");
        } while (!goBackToLevel1);
    }

    public synchronized void manageMyTasks() {
        goBackToLevel1 = false;
        do {
            showTasksMenu();
            chooseTaskOption();
        } while (!goBackToLevel1);
    }

    public synchronized void manageMyProjectsAsACollaborator() {
        do {
            listProjectsAsCollaborator(getLogintdto());
        } while (!goBackToLevel1);
    }

    public void logout() {
        writer.println("\n" + getLogintdto().getName() + " has logged off.\n\n");
        leaveUI = true;
    }

    public void showTasksMenu() {

        String separatorMyTasksMenu = "____________________________________________";
        writer.println(separatorMyTasksMenu);
        writer.println(    "\n                MY TASKS");
        writer.println(separatorMyTasksMenu + "\n");
        writer.println("[1] - CONSULT TASKS");
        writer.println("[2] - TASK MANAGEMENT");
        writer.println(goBackMessage);
        writer.println(separatorMyTasksMenu);
        writer.println("\n\n");
    }

    public synchronized void consultTasks() {

        goBackToLevel2 = false;
        do {
            showConsultTasksMenu();
            chooseConsultTasksOption();
        } while (!goBackToLevel2);
    }

    public synchronized void editTasks() {
        goBackToLevel2 = false;
        do {
            showEditTaskMenu();
            chooseEditTaskOption();
        } while (!goBackToLevel2);
    }

    public void showConsultTasksMenu() {
        String separatorConsultTasksMenu = "_____________________________________________________";
        writer.println("\n\n" + separatorConsultTasksMenu);
        writer.println("\n                  CONSULT TASKS");
        writer.println(separatorConsultTasksMenu + "\n");
        writer.println("[1] - LIST ALL TASKS");
        writer.println("[2] - LIST PENDING TASKS");
        writer.println("[3] - LIST COMPLETED TASKS");
        writer.println("[4] - LIST COMPLETED TASKS LAST MONTH");
        writer.println("[5] - CONSULT TOTAL TIME SPENT IN TASKS LAST MONTH");
        writer.println("[6] - CONSULT AVERAGE TIME SPENT IN TASKS LAST MONTH");
        writer.println(goBackMessage);
        writer.println(separatorConsultTasksMenu);
        writer.println("\n\n");
    }

    public synchronized void chooseConsultTasksOption() {

        String choiceMyTaskList = scan.nextLine();
        switch (choiceMyTaskList) {
            case "1":
                listAllTasks();
                break;
            case "2":
                taskListUi.us203ListCollaboratorPendentTasks();
                break;
            case "3":
                taskListUi.us210ListCollaboratorFinishedTaks();
                break;
            case "4":
                taskListUi.us211ListCollaboratorFinishedTaksLastMonth();
                break;
            case "5":
                taskListUi.us215calculateCollaboratorsHoursCompletedTasksLastMonthInAllProjects();
                break;
            case "6":
                taskListUi.us216calculateCollaboratorsAverageHoursCompletedTasksLastMonthInAllProjects();
                break;
            default:
                goBackToLevel2 = true;
                break;
        }
    }

    public void showEditTaskMenu() {
        String separatorTaskManagementMenu = "_____________________________________________________";
        writer.println(separatorTaskManagementMenu);
        writer.println("\n                TASK MANAGEMENT");
        writer.println(separatorTaskManagementMenu + "\n");
        writer.println("[1] - MAKE TASK ASSIGNMENT REQUEST");
        writer.println("[2] - MARK TASK AS COMPLETED");
        writer.println("[3] - MAKE TASK REMOVAL REQUEST");
        writer.println("[4] - ADD REPORT TO TASK");
        writer.println("[5] - EDIT REPORT");
        writer.println(goBackMessage);
        writer.println(separatorTaskManagementMenu);
        writer.println("\n\n");
    }

    public synchronized void chooseEditTaskOption() {

        String choiceEditTask = scan.nextLine();
        String separator = "________________________________________________";
        String separator2 = "_____________________________________________________________";


        if ("1".equals(choiceEditTask)) {
            writer.println(separator);
            writer.println("\n         MARK TASK ASSIGNMENT REQUEST");
            writer.println(separator + "\n");
            taskListUi.addTask();
            writer.println(fourLinesFeed);
        } else if ("2".equals(choiceEditTask)) {
            writer.println(separator2);
            writer.println("\n                MARK TASK AS COMPLETED ");
            writer.println(separator2 + "\n");
            taskListUi.markTaskCompleted();
            writer.println(fourLinesFeed);
        } else if ("3".equals(choiceEditTask)) {
            writer.println(separator2);
            writer.println("\n                MARK TASK REMOVAL REQUEST");
            writer.println(separator2 + "\n");
            taskListUi.removeTask();
            writer.println(fourLinesFeed);
        } else if ("4".equals(choiceEditTask)) {
            writer.println(separator2);
            writer.println("\n                 ADD REPORT TO TASK");
            writer.println(separator2 + "\n");
            taskListUi.addReport();
            writer.println(fourLinesFeed);
        } else if ("5".equals(choiceEditTask)) {
            writer.println(separator2);
            writer.println("\n                      EDIT REPORT");
            writer.println(separator2 + "\n");
            taskListUi.us208EditReport();
            writer.println(fourLinesFeed);
        } else
            goBackToLevel2 = true;
    }

    public synchronized void chooseTaskOption() {

        String choiceListTask = scan.nextLine();

        if ("1".equals(choiceListTask)) {
            consultTasks();
        } else if ("2".equals(choiceListTask)) {
            editTasks();
        } else
            goBackToLevel1 = true;
    }

    private void listAllTasks() {

        taskListUi.listAllTasks();
    }

    public void listProjectsAsCollaborator(LoginTokenDTO logindto) {
        String separatorMyProjectsCollaboratorMenu = "________________________________________________";
        writer.println(separatorMyProjectsCollaboratorMenu);
        writer.println("\n         MY PROJECTS AS COLLABORATOR");
        writer.println(separatorMyProjectsCollaboratorMenu + "\n");
        taskListUi.listAllCollaboratorProjects(logindto.getEmail());
        writer.println("[ ] ANY KEY TO GO BACK TO PREVIOUS MENU.");
        scan.nextLine();
        goBackToLevel1 = true;
    }

}
