package project.ui.console;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import project.dto.UserDTO;
import project.dto.UserRegistryDTO;
import project.jparepositories.RoleRepository;
import project.model.UserService;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.util.UserBuilder;
import project.util.UserConverter;
import project.util.UserConverterFactory;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Component
public class PopulateFirstUsersUI {

    @Autowired
    PasswordEncoder passwordEncoder;
    private UserService userService;

    @Autowired
    private RoleRepository roleRepository;

    public PopulateFirstUsersUI(UserService userService) {
        this.userService = userService;
    }

    public void firstUsersPopulateDatabase(String choice) throws Exception {
        UserBuilder userBuilder;

        Role roleAdmin = roleRepository.getOneByName(RoleName.ROLE_ADMINISTRATOR);
        Role roleDirector = roleRepository.getOneByName(RoleName.ROLE_DIRECTOR);

        if (!roleRepository.existsByName(RoleName.ROLE_ADMINISTRATOR)) {
            roleAdmin = new Role(RoleName.ROLE_ADMINISTRATOR);
            roleRepository.save(roleAdmin);
        }

        if (!roleRepository.existsByName(RoleName.ROLE_DIRECTOR)) {
            roleDirector = new Role(RoleName.ROLE_DIRECTOR);
            roleRepository.save(roleDirector);
        }
        
        if (!roleRepository.existsByName(RoleName.ROLE_COLLABORATOR)) {
            Role roleCollaborator = new Role(RoleName.ROLE_COLLABORATOR);
            roleRepository.save(roleCollaborator);
        }

        if (!roleRepository.existsByName(RoleName.ROLE_REGISTEREDUSER)) {
            Role roleUser = new Role(RoleName.ROLE_REGISTEREDUSER);
            roleRepository.save(roleUser);
        }

        UserConverter userConverter;
        UserRegistryDTO userRegistryDto;

        userBuilder = new UserBuilder(userService);

        // Convert info from Utilizador_v00.xml to User object and persist in database
        userConverter = UserConverterFactory.createUserConverter(choice);
        userRegistryDto = userConverter.parseToUser(choice);
        for (UserDTO userDTO : userRegistryDto.getListUsers()) {
            userBuilder.createAndPersistUser(userDTO);
        }

        // Get all users from database
        List<User> users = userService.listAllUsers();
        for (User u : users) {
            u.setPassword(passwordEncoder.encode(u.getPassword()));

        }

        // Assign profile ROLE_ADMINISTRATOR to the first user in the users list
        User user1 = users.get(0);

        user1.setRoles(Collections.singleton(roleAdmin));

        user1.getUserLoginData().setApprovedDateTime(LocalDateTime.now());
        user1.getUserLoginData().setLastLoginDate(LocalDateTime.now());
        userService.updateUser(user1);

        // Assign profile ROLE_DIRECTOR to the second user in the users list
        User user2 = users.get(1);
        user2.setRoles(Collections.singleton(roleDirector));

        user2.getUserLoginData().setApprovedDateTime(LocalDateTime.now());
        user2.getUserLoginData().setLastLoginDate(LocalDateTime.now());
        userService.updateUser(user2);

    }

}
