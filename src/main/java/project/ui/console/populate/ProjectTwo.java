package project.ui.console.populate;

import org.springframework.stereotype.Component;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.user.User;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class ProjectTwo{

    ProjectService projectService;
    TaskService taskService;
    ProjectCollaboratorService projectCollaboratorService;
    UserService userService;


    public ProjectTwo(UserService userService, ProjectService projectService, TaskService taskService, ProjectCollaboratorService projectCollaboratorService) {
        this.projectService= projectService;
        this.taskService=taskService;
        this.projectCollaboratorService=projectCollaboratorService;
        this.userService=userService;
    }



    public void projectTwoData() {
        /**
         * Projects initialization, project collaborators assignment and task related
         * work
         **/



        // Create Project 1 and persist in database

        /*Project Data*/
        String projectId="100";
        String projectName="Best Group Ever";
        String projectDescription="Quisque Nam blandit, lacus non cursus laoreet, erat velit molestie" +
                " ex, a fermentum dui mi sit amet quam. Quisque tristique sagittis egestas. In et es" +
                "t erat. Proin sem justo, luctus eget risus at, dapibus sagittis turpis. Fusce f" +
                "aucibus lorem hendrerit mi.";
        double globalBudget = 550;
        List<String> costOptions=  Arrays.asList("AverageCost", "MinimumTimePeriodCost", "MaximumTimePeriodCost");
        LocalDateTime projectStartDate = LocalDateTime.of(2017, 12, 18, 0, 0);


        projectService.addProject(projectId, projectName);
        Project project1 = projectService.getProjectByID(projectId);
        project1.setDescription(projectDescription);
        project1.setGlobalBudget (globalBudget);
        List<String> option = new ArrayList<>();
        option.addAll(costOptions);
        project1.setCostCalculationOptions(option);
        project1.setActive();
        project1.setStartDate(projectStartDate);


        /*Project Team*/
        User projectManager = userService.searchUserByEmail("filipa@switch.com");
        projectCollaboratorService.setProjectManager(project1, projectManager);


        User user1 = userService.searchUserByEmail("lisa@switch.com");
        double projCollab1Cost= 3 ;
        projectCollaboratorService.addProjectCollaborator(project1, user1,  projCollab1Cost);
        ProjectCollaborator projectCollaborator1 = project1.findProjectCollaborator(user1);
        projectCollaborator1.getCostAndTimePeriodList().get(0).setStartDate(projectStartDate.toLocalDate());



        User user2 = userService.searchUserByEmail("ana@switch.com");
        double projCollab2Cost= 5 ;
        projectCollaboratorService.addProjectCollaborator(project1, user2,  projCollab2Cost);
        ProjectCollaborator projectCollaborator2 = project1.findProjectCollaborator(user2);
        projectCollaborator2.getCostAndTimePeriodList().get(0).setStartDate(projectStartDate.toLocalDate());



        double projCollab3Cost= 5 ;
        projectCollaboratorService.addProjectCollaborator(project1, projectManager, projCollab3Cost);
        ProjectCollaborator projectCollaborator3 = project1.findProjectCollaborator(projectManager);
        projectCollaborator3.getCostAndTimePeriodList().get(0).setStartDate(projectStartDate.toLocalDate());


        User user4 = userService.searchUserByEmail("bebiana@switch.com");
        double projCollab4Cost= 5 ;
        projectCollaboratorService.addProjectCollaborator(project1, user4, projCollab4Cost);
        ProjectCollaborator projectCollaborator4 = project1.findProjectCollaborator(user4);
        projectCollaborator4.getCostAndTimePeriodList().get(0).setStartDate(projectStartDate.toLocalDate());


        User user5 = userService.searchUserByEmail("pedro@switch.com");
        double projCollab5Cost= 5 ;
        projectCollaboratorService.addProjectCollaborator(project1,  user5, projCollab5Cost);
        ProjectCollaborator projectCollaborator5 = project1.findProjectCollaborator(user5);
        projectCollaborator5.getCostAndTimePeriodList().get(0).setStartDate(projectStartDate.toLocalDate());


        User user6 = userService.searchUserByEmail("joao@switch.com");
        double projCollab6Cost= 5 ;
        projectCollaboratorService.addProjectCollaborator(project1,  user6, projCollab6Cost);
        ProjectCollaborator projectCollaborator6 = project1.findProjectCollaborator(user6);
        projectCollaborator6.getCostAndTimePeriodList().get(0).setStartDate(projectStartDate.toLocalDate());



        User user7 = userService.searchUserByEmail("daniel@switch.com");
        double projCollab7Cost= 5 ;
        projectCollaboratorService.addProjectCollaborator(project1,  user7, projCollab7Cost);
        ProjectCollaborator projectCollaborator7 = project1.findProjectCollaborator(user7);
        projectCollaborator7.getCostAndTimePeriodList().get(0).setStartDate(projectStartDate.toLocalDate());


        User user8 = userService.searchUserByEmail("antonio@switch.com");
        double projCollab8Cost= 5 ;
        projectCollaboratorService.addProjectCollaborator(project1,  user8, projCollab8Cost);
        ProjectCollaborator projectCollaborator8 = project1.findProjectCollaborator(user8);
        projectCollaborator8.getCostAndTimePeriodList().get(0).setStartDate(projectStartDate.toLocalDate());



        User user9= userService.searchUserByEmail("sergio@switch.com");
        double projCollab9Cost= 5 ;
        projectCollaboratorService.addProjectCollaborator(project1,  user9, projCollab9Cost);
        ProjectCollaborator projectCollaborator9 = project1.findProjectCollaborator(user9);
        projectCollaborator9.getCostAndTimePeriodList().get(0).setStartDate(projectStartDate.toLocalDate());


        /*Project Tasks*/
        String taskGeneralDescription="In hac habitasse platea dictumst. Suspendisse" +
                " aliquet rutrum varius. Ut vitae pretium sem. Ut molestie pretium arcu vel congue. " +
                "Vestibulum non urna tortor. Maecenas non turpis est. Cras luctus ipsum ac gravida " +
                "scelerisque. Aliquam mattis ex et lorem iaculis mattis. Nam faucibus euismod tellus, " +
                "id rhoncus purus dignissim at. Mauris viverra elit eu purus tempor, at pulvinar elit" +
                " bibendum. Integer semper metus vitae nunc condimentum, sed tempor nisi imperdiet.";

        //Task1
        String task1Title="Have fun !!!";

        double task1UnitCost= 1.5;
        double task1EstimatedEffort= 2;
        taskService.addTask(project1, task1Title, taskGeneralDescription, projectStartDate, projectStartDate.plusMonths(1), task1UnitCost, task1EstimatedEffort);
        Task task1 = project1.findTaskByTitle(task1Title);


        // task1 Collaborator 1
        taskService.addProjectCollaboratorToTask(projectCollaborator1, task1);
        TaskCollaboratorRegistry col2Task1Registry = task1.getTaskCollaboratorRegistryByID(task1.getId() + "-" + projectCollaborator1.getUser().getEmail());
        col2Task1Registry.setCollaboratorAddedToTaskDate(projectStartDate);

        //reports
        task1.addReport(projectCollaborator1, 0.3, projectStartDate.plusDays(5),  projectStartDate.plusDays(10));
        task1.addReport(projectCollaborator1, 0.8, projectStartDate.plusDays(15), projectStartDate.plusDays(20));


        // task1 Collaborator 2
        taskService.addProjectCollaboratorToTask(projectCollaborator2, task1);
        TaskCollaboratorRegistry col3Task1Registry = task1.getTaskCollaboratorRegistryByID(task1.getId() + "-" + projectCollaborator2.getUser().getEmail());
        col3Task1Registry.setCollaboratorAddedToTaskDate(projectStartDate.plusDays(6));

        //reports
        task1.addReport(projectCollaborator2, 0.5, projectStartDate.plusDays(17),  projectStartDate.plusDays(23));
        task1.addReport(projectCollaborator2, 0.7,  projectStartDate.plusDays(28), projectStartDate.plusDays(40));


        task1.setTaskCompleted();
        task1.setEffectiveDateOfConclusion(projectStartDate.plusMonths(1));






        //Task2
        String task2Title="Work Hard";

        double task2UnitCost= 1;
        double task2EstimatedEffort= 2;
        LocalDateTime taskPredictedStartDate= getStartDateFollowingTask(task1);
        taskService.addTask(project1, task2Title, taskGeneralDescription, taskPredictedStartDate,  taskPredictedStartDate.plusMonths(3), task2UnitCost, task2EstimatedEffort);
        Task task2 = project1.findTaskByTitle(task2Title);


        // task2 Collaborators 1

        taskService.addProjectCollaboratorToTask(projectCollaborator3, task2);
        TaskCollaboratorRegistry col1Task2Registry = task2.getTaskCollaboratorRegistryByID(task2.getId() + "-" + projectCollaborator3.getUser().getEmail());
        col1Task2Registry.setCollaboratorAddedToTaskDate(taskPredictedStartDate.plusDays(3));

        task2.addReport(projectCollaborator3, 2,taskPredictedStartDate.plusDays(5), taskPredictedStartDate.plusDays(5));
        task2.addReport(projectCollaborator3, 0.9, taskPredictedStartDate.plusDays(8), taskPredictedStartDate.plusDays(18));
        task2.addReport(projectCollaborator3, 0.1, taskPredictedStartDate.plusMonths(1), taskPredictedStartDate.plusMonths(1).plusDays(5));



        // task2 Collaborators 2

        taskService.addProjectCollaboratorToTask(projectCollaborator4, task2);
        TaskCollaboratorRegistry col3Task2Registry = task2.getTaskCollaboratorRegistryByID(task2.getId() + "-" + projectCollaborator4.getUser().getEmail());
        col3Task2Registry.setCollaboratorAddedToTaskDate(taskPredictedStartDate.plusMonths(1));


        task2.addReport(projectCollaborator4, 1.2, taskPredictedStartDate.plusMonths(1).plusDays(3), taskPredictedStartDate.plusMonths(1).plusDays(4));


        //task2 status
        task2.setTaskCompleted();
        task2.setEffectiveDateOfConclusion(taskPredictedStartDate.plusMonths(3).plusDays(4));



        // task3

        String task3Title="Be awesome";

        double task3UnitCost= 4;
        double task3EstimatedEffort= 2;
        LocalDateTime task3PredictedStartDate= getStartDateFollowingTask(task2);

        taskService.addTask(project1, task3Title, taskGeneralDescription, task3PredictedStartDate, task3PredictedStartDate.plusMonths(5), task3UnitCost, task3EstimatedEffort);
        Task task3 = project1.findTaskByTitle(task3Title);
        taskService.addProjectCollaboratorToTask(projectCollaborator5, task3);
        TaskCollaboratorRegistry col1Task3Registry = task3.getTaskCollaboratorRegistryByID(task3.getId() + "-" + projectCollaborator5.getUser().getEmail());
        col1Task3Registry.setCollaboratorAddedToTaskDate(task3PredictedStartDate.plusDays(4));

        //Requests
        task3.requestAddProjectCollaborator(projectCollaborator6);


        //Dependencies





        // task4
        String task4Title="Get plenty of sleep";
        double task4UnitCost= 3;
        double task4EstimatedEffort=5;
        LocalDateTime task4PredictedStartDate= getStartDateFollowingTask(task3);


        taskService.addTask(project1, task4Title, taskGeneralDescription, task4PredictedStartDate.plusDays(15), task4PredictedStartDate.plusMonths(2),  task4UnitCost,  task4EstimatedEffort);
        Task task4 = project1.findTaskByTitle(task4Title);
        taskService.addProjectCollaboratorToTask(projectCollaborator7, task4);
        TaskCollaboratorRegistry col1Task4Registry = task4.getTaskCollaboratorRegistryByID(task4.getId() + "-" + projectCollaborator7.getUser().getEmail());
        col1Task4Registry.setCollaboratorAddedToTaskDate(task4PredictedStartDate.plusDays(4));

        //Dependencies

        task4.addTaskDependency(task3);



        // task5

        String task5Title="Listen to music";
        double task5UnitCost= 4;
        double task5EstimatedEffort=4;

        LocalDateTime task5PredictedStartDate= getStartDateFollowingTask(task4);


        taskService.addTask(project1,  task5Title, taskGeneralDescription, task5PredictedStartDate,  task5PredictedStartDate.plusMonths(1), task5UnitCost, task5EstimatedEffort);
        Task task5 = project1.findTaskByTitle(task5Title);
        taskService.addProjectCollaboratorToTask(projectCollaborator8, task5);

        TaskCollaboratorRegistry col3Task5Registry = task5.getTaskCollaboratorRegistryByID(task5.getId() + "-" + projectCollaborator8.getUser().getEmail());
        col3Task5Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusMonths(2));

        task5.addReport(projectCollaborator8, 4, LocalDateTime.now().minusDays(20), LocalDateTime.now().minusDays(7));
        task5.addReport(projectCollaborator8, 6, LocalDateTime.now().minusDays(10), LocalDateTime.now().minusDays(7));





        // task6

        String task6Title="Write Code";

        double task6UnitCost=2;
        double task6EstimatedEffort= 4;
        LocalDateTime task6PredictedStartDate= getStartDateFollowingTask(task5);


        taskService.addTask(project1, task6Title, taskGeneralDescription,  task6PredictedStartDate,  task6PredictedStartDate.plusMonths(6), task6UnitCost, task6EstimatedEffort);
        Task task6 = project1.findTaskByTitle(task6Title);

        taskService.addProjectCollaboratorToTask(projectCollaborator9, task6);

        TaskCollaboratorRegistry col3Task6Registry = task6.getTaskCollaboratorRegistryByID(task6.getId() + "-" + projectCollaborator9.getUser().getEmail());
        col3Task6Registry.setCollaboratorAddedToTaskDate(task6PredictedStartDate);

        task6.addReport(projectCollaborator9, 4, LocalDateTime.now().minusDays(16), LocalDateTime.now().minusDays(7));




        // task7

        String task7Title="Test everything";
        double task7UnitCost= 1;
        double task7EstimatedEffort= 3;


        taskService.addTask(project1, task7Title, taskGeneralDescription, projectStartDate, projectStartDate.plusYears(1), task7UnitCost, task7EstimatedEffort);

        Task task7 = project1.findTaskByTitle(task7Title);
        taskService.addProjectCollaboratorToTask(projectCollaborator4, task7);

        TaskCollaboratorRegistry col1Task7Registry = task7.getTaskCollaboratorRegistryByID(task7.getId() + "-" + projectCollaborator4.getUser().getEmail());
        col1Task7Registry.setCollaboratorAddedToTaskDate(projectStartDate);

        task7.addReport(projectCollaborator4, 4, LocalDateTime.now().minusDays(10), LocalDateTime.now().minusDays(7));
        task7.requestRemoveProjectCollaborator(projectCollaborator4);








        projectService.updateProject(project1);

        projectCollaboratorService.updateProjectCollaborator(projectCollaborator1);
        projectCollaboratorService.updateProjectCollaborator(projectCollaborator2);
        projectCollaboratorService.updateProjectCollaborator(projectCollaborator3);
        projectCollaboratorService.updateProjectCollaborator(projectCollaborator4);
        projectCollaboratorService.updateProjectCollaborator(projectCollaborator5);
        projectCollaboratorService.updateProjectCollaborator(projectCollaborator6);
        projectCollaboratorService.updateProjectCollaborator(projectCollaborator7);
        projectCollaboratorService.updateProjectCollaborator(projectCollaborator8);
        projectCollaboratorService.updateProjectCollaborator(projectCollaborator9);




        taskService.updateTask(task1);
        taskService.updateTask(task2);
        taskService.updateTask(task3);
        taskService.updateTask(task4);
        taskService.updateTask(task5);
        taskService.updateTask(task6);
        taskService.updateTask(task7);

    }


    public LocalDateTime getStartDateFollowingTask(Task task){
        if(task.getEfectiveDateOfConclusion()!= null){
            return task.getEfectiveDateOfConclusion();
        }
        else if(task.getPredictedDateOfConclusion()!= null){

            return task.getPredictedDateOfConclusion();
        }

        return null;
    }

}
