package project.model.projectcollaborator;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import project.model.project.Project;
import project.model.user.User;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class to create Project Collaborators and manage their cost
 */

@Entity
public class ProjectCollaborator {

    private static AtomicInteger idGenerator = new AtomicInteger();
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Embedded
    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    protected ProjectCollaborator() {
    }

    /**
     * Constructs a Project ROLE_COLLABORATOR
     *
     * @param user
     * @param cost cost of this collaborator in Project
     */
    public ProjectCollaborator(Project project, User user, double cost) {
        this.user = user;
        this.project = project;
        this.costAndTimePeriodList = new ArrayList<>();
        addNewCostAndAssociatedPeriod(cost);
    }

    /**
     * Method to get and increment id
     *
     * @return id
     */
    public static int getAndIncrementId() {
        return idGenerator.incrementAndGet();
    }

    /**
     * Method to set a start id generator
     *
     * @param i
     */
    public static void setStartIdGenerator(int i) {
        idGenerator.set(i - 1);
    }

    /**
     * Get user's instance of this ProjectCollaborator
     *
     * @return User
     */
    public User getUser() {
        return user;
    }

    /**
     * Set user of ProjectCollaborator
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Get cost of ProjectCollaborator
     *
     * @return double
     */
    public double getCurrentCost() {
        double currentCost = 0;
        for (ProjectCollaboratorCostAndTimePeriod projCollab : costAndTimePeriodList) {
            if (projCollab.getEndDate() == null) {
                currentCost = projCollab.getCost();
            }
        }
        return currentCost;
    }

    /**
     * Get Project ROLE_COLLABORATOR's ID
     *
     * @return integer
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Method to check if collaborator is active.
     *
     * @return true if Project ROLE_COLLABORATOR is Active in project, false otherwise
     */
    public boolean isActiveInProject() {
        for (ProjectCollaboratorCostAndTimePeriod p : costAndTimePeriodList) {
            if (p.getEndDate() == null)
                return true;
        }

        return false;
    }

    // TOSOLVE: verificar se boolean deste método faz sentido, ou se fica void

    /**
     * Method to return last Cost and Time Period
     *
     * @return ProjectCollaboratorCostAndTimePeriod
     */
    public ProjectCollaboratorCostAndTimePeriod getLastCostAndTimePeriod() {
        return costAndTimePeriodList.get(costAndTimePeriodList.size() - 1);
    }

    /**
     * Method to add a new Cost And Associated Period in Project
     *
     * @param cost
     * @return boolean
     */
    public boolean addNewCostAndAssociatedPeriod(double cost) {
        ProjectCollaboratorCostAndTimePeriod projCollanReg;

        if (this.isActiveInProject()) {
            this.getLastCostAndTimePeriod().setEndDate(LocalDate.now());
            projCollanReg = new ProjectCollaboratorCostAndTimePeriod(cost);
            projCollanReg.setStartDate(LocalDate.now().plusDays(1));
            return costAndTimePeriodList.add(projCollanReg);
        } else {
            projCollanReg = new ProjectCollaboratorCostAndTimePeriod(cost);
            return costAndTimePeriodList.add(projCollanReg);
        }
    }

    /**
     * Get a list of Costs and associated periods to project
     *
     * @return List of ProjectCollaboratorCostAndTimePeriod
     */
    public List<ProjectCollaboratorCostAndTimePeriod> getCostAndTimePeriodList() {
        return costAndTimePeriodList;
    }

    /**
     * Set a list of Costs and associated periods to project
     */
    public void setCostAndTimePeriodList(List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList) {
        this.costAndTimePeriodList = costAndTimePeriodList;
    }

    /**
     * Get project of this ProjectCollaborator
     *
     * @return Project
     */
    public Project getProject() {
        return project;
    }

    @Override
    public String toString() {
        return "\nProject ROLE_COLLABORATOR " + "Name=" + user.getName() + ", " + "Email=" + user.getEmail() + ", " + "cost="
                + getCurrentCost() + "\n";
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (project != null ? project.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ProjectCollaborator other = (ProjectCollaborator) obj;
        if (project == null) {
            if (other.project != null) {
                return false;
            }
        } else if (!project.equals(other.project)) {
            return false;
        }
        return (user.equals(other.user));
    }

}