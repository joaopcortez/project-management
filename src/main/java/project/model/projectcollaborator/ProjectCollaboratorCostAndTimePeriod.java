package project.model.projectcollaborator;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.LocalDate;

@Embeddable
public class ProjectCollaboratorCostAndTimePeriod {

    @Column(nullable = false)
    private LocalDate startDate;

    @Column
    private LocalDate endDate;

    @Column(nullable = false)
    private double cost;

    public ProjectCollaboratorCostAndTimePeriod(double cost) {
        this.cost = cost;
        this.startDate = LocalDate.now();
        this.endDate = null;
    }

    protected ProjectCollaboratorCostAndTimePeriod() {
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

}