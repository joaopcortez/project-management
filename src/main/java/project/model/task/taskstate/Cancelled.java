package project.model.task.taskstate;

import java.util.ArrayList;
import java.util.List;

import project.model.task.Task;

public class Cancelled extends BaseState {
    Task task;

    public Cancelled(Task task) {
        this.task = task;
    }

    @Override
    public boolean isValid() {
        return (task.getTaskState().isOnCancelledState());

    }

    @Override
    public boolean isOnCancelledState() {
        return true;
    }

    @Override
    public void changeTo() {
//Not method because Cancelled is a final State.
    }
    
    public List<String> listAvailableActions() {
    	
    		return new ArrayList<>();
    }

}
