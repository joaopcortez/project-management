package project.model.task.taskstate;

import java.util.List;

public interface TaskState {

    boolean isValid();

    boolean isOnAssignedState();

    boolean isOnCancelledState();

    boolean isOnCompletedState();

    boolean isOnCreatedState();

    boolean isOnInProgressState();

    boolean isOnPlannedState();

    boolean isOnReadyToStartState();

    boolean isOnSuspendedState();
    
    List<String> listAvailableActions();

    /* Changes TaskState to a valid state,
     * @param task
     */
    void changeTo();

}
