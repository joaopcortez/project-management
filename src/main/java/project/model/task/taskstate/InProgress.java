package project.model.task.taskstate;

import java.util.ArrayList;
import java.util.List;

import project.model.task.Task;

public class InProgress extends BaseState {
    Task task;

    public InProgress(Task task) {
        this.task = task;
    }

    @Override
    public boolean isOnInProgressState() {
        return true;
    }

    @Override
    public boolean isValid() {
        return (task.hasAssignedProjectCollaborator() && task.getEffectiveStartDate() != null);
                       

    }

    @Override
    public void changeTo() {
        TaskState suspended = new Suspended(task);
        TaskState completed = new Completed(task);
        TaskState cancelled = new Cancelled(task);

        List<TaskState> validTransitions = new ArrayList<>();
        validTransitions.add(completed);
        validTransitions.add(suspended);
        validTransitions.add(cancelled);

        for (TaskState ts : validTransitions) {
            if (ts.isValid()) {
                task.setTaskState(ts);
                return;
            }
        }
    }

    public List<String> listAvailableActions() {
    	
		List<String> availableActions = new ArrayList<>();
		
		availableActions.add(taskStateName + REQUEST_ADD_COLLABORATOR);
		availableActions.add(taskStateName + ADD_COLLABORATOR);
		availableActions.add(taskStateName + REQUEST_REMOVE_COLLABORATOR);
		availableActions.add(taskStateName + REMOVE_COLLABORATOR);
		availableActions.add(taskStateName + ADD_REPORT);
		availableActions.add(taskStateName + CANCEL_TASK);	
		availableActions.add(taskStateName + REQUEST_SET_TASK_COMPLETED);
		availableActions.add(taskStateName + SET_TASK_COMPLETED);
		return availableActions;
}
}
