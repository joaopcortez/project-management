package project.model.task.taskstate;

public abstract class BaseState implements TaskState {
	
	protected final String taskStateName = this.getClass().getSimpleName();
	
	protected static final String SET_PREDICTED_START_DATE = ".setPredictedDateOfStart";
	protected static final String SET_PREDICTED_END_DATE = ".setPredictedDateOfConclusion";
	protected static final String REQUEST_ADD_COLLABORATOR = ".requestAddProjectCollaborator";
	protected static final String ADD_COLLABORATOR = ".addProjectCollaborator";
	protected static final String REQUEST_REMOVE_COLLABORATOR = ".requestRemoveProjectCollaborator";
	protected static final String REMOVE_COLLABORATOR = ".removeProjectCollaborator";
	protected static final String ADD_TASK_DEPENDENCY = ".addTaskDependency";
	protected static final String REMOVE_TASK = ".removeTask";
	protected static final String ADD_REPORT = ".addReport";
	protected static final String CANCEL_TASK = ".setTaskCancelled";
	protected static final String REQUEST_SET_TASK_COMPLETED = ".requestTaskCompleted";
	protected static final String SET_TASK_COMPLETED = ".setTaskCompleted";
	
    @Override
    public boolean isOnAssignedState() {
        return false;
    }

    @Override
    public boolean isOnCancelledState() {
        return false;
    }

    @Override
    public boolean isOnCompletedState() {
        return false;
    }

    @Override
    public boolean isOnCreatedState() {
        return false;
    }

    @Override
    public boolean isOnInProgressState() {
        return false;
    }

    @Override
    public boolean isOnPlannedState() {
        return false;
    }

    @Override
    public boolean isOnReadyToStartState() {
        return false;
    }

    @Override
    public boolean isOnSuspendedState() {
        return false;
    }
    
    public String toString() {
    		return this.getClass().getSimpleName();
    }
}
