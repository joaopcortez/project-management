package project.model.task;

import project.dto.rest.ReportRestDTO;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Entity
public class Report implements Comparable<Report>{

    @Id
    private String code;
    private double quantity;
    private LocalDateTime reportCreationDate;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private String userEmail;



    protected Report() {

    }
    
    /**
     * Builds a report.
     *
     * @param id
     * @param quantity
     * @param startDate
     * @param endDate
     */
    public Report(String id, double quantity, LocalDateTime startDate, LocalDateTime endDate) {
    		code = id;
    		this.quantity = quantity;
    		reportCreationDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
        this.startDate = startDate;
        this.endDate = endDate;
    }

    /**
     * @return the idReport
     */
    public String getCode() {
        return code;
    }

    /**
     * Get the quantity.
     *
     * @return quantity.
     */
    public double getQuantity() {
        return quantity;
    }

    /**
     * Sets the quantity.
     *
     * @param quantity
     */
    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    /**
     * Get the report date.
     *
     * @return
     */
    public LocalDateTime getReportCreationDate() {
        return reportCreationDate;
    }

    /**
     * Sets the report date.
     *
     * @param date
     */
    public void setReportDate(LocalDateTime date) {
        reportCreationDate = date;
    }

    /**
     * Get the report start date.
     *
     * @return the start date.
     */
    public LocalDateTime getStartDate() {
        return startDate;
    }

    /**
     * Sets the report start date.
     *
     * @param startDate
     */
    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }
    
    

    public void setReportCreationDate(LocalDateTime reportCreationDate) {
		this.reportCreationDate = reportCreationDate;
	}

	public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Report other = (Report) obj;
        if (reportCreationDate == null) {
            if (other.reportCreationDate != null)
                return false;
        } else if (!reportCreationDate.equals(other.reportCreationDate))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        return reportCreationDate.hashCode();
    }

    @Override
    public String toString() {
        return "\nReport id = " + code + "\nSpended Time=" + quantity + "\nreportDate=" + reportCreationDate + "\nstartDate="
                + startDate + "\n";
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }


    /**
     * Method to pass task to DTO.
     *
     * @return taskRestDTO.
     */
    public ReportRestDTO toDTO() {
        ReportRestDTO reportRestDTO = new  ReportRestDTO();
        reportRestDTO.setEffort(quantity);
        reportRestDTO.setStartDate(startDate);
        reportRestDTO.setEndDate(endDate);
        reportRestDTO.setUserEmail(userEmail);
        reportRestDTO.setCode(code);
        reportRestDTO.setReportCreationDate(reportCreationDate);
        return reportRestDTO;
    }

	@Override
	public int compareTo(Report report) {
		if (this.reportCreationDate.isAfter(report.getReportCreationDate()))
			return 1;
		if (this.reportCreationDate.isBefore(report.getReportCreationDate()))
			return -1;
		return 0;
	}

}
