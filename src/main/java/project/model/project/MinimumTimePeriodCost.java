package project.model.project;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;
import project.model.task.Report;

public class MinimumTimePeriodCost implements CostCalculator {

	@Override
	public double calculateReportCost(Report report, List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList) {
		
		double minValue=-1;
		
		Optional<Double> min =costAndTimePeriodList.stream()
				.map(ProjectCollaboratorCostAndTimePeriod :: getCost)
				.min(Comparator.naturalOrder());
	
		if(min.isPresent()) {
			
			minValue= min.get();
		}
		
	  return report.getQuantity() * minValue; 
	}

}
