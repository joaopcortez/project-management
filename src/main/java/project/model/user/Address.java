package project.model.user;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Class to manage addresses.
 */

@Embeddable
public class Address {

    @Column(nullable = false)
    private String city;

    @Column(nullable = false)
    private String country;

    @Column(nullable = false)
    private String addressDescription;

    @Column(nullable = false)
    private String postalCode;

    @Column(nullable = false)
    private String street;

    /**
     * Class constructor.
     *
     * @param addressDescription Unique ID for address.
     * @param street     Street of the address.
     * @param postalCode Postal code number of the address.
     * @param city       City of the address.
     * @param country    Country of the address.
     */
    public Address(String addressDescription, String street, String postalCode, String city, String country) {
        this.street = street;
        this.postalCode = postalCode;
        this.city = city;
        this.country = country;
        this.addressDescription = addressDescription;

    }

    protected Address() {
    }

    /**
     * Get City name.
     *
     * @return String City.
     */
    public String getCity() {
        return city;
    }

    /**
     * Set City name.
     *
     * @param city name of the city.
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Get Country.
     *
     * @return String country.
     */
    public String getCountry() {
        return country;
    }

    /**
     * Set name of country.
     *
     * @param country String country.
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Get ID for address.
     *
     * @return String with unique address ID.
     */
    public String getAddressDescription() {
        return addressDescription;
    }

    /**
     * Set ID for address.
     *
     * @param id String represents unique ID.
     */
    public void setAddressDescription(String id) {
        addressDescription = id;
    }

    /**
     * Get Postal Code.
     *
     * @return String Postal Code.
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Set Postal Code.
     *
     * @param postalCode String represents Postal Code.
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * Get Street.
     *
     * @return String street.
     */
    public String getStreet() {
        return street;
    }

    /**
     * Set Street.
     *
     * @param street String represents street.
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * Validates address, using city, street, postal code and id.
     *
     * @return True if address is valid, false otherwise.
     */
    public boolean isValid() {

        if (city == null || city.isEmpty() || postalCode == null || postalCode.isEmpty())
            return false;

        return !(street == null || street.isEmpty() || addressDescription == null || addressDescription.isEmpty());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Address))
            return false;

        Address address = (Address) o;

        return addressDescription.equals(address.addressDescription);
    }

    @Override
    public int hashCode() {
        return addressDescription.hashCode();
    }

    @Override
    /**
     * Return a string with street, postal code, city and country.
     */
    public String toString() {

        return "[" + addressDescription + ", " + street + ", " + postalCode + ", " + city + ", " + country + "]";
    }
}