package project.model.user;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class EmailAddress {

    private InternetAddress emailAddre;

    public EmailAddress(String email) throws AddressException {
        emailAddre = new InternetAddress(email);
        emailAddre.validate();
    }

    public InternetAddress getEmailAddre() {
        return emailAddre;
    }

    public void setEmailAddre(String email) throws AddressException {

        this.emailAddre = new InternetAddress(email);
    }

    @Override
    public String toString() {
        return "" + emailAddre;
    }

    @Override
    public int hashCode() {
        return emailAddre.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EmailAddress other = (EmailAddress) obj;
        return emailAddre.equals(other.emailAddre);
    }

}
