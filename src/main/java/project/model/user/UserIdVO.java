package project.model.user;

import javax.persistence.Embeddable;
import java.io.Serializable;

@SuppressWarnings("serial")
@Embeddable
public class UserIdVO implements Serializable {
    private String userEmail;

    public UserIdVO() {

    }

    public UserIdVO(String userEmail) {
        this.userEmail = userEmail;

    }

    public static UserIdVO create(String email) {
        return new UserIdVO(email);
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserIdVO)) {
            return false;
        }
        UserIdVO userIdVO = (UserIdVO) o;

        return userEmail.equals(userIdVO.userEmail);
    }

    @Override
    public int hashCode() {
        return userEmail.hashCode();
    }
}
