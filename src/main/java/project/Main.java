package project;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Main {

    public static void main(String[] args) {/*To nothing*/}
    // This class is only to run base SpringBoot tests.

    @Bean
    public CommandLineRunner demo() {
        return (String... args) -> {/*To nothing*/};
    }

}
