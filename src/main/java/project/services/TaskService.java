package project.services;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import project.dto.rest.TaskCollaboratorRegistryRESTDTO;
import project.dto.rest.TaskRestDTO;
import project.jparepositories.TaskRepository;
import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;

@Service
public class TaskService {

	@Autowired
	private TaskRepository taskRepository;

	/**
	 * Constructor of a new task list.
	 */
	public TaskService(TaskRepository taskRepository) {
		this.taskRepository = taskRepository;
	}

	/**
	 * Method to update Task
	 *
	 * @param task
	 * @return true if is sucessfully updated, false otherwise
	 */
	public boolean updateTask(Task task) {

		if (taskRepository.existsById(task.getId())) {
			task.getProject().refreshTaskList(task);
			taskRepository.save(task);
			return true;
		}
		return false;
	}

	/**
	 * Instantiates and adds a task with title to project
	 *
	 * @param project
	 * @param title
	 */
	public void addTask(Project project, String title) {

		Task task = new Task(project, title);
		project.addTask(task);
		taskRepository.save(task);
	}

	/**
	 * Instantiates and adds a task with ID and TITLE to the project's task list
	 *
	 * @param project
	 * @param title
	 * @param id
	 */
	public void addTask(Project project, String title, String id) {

		Task task = new Task(project, title);
		project.addTask(task);
		task.setTaskId(id);
		taskRepository.save(task);
	}

	/**
	 * Instantiates and adds a task to the project's task list.
	 *
	 * @param title
	 *            Parameter to use in the task's constructor.
	 * @param description
	 *            Description of task
	 * @param daysAfterProjectStart
	 *            Number of days between project start and the beginning of task
	 * @param daysTaskDuration
	 *            duration of the task in days
	 * @param unitCost
	 *            Unit cost
	 * @param estimatedEffort
	 *            Estimated Effort
	 */
	public void addTask(Project project, String title, String description, int daysAfterProjectStart,
			int daysTaskDuration, double unitCost, double estimatedEffort) {

		LocalDateTime predictedDateOfStart = project.getStartDate().plusDays(daysAfterProjectStart);
		LocalDateTime predictedDateOfConclusion = predictedDateOfStart.plusDays(daysTaskDuration);

		Task task = new Task(project, title, description, predictedDateOfStart, predictedDateOfConclusion, unitCost,
				estimatedEffort);
		project.addTask(task);
		taskRepository.save(task);
	}

	/**
	 * Instantiates and adds a task to the project's task list.
	 *
	 * @param title
	 *            Parameter to use in the task's constructor.
	 * @param description
	 *            Description of task
	 * @param predictedDateOfStart
	 *            Predicted date of start
	 * @param predictedDateOfConclusion
	 *            Predicted date of conclusion
	 * @param unitCost
	 *            Unit cost
	 * @param estimatedEffort
	 *            Estimated Effort
	 */
	public void addTask(Project project, String title, String description, LocalDateTime predictedDateOfStart,
			LocalDateTime predictedDateOfConclusion, double unitCost, double estimatedEffort) {
		Task task = new Task(project, title, description, predictedDateOfStart, predictedDateOfConclusion, unitCost,
				estimatedEffort);
		project.addTask(task);
		taskRepository.save(task);
	}

	/**
	 * Method to add a ProjectCollaborator to Task
	 *
	 * @param projectCollaborator
	 * @param task
	 * @return true if is successfully added, false otherwise
	 */
	public boolean addProjectCollaboratorToTask(ProjectCollaborator projectCollaborator, Task task) {

		if ((task.getProject().listActiveProjectCollaborators().contains(projectCollaborator))
				&& (!task.hasProjectCollaboratorInTaskCollaboratorRegistryList(projectCollaborator)
						|| task.getLastTaskCollaboratorRegistryOf(projectCollaborator)
								.getCollaboratorRemovedFromTaskDate() != null)) {
			task.addProjectCollaborator(projectCollaborator);
			// colocar esta instrução a funcionar!! "updateTask(task);"
			return true;
		}
		return false;
	}

	/**
	 * Method to get active task collaborators.
	 *
	 * @param project
	 * @return a list of project tasks.
	 */
	public List<Task> getProjectTasks(Project project) {

		return project.getTasksList();
	}

	public List<TaskCollaboratorRegistryRESTDTO> listActiveTaskCollaborators(String taskId) {
		Task task = taskRepository.getOneByTaskId(taskId);
		return task.activeTaskCollaboratorRegistryToDTO();

	}

	public List<TaskCollaboratorRegistryRESTDTO> listAllTaskCollaborators(String taskId) {
		Task task = taskRepository.getOneByTaskId(taskId);
		return task.allTaskCollaboratorRegistryToDTO();

	}

	/**
	 * Searches for a task with a specific ID.
	 *
	 * @param id
	 *            search parameter.
	 * @return the task with matched ID, or null if one is not found.
	 */
	public Task findTaskByID(String id) {

		for (Task task : taskRepository.findAll()) {
			if (task.getId().equals(id)) {
				return task;
			}
		}
		return null;
	}

	/**
	 * Find a task and convert it to DTO.
	 *
	 * @param id
	 * @return task choosed to DTO.
	 */
	public TaskRestDTO findTaskByIDtoDTO(String id) {

		Task task = findTaskByID(id);
		if (task != null) {
			return task.toDTO();
		}
		return new TaskRestDTO();
	}

	/**
	 * Searches for a task with a specific ID.
	 *
	 * @param title
	 *            search parameter.
	 * @return the task with matched ID, or null if one is not found.
	 */
	public Task findTaskByTitle(String title) {

		for (Task task : taskRepository.findAll()) {
			if (task.getTitle().equals(title)) {
				return task;
			}
		}
		return null;
	}

	/**
	 * Removes ProjectCollaborator from task.
	 *
	 * @param collab
	 *            ROLE_COLLABORATOR to be removed.
	 * @param t
	 *            task to remove ROLE_COLLABORATOR from.
	 */
	public void removeProjectCollaboratorFromTask(ProjectCollaborator collab, Task t) {

		t.removeProjectCollaborator(collab);
	}

	/**
	 * Remove task from list of tasks.
	 *
	 * @param task
	 * @return true if task exists on the list of tasks (and remove the task) and
	 *         false if not.
	 */
	public boolean removeTask(Task task) {
		if (task.getTaskState().isOnCreatedState() || task.getTaskState().isOnPlannedState()
				|| task.getTaskState().isOnAssignedState() || task.getTaskState().isOnReadyToStartState()) {
			task.getProject().changeDependentTasksStatus(task);
			task.getProject().removeTask(task);
			taskRepository.delete(task);
			return true;
		}
		return false;
	}

	/**
	 * Remove task from list of tasks.
	 *
	 * @param taskDto
	 * @return true if task exists on the list of tasks (and remove the task) and
	 *         false if not.
	 */
	public boolean removeTask(TaskRestDTO taskDto) {

		Task task = findTaskByID(taskDto.getTaskId());

		return ((task != null) && removeTask(task));
	}

	/**
	 * Get collaborators completed tasks in reverse order
	 *
	 * @param colab
	 *            Collaborators from which return your own completed tasks
	 * @returncollaborator's completed tasks in reverse order
	 */
	public List<Task> listCollaboratorCompletedTasks(ProjectCollaborator colab) {

		List<Task> collaboratorCompletedTasks = new ArrayList<>();

		for (Task t : listCollaboratorTasks(colab))
			if (t.getTaskState().isOnCompletedState())
				collaboratorCompletedTasks.add(t);

		collaboratorCompletedTasks.sort(Collections.reverseOrder());

		return collaboratorCompletedTasks;
	}

	/**
	 * Checks all the tasks a user is involved with.
	 *
	 * @param projectCollaborator
	 *            User to search for in all the project's tasks.
	 * @return List of all the tasks pertaining to User u.
	 */
	public List<Task> listCollaboratorTasks(ProjectCollaborator projectCollaborator) {

		List<Task> collaboratorTasks = new ArrayList<>();

		for (Task t : taskRepository.findAll())
			if (t.hasProjectCollaboratorInTaskCollaboratorRegistryList(projectCollaborator)) {
				collaboratorTasks.add(t);
			}
		return collaboratorTasks;
	}

	/**
	 * Get collaborators completed tasks in the last month
	 *
	 * @param projectCollaborator
	 *            ROLE_COLLABORATOR from which return your own completed tasks in
	 *            the last month
	 * @return completed tasks in the last month
	 */
	public List<Task> listCollaboratorsCompletedTasksLastMonth(ProjectCollaborator projectCollaborator) {

		List<Task> completedLastMonth = new ArrayList<>();

		for (Task task : listCollaboratorTasks(projectCollaborator))
			if (task.isCompletedLastMonth()) {
				completedLastMonth.add(task);
			}

		completedLastMonth.sort(Collections.reverseOrder());
		return completedLastMonth;
	}

	/**
	 * Method to return a project's list of possible tasks to be added as
	 * dependency.
	 *
	 * @param project
	 *            project to get the list of tasks.
	 * @return a list of possible dependencies.
	 */
	public List<Task> listTasksThatCanBeDependency(Project project) {

		List<Task> listOfPossibleDependencies = new ArrayList<>();

		for (Task possibleDependency : getProjectTasks(project)) {

			if (!possibleDependency.getTaskState().isOnCancelledState()
					&& !possibleDependency.getTaskState().isOnCompletedState()
					&& !possibleDependency.getTaskState().isOnSuspendedState()) {

				listOfPossibleDependencies.add(possibleDependency);
			}
		}
		return listOfPossibleDependencies;
	}

	public List<TaskRestDTO> listTaskDependencies(String taskid) {
		List<TaskRestDTO> listTaskDependencies = new ArrayList<>();

		Task task = taskRepository.getOneByTaskId(taskid);
		List<Task> listDependenciesTask = task.getTaskDependencies();

		for (Task t : listDependenciesTask) {
			listTaskDependencies.add(t.toDTO());
		}

		return listTaskDependencies;
	}

	/**
	 * Method to get a list of DTOs with information of all completed last month
	 * tasks in project
	 *
	 * @param project
	 * @return List of TaskRestDTO
	 */
	public List<TaskRestDTO> listProjectCompletedTasksLastMonth(Project project) {

		List<TaskRestDTO> completedLastMonth = new ArrayList<>();

		for (Task task : listTasksByProject(project))

			if (task.isCompletedLastMonth()) {

				completedLastMonth.add(task.toDTO());
			}

		completedLastMonth.sort(Collections.reverseOrder());
		return completedLastMonth;
	}

	/**
	 * Method to get a list of DTOs with information of all completed tasks in
	 * Project
	 *
	 * @param project
	 * @return List of TaskRestDTO
	 */
	public List<TaskRestDTO> listProjectCompletedTasks(Project project) {

		List<TaskRestDTO> listTasks = new ArrayList<>();

		for (Task t : listTasksByProject(project)) {

			if (t.isCompleted())

				listTasks.add(t.toDTO());
		}
		return listTasks;
	}

	/**
	 * Method to get a list of DTOs with information of all tasks iniciated but not
	 * completed in Project
	 *
	 * @param project
	 * @return TaskRestDTO
	 */
	public List<TaskRestDTO> listProjectIniciatedButNotCompletedTasks(Project project) {

		List<TaskRestDTO> listTasks = new ArrayList<>();

		for (Task t : listTasksByProject(project)) {

			if ((t.getStatus().isOnInProgressState() || t.getStatus().isOnSuspendedState())) {

				listTasks.add(t.toDTO());
			}
		}
		return listTasks;
	}

	/**
	 * Method to get a list of DTOs with information of all cancelled tasks in
	 * Project
	 *
	 * @param project
	 * @return TaskRestDTO
	 */
	public List<TaskRestDTO> listProjectCancelledTasks(Project project) {

		List<TaskRestDTO> listTasks = new ArrayList<>();

		for (Task t : listTasksByProject(project)) {

			if (t.getTaskState().isOnCancelledState()) {

				listTasks.add(t.toDTO());
			}
		}
		return listTasks;
	}

	/**
	 * Method to get a list of DTOs with information of all tasks not completed and
	 * with predicted date expired in Project
	 *
	 * @param project
	 * @return TaskRestDTO
	 */
	public List<TaskRestDTO> listProjectNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks(Project project) {

		List<TaskRestDTO> listTasks = new ArrayList<>();

		for (Task task : listTasksByProject(project)) {

			if (!(task.isCompleted()) && (task.getPredictedDateOfConclusion() != null)
					&& (task.getPredictedDateOfConclusion().toLocalDate().isBefore(LocalDate.now()))) {

				listTasks.add(task.toDTO());
			}
		}
		return listTasks;
	}

	/**
	 * Method to get a list of DTOs with information of all tasks not iniciated in
	 * Project
	 *
	 * @param project
	 * @return TaskRestDTO
	 */
	public List<TaskRestDTO> listProjectNotInitiatedTasks(Project project) {

		List<TaskRestDTO> listTasks = new ArrayList<>();

		for (Task t : listTasksByProject(project)) {

			if (t.getTaskState().isOnCreatedState() || t.getTaskState().isOnPlannedState()
					|| t.getTaskState().isOnAssignedState() || t.getTaskState().isOnReadyToStartState()) {

				listTasks.add(t.toDTO());
			}
		}
		return listTasks;
	}

	/**
	 * Method to get all task from Project
	 *
	 * @param project
	 * @return list of tasks
	 */
	public List<Task> listTasksByProject(Project project) {

		return taskRepository.findByProject(project);
	}

	/**
	 * Method to get all task from Project
	 *
	 * @param project
	 * @return list of tasks
	 */
	public List<TaskRestDTO> listTasksDTOByProject(Project project) {

		List<TaskRestDTO> taskListDTO = new ArrayList<>();

		for (Task t : listTasksByProject(project)) {
			taskListDTO.add(t.toDTO());
		}

		return taskListDTO;
	}

	/**
	 * Method to remove ProjectCollaborator from all Task in Project
	 *
	 * @param project
	 * @param projectCollaborator
	 */
	public void removeProjectCollaboratorFromAllTasksInProject(Project project,
			ProjectCollaborator projectCollaborator) {
		for (Task t : listTasksByProject(project)) {
			t.removeProjectCollaborator(projectCollaborator);
			updateTask(t);
		}
	}

	/**
	 * Method to delete all entities managed by this repository.
	 */
	public void deleteAll() {
		taskRepository.deleteAll();
	}

	public TaskRestDTO markTaskCompleted(String taskid) {

		Task task = taskRepository.getOneByTaskId(taskid);
		task.setTaskCompleted();
		task.setRequestTaskCompleted(null);
		updateTask(task);
		return task.toDTO();

	
}

public TaskRestDTO cancelMarkTaskCompleted(String taskid) {

	Task task = taskRepository.getOneByTaskId(taskid);
	task.setRequestTaskCompleted(null);
	updateTask(task);
	return task.toDTO();

}

}
