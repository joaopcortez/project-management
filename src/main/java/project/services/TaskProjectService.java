package project.services;

import org.springframework.web.bind.annotation.RestController;
import project.dto.rest.TaskRestDTO;
import project.jparepositories.ProjectRepository;
import project.jparepositories.TaskRepository;
import project.model.project.Project;
import project.model.task.Task;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TaskProjectService {

    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;

    public TaskProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    /**
     * Method to fill task parameters.
     *
     * @param taskRestDTO
     * @param task
     */
    private static void fillTaskParameters(TaskRestDTO taskRestDTO, Task task) {
        taskRestDTO.setDateOfCreation(task.getDateOfCreation());
        taskRestDTO.setTaskId(task.getId());

        if (taskRestDTO.getDescription() != null) {
            task.setDescription(taskRestDTO.getDescription());
        }
        if (taskRestDTO.getPredictedDateOfStart() != null) {
            task.setPredictedDateOfStart(taskRestDTO.getPredictedDateOfStart());
        }
        if (taskRestDTO.getPredictedDateOfConclusion() != null) {
            task.setPredictedDateOfConclusion(taskRestDTO.getPredictedDateOfConclusion());
        }
        if (!Double.isNaN(taskRestDTO.getUnitCost())) {
            task.setUnitCost(taskRestDTO.getUnitCost());
        }
        if (!Double.isNaN(taskRestDTO.getEstimatedEffort())) {
            task.setEstimatedEffort(taskRestDTO.getEstimatedEffort());
        }

    }

    /**
     * Method to create task rest.
     *
     * @param taskRestDTO
     * @return TaskRestDTO.
     */
    public TaskRestDTO createTaskRest(TaskRestDTO taskRestDTO) {

        if (taskRestDTO.getProjectId() == null || !projectRepository.existsById(taskRestDTO.getProjectId()) || taskRestDTO.getTitle() == null) {
            return null;
        }
        Project project = projectRepository.getOneByCode(taskRestDTO.getProjectId());

        Task task = new Task(project, taskRestDTO.getTitle());
        fillTaskParameters(taskRestDTO, task);
        project.addTask(task);
        taskRepository.save(task);

        return task.toDTO();
    }

    /**
     * Method to list task by project to DTO.
     *
     * @param projectId
     * @return list of TaskRestDTO.
     */
    public List<TaskRestDTO> listTasksByProjectToDTO(String projectId) {

        Project project = projectRepository.getOneByCode(projectId);
        List<TaskRestDTO> taskList = new ArrayList<>();

        for (Task t : taskRepository.findByProject(project)) {
            taskList.add(t.toDTO());
        }
        return taskList;
    }

    /**
     * Method to return a project's list of possible tasks to be added as
     * dependency.
     *
     * @param taskId to get the list of tasks.
     * @return a list of possible dependencies.
     */
    public List<TaskRestDTO> listTasksThatCanBeAddedAsDependency(String taskId) {

        List<TaskRestDTO> listOfPossibleDependencies = new ArrayList<>();
        Task taskToAddDependency = taskRepository.getOneByTaskId(taskId);

        if (taskToAddDependency == null) {
            return listOfPossibleDependencies;
        }
        Project project = taskToAddDependency.getProject();

        for (Task possibleDependency : taskRepository.findByProject(project)) {

            if (listTasksThatCanBeAddedAsDependencyStateConditions(possibleDependency)
//                    && possibleDependency.getPredictedDateOfConclusion()!= null
//                    && possibleDependency.getPredictedDateOfConclusion().isBefore(taskToAddDependency.getPredictedDateOfStart())
                    && !taskToAddDependency.equals(possibleDependency)
                    && !taskToAddDependency.getTaskDependencies().contains(possibleDependency)
                    ) {

                listOfPossibleDependencies.add(possibleDependency.toDTO());
            }
        }
        return listOfPossibleDependencies;
    }

    private static boolean listTasksThatCanBeAddedAsDependencyStateConditions(Task possibleDependency) {
        return !possibleDependency.getTaskState().isOnCancelledState()
                && !possibleDependency.getTaskState().isOnCompletedState()
                && !possibleDependency.getTaskState().isOnSuspendedState();
    }


    public boolean addDependencies(String taskId, List<String> taskIdependencyList) {
        Task task = taskRepository.getOneByTaskId(taskId);
        if (task != null) {
            for (String dependencytaskId : taskIdependencyList) {
                task.addTaskDependency(taskRepository.getOneByTaskId(dependencytaskId));
                taskRepository.save(task);
            }

            return true;
        }
        return false;
    }

    public boolean setTaskCanceles(String taskId) {

        Task task = taskRepository.getOneByTaskId(taskId);

        if (task != null && task.setTaskCancelled()) {
            taskRepository.save(task);

            return true;

        }
        return false;
    }


}
