package project.services;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import project.dto.EmailMessageDTO;

@Service
public class EmailServiceImpl implements EmailService {

    private JavaMailSender javaMailSender;
    private SimpleMailMessage message;

    public EmailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
        message = new SimpleMailMessage();
    }

    @Override
    public void sendSimpleMessage(String to, String subject, String text) {
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        javaMailSender.send(message);
    }

    @Override
    public void sendSimpleMessage(EmailMessageDTO emailMessageDTO) {
        message.setTo(emailMessageDTO.getTo());
        message.setSubject(emailMessageDTO.getSubject());
        message.setText(emailMessageDTO.getMessage());

        javaMailSender.send(message);
    }
}