package project.payload;

import project.model.user.User;

import java.time.LocalDate;

public class UserProfile {
    private String email;
    private String name;
    private LocalDate birth;
    private String taxPayer;
    private String phone;

    public UserProfile(User user) {
        this.email = user.getEmail();
        this.name = user.getName();
        this.birth = user.getBirthDate();
        this.taxPayer = user.getTaxPayerId();
        this.phone = user.getPhone();
    }

    public UserProfile() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirth() {
        return birth;
    }

    public void setBirth(LocalDate birth) {
        this.birth = birth;
    }

    public String getTaxPayer() {
        return taxPayer;
    }

    public void setTaxPayer(String taxPayer) {
        this.taxPayer = taxPayer;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
