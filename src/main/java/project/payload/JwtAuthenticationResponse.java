package project.payload;

public class JwtAuthenticationResponse {
    private String accessToken;
    private String tokenType = "Bearer";
    private boolean needActivation;
    private boolean needToChangePassword;

        public JwtAuthenticationResponse(String accessToken) {
        this.accessToken = accessToken;
    }

    public JwtAuthenticationResponse(String accessToken, boolean needActivation, boolean needToChangePassword) {
        this.accessToken = accessToken;
        this.needActivation = needActivation;
        this.needToChangePassword = needToChangePassword;

    }

    public JwtAuthenticationResponse() {
    }

    public boolean isNeedToChangePassword() {
        return needToChangePassword;
    }

    public void setNeedToChangePassword(boolean needToChangePassword) {
        this.needToChangePassword = needToChangePassword;
    }

    public boolean isNeedActivation() {
        return needActivation;
    }

    public void setNeedActivation(boolean needActivation) {
        this.needActivation = needActivation;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }
}
