package project.controllers.rest;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import project.dto.rest.PasswordRestDTO;
import project.dto.rest.ProfileRestDTO;
import project.dto.rest.UserRestDTO;
import project.model.UserService;
import project.model.user.User;
import project.payload.ApiAuthResponse;
import project.security.CurrentUser;
import project.security.UserPrincipal;

@RestController
public class UserDataControllerREST {

    UserService userService;
    
    @Autowired
    PasswordEncoder passwordEncoder;

    public UserDataControllerREST(UserService userService) {
        this.userService = userService;
    }

    /**
     * Put method to change user password.
     *
     * @param email             user email
     * @param passwordRestInDTO password DTO
     * @return ResponseEntity with code = BAD_REQUEST if passwordRestDTO is invalid. Code = CREATED if input is valid
     * and the method has no errors.
     */
    @PutMapping(value = "/users/{userId}/password")//DODO Check Permissions
    public HttpEntity<String> setPassword(@PathVariable("userId") String email, @RequestBody PasswordRestDTO passwordRestInDTO) {

        PasswordRestDTO passwordRestOutDTO = userService.setPassword(email, passwordRestInDTO);
        String message = "New password submitted";

        if (passwordRestOutDTO == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(message, HttpStatus.ACCEPTED);
    }

    /**
     * Put Request Method to edit personal data of an user
     *
     * @param userId    - email
     * @param userInDTO - userDTO
     * @return ResponseEntity with status code=ACCEPTED if information inputed is valid, code=NOT_FOUND if userId is invalid
     * and code=BAD_REQUEST if information inputed is invalid
     */

    @PutMapping(value = "/users/{userId}")//DODO Check Permissions
    public HttpEntity<UserRestDTO> editUserData(@PathVariable("userId") String userId, @RequestBody UserRestDTO userInDTO) {
        if (userService.searchUserDTOByEmail(userId) == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        if (!setUserProfile(userId, userInDTO.getProfile()))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        UserRestDTO userOutDTO = userService.searchUserDTOByEmail(userId);
        return new ResponseEntity<>(userOutDTO, HttpStatus.ACCEPTED);
    }

    @PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    public boolean setUserProfile(@PathVariable("userId") String userId, @RequestBody String profile) {
        if (profile == null)
            return true;

        switch (profile.toUpperCase()) {

            case "COLLABORATOR":
                userService.setUserProfileCollaborator(userService.searchUserByEmail(userId));
                return true;

            case "DIRECTOR":
                userService.setUserProfileDirector(userService.searchUserByEmail(userId));
                return true;

            default:
                return false;
        }
    }

    /**
     * Auxiliary Method to set profile to an user. Return true to a valid profile name or null profiles. In this case,
     * a null profile value means a non called request to change profile in Put Rest Controller editUserData.
     *
     * @param userEmail
     * @param profileDTO
     * @return true if profile name is valid or null. Return false if profile is unknown.
     */
    @PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    @PutMapping(value = "/users/{userEmail}/profile")
    public boolean setUserProfileRest(@PathVariable("userEmail") String userEmail, @RequestBody ProfileRestDTO profileDTO) {
        String profile;
        profile = profileDTO.getProfile();
        if (profile == null)
            return true;

        switch (profile.toUpperCase()) {

            case "ADMINISTRATOR":
                return userService.setUserProfileAdministrator(userService.searchUserByEmail(userEmail));

            case "DIRECTOR":
                return userService.setUserProfileDirectorRest(userService.searchUserByEmail(userEmail));

            case "COLLABORATOR":
                return userService.setUserProfileCollaboratorRest(userService.searchUserByEmail(userEmail));

            case "REGISTEREDUSER":
                return userService.setUserProfileRegisteredUserRest(userService.searchUserByEmail(userEmail));

            default:
                return false;
        }
    }

    @PutMapping(value = "/users/changePassword")
    public HttpEntity setPasswordRest(@CurrentUser UserPrincipal currentUser, @RequestBody PasswordRestDTO passwordRestInDTO) {
    		
    		
        if(!passwordEncoder.matches(passwordRestInDTO.getOldPassword(), currentUser.getPassword())) {
        		return new ResponseEntity(new ApiAuthResponse(false, ""), HttpStatus.BAD_REQUEST);
        }
        
        User user = userService.searchUserByEmail(currentUser.getEmail());
        user.setPassword(passwordEncoder.encode(passwordRestInDTO.getNewPassword()));
        user.getUserLoginData().setLastLoginDate(LocalDateTime.now());
        userService.updateUser(user);
        
        return new ResponseEntity(new ApiAuthResponse(true, ""), HttpStatus.ACCEPTED);
    }
}
