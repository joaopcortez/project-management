package project.controllers.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import project.dto.rest.AvailableCostOptionsDTO;
import project.model.project.ProjectService;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class ChooseCostCalculationRESTController {

    private ProjectService projectService;

    public ChooseCostCalculationRESTController(ProjectService projectService) {
        this.projectService = projectService;
    }

    /**
     * Requests projectService to list all Project's cost calculation options.
     * @param projectId
     * @return
     */
    @PreAuthorize("isProjectManager(#projectId)")
    @GetMapping("/projects/{projectId}/costcalculation")
    public ResponseEntity<AvailableCostOptionsDTO> listProjectCostCalculationOptions(@PathVariable("projectId") String projectId) {

        AvailableCostOptionsDTO availableCostOptions = projectService.listProjectCostCalculationOptionsToDTO(projectId);

        availableCostOptions.add(linkTo(methodOn(ChooseCostCalculationRESTController.class).listProjectCostCalculationOptions(projectId)).withSelfRel());
        return new ResponseEntity<>(availableCostOptions, HttpStatus.OK);
    }

    /**
     * Requests projectService to set a project's cost calculation method.
     * @param projectId
     * @param choice    Cost calculation choice.
     */
    @PreAuthorize("isProjectManager(#projectId)")
    @PutMapping("/projects/{projectId}/costcalculation")
    public ResponseEntity<AvailableCostOptionsDTO> setProjectCostCalculationMechanism(@PathVariable("projectId") String projectId,@RequestBody AvailableCostOptionsDTO choice) {

        AvailableCostOptionsDTO availableCostOptions = projectService.setProjectCostCalculationMechanismToDTO(projectId, choice.getCalculationOption());

        availableCostOptions.add(linkTo(methodOn(ChooseCostCalculationRESTController.class).setProjectCostCalculationMechanism(projectId, choice)).withSelfRel());
        return new ResponseEntity<>(availableCostOptions, HttpStatus.OK);


    }
}
