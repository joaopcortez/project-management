package project.controllers.rest;

import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import project.dto.rest.UserRegistryRestDTO;
import project.dto.rest.UserRestDTO;
import project.model.UserService;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class UserRegistryRESTController {

    UserService userService;
    UserRegistryRestDTO userOutDTO;

    public UserRegistryRESTController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Auxiliary method that adds HATEOAS self links to userDTO elements of a given list
     *
     * @param userList
     * @return list with self links added.
     */
    public static List<UserRestDTO> addSelfLinkToEachElement(List<UserRestDTO> userList) {

        for (UserRestDTO userDTO : userList) {
            userDTO.add(linkTo(methodOn(UserRegistryRESTController.class).listUsersByEmail(userDTO.getEmail()))
                    .withSelfRel());
        }
        return userList;
    }

    public static List<UserRestDTO> addProfileActionsLinkToEachElement(List<UserRestDTO> userList) {
        for (UserRestDTO userDTO : userList) {
            String link = "/users/" + userDTO.getEmail() + "/profile";

            Set<Link> profileLinks = new LinkedHashSet<>();
            Link linkAdministrator = new Link(link, "ADMINISTRATOR");
            Link linkDirector = new Link(link, "DIRECTOR");
            Link linkCollaborator = new Link(link, "COLLABORATOR");
            Link linkRegisterdedUser = new Link(link, "REGISTEREDUSER");

            profileLinks.add(linkAdministrator);
            profileLinks.add(linkDirector);
            profileLinks.add(linkCollaborator);
            profileLinks.add(linkRegisterdedUser);

            switch (userDTO.getProfile()) {
                case "[ROLE_ADMINISTRATOR]":
                    profileLinks.remove(linkAdministrator);
                    break;
                case "[ROLE_DIRECTOR]":
                    profileLinks.remove(linkDirector);
                    break;
                case "[ROLE_COLLABORATOR]":
                    profileLinks.remove(linkCollaborator);
                    break;
                case "[ROLE_REGISTEREDUSER]":
                    profileLinks.remove(linkRegisterdedUser);
                    break;
                default:
                    break;
            }

            userDTO.add(profileLinks);
        }

        return userList;
    }

    /**
     * Method that lists all of the company's users (responds to US130).
     *
     * @return List with all users.
     */
    @GetMapping("/users")
    @PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<List<UserRestDTO>> listAllUsers() {

        List<UserRestDTO> userList = userService.listAllUsersToUI();
        addSelfLinkToEachElement(userList);
        addProfileActionsLinkToEachElement(userList);
        return new ResponseEntity<>(userList, HttpStatus.OK);
    }

    /**
     * Method that finds and lists users according to their profile. (Responds to
     * US136)
     *
     * @param userProfile Parameter to be searched for.
     * @return List of users with the predetermined profile.
     */
    @GetMapping(value = "/users", params = "profile")
    public ResponseEntity<List<UserRestDTO>> listUsersByProfile
    (@RequestParam(value = "profile", required = true) String userProfile) {

        RoleName profile = Enum.valueOf(RoleName.class, userProfile.toUpperCase());
        List<UserRestDTO> userList = userService.searchUserDTOByProfile(new Role(profile));

        addSelfLinkToEachElement(userList);
        return new ResponseEntity<>(userList, HttpStatus.OK);
    }

    /**
     * Method that finds users according to their email. (Responds to US135)
     *
     * @param email Parameter to be searched for.
     * @return User with the predetermined email.
     */
    @PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    @GetMapping(value = "/users", params = "email")
    public ResponseEntity<UserRestDTO> listUsersByEmail(@RequestParam(value = "email", required = true) String
                                                                email) {

        UserRestDTO userDTO = userService.searchUserDTOByEmail(email);
        if (userDTO == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        userDTO.add(linkTo(methodOn(UserRegistryRESTController.class).listUsersByEmail(email)).withSelfRel());
        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    /**
     * Validates and adds a new user to userRegistry
     *
     * @param userInDTO userDTO.
     * @return Successful message if user was added, failure message otherwise.
     */
    @PostMapping(value = "/users")
    public HttpEntity<UserRegistryRestDTO> addUser(@RequestBody UserRegistryRestDTO userInDTO) {

        userOutDTO = userService.addUserDTO(userInDTO);
        if (userOutDTO == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        userOutDTO.add(linkTo(methodOn(UserRegistryRESTController.class).addUser(userInDTO)).withSelfRel());
        return new ResponseEntity<>(userInDTO, HttpStatus.CREATED);
    }
}
