package project.controllers.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import project.dto.rest.TaskRestDTO;
import project.services.TaskProjectService;


@RestController
public class TaskStateRestController {

    TaskProjectService taskProjectService;

    public TaskStateRestController(TaskProjectService taskProjectService) {
        this.taskProjectService = taskProjectService;
    }

    /**
     * Set task cancelled.
     *
     * @param taskId id
     * @return a string saying if a task was set as cancelled.
     */
    @PreAuthorize("isTaskBelongingToProjectWhereProjectManager(#taskId)")
    @PutMapping(value = "/tasks/{id}/canceled")
    public ResponseEntity<TaskRestDTO> setTaskCanceled(@PathVariable("id") String taskId) {
        TaskRestDTO taskRestDTO = new TaskRestDTO();

        if (taskProjectService.setTaskCanceles(taskId)) {
            taskRestDTO.setMessage("Canceled" + taskId);
            return new ResponseEntity<>(taskRestDTO, HttpStatus.OK);
        } else {
            taskRestDTO.setMessage("Not Canceled" + taskId);
            return new ResponseEntity<>(taskRestDTO, HttpStatus.BAD_REQUEST);
        }
    }

}
