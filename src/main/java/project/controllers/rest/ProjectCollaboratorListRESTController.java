package project.controllers.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import project.dto.rest.ProjectCollaboratorRestDTO;
import project.dto.rest.UserRestDTO;
import project.model.UserService;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.services.ProjectCollaboratorService;

@RestController
public class ProjectCollaboratorListRESTController {

	ProjectCollaboratorService projectCollaboratorService;
	 ProjectService projectService;
	    UserService userService;

	    public ProjectCollaboratorListRESTController(ProjectCollaboratorService projectCollaboratorService, UserService userService, ProjectService projectService) {
	        this.projectCollaboratorService=projectCollaboratorService;
	        this.userService=userService;
	        this.projectService=projectService;
	       
	    }
	    
	    
	    @PreAuthorize("hasRole('ROLE_COLLABORATOR') || hasRole('ROLE_DIRECTOR')")
	    @GetMapping("/projects/{projectId}/collaborators")
	      public HttpEntity<List<ProjectCollaboratorRestDTO>> listProjectCollaborators(@PathVariable("projectId") String projectId) {
            
	    	List<ProjectCollaboratorRestDTO> collabList  =new ArrayList<>();
	    	for (ProjectCollaborator pc: projectCollaboratorService.listProjectCollaboratorsByProject(projectId)) {
	    		ProjectCollaboratorRestDTO collabDTO= new ProjectCollaboratorRestDTO();
	    		collabDTO.setUserId(pc.getUser().getEmail());
	    		collabDTO.setProjectId(projectId);
	    		collabDTO.setCost(pc.getCurrentCost());
	    		collabDTO.setUserName(pc.getUser().getName());
	    		collabDTO.setPhone(pc.getUser().getPhone());
	    		
	    		collabList.add(collabDTO);
	    		
	    	}
	        
	    	return new ResponseEntity<>(collabList, HttpStatus.OK);
	       
	    }
	    
	    
	    @PreAuthorize("isProjectManager(#projectId)")
	    @GetMapping(value = "/projects/{projectId}/collaboratorsnotinproject")
	    public HttpEntity<List<UserRestDTO>>listCollaboratorsNotToProject(@PathVariable("projectId") String projectId) {
	    	
	    	List<UserRestDTO> listUsersNotInProjectDto = new ArrayList<>();
	    	
	    	List<User> listUserNotInProject = userService.searchUserByProfile(new Role(RoleName.ROLE_COLLABORATOR));
	        List<User> listUsersInProject = projectCollaboratorService.listActiveUsersByProject(projectId);

	        listUserNotInProject.removeAll(listUsersInProject);
	        
	        for (User u : listUserNotInProject) {
	        	if(u.isActive()) {
	        	listUsersNotInProjectDto.add(u.toDTO());
	        	}
	        }
	        
	        return new ResponseEntity<>(listUsersNotInProjectDto, HttpStatus.OK);
	    }
	
	    
	    @PreAuthorize("isProjectManager(#projectId)")
	    @GetMapping("/projects/{projectId}/unassignedcollaborators")
	      public HttpEntity<List<ProjectCollaboratorRestDTO>> listUnassignedProjectCollaborators(@PathVariable("projectId") String projectId) {
            
	    	List<ProjectCollaboratorRestDTO> collabList =new ArrayList<>();
	    	for (ProjectCollaborator pc: projectService.listUnassignedProjectCollaborators(projectId)) {
	    		ProjectCollaboratorRestDTO collabDTO= new ProjectCollaboratorRestDTO();
	    		collabDTO.setUserId(pc.getUser().getEmail());
	    		collabDTO.setProjectId(projectId);
	    		collabDTO.setCost(pc.getCurrentCost());
	    		collabDTO.setUserName(pc.getUser().getName());
	    		collabDTO.setPhone(pc.getUser().getPhone());
	    		
	    		collabList.add(collabDTO);
	    		
	    	}
	        
	    	return new ResponseEntity<>(collabList, HttpStatus.OK);
	       
	    }
}
