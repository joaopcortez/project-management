package project.controllers.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import project.dto.rest.CreateReportRestDTO;
import project.dto.rest.EditReportQuantityDTO;
import project.dto.rest.ReportRestDTO;
import project.services.TaskProjectCollaboratorService;

import java.util.List;

@RestController
public class ReportRESTController {

    private TaskProjectCollaboratorService taskProjectCollaboratorService;

    public ReportRESTController(TaskProjectCollaboratorService taskProjectCollaboratorService) {
        this.taskProjectCollaboratorService = taskProjectCollaboratorService;
    }

    /**
     * Method to list all reports related to certain task.
     *
     * @param taskId
     * @return a list of reports.
     */
    @GetMapping(value = "/tasks/{taskId}/reports")
    public ResponseEntity<List<ReportRestDTO>>listTaskReportsDTO(@PathVariable String taskId) {
        return new ResponseEntity<>(taskProjectCollaboratorService.listTaskReportsDTO(taskId),  HttpStatus.OK);
    }
    


    /**
     * Method to create a report.
     *
     * @param taskId
     * @param reportRestDTO
     * @return a message saying if a report was or not added.
     */
    @PreAuthorize("hasRole('ROLE_COLLABORATOR')")
    @PostMapping(value = "/tasks/{taskId}/reports2")
    public ResponseEntity<CreateReportRestDTO> createReport2(@PathVariable String taskId, @RequestBody CreateReportRestDTO reportRestDTO) {

        if (taskProjectCollaboratorService.addReportRest2(reportRestDTO)) {
            return new ResponseEntity<>(reportRestDTO, HttpStatus.OK);

        }
        return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);

    }


    /**
     * Method to update a report.
     *
     * @param reportId
     * @param taskId
     * @param editReportQuantityDTO
     * @return a message saying if a report was or not updated.
     */
    @PreAuthorize("isTaskCollaborator(#taskId)")
    @PutMapping(value = "/tasks/{taskId}/reports2/{reportId}")
    public ResponseEntity<EditReportQuantityDTO> updateReport2(@PathVariable String reportId,
                               @PathVariable String taskId, @RequestBody EditReportQuantityDTO editReportQuantityDTO) {

        if (editReportQuantityDTO.getQuantity() < 0) {
            return  new ResponseEntity<>(editReportQuantityDTO, HttpStatus.NOT_ACCEPTABLE);
        }
        if (taskProjectCollaboratorService.us208EditReport2(editReportQuantityDTO)) {
            editReportQuantityDTO.setQuantity(editReportQuantityDTO.getQuantity());
            return new ResponseEntity<>(editReportQuantityDTO, HttpStatus.OK);
        }

        return  new ResponseEntity<>(editReportQuantityDTO, HttpStatus.NOT_ACCEPTABLE);
    }
}
