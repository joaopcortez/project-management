package project.controllers.rest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import project.dto.rest.TaskProjectCollaboratorRestDTO;
import project.services.TaskProjectCollaboratorService;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class AddOrRemoveTaskFromCollaboratorRESTController {

    TaskProjectCollaboratorService taskProjectCollaboratorService;

    public AddOrRemoveTaskFromCollaboratorRESTController(TaskProjectCollaboratorService taskProjectCollaboratorService) {
        this.taskProjectCollaboratorService = taskProjectCollaboratorService;
    }

    /**
     * Add task to a collaborator.
     *
     * @param taskProjectCollaboratorRestDTO
     * @param taskId
     * @return a string saying if collaborator was added or not.
     */
    @PreAuthorize("isTaskBelongingToProjectWhereProjectManager(#taskId)")
    @PostMapping("/tasks/{id}")
    public HttpEntity<TaskProjectCollaboratorRestDTO> addTaskToCollaborator(@RequestBody TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO, @PathVariable("id") String taskId) {

        taskProjectCollaboratorRestDTO.setTaskId(taskId);

        if (taskProjectCollaboratorService.addProjectCollaboratorToTask(taskProjectCollaboratorRestDTO)) {
            return new ResponseEntity<>(taskProjectCollaboratorRestDTO, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * Remove task from collaborator.
     *
     * @param taskProjectCollaboratorRestDTO
     * @param taskId
     * @return a string saying if task was remove or not.
     */
    @PreAuthorize("isTaskBelongingToProjectWhereProjectManager(#taskId)")
    @PutMapping(value = "/tasks/{taskId}")
    public ResponseEntity<TaskProjectCollaboratorRestDTO> removeTaskFromCollaborator(@RequestBody TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO, @PathVariable("taskId") String taskId) {

        TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTOOut = taskProjectCollaboratorService.removeProjectCollaboratorFromTaskRest(taskProjectCollaboratorRestDTO, taskId);
        if (taskProjectCollaboratorRestDTOOut==null || taskProjectCollaboratorRestDTOOut.getTaskId() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        taskProjectCollaboratorRestDTOOut.add(linkTo(methodOn(AddOrRemoveTaskFromCollaboratorRESTController.class).removeTaskFromCollaborator(taskProjectCollaboratorRestDTOOut, taskId)).withSelfRel());
        return new ResponseEntity<>(taskProjectCollaboratorRestDTOOut, HttpStatus.CREATED);
    }
}
