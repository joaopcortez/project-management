package project.controllers.rest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import project.dto.rest.ProjectCostOptionsDTO;
import project.dto.rest.SetProjectManagerDTO;
import project.model.project.ProjectService;
import project.services.ProjectUserService;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class EditProjectControllerREST {

    ProjectUserService projectUserService;
    ProjectService projectService;

    public EditProjectControllerREST(ProjectUserService projectUserService, ProjectService projectService) {
        this.projectUserService = projectUserService;
        this.projectService = projectService;
    }

    /**
     * PUT method to change project manager.
     *
     * @param projectId              project ID
     * @param setProjectManagerInDTO setProjectManagerDTO with filled info about new project manager.
     * @return ResponseEntity with code = BAD_REQUEST if passwordRestDTO is invalid. Code = CREATED if input is valid
     * and the method has no errors.
     */
    @PreAuthorize("hasRole('ROLE_DIRECTOR')")
    @PutMapping(value = "/projects/{projectId}/project-manager")
    public HttpEntity<SetProjectManagerDTO> setProjectManager(@PathVariable("projectId") String projectId, @RequestBody SetProjectManagerDTO setProjectManagerInDTO) {

        setProjectManagerInDTO.setProjectId(projectId);
        SetProjectManagerDTO setProjectManagerOutDTO = projectUserService.setProjectManager(setProjectManagerInDTO);

        if (setProjectManagerOutDTO == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(setProjectManagerOutDTO, HttpStatus.ACCEPTED);
    }

    /**
     * Put Request Method to edit list of cost options to the project
     *
     * @param projectId                  - String
     * @param setProjectCostOptionsInDTO - DTO
     * @return ResponseEntity with status code=ACCEPTED if options inputed are valid, code=NOT_FOUND if projectId is invalid
     * and code=BAD_REQUEST if DTO is null
     */
    @PreAuthorize("hasRole('ROLE_DIRECTOR')")
    @PutMapping(value = "/projects/{projectId}/costOptions")
    public HttpEntity<ProjectCostOptionsDTO> setCostOptions(@PathVariable("projectId") String projectId, @RequestBody ProjectCostOptionsDTO setProjectCostOptionsInDTO) {

        if (projectService.getProjectDTOByID(projectId) == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        setProjectCostOptionsInDTO.setProjectId(projectId);
        ProjectCostOptionsDTO setProjectCostOptionsOutDTO = projectService.setListOfCostCalculationOptions(setProjectCostOptionsInDTO);

        if (setProjectCostOptionsOutDTO == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(setProjectCostOptionsOutDTO, HttpStatus.ACCEPTED);
    }

    /**
     * Get Request Method to list cost options of project
     *
     * @param projectId - String
     * @return ResponseEntity status code=OK if projectId exists, code=NOT_FOUND if projectId is invalid
     */
    @GetMapping(value = "/projects/{projectId}/costOptions")//DODO Check Permissions
    public HttpEntity<ProjectCostOptionsDTO> listAvailableCostOptions(@PathVariable("projectId") String projectId) {

        if (projectService.getProjectDTOByID(projectId) == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        ProjectCostOptionsDTO setProjectCostOptionsOutDTO = new ProjectCostOptionsDTO();
        setProjectCostOptionsOutDTO.setProjectId(projectId);
        setProjectCostOptionsOutDTO.setCostCalculationOptions(projectService.getProjectByID(projectId).getCostCalculationOptions());

        setProjectCostOptionsOutDTO.add(linkTo(methodOn(EditProjectControllerREST.class).listAvailableCostOptions(projectId))
                .withSelfRel());

        return new ResponseEntity<>(setProjectCostOptionsOutDTO, HttpStatus.OK);
    }
}

