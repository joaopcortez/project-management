package project.controllers.authcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import project.jparepositories.UserRepository;
import project.model.user.User;
import project.model.user.UserIdVO;
import project.payload.UserIdentityAvailability;
import project.payload.UserProfile;
import project.security.CurrentUser;
import project.security.UserPrincipal;
import system.dto.LoginTokenDTO;

@RestController
@RequestMapping("/api")
public class UserController {

    java.util.logging.Logger logger = java.util.logging.Logger.getAnonymousLogger();
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/user/me")
    public LoginTokenDTO getCurrentUser(@CurrentUser UserPrincipal currentUser) {
        return new LoginTokenDTO(currentUser.getEmail(), currentUser.getName(), currentUser.getAuthorities().toString());
    }

    @GetMapping(value = "/user/checkEmailAvailability", params = "email")
    public UserIdentityAvailability checkEmailAvailability(@RequestParam(value = "email") String email) {
        Boolean isAvailable = !userRepository.existsByUserIdVO(new UserIdVO(email));
        return new UserIdentityAvailability(isAvailable);
    }

    @GetMapping("/users/{email}")
    public UserProfile getUserProfile(@PathVariable(value = "email") String email) {

        User user;
        user = userRepository.getOneByUserIdVO(new UserIdVO(email));

        return new UserProfile(user);
    }
}