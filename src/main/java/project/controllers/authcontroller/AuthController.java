package project.controllers.authcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import project.exception.AppException;
import project.jparepositories.RoleRepository;
import project.jparepositories.UserRepository;
import project.model.user.User;
import project.model.user.UserIdVO;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.payload.ApiAuthResponse;
import project.payload.JwtAuthenticationResponse;
import project.payload.LoginRequest;
import project.payload.SignUpRequest;
import project.security.CurrentUser;
import project.security.JwtTokenProvider;
import project.security.UserPrincipal;
import project.services.EmailServiceImpl;

import javax.mail.internet.AddressException;
import javax.validation.Valid;
import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * AuthController contains APIs for login and signup.
 */
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    UserController userController;
    UserPrincipal userPrincipal;
    @Autowired
    JwtTokenProvider tokenProvider;
    Logger logger = Logger.getAnonymousLogger();
    @Autowired
    private EmailServiceImpl emailService;

    @PostMapping("/signin")
    public ResponseEntity authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsernameOrEmail(), loginRequest.getPassword()));

        User user = userRepository.getOneByUserIdVO(UserIdVO.create(loginRequest.getUsernameOrEmail()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);

        if (user.getUserLoginData().getApprovedDateTime() != null && user.getUserLoginData().getLastLoginDate() == null) {
            return new ResponseEntity(new JwtAuthenticationResponse(jwt, false, true), HttpStatus.ACCEPTED);
        } else if (user.getUserLoginData().getApprovedDateTime() == null && user.getUserLoginData().getLastLoginDate() == null) {
            return new ResponseEntity(new JwtAuthenticationResponse(jwt, true, false), HttpStatus.ACCEPTED);

        } else {
            user.getUserLoginData().setLastLoginDate(LocalDateTime.now());
            userRepository.save(user);
            return new ResponseEntity(new JwtAuthenticationResponse(jwt, false, false), HttpStatus.OK);
        }
    }

    @PostMapping("/activate")
    public ResponseEntity activateUser(@RequestBody String confirmationCode, @CurrentUser UserPrincipal userPrincipal) {

        User user = userRepository.getOneByUserIdVO(UserIdVO.create(userPrincipal.getEmail()));

        if (user.getUserLoginData().getConfirmationToken().equals(confirmationCode)) {
            user.setActive();
            user.getUserLoginData().setApprovedDateTime(LocalDateTime.now());
            user.getUserLoginData().setLastLoginDate(LocalDateTime.now());
            userRepository.save(user);
            return new ResponseEntity(new ApiAuthResponse(true, "Account verified!"), HttpStatus.OK);

        } else {
            return new ResponseEntity(new ApiAuthResponse(false, "Wrong confirmation code!"), HttpStatus.UNAUTHORIZED);

        }
    }

    @PostMapping("/signup")
    public ResponseEntity<ApiAuthResponse> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) throws AddressException {

        if (userRepository.existsByUserIdVO(new UserIdVO(signUpRequest.getEmail()))) {
            return new ResponseEntity<>(new ApiAuthResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate birth = LocalDate.parse(signUpRequest.getBirthDate(), formatter);
        // Creating user's account
        User user = new User(signUpRequest.getName(), signUpRequest.getPhone(), signUpRequest.getEmail(),
                signUpRequest.getTaxPayerId(), birth, signUpRequest.getAddressId(), signUpRequest.getStreet(),
                signUpRequest.getPostalCode(), signUpRequest.getCity(), signUpRequest.getCountry());
        user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));

        Role userRole;
        try {
            userRole = roleRepository.getOneByName(RoleName.ROLE_REGISTEREDUSER);
        } catch (Exception e) {
            logger.log(Level.INFO, "an exception was thrown", e);

            throw new AppException("User Role not set.");
        }

        user.setRoles(Collections.singleton(userRole));

        String text = "Your confirmation code is: " + user.getUserLoginData().getConfirmationToken();
        emailService.sendSimpleMessage(user.getEmail(), "Confirmation Code", text);

        User result = userRepository.save(user);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/users/{email}")
                .buildAndExpand(result.getName()).toUri();

        return ResponseEntity.created(location).body(new ApiAuthResponse(true, "User registered successfully"));
    }

    @GetMapping("/activate/resendmail")
    public ResponseEntity<ApiAuthResponse> resendEmail(@CurrentUser UserPrincipal userPrincipal) {
        User user = userRepository.getOneByUserIdVO(UserIdVO.create(userPrincipal.getEmail()));
        String text = "Your confirmation code is: " + user.getUserLoginData().getConfirmationToken();
        emailService.sendSimpleMessage(user.getEmail(), "Confirmation Code", text);
        return ResponseEntity.ok(new ApiAuthResponse(true, "Confirmation code resent to your email."));
    }
}
