package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;
import project.model.UserService;
import project.model.user.User;
import system.dto.LoginTokenDTO;

import java.time.LocalDateTime;

@Component
public class AccountController {

    UserService userService;

    public AccountController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Handles user login into the web app.
     * @param email    Required login parameter.
     * @param password Required login parameter.
     * @return String with user email and user profile, if successful, or an error
     * message, if unsuccessful.
     */
    public LoginTokenDTO login(String email, String password) {

        return userService.validateData(email, password);

    }
    
    
    /**
     * Method to approve account - set user active and fill approved date time
     * @param email
     */
    public void approveAccount(String email)
    {
    	User user = userService.searchUserByEmail(email);
        user.setActive();
        user.getUserLoginData().setApprovedDateTime(LocalDateTime.now());

        userService.updateUser(user);
    }
    
    /**
     * method to update Last Login Date
     * @param email
     */
    public void updateLastLoginDate(String email)
    {
    	User user = userService.searchUserByEmail(email);
        user.setActive();
        user.getUserLoginData().setLastLoginDate(LocalDateTime.now());

        userService.updateUser(user);
    }
}
