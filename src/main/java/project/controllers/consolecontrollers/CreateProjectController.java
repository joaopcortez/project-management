package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.services.ProjectCollaboratorService;
import project.model.user.User;

@Component
public class CreateProjectController {

    UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projCollabRegistry;

    /**
     * Constructor for CreateProjectController.
     *
     * @param userService
     * @param projectService
     * @param projCollabRegistry
     */
    public CreateProjectController(UserService userService, ProjectService projectService, ProjectCollaboratorService
            projCollabRegistry) {
        this.userService = userService;
        this.projectService = projectService;
        this.projCollabRegistry = projCollabRegistry;
    }

    /**
     * Add a new project based on his token, id and name, to project authcontroller.
     *
     * @param userEmail
     * @param projectID
     * @param name
     * @return
     */
    public String addProject(String projectID, String name, String userEmail) {

        if (projectService.addProject(projectID, name) && setProjectManager(userEmail, projectID)) {
            return "The project " + projectID + " was created and the project manager is " + userEmail;
        }
        return "\nProject was not created because one project already exists with the same id "
                + "or project manager's email isn't valid";
    }

    /**
     * Set a project manager by user email and projectID.
     *
     * @param userEmail
     * @param projectID
     */
    private boolean setProjectManager(String userEmail, String projectID) {
        User user = userService.searchUserByEmail(userEmail);
        Project proj = projectService.getProjectByID(projectID);
        if (user != null) {
            projCollabRegistry.setProjectManager(proj, user);
            return projectService.updateProject(proj);
        }
        return false;
    }
}