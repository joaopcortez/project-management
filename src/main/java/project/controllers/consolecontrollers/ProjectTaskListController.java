package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.services.TaskService;

import java.util.*;

@Component
public class ProjectTaskListController {

    ProjectService projectService;
    TaskService taskService;

    public ProjectTaskListController(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    /**
     * @param projectId
     * @param list
     * @param taskListOf
     * @return task information.
     */
    private static StringBuilder taskInformation(String projectId, Set<Task> list, String taskListOf) {

        SortedSet<Task> taskSorted = new TreeSet<>(Comparator.comparing(Task::getTitle));
        taskSorted.addAll(list);
        String separatorEditTasksMenu = "_____________________________________________________________________";
        String fourLinesFeed = "\n\n\n\n";
        StringBuilder sb = new StringBuilder();
        sb.append(fourLinesFeed);
        sb.append(separatorEditTasksMenu);
        sb.append("\n\n").append(taskListOf).append(projectId).append("\n").append(separatorEditTasksMenu).append("\n\n");
        for (Task t : taskSorted) {
            sb.append(t.toString());
        }
        sb.append(separatorEditTasksMenu);
        return sb;
    }

    /**
     * @param projectId
     * @param list
     * @param taskListOf
     * @return full information about certain task.
     */
    private static StringBuilder taskInformationFull(String projectId, Set<Task> list, String taskListOf) {
        StringBuilder sb = new StringBuilder();

        SortedSet<Task> taskSorted = new TreeSet<>(Comparator.comparing(Task::getId));
        taskSorted.addAll(list);
        String separatorEditTasksMenuFullInfo = "______________________________________________________________________________________________________";
        String fourLinesFeed = "\n\n\n\n";
        sb.append(fourLinesFeed);
        sb.append(separatorEditTasksMenuFullInfo);
        sb.append("\n\n").append(taskListOf).append(projectId).append("\n").append(separatorEditTasksMenuFullInfo).append("\n\n");
        for (Task t : taskSorted) {
            sb.append("Task ID: ").append(t.getId()).append("\t");
            sb.append("Task Status: ").append(t.getStatus()).append("\n");
            sb.append("\t\tTask Title: ").append(t.getTitle()).append("\t");
            sb.append("Task Description: ").append(t.getDescription()).append("\n");
            sb.append("\t\tTask Date of Creation: ").append(t.getDateOfCreation()).append("\n");
            sb.append("\t\tTask Predicted Start Date: ").append(t.getPredictedDateOfStart()).append("\n");
            sb.append("\t\tTask Predicted End Date: ").append(t.getPredictedDateOfConclusion()).append("\n");
            sb.append("\t\tTask Effective Start Date: ").append(t.getEffectiveStartDate()).append("\n");
            sb.append("\t\tTask Effective End Date: ").append(t.getEfectiveDateOfConclusion()).append("\n");
            sb.append("\t\tActive Collaborators in Task\n");
            for (ProjectCollaborator pj : t.listActiveCollaborators()) {

                sb.append("\t\t\tROLE_COLLABORATOR Name: ").append(pj.getUser().getName()).append(", ");
                sb.append("ROLE_COLLABORATOR Email: ").append(pj.getUser().getEmail()).append(", ");
                sb.append("ROLE_COLLABORATOR Cost: ").append(pj.getCurrentCost()).append(" Euros\n");
            }
            sb.append("\t\tDependency tasks\n");
            for (Task t2 : t.getTaskDependencies()) {

                sb.append("\t\t\tTask ID: ").append(t2.getId()).append("\t");
                sb.append("\t\t\tTask Title: ").append(t2.getTitle()).append("\t");
                sb.append("\t\tTask Status: ").append(t2.getStatus()).append("\n");
                sb.append("\t\tTask Predicted End Date: ").append(t2.getPredictedDateOfConclusion()).append("\n");
                sb.append("\t\tTask Effective End Date: ").append(t2.getEfectiveDateOfConclusion()).append("\n");
            }
            sb.append("\t\tUnit Cost: ").append(t.getUnitCost()).append("Euros \t");
            sb.append("Estimated effort: ").append(t.getUnitCost()).append("\n\n");
        }
        sb.append(separatorEditTasksMenuFullInfo);
        return sb;
    }

    /**
     * Get tasks without collaborator in certain project.
     * @param projectId
     * @return tasks without collaborator.
     */
    public String lisProjectManagerTasksNoCollaborator(String projectId) {
        Project project1 = projectService.getProjectByID(projectId);
        Set<Task> list = project1.listUnassignedTasks();
        String taskListOf = "     LIST OF TASKS WITHOUT ASSIGNED COLLABORATORS FOR PROJECT ";
        StringBuilder sb = taskInformation(projectId, list, taskListOf);
        return sb.toString();
    }

    /**
     * Get tasks Cancelled list in certain project.
     * @param projectId
     * @return project tasks cancelled list.
     */
    public StringBuilder listProjectManagerCancelledTasks(String projectId) {
        Project project1 = projectService.getProjectByID(projectId);
        Set<Task> list = project1.listCancelledTasks();
        String taskListOf = "                LIST OF CANCELLED TASKS FOR PROJECT ";
        return taskInformation(projectId, list, taskListOf);
    }

    /**
     * Get completed tasks in certain project.
     * @param projectId
     * @return completed tasks.
     */
    public StringBuilder listProjectManagerCompletedTasks(String projectId) {
        Project project1 = projectService.getProjectByID(projectId);
        Set<Task> list = project1.listCompletedTasks();

        String taskListOf = "               LIST OF COMPLETED TASKS FOR PROJECT ";

        return taskInformation(projectId, list, taskListOf);
    }

    /**
     * Get tasks initiated but not completed in certain project.
     * @param projectId
     * @return initiated but not completed tasks.
     */
    public StringBuilder listProjectManagerGetInitiatedButNotCompletedTasks(String projectId) {
        Project project1 = projectService.getProjectByID(projectId);
        Set<Task> list = project1.listInitiatedButNotCompletedTasks();

        String taskListOf = "          LIST OF TASKS THAT ARE IN PROGRESS FOR PROJECT ";
        return taskInformation(projectId, list, taskListOf);
    }
    /**
     * List not initiated tasks
     * @param projectId id
     * @return list of not initiated tasks
     */
    public StringBuilder listNotInitiatedTask(String projectId) {
        Project project1 = projectService.getProjectByID(projectId);
        Set<Task> list = project1.listNotInitiatedTasks();
        String taskListOf = "          LIST OF NOT INITIATED TASKS FOR PROJECT ";
        return taskInformation(projectId, list, taskListOf);
    }

    /**
     * US345 Removes not initiated task
     * @param projectId id
     * @param taskId    id
     * @return String with a success or fail message
     */
    public String removeNotInitiatedTask(String taskId, String projectId) {
        Project project1 = projectService.getProjectByID(projectId);
        Set<Task> list = project1.listNotInitiatedTasks();
        Task task = project1.findTaskByID(taskId);

        if (list.contains(task)) {
        	taskService.removeTask(task);
            project1.removeTask(task);
            return "TASK WITH ID " + taskId + " SUCCESSFULLY REMOVED FROM PROJECT " + projectId + ".";
        }
        return "TASK WAS NOT REMOVED.";
    }

    /**
     * US367 Revert Complete task to In Progress state
     * @param projectId id
     * @param taskId    id
     * @return String with a success or fail message
     */
    public String changeCompletedTaskToSuspendedState(String taskId, String projectId) {
        Project project1 = projectService.getProjectByID(projectId);
        project1.findTaskByID(taskId).changeStateTo();

        if (project1.findTaskByID(taskId).getTaskState().isOnSuspendedState()) {
            return "TASK WITH ID " + taskId + " , IN PROJECT " + projectId
                    + " WAS SUCCESSFULLY REVERTED TO SUSPENDED STATE.";
        }
        return "TASK WAS UNABLE TO REVERT TO SUSPENDED STATE.";
    }

    /**
     * Get tasks not initiated in certain project.
     * @param projectId
     * @return not initiated tasks.
     */
    public StringBuilder listProjectManagerGetNotInitiatedTasks(String projectId) {
        Project project1 = projectService.getProjectByID(projectId);
        Set<Task> list = project1.listUninitializedTasks();
        String taskListOf = "       LIST OF NOT INITIATED TASKS NOT COMPLETED FOR PROJECT ";
        return taskInformation(projectId, list, taskListOf);
    }

    /**
     * Get tasks not initiated in certain project.
     * @param projectId
     * @return not initiated tasks.
     */
    public StringBuilder listProjectManagerNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks(
            String projectId) {
        Project project1 = projectService.getProjectByID(projectId);

        Set<Task> list = project1.listNotCompletedTasksAndWithPredictedDateOfConclusionExpired();

        String taskListOf = "LIST OF TASKS NOT COMPLETED BUT WITH CONCLUSION EXPIRED FOR PROJECT ";
        return taskInformation(projectId, list, taskListOf);
    }

    /**
     * Get tasks list in certain project.
     * @param projectId
     * @return project tasks list.
     */
    public StringBuilder listProjectTasksList(String projectId) {
        Project project1 = projectService.getProjectByID(projectId);
        List<Task> list = project1.getTasksList();

        Set<Task> projectTasksList = new LinkedHashSet<>(list);

        String taskListOf = "                LIST TASKS FOR PROJECT ";
        return taskInformation(projectId, projectTasksList, taskListOf);
    }

    /**
     * Get tasks list in certain project.
     * @param projectId
     * @return project tasks list.
     */
    public StringBuilder listProjectTasksListFull(String projectId) {
        Project project1 = projectService.getProjectByID(projectId);
        List<Task> list = project1.getTasksList();

        Set<Task> projectTasksList = new LinkedHashSet<>(list);
        String taskListOf = "                               TASK LIST WITH FULL INFORMATION FOR PROJECT ";
        return taskInformationFull(projectId, projectTasksList, taskListOf);
    }

    /**
     * List project manager removal requests.
     * @param projectId
     * @return a string showing removal requests.
     */
    public String listProjectManagerRemovalRequests(String projectId) {
        Project project1 = projectService.getProjectByID(projectId);
        List<TaskCollaboratorRegistry> tasksCollReg = project1.listRemovalRequests();
        Set<Task> taskList = new LinkedHashSet<>(project1.getTasksList());
        int idRequest = 0;
        StringBuilder sb = new StringBuilder();
        for (TaskCollaboratorRegistry tcr : tasksCollReg) {
            for (Task t : taskList) {
                if (t.listAllTaskCollaboratorRegistry().contains(tcr)) {
                    idRequest++;
                    sb.append("Removal Request ID: ").append(idRequest).append("\n");
                    sb.append("Task id: ");
                    sb.append(t.getId()).append("\n");
                    sb.append("Task tittle: ").append(t.getTitle()).append("\n");
                    sb.append("ROLE_COLLABORATOR: ").append(tcr.getProjectCollaborator()).append("\n");
                }
            }
        }
        return sb.toString();
    }
}
