package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;
import project.model.UserService;
import project.model.user.User;

@Component
public class UserActivationController {

    UserService userService;

    public UserActivationController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Set user as active.
     * @param userEmail
     * @return String with a success or fail message.
     */
    public String reactivateUser(String userEmail) {

        String message;

        User user = userService.searchUserByEmail(userEmail);

        if (!userService.listAllUsers().contains(user))
            message = "User activation has failed because there is not registration in the company's user registry.";
        else if (user.isActive())
            message = "User is already active.";
        else {
            user.setActive();
            message = "User was sucessfully activated.";
            userService.updateUser(user);
        }
        return message;
    }

    /**
     * Set user as inactive.
     * @param userEmail
     * @return String with a success or fail message.
     */
    public String inactivateUser(String userEmail) {

        String message;

        User user = userService.searchUserByEmail(userEmail);

        if (!userService.listAllUsers().contains(user))
            message = "User activation has failed because there is not registration in the company's user registry.";
        else if (!user.isActive())
            message = "User is already inactive.";
        else {
            user.setInactive();
            userService.updateUser(user);
            message = "User was sucessfully inactivated.";
        }
        return message;
    }
}
