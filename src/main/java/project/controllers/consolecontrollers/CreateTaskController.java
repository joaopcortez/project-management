package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.services.TaskService;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Component
public class CreateTaskController {

    UserService userService;
    ProjectService projectService;
    TaskService taskService;
    String message = "\nTASK SUCESSFULLY CREATED!";

    public CreateTaskController(UserService userService, ProjectService projectService, TaskService taskService) {
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    /**
     * Create a task related to project start date.
     * @param projectId
     * @param tittle
     * @param description
     * @param daysAfterProjectStart
     * @param daysTaskDuration
     * @param unitCost
     * @param estimatedEffort
     * @return a string showing information about the task created (like days after
     * project start or the numbers of days of the task).
     */
    public String createTaskWithDateRelatedToProjectStartDate(String projectId, String tittle, String description,
                                                              int daysAfterProjectStart, int daysTaskDuration, double
                                                                      unitCost, double estimatedEffort) {

        Project project = projectService.getProjectByID(projectId);

        if (!project.getTasksList().contains(project.findTaskByTitle(tittle)))
            taskService.addTask(project, tittle, description, daysAfterProjectStart, daysTaskDuration, unitCost, estimatedEffort);
        return message + "\nThe task will start " + daysAfterProjectStart
                + " days after the project start date (" + project.getStartDate().truncatedTo(ChronoUnit.DAYS)
                + ") and will have the duration of " + daysTaskDuration + " days.\n\n";
    }

    /**
     * Create a task based on project ID and title only.
     * @param projectId
     * @param tittle
     * @return a String saying that the task was added.
     */
    public String createTaskWithOutDates(String projectId, String tittle) {

        Project project = projectService.getProjectByID(projectId);

        if (!project.getTasksList().contains(project.findTaskByTitle(tittle)))
            taskService.addTask(project, tittle);
        return message + "\n\n";
    }

    /**
     * Create a task based on information like its project ID, title, description,
     * date of start and conclusion, unit cost and estimated effort.
     * @param projectId
     * @param tittle
     * @param description
     * @param predictedDateOfStart
     * @param predictedDateOfConclusion
     * @param unitCost
     * @param estimatedEffort
     * @return a String saying that the task was added.
     */
    public String createTaskWithDateEffectiveDate(String projectId, String tittle, String description,
                                                  LocalDateTime predictedDateOfStart, LocalDateTime predictedDateOfConclusion,
                                                  double unitCost, double estimatedEffort) {

        Project project = projectService.getProjectByID(projectId);

        if (!project.getTasksList().contains(project.findTaskByTitle(tittle)))
            taskService.addTask(project, tittle, description, predictedDateOfStart, predictedDateOfConclusion, unitCost,
                    estimatedEffort);

        return message + "\n\n";
    }
}
