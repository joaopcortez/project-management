package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;
import project.model.UserService;
import project.model.user.Address;
import project.model.user.User;

@Component
public class UserAddAddressController {

    UserService userService;

    public UserAddAddressController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Adds a new address to a user.
     * @param email      Identifies the user.
     * @param id         Id of new address.
     * @param street     New address's street.
     * @param postalCode New address's postal code.
     * @param city       New address's city.
     * @param country    New address's country.
     * @return Message with adding's success or failure.
     */
    public String addAdress(String email, String id, String street, String postalCode, String city, String country) {

        User user = userService.searchUserByEmail(email);

        if (user == null)
            return "No address added. Try again.";

        Address newAddress = user.createAddress(id, street, postalCode, city, country);
        String result = (user.addAddress(newAddress) ? "ADDRESS SUCESSFULLY ADDED." : "NO ADDRESS ADDED. TRY AGAIN.");
        userService.updateUser(user);
        return result;
    }
}
