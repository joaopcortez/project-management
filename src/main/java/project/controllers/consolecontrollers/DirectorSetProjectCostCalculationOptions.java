package project.controllers.consolecontrollers;

import java.util.List;

import org.springframework.stereotype.Component;

import project.model.project.ProjectService;

@Component
public class DirectorSetProjectCostCalculationOptions {

	ProjectService projectService;
	
	public DirectorSetProjectCostCalculationOptions(ProjectService projectService) {
		this.projectService = projectService;	
	}
	
	/**
	 * Lists all classes that implement the CostCalculation interface (representing all project's options for calculating its costs).
	 * @return list of string containing the simple name of all those classes.
	 */
	public List<String> listAllCostCalculationOptions() {
		
		return projectService.listAllCostCalculationOptions();
	}
	
	/**
	 * Establishes the available options to calculate the cost of a project thus far, namely to resolve the ambiguities 
	 * from a user submitting a report which time span encompasses two or more costs.
	 * @param projectId ID of project.
	 * @param costCalculationOptions List of chosen available options to calculate cost of project.
	 */
	public void setProjectListOfCostCalculationOptions(String projectId, List<String> costCalculationOptions) {
		
		projectService.setListOfCostCalculationOptions(projectId, costCalculationOptions);
	}

	/**
	 * Lists a project's cost calculation options.
	 * @param projectId
	 * @return
	 */
	public List<String> listProjectCalculationOptions(String projectId) {
		
		return projectService.listProjectCostCalculationOptions(projectId);
	}
}
