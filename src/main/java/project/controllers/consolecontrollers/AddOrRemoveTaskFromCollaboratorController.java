package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.model.task.Task;
import project.services.TaskService;
import project.model.user.User;

import java.util.ArrayList;
import java.util.List;

@Component
public class AddOrRemoveTaskFromCollaboratorController {

    UserService userService;
    ProjectService projectService;
    TaskService taskService;
    ProjectCollaboratorService projectCollaboratorService;
    List<ProjectCollaborator> collaboratorsNotInTask = new ArrayList<>();
    List<ProjectCollaborator> collaboratorsInTask = new ArrayList<>();
    List<ProjectCollaborator> collaboratorsInProject = new ArrayList<>();

    public AddOrRemoveTaskFromCollaboratorController(UserService userService, ProjectService projectService, TaskService taskService, ProjectCollaboratorService projectCollaboratorService) {
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
        this.projectCollaboratorService = projectCollaboratorService;
    }

    /**
     * Add task to a collaborator.
     *
     * @param projectId
     * @param taskId
     * @param email
     * @return a string saying if collaborator was added or not.
     */
    public String addTaskToCollaborator(String projectId, String taskId, String email) {

        String message = "Can not add Colaborator to this Task";
        if (email == null) {
            return message;
        }
        User user = userService.searchUserByEmail(email);
        Task task = taskService.findTaskByID(taskId);
        ProjectCollaborator projCollab = projectCollaboratorService.getProjectCollaborator(projectId, email);

        if ((user != null) && (task != null) && (taskService.addProjectCollaboratorToTask(projCollab, task))) {
            taskService.updateTask(task);
            return "ROLE_COLLABORATOR added to Task " + taskId;
        }
        return message;
    }

    /**
     * Remove task from a collaborator.
     *
     * @param projectId
     * @param taskId
     * @param email
     * @return a string saying if collaborator was removed or not.
     */
    public String removeTasktoCollaborator(String projectId, String taskId, String email) {

        String message = "Can not remove Colaborator to this Task";
        if (email == null) {
            return message;
        }

        User user = userService.searchUserByEmail(email);
        Task task = taskService.findTaskByID(taskId);
        ProjectCollaborator projCollab = projectCollaboratorService.getProjectCollaborator(projectId, email);

        if ((user != null) && (task != null) && (task.removeProjectCollaborator(projCollab))) {
            taskService.updateTask(task);
            return "ROLE_COLLABORATOR was removed to Task " + taskId;
        }

        return message;
    }

    /**
     * List collaborators who are not in the task.
     *
     * @param projectId
     * @param taskId
     * @return a String with collaborators who are not in the task in case of sucess
     * or a message of insucess.
     */
    public String listCollaboratorsNotInTask(String projectId, String taskId) {

        Project project1 = projectService.getProjectByID(projectId);
        Task task = taskService.findTaskByID(taskId);

        collaboratorsInProject = projectCollaboratorService.listProjectCollaboratorsByProject(projectId);

        for (ProjectCollaborator col : collaboratorsInProject) {

            if (!task.hasProjectCollaborator(col) && !project1.getProjectManager().equals(col)) { //TOCHECK: Verificar se a verificação do project manager é necessária

                collaboratorsNotInTask.add(col);
            }
        }

        StringBuilder sb = new StringBuilder();

        if (collaboratorsNotInTask.isEmpty()) {

            return "There are no collaborators to add to this task";
        }

        for (ProjectCollaborator col : collaboratorsNotInTask) {

            sb.append("\nROLE_COLLABORATOR name ").append(col.getUser().getName());
            sb.append("\nROLE_COLLABORATOR email" + ": ").append(col.getUser().getEmail());
            sb.append("\nROLE_COLLABORATOR cost" + ": ").append(col.getCurrentCost()).append("\n");

        }
        return sb.toString();
    }

    /**
     * List collaborators who are not in the task.
     *
     * @param projectId
     * @param taskId
     * @return a String with collaborators who are not in the task in case of sucess
     * or a message of insucess.
     */
    public String listActiveCollaboratorsInTask(String projectId, String taskId) {

        Project project1 = projectService.getProjectByID(projectId);
        Task task = taskService.findTaskByID(taskId);

        collaboratorsInProject = projectCollaboratorService.listProjectCollaboratorsByProject(projectId);

        for (ProjectCollaborator col : collaboratorsInProject) {

            if (task.hasProjectCollaborator(col) && !project1.getProjectManager().equals(col)) {

                collaboratorsInTask.add(col);
            }
        }

        StringBuilder sb = new StringBuilder();

        if (collaboratorsInTask.isEmpty()) {

            return "There are no active colaborators associated to this task";
        }

        for (ProjectCollaborator col : collaboratorsInTask) {

            sb.append("\nROLE_COLLABORATOR name ").append(col.getUser().getName());
            sb.append("\nROLE_COLLABORATOR email" + ": ").append(col.getUser().getEmail());
            sb.append("\nROLE_COLLABORATOR cost" + ": ").append(col.getCurrentCost()).append("\n");

        }
        return sb.toString();
    }

}
