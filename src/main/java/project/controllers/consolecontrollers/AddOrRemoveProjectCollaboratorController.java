package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;

import java.util.List;

@Component
public class AddOrRemoveProjectCollaboratorController {

    UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;
    TaskService taskService;

    public AddOrRemoveProjectCollaboratorController(UserService userService, ProjectService projectService, ProjectCollaboratorService projectCollaboratorService,TaskService taskService) {
        this.userService = userService;
        this.projectService = projectService;
        this.projectCollaboratorService = projectCollaboratorService;
        this.taskService = taskService;
    }

    /**
     * Add a project collaborator.
     * @param projectId
     * @param email
     * @param cost
     * @return a string saying if collaborator was added to the project or not.
     */
    public String addProjectCollaborator(String projectId, String email, double cost) {
        User user = userService.searchUserByEmail(email);
        Project project = projectService.getProjectByID(projectId);
        ProjectCollaborator collab = projectCollaboratorService.getProjectCollaborator(projectId, email);

        if (user == null || project == null || ( collab != null && collab.isActiveInProject())) {
            return "\nCAN NOT ADD COLLABORATOR TO THIS PROJECT.";

        }
        projectCollaboratorService.addProjectCollaborator(project, user, cost);

        return "\nCOLLABORATOR ADDED TO PROJECT.";
    }

    /**
     * Remove a project collaborator.
     * @param projectId
     * @param email
     * @return a string saying if collaborator was removed from a project or not.
     */
    public String removeProjectCollaborator(String projectId, String email) {
        Project project = projectService.getProjectByID(projectId);

        for (ProjectCollaborator projectCollaborator : project.listActiveProjectCollaborators()) {
            if (email.equals(projectCollaborator.getUser().getEmail())) {
            	taskService.removeProjectCollaboratorFromAllTasksInProject(project, projectCollaborator);
                projectCollaboratorService.removeProjectCollaborator(project, email);
                projectService.updateProject(project);
                return "\nCOLLABORATOR REMOVED FROM PROJECT.";
            }
        }
        return "ROLE_COLLABORATOR not found";
    }

    /**
     * List all collaborators who are not in project.
     *
     * @param projectId
     * @return a String with collaborators info in case of success, or a message of
     * failure.
     */
    public String listCollaboratorsNotInProject(String projectId) {

        List<User> listUserNotInProject = userService.searchUserByProfile(new Role(RoleName.ROLE_COLLABORATOR));
        List<User> listUsersInProject = projectCollaboratorService.listActiveUsersByProject(projectId);

        listUserNotInProject.removeAll(listUsersInProject);

        if (listUserNotInProject.isEmpty()) {
            return "THERE ARE NO PROJECTS COLLABORATORS AVAILABLE TO ADD TO THIS PROJECT";
        }
        int count = 0;
        StringBuilder sb = new StringBuilder();
        for (User user : listUserNotInProject) {
            count++;
            sb.append("\n[").append(count).append("] ROLE_COLLABORATOR name:  ").append(user.getName());
            sb.append("\n    ROLE_COLLABORATOR email: ").append(user.getEmail()).append("\n");
        }
        return sb.toString();
    }
}
