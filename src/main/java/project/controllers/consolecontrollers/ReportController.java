package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Report;
import project.model.task.Task;
import project.model.user.User;
import project.services.TaskService;

import java.time.LocalDateTime;

@Component
public class ReportController {

    UserService userService;
    ProjectService projectService;
    TaskService taskService;

    public ReportController(UserService userService, ProjectService projectService, TaskService taskService) {
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    /**
     * Method to add a report.
     *
     * @param email
     * @param taskID
     * @param reportQuantity
     * @param reportStartDate
     * @param reportEndDate
     * @return a message according to success or insuccess of adding a report.
     */
    public String addReport(String email, String taskID, int reportQuantity, LocalDateTime reportStartDate,
                            LocalDateTime reportEndDate) {

        String statusMessage;
        ProjectCollaborator projCollab;
        User user = userService.searchUserByEmail(email);
        Task t = taskService.findTaskByID(taskID);

        if (t == null) {
            return "Invalid task ID";
        }

        projCollab = t.getProject().findProjectCollaborator(user);

        if (projCollab == null) {
            return "Invalid email.";
        }

        statusMessage = t.addReport(projCollab, reportQuantity, reportStartDate, reportEndDate);
        if ("Report successfully added".equals(statusMessage)) {
            taskService.updateTask(t);
        }
        return statusMessage;

    }

    /**
     * List all task reports made by this collaborator.
     *
     * @param email email and task id
     * @return String with all reports from a specific collaborator.
     */
    public String listAllReportsByThisCollaborator(String email, String taskID) {

        StringBuilder allReports = new StringBuilder();
        User user = userService.searchUserByEmail(email);
        Task t;

        for (Project proj : projectService.listUserProjects(user)) {
            t = proj.findTaskByID(taskID);
            if (t == null) {
                continue;
            }
            ProjectCollaborator projCollab = proj.findProjectCollaborator(user);
            allReports.append(t.listCollaboratorReports(projCollab));
        }
        return allReports.toString();
    }

    /**
     * Find a report by is ID.
     *
     * @param reportID
     * @param taskID
     * @param email
     * @return quantity.
     */
    public double findReportByID(String email, String taskID, String reportID) {

        User user = userService.searchUserByEmail(email);
        Task t;

        for (Project proj : projectService.listUserProjects(user)) {
            t = proj.findTaskByID(taskID);
            if (t == null) {
                continue;
            }
            Report report = t.findReportById(reportID);
            if (report != null) {
                return report.getQuantity();
            }
        }
        return -1;
    }

    /**
     * Edit a report.
     *
     * @return a string saying if report was edited.
     */
    public String us208EditReport(String email, String taskID, String reportID, double quantity) {

        User user = userService.searchUserByEmail(email);
        Task t;

        for (Project proj : projectService.listUserProjects(user)) {
            t = proj.findTaskByID(taskID);
            if (t == null) {
                continue;
            }
            Report report = t.findReportById(reportID);
            report.setQuantity(quantity);
            taskService.updateTask(t);
        }
        return "\nReport successfully edited.\n";
    }

    /**
     * Find how many reports does a collaborator in a task.
     *
     * @param email
     * @param taskID
     * @return quantity of reports.
     */
    public int findHowManyReportsDoesACollaboratorHasInTask(String email, String taskID) {

        User user = userService.searchUserByEmail(email);
        Task t;

        for (Project proj : projectService.listUserProjects(user)) {
            t = proj.findTaskByID(taskID);
            if (t == null) {
                continue;
            }
            ProjectCollaborator projCollab = proj.findProjectCollaborator(user);
            return t.listCollaboratorReports(projCollab).size();
        }
        return -1;
    }
}
