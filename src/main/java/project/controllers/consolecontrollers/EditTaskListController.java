package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.services.TaskService;
import project.model.user.User;

import java.util.Comparator;
import java.util.List;

@Component
public class EditTaskListController {

    UserService userService;
    ProjectService projectService;
    TaskService taskService;

    public EditTaskListController(UserService userService, ProjectService projectService, TaskService taskService) {
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    /**
     * US204 S4 Adds project collaborator to task
     * @param email
     * @param projID
     * @param taskID
     * @return true if collaborator is added to task false otherwise
     */
    public String requestAddProjectCollaboratorToTask(String email, String projID, String taskID) {

        User user = userService.searchUserByEmail(email);
        Project proj = projectService.getProjectByID(projID);
        Task task = taskService.findTaskByID(taskID);
        ProjectCollaborator projColab = proj.findProjectCollaborator(user);

        if (task == null)
            return "No task found.";
        return task.requestAddProjectCollaborator(projColab);
    }

    /**
     * US206 S0 Removes task from projectCollaborator
     * @param email
     * @param taskID
     * @return true if task is removed false otherwise
     */
    public String requestRemoveTaskFromProjectCollaborator(String email, String taskID) {
        User user = userService.searchUserByEmail(email);
        Task t;

        for (Project proj : projectService.listUserProjects(user)) {
            t = proj.findTaskByID(taskID);
            if (t == null)
                continue;
            if (t.requestRemoveProjectCollaborator(proj.findProjectCollaborator(user)))
                return "Request successfully sent.";
            else
                return "Request denied.";
        }
        return "Request denied.";
    }

    /**
     * US205 S0 Sets task completed
     * @param email
     * @param taskID
     * @return true if operation is successful false otherwise
     */
    public String setTaskCompleted(String email, String taskID) {

        List<Task> tasksThatChangedState;

        User user = userService.searchUserByEmail(email);
        Task t;

        for (Project proj : projectService.listUserProjects(user)) {
            t = proj.findTaskByID(taskID);
            if (t == null)
                continue;

            ProjectCollaborator projColab = proj.findProjectCollaborator(user);
            if (t.hasProjectCollaborator(projColab) && t.setTaskCompleted()) {
                taskService.updateTask(t);
                tasksThatChangedState = t.markAsCompletedByCollaborator(projColab);

                for (Task task : tasksThatChangedState) {
                    taskService.updateTask(task);
                }

                return "\nTask successfully marked as completed.";
            } else
                return "\nThis task is not assigned to this collaborator.";
        }
        return "\nTask not found!";
    }

    /**
     * Get a list with all user projects.
     * @param email
     * @return a list with all user projects.
     */
    public List<Project> getAllUserProjects(String email) {

        User user = userService.searchUserByEmail(email);
        return projectService.listUserProjects(user);
    }

    /**
     * Get all user projects.
     * @param email
     * @return A string showing all user projects.
     */
    public String listAllUserProjects(String email) {

        StringBuilder sb = new StringBuilder();

        for (Project project : getAllUserProjects(email)) {

            sb.append("\nProject ID: ").append(project.getId()).append("\n");
            sb.append("Project name: ").append(project.getName()).append("\n");
            sb.append("Project description: ").append(project.getDescription()).append("\n");
            sb.append("\n");
        }
        return sb.toString();
    }

    /**
     * Validate a project.
     * @param projectID
     * @param email
     * @return a string saying it it was validated or not.
     */
    public String validateProject(String projectID, String email) {

        for (Project project : getAllUserProjects(email)) {

            if (project.getId().equals(projectID)) {
                return projectID;
            }
        }
        return null;
    }

    /**
     * Get all project pendent tasks.
     * @param projID
     * @return a string showing all project pendent tasks.
     */
    public String listAllProjectPendingTasks(String projID) {

        Project proj = projectService.getProjectByID(projID);

        StringBuilder sb = new StringBuilder();

        List<Task> taskList = proj.getTasksList();

        taskList.sort(Comparator.comparing(Task::getTitle));

        for (Task task : taskList) {

            if (!task.getTaskState().isOnCompletedState() && !task.getTaskState().isOnCancelledState()) {
                sb.append("Task ID: ").append(task.getId()).append("\n");
                sb.append("Task name: ").append(task.getTitle()).append("\n");
                sb.append("Task status: ").append(task.getStatus()).append("\n");
            }
        }
        return sb.toString();
    }

    /**
     * Validate task by is ID.
     * @param projID
     * @param taskID
     * @return a string saying if it was validated or not.
     */
    public String validateTaskID(String projID, String taskID) {

        Project proj = projectService.getProjectByID(projID);

        for (Task task : proj.getTasksList()) {

            if (task.getId().equals(taskID)) {
                return taskID;
            }
        }
        return null;
    }
}
