package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;
import project.model.UserService;
import project.model.project.ProjectService;
import project.model.task.Task;
import project.services.TaskService;
import project.model.user.User;

import java.util.List;

@Component
public class CollaboratorTaskListController {

    UserService userService;
    ProjectService projectService;
    TaskService taskService;

    public CollaboratorTaskListController(UserService userService, ProjectService projectService, TaskService taskService) {
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    /**
     * Us203 S3 Get user pending tasks (find user by email)
     * @param email
     * @return users pending tasks.
     */
    public String getCollaboratorPendentTasks(String email) {
        User user1 = userService.searchUserByEmail(email);
        List<Task> list = projectService.listUserPendentTasksInAllProjects(user1);
        StringBuilder sb = new StringBuilder();
        for (Task t : list) {
            sb.append(t.toString());
        }

        if (sb.toString().isEmpty()) {
            return "YOU HAVE NO PENDENT TASKS.\n\n";
        }
        return sb.toString();
    }

    /**
     * US210 S0 Get user completed Tasks
     * @param email
     * @return user completed tasks
     */
    public String getCollaboratorCompletedTasks(String email) {
        User user1 = userService.searchUserByEmail(email);
        List<Task> list = projectService.listUserCompletedTasksInAllProjectsSortedByReverseOrder(user1);
        StringBuilder sb = new StringBuilder();
        for (Task t : list) {
            sb.append(t.toString());
        }

        if (sb.toString().isEmpty()) {
            return "YOU HAVE NO TASKS COMPLETED AT THIS MOMENT.\n\n";
        }
        return sb.toString();
    }

    /**
     * US211 S0 Get user completed Tasks completed last month
     * @param email
     * @return user completed Tasks completed last month
     */
    public String getCollaboratorConcludedTasksLastMonth(String email) {
        User user1 = userService.searchUserByEmail(email);
        List<Task> list = projectService.listUserCompletedTasksLastMonthInAllProjectsSortedByReverseOrder(user1);
        StringBuilder sb = new StringBuilder();
        for (Task t : list) {
            sb.append(t.toString());
        }
        if (sb.toString().isEmpty()) {
            return "YOU HAVE NO TASKS COMPLETED IN LAST 30 DAYS.\n\n";
        }
        return sb.toString();
    }

    /**
     * Get collaborators tasks in all projects.
     * @param email
     * @return a String with collaborators tasks.
     */
    public String getCollabaratorsTasksInAllProjects(String email) {

        User user1 = userService.searchUserByEmail(email);
        List<Task> list = projectService.listUserTasksInAllProjects(user1);

        StringBuilder sb = new StringBuilder();

        for (Task t : list) {
            sb.append(t.toString());
        }

        if (sb.toString().isEmpty()) {
            return "YOU HAVE NO TASKS\n\n";
        }
        return sb.toString();
    }
}
