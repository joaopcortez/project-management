package project.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.jparepositories.ProjectCollaboratorRepository;
import project.jparepositories.ProjectRepository;
import project.jparepositories.TaskRepository;
import project.jparepositories.UserRepository;
import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.user.User;
import project.model.user.UserIdVO;

import java.util.ArrayList;
import java.util.List;

@Service
public class SecurityRepositoryService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    ProjectCollaboratorRepository projectCollaboratorRepository;
    @Autowired
    TaskRepository taskRepository;

    public SecurityRepositoryService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public List<String> listProjectWhereProjectManager(String userEmail) {
        List<String> listProjectWhereProjectManager = new ArrayList<>();
        for (Project project : projectRepository.findAll()) {
            if (project.getProjectManager().getUser().getEmail().equals(userEmail)) {

                listProjectWhereProjectManager.add(project.getId());
            }
        }
        return listProjectWhereProjectManager;
    }
    
    public List<String> listTasksWhereIsTaskCollaborator(String userEmail) {
        List<String> listTasksWhereIsTaskCollaborator = new ArrayList<>();
        List<ProjectCollaborator> listUserProjectCollabIds = new ArrayList<>();

        for (ProjectCollaborator projectCollaborator : projectCollaboratorRepository.findAll()) {
            if (userEmail.equals(projectCollaborator.getUser().getEmail())) {
                listUserProjectCollabIds.add(projectCollaborator);
            }
        }
        for (Task task : taskRepository.findAll()) {
            for (ProjectCollaborator projectCollaboratortest : listUserProjectCollabIds)
                if (task.hasProjectCollaborator(projectCollaboratortest)) {
                    listTasksWhereIsTaskCollaborator.add(task.getId());
                }
        }
        return listTasksWhereIsTaskCollaborator;
    }

    public List<String> listTasksWhereIsProjectManager(String userEmail) {
    		List<String> listOfTasksWhereProjectManager = new ArrayList<>();
    		
    		for(String projectId: listProjectWhereProjectManager(userEmail)) {
    			Project project = projectRepository.getOne(projectId);
    			List<Task> projectTasks = project.getTasksList();
    			for(Task task: projectTasks) {
    				listOfTasksWhereProjectManager.add(task.getId());
    			}
    		}
    		return listOfTasksWhereProjectManager;
    }
    
    public User getOneByUserIdVO(String userEmail) {
        return userRepository.getOneByUserIdVO(UserIdVO.create(userEmail));
    }
}
