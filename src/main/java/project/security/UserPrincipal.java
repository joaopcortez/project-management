package project.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import project.model.user.User;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class UserPrincipal implements UserDetails {

    private static final long serialVersionUID = 1L;
    private List<String> listProjectWhereProjectManager;
    private List<String> listTasksWhereIsTaskCollaborator;
    private List<String> listTasksWhereIsProjectManager;
    private String name;
    @JsonIgnore
    private String email;
    @JsonIgnore
    private String password;
    private Collection<? extends GrantedAuthority> authorities;

    /**
     * Constructor for UserPrincipal.
     *
     * @param email
     * @param name
     * @param password
     * @param authorities
     */
    public UserPrincipal(String email, String name, String password, List<String> listProjectWhereProjectManager, List<String> listTasksWhereIsTaskCollaborator, List<String> listTasksWhereProjectManager, Collection<? extends GrantedAuthority> authorities) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.authorities = authorities;
        this.listProjectWhereProjectManager = listProjectWhereProjectManager;
        this.listTasksWhereIsTaskCollaborator = listTasksWhereIsTaskCollaborator;
        this.listTasksWhereIsProjectManager = listTasksWhereProjectManager;
    }

    /**
     * Method to create UserPrincipal based on certain User.
     *
     * @param user
     * @param listProjectWhereProjectManager
     * @return UserPrincipal.
     */
    public static UserPrincipal create(User user, List<String> listProjectWhereProjectManager, List<String> listTasksWhereIsTaskCollaborator, List<String> listTasksWhereIsProjectManager) {
        List<GrantedAuthority> authorities = user.getRoles().stream().map(role ->
                new SimpleGrantedAuthority(role.getName().name())
        ).collect(Collectors.toList());

        return new UserPrincipal(
                user.getEmail(),
                user.getName(),
                user.getPassword(),
                listProjectWhereProjectManager,
                listTasksWhereIsTaskCollaborator,
                listTasksWhereIsProjectManager,
                authorities
        );
    }

    public List<String> getListProjectWhereProjectManager() {
        return listProjectWhereProjectManager;
    }

    public List<String> getListTasksWhereIsTaskCollaborator() {
        return listTasksWhereIsTaskCollaborator;
    }

    public List<String> getListTasksWhereIsProjectManager() {
		return listTasksWhereIsProjectManager;
	}

	/**
     * Method to get email.
     *
     * @return email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Method to get name.
     *
     * @return name.
     */
    public String getName() {
        return name;
    }

    /**
     * Method to get password.
     *
     * @return password.
     */
    @Override
    public String getPassword() {
        return password;
    }

    /**
     * Method to get username.
     *
     * @return username.
     */
    @Override
    public String getUsername() {

        return email;
    }

    /**
     * Method to get authorities.
     *
     * @return authorities.
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    /**
     * Method to check if account is expired.
     *
     * @return true if not expired, false otherwise.
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Method to check if account is locked.
     *
     * @return true if not locked, false otherwise.
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * Method to check if credentials are expired.
     *
     * @return true if not expired, false otherwise.
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Method to check if UserPrincipal is enabled..
     *
     * @return true if it is enabled, false otherwise.
     */
    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserPrincipal that = (UserPrincipal) o;
        return Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {

        return Objects.hash(email);
    }
}