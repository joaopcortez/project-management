package project.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import project.model.user.User;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    SecurityRepositoryService securityRepositoryService;

    @Transactional
    public UserDetails loadUserByUsername(String userEmail) {
        // Let people login with email
        User user = securityRepositoryService.getOneByUserIdVO(userEmail);

        List<String> listProjectWhereProjectManager = securityRepositoryService.listProjectWhereProjectManager(userEmail);
        List<String> listTasksWhereIsTaskCollaborator = securityRepositoryService.listTasksWhereIsTaskCollaborator(userEmail);
        List<String> listTasksWhereIsProjectManager = securityRepositoryService.listTasksWhereIsProjectManager(userEmail);

        return UserPrincipal.create(user, listProjectWhereProjectManager, listTasksWhereIsTaskCollaborator, listTasksWhereIsProjectManager);
    }

}