package project.jparepositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;

@Repository
public interface ProjectCollaboratorRepository extends JpaRepository<ProjectCollaborator, Integer> {

    List<ProjectCollaborator> findByProject(Project project);

    ProjectCollaborator getOneById(Integer id);

}