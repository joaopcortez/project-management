package project.jparepositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import project.model.project.Project;

//This will be AUTO IMPLEMENTED by Spring into a Bean called projectRepository
//CRUD refers Create, Read, Update, Delete

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    Project getOneByCode(String id);
}