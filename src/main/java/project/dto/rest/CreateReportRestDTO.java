package project.dto.rest;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class CreateReportRestDTO {
	
	
	private double quantity;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime startDate;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime endDate;
	private int  projectCollaboratorID;
	private String  projectCollaboratorEmail;
	private String taskID;



	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public LocalDateTime getStartDate() {
		return startDate;
	}
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public LocalDateTime getEndDate() {
		return endDate;
	}
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}
	public int getProjectCollaboratorID() {
		return projectCollaboratorID;
	}
	public void setProjectCollaboratorID(int projectCollaboratorID) {
		this.projectCollaboratorID = projectCollaboratorID;
	}
	public String getTaskID() {
		return taskID;
	}
	public void setTaskID(String taskID) {
		this.taskID = taskID;
	}

	public String getProjectCollaboratorEmail() {
		return projectCollaboratorEmail;
	}

	public void setProjectCollaboratorEmail(String projectCollaboratorEmail) {
		this.projectCollaboratorEmail = projectCollaboratorEmail;
	}
}
