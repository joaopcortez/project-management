package project.dto.rest;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.hateoas.ResourceSupport;

import java.time.LocalDateTime;

public class ProjectDTO extends ResourceSupport {

    private String projectId;
    private String name;
    private String description;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime finalDate;
    private String userEmail;
    private String unit;
    private double globalBudget;
    private String costOptionSelected;
    private double currentCost;
    private boolean isActive = true;
    private int requests;
    private int completedTasks;
    private int notInitiatedTasks;
    private int initiatedButNotCompletedTasks;
    private int canceledTasks;
    private int tasks;
    private int collaborators;
    private int unassignedCollaborators;
    private String messageError;

    /**
     * Method to get message error
     *
     * @return message error
     */
    public String getMessageError() {
        return messageError;
    }

    /**
     * Method to set message error.
     *
     * @param messageError
     */
    public void setMessageError(String messageError) {
        this.messageError = messageError;
    }

    /**
     * Method to get user email.
     *
     * @return user email.
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     * Method to set user email.
     *
     * @param userEmail
     */
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    /**
     * Method to get project ID.
     *
     * @return project ID.
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * Method to set project ID.
     *
     * @param projectId
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    /**
     * Method to get description.
     *
     * @return description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Method to set description.
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Method to get name.
     *
     * @return name.
     */
    public String getName() {
        return name;
    }

    /**
     * Method to set name.
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method to get unit.
     *
     * @return unit chosen.
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Method to set unit.
     *
     * @param unit
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * Method to get global budget.
     *
     * @return global budget.
     */
    public double getGlobalBudget() {
        return globalBudget;
    }

    /**
     * Method to set global budget.
     *
     * @param globalBudget
     */
    public void setGlobalBudget(double globalBudget) {
        this.globalBudget = globalBudget;
    }
    
    public double getCurrentCost() {
		return currentCost;
	}

	public void setCurrentCost(double currentCost) {
		this.currentCost = currentCost;
	}

	/**
     * Method to get start date.
     *
     * @return start date.
     */
    public LocalDateTime getStartDate() {
        return startDate;
    }

    /**
     * Method to set start date.
     *
     * @param startDate
     */
    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    /**
     * Method to get final date.
     *
     * @return final date.
     */
    public LocalDateTime getFinalDate() {
        return finalDate;
    }

    /**
     * Method to set final date.
     *
     * @param finalDate
     */
    public void setFinalDate(LocalDateTime finalDate) {
        this.finalDate = finalDate;
    }

    /**
     * Method to check if project is active.
     *
     * @return true if is active, false otherwise.
     */
    public boolean isActive() {
        return isActive;
    }

    /**
     * Method to set project activity.
     *
     * @param isActive
     */
    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public int getRequests() {
		return requests;
	}

	public void setRequests(int requests) {
		this.requests = requests;
	}
	

	public int getCompletedTasks() {
		return completedTasks;
	}

	public void setCompletedTasks(int completedTasks) {
		this.completedTasks = completedTasks;
	}

	public int getNotInitiatedTasks() {
		return notInitiatedTasks;
	}

	public void setNotInitiatedTasks(int notInitiatedTasks) {
		this.notInitiatedTasks = notInitiatedTasks;
	}

	public int getInitiatedButNotCompletedTasks() {
		return initiatedButNotCompletedTasks;
	}

	public void setInitiatedButNotCompletedTasks(int initiatedButNotCompletedTasks) {
		this.initiatedButNotCompletedTasks = initiatedButNotCompletedTasks;
	}
	
	public int getCanceledTasks() {
		return canceledTasks;
	}

	public void setCanceledTasks(int canceledTasks) {
		this.canceledTasks = canceledTasks;
	}
	


	public int getUnassignedCollaborators() {
		return unassignedCollaborators;
	}

	public void setUnassignedCollaborators(int unassignedCollaborators) {
		this.unassignedCollaborators = unassignedCollaborators;
	}

	public int getCollaborators() {
		return collaborators;
	}

	public void setCollaborators(int collaborators) {
		this.collaborators = collaborators;
	}

	public int getTasks() {
		return tasks;
	}

	public void setTasks(int tasks) {
		this.tasks = tasks;
	}
	

	public String getCostOptionSelected() {
		return costOptionSelected;
	}

	public void setCostOptionSelected(String costOptionSelected) {
		this.costOptionSelected = costOptionSelected;
	}

	@Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProjectDTO{");
        sb.append("projectId='").append(projectId).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", startDate=").append(startDate);
        sb.append(", finalDate=").append(finalDate);
        sb.append(", userEmail='").append(userEmail).append('\'');
        sb.append(", unit='").append(unit).append('\'');
        sb.append(", globalBudget=").append(globalBudget);
        sb.append(", isActive=").append(isActive);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((projectId == null) ? 0 : projectId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ProjectDTO)) {
            return false;
        }
        ProjectDTO other = (ProjectDTO) obj;
        if (projectId == null) {
            if (other.projectId != null) {
                return false;
            }
        } else if (!projectId.equals(other.projectId)) {
            return false;
        }
        return true;
    }
}
