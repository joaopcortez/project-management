package project.dto.rest;

import java.time.LocalDate;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;

public class UserRestDTO extends ResourceSupport {

    private String name;
    private String email;
    private String phone;
    private String taxPayerId;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;
    private String profile;

    @JsonCreator
    public UserRestDTO() {
        // Do nothing because the constructor needs to be empty
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTaxPayerId() {
        return taxPayerId;
    }

    public void setTaxPayerId(String taxPayerId) {
        this.taxPayerId = taxPayerId;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		UserRestDTO other = (UserRestDTO) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}

    @Override
    public int hashCode() {
        return email.hashCode();
    }

   
    @Override
    public String toString() {
        return "UserEditDataRestDTO [name=" + name + ", email=" + email + ", phone=" + phone + ", taxPayerId="
                + taxPayerId + ", birthDate=" + birthDate + ", profile=" + profile + "]";
    }

}
