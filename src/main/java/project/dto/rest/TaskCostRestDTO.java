package project.dto.rest;

public class TaskCostRestDTO {
	
	private String taskId;
	private String title;
	private double cost;
	
	

	public TaskCostRestDTO() {
		super();
	}

	public String getTaskId() {
		return taskId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "TaskCostRestDTO{" +
				"taskId='" + taskId + '\'' +
				", title='" + title + '\'' +
				", cost=" + cost +
				'}';
	}
}
