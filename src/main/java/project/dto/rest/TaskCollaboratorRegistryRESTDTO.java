package project.dto.rest;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.hateoas.ResourceSupport;

import java.time.LocalDateTime;
import java.util.Objects;

public class TaskCollaboratorRegistryRESTDTO extends ResourceSupport {

    private String taskId;
    private int projectCollaboratorId;
    private String projectCollaboratorName;
    private String projectCollaboratorEmail;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime collaboratorAddedToTaskDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime collaboratorRemovedFromTaskDate;

    private String registryStatus;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public int getProjectCollaboratorId() {
        return projectCollaboratorId;
    }

    public void setProjectCollaboratorId(int projectCollaboratorId) {
        this.projectCollaboratorId = projectCollaboratorId;
    }

    public String getProjectCollaboratorName() {
        return projectCollaboratorName;
    }

    public void setProjectCollaboratorName(String projectCollaboratorName) {
        this.projectCollaboratorName = projectCollaboratorName;
    }

    public String getProjectCollaboratorEmail() {
        return projectCollaboratorEmail;
    }

    public void setProjectCollaboratorEmail(String projectCollaboratorEmail) {
        this.projectCollaboratorEmail = projectCollaboratorEmail;
    }

    public LocalDateTime getCollaboratorAddedToTaskDate() {
        return collaboratorAddedToTaskDate;
    }

    public void setCollaboratorAddedToTaskDate(LocalDateTime collaboratorAddedToTaskDate) {
        this.collaboratorAddedToTaskDate = collaboratorAddedToTaskDate;
    }

    public LocalDateTime getCollaboratorRemovedFromTaskDate() {
        return collaboratorRemovedFromTaskDate;
    }

    public void setCollaboratorRemovedFromTaskDate(LocalDateTime collaboratorRemovedFromTaskDate) {
        this.collaboratorRemovedFromTaskDate = collaboratorRemovedFromTaskDate;
    }

    public String getRegistryStatus() {
        return registryStatus;
    }

    public void setRegistryStatus(String registryStatus) {
        this.registryStatus = registryStatus;
    }

    @Override
    public String toString() {
        return "TaskCollaboratorRegistryRESTDTO [taskId=" + taskId + ", projectCollaboratorId=" + projectCollaboratorId
                + ", projectCollaboratorName=" + projectCollaboratorName + ", projectCollaboratorEmail="
                + projectCollaboratorEmail + ", collaboratorAddedToTaskDate=" + collaboratorAddedToTaskDate
                + ", collaboratorRemovedFromTaskDate=" + collaboratorRemovedFromTaskDate + ", registryStatus="
                + registryStatus + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TaskCollaboratorRegistryRESTDTO)) {
            return false;
        }
        TaskCollaboratorRegistryRESTDTO that = (TaskCollaboratorRegistryRESTDTO) o;
        return projectCollaboratorId == that.projectCollaboratorId;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), taskId, projectCollaboratorId);
    }
}
