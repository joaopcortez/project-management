package project.dto.rest;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.util.Objects;

public class ReportRestDTO {

	private String code;
	private String userEmail;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime startDate;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime endDate;
	private double quantity;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime reportCreationDate;

	public LocalDateTime getReportCreationDate() {
		return reportCreationDate;
	}

	public void setReportCreationDate(LocalDateTime reportCreationDate) {
		this.reportCreationDate = reportCreationDate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public double getEffort() {
		return quantity;
	}

	public void setEffort(double effort) {
		this.quantity = effort;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof ReportRestDTO)) {
			return false;
		}
		ReportRestDTO that = (ReportRestDTO) o;
		return Objects.equals(getCode(), that.getCode());
	}

	@Override
	public int hashCode() {

		return Objects.hash(getCode());
	}
}
