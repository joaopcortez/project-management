package project.dto.rest;

import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AvailableCostOptionsDTO extends ResourceSupport {

    private String calculationOption;
    private List<String> availableCalculationOptions = new ArrayList<>();

    public String getCalculationOption() {
        return calculationOption;
    }

    public void setCalculationOption(String calculationOption) {
        this.calculationOption = calculationOption;
    }

    public List<String> getAvailableCalculationOptions() {
        return availableCalculationOptions;
    }

    public void setAvailableCalculationOptions(List<String> availableCalculationOptions) {
        this.availableCalculationOptions = availableCalculationOptions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AvailableCostOptionsDTO)) {
            return false;
        }
        AvailableCostOptionsDTO that = (AvailableCostOptionsDTO) o;
        return Objects.equals(calculationOption, that.calculationOption) &&
                Objects.equals(availableCalculationOptions, that.availableCalculationOptions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), calculationOption);
    }
}
