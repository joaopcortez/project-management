package project.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class ProjectCollaboratorDTO {

    @XmlElement(name = "colaborador_id")
    private String id;

    @XmlElementWrapper(name = "lista_ligacoes_projeto")
    @XmlElement(name = "ligacao_projeto")
    private List<ProjectCollaboratorCostAndTimePeriodDTO> costAndTimePeriodList;

    public String getId() {
        return id;
    }

    public List<ProjectCollaboratorCostAndTimePeriodDTO> getCostAndTimePeriodList() {
        return costAndTimePeriodList;
    }

}
