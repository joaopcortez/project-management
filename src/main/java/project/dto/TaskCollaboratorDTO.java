package project.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TaskCollaboratorDTO {

    @XmlElement(name = "colaborador_id")
    private String id;

    //	@XmlElementWrapper(name = "lista_ligacoes_tarefa")
    @XmlElement(name = "lista_ligacoes_tarefa")
    private List<TaskCollaboratorRegistryDTO> taskCollaboratorRegistry;

    public String getId() {
        return id;
    }

    public List<TaskCollaboratorRegistryDTO> getTaskCollaboratorLink() {
        return taskCollaboratorRegistry;
    }
}
