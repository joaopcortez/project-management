package project.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "lista_projectos")
public class ProjectRegistryDTO {

    @XmlElement(name = "projeto")
    private List<ProjectDTO> listProjects;

    public List<ProjectDTO> getListProjects() {
        return listProjects;
    }

    public void setListProjects(List<ProjectDTO> listProjects) {
		this.listProjects = listProjects;
	}

	@Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[lista_projetos=");
        builder.append(listProjects);
        builder.append("]");
        return builder.toString();
    }
}
