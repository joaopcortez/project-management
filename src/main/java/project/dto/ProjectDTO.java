package project.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "projeto")
public class ProjectDTO {

    @XmlAttribute
    private String id;

    @XmlElement(name = "nome_projeto")
    private String name;

    @XmlElement(name = "descricao_projeto")
    private String description;

    @XmlElement(name = "data_inicio_projeto")
    private String startDate;

    @XmlElement(name = "data_conclusao_projeto")
    private String finalDate;

    @XmlElement(name = "unidade_esforco")
    private String unit;

    @XmlElement(name = "orcamento_projeto")
    private double globalBudget;

    @XmlElement(name = "estado_projeto")
    private String status;

    @XmlElement(name = "gestor_projeto")
    private String projectManager;

    @XmlElementWrapper(name = "lista_colaboradores")
    @XmlElement(name = "colaborador_projeto")
    private List<ProjectCollaboratorDTO> projectCollaboratorList;

    @XmlElementWrapper(name = "lista_tarefas")
    @XmlElement(name = "tarefa")
    private List<TaskDTO> tasks;
    
    @XmlElement(name = "calculo_custo")
    private String costCalculatorPersistence;
    
    @XmlElementWrapper(name = "lista_opcoes_calculo")
    @XmlElement(name = "opcao")
    private List<String> costCalculationOptions;
    

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getFinalDate() {
        return finalDate;
    }

    public String getUnit() {
        return unit;
    }

    public double getGlobalBudget() {
        return globalBudget;
    }

    public String getStatus() {
        return status;
    }

    public String getProjectManager() {
        return projectManager;
    }

    public List<ProjectCollaboratorDTO> getProjectCollaboratorList() {
        return projectCollaboratorList;
    }

    public List<TaskDTO> getTasks() {
        return tasks;
    }

	public String getCostCalculatorPersistence() {
		return costCalculatorPersistence;
	}

}
