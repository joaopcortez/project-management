package project.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class TaskDTO {

    @XmlElement(name = "nome_tarefa")
    private String title;

    @XmlElement(name = "descricao_tarefa")
    private String description;

    @XmlElement(name = "codigo_tarefa")
    private String taskId;

    @XmlElement(name = "estado_tarefa")
    private String state;

    @XmlElement(name = "esforco_estimado")
    private double estimatedEffort;

    @XmlElement(name = "custo_unitario_orcamentado")
    private double unitCost;

    @XmlElement(name = "data_inicio_prevista")
    private String predictedDateOfStart;

    @XmlElement(name = "data_conclusao_prevista")
    private String predictedDateOfConclusion;

    @XmlElement(name = "data_conclusao_efetiva")
    private String effectiveDateOfConclusion;

    @XmlElementWrapper(name = "lista_dependencias")
    @XmlElement(name = "tarefa_id")
    private List<String> taskDependencies;

    @XmlElementWrapper(name = "lista_colaboradores_tarefa")
    @XmlElement(name = "colaborador_tarefa")
    private List<TaskCollaboratorDTO> taskCollaborators;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getState() {
        return state;
    }

    public double getEstimatedEffort() {
        return estimatedEffort;
    }

    public double getUnitCost() {
        return unitCost;
    }

    public String getPredictedDateOfStart() {
        return predictedDateOfStart;
    }

    public String getPredictedDateOfConclusion() {
        return predictedDateOfConclusion;
    }

    public String getEffectiveDateOfConclusion() {
        return effectiveDateOfConclusion;
    }

    public List<String> getTaskDependencies() {
        return taskDependencies;
    }

    public List<TaskCollaboratorDTO> getTaskCollaborators() {
        return taskCollaborators;
    }
}
