package project.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "lista_utilizadores")
public class UserRegistryDTO {

    @XmlAttribute
    private String id;

    @XmlAttribute
    private String description;

    @XmlElement(name = "utilizador")
    private List<UserDTO> listUsers;

    public List<UserDTO> getListUsers() {
        return listUsers;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[lista_utilizadores=");
        builder.append(listUsers);
        builder.append("]");
        return builder.toString();
    }

}
