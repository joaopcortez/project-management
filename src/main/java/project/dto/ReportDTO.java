package project.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReportDTO {

    @XmlElement(name = "data_inicio")
    private String startDate;

    @XmlElement(name = "data_fim")
    private String endDate;

    @XmlElement(name = "esforco")
    private double effort;

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public double getEffort() {
        return effort;
    }
}
