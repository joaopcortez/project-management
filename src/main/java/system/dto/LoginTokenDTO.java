package system.dto;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Objects;

@Component
public class LoginTokenDTO extends ResourceSupport {

    private String name;
    private String email;
    private String profile;
    private String message;
    private int statusCode;

    private boolean isActive;
    private String confirmationCode;
    private LocalDateTime approvedDateTime;
    private LocalDateTime lastLoginDate;

    public LoginTokenDTO() {
    }

    /**
     * Constructor for LoginTokenDTO
     *
     * @param name
     * @param email
     * @param profile
     * @param isActive
     * @param approvedDateTime
     * @param lastLoginDate
     * @param confirmationCode
     * @param message
     */
    public LoginTokenDTO(String name, String email, String profile, boolean isActive, LocalDateTime approvedDateTime,
                         LocalDateTime lastLoginDate, String confirmationCode, String message) {
        this.name = name;
        this.email = email;
        this.profile = profile;
        this.message = message;
        this.isActive = isActive;
        this.approvedDateTime = approvedDateTime;
        this.lastLoginDate = lastLoginDate;
        this.confirmationCode = confirmationCode;
        this.statusCode = 0;

    }

    /**
     * Contructor for LoginTokenDTO.
     * @param email
     * @param name
     * @param profile
     */
    public LoginTokenDTO(String email, String name, String profile) {
        this.email = email;
        this.name = name;
        this.profile = profile;
    }

    /**
     * Constructor for LoginTokenDTO.
     *
     * @param message
     * @param statusCode
     */
    public LoginTokenDTO(String message, int statusCode) {
        this.message = message;
        this.statusCode = statusCode;

    }

    /**
     * Constructor for LoginTokenDTO.
     *
     * @param message
     */
    public LoginTokenDTO(String message) {
        this.message = message;

    }

    /**
     * Method to get status code.
     *
     * @return status code.
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Method to set status code.
     *
     * @param statusCode
     */
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Method to get message.
     *
     * @return message.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Method to set message.
     *
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Method to get name.
     *
     * @return name.
     */
    public String getName() {
        return name;
    }

    /**
     * Method to set name.
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method to get email.
     *
     * @return email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Method to set email.
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Method to get profile,
     *
     * @return profile.
     */
    public String getProfile() {
        return profile;
    }

    /**
     * Method to set profile.
     *
     * @param profile
     */
    public void setProfile(String profile) {
        this.profile = profile;
    }

    /**
     * Method to check if is active.
     *
     * @return true if it is active, false otherwise.
     */
    public boolean isActive() {
        return isActive;
    }

    /**
     * Method to set activity.
     *
     * @param isActive
     */
    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * Method to get approval date time.
     *
     * @return approval date time.
     */
    public LocalDateTime getApprovedDateTime() {
        return approvedDateTime;
    }

    /**
     * Method to set approval time.
     *
     * @param approvedDateTime
     */
    public void setApprovedDateTime(LocalDateTime approvedDateTime) {
        this.approvedDateTime = approvedDateTime;
    }

    /**
     * Method to get confirmation code.
     *
     * @return confirmation code.
     */
    public String getConfirmationCode() {
        return confirmationCode;
    }

    /**
     * Method to set confirmation code.
     *
     * @param confirmationCode
     */
    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    /**
     * Method to get last login date.
     *
     * @return last login date.
     */
    public LocalDateTime getLastLoginDate() {
        return lastLoginDate;
    }

    /**
     * Method to set last login date.
     *
     * @param lastLoginDate
     */
    public void setLastLoginDate(LocalDateTime lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LoginTokenDTO{");
        sb.append("name='").append(name).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", profile='").append(profile).append('\'');
        sb.append(", message='").append(message).append('\'');
        sb.append(", statusCode=").append(statusCode);
        sb.append(", isActive=").append(isActive);
        sb.append(", confirmationCode='").append(confirmationCode).append('\'');
        sb.append(", approvedDateTime=").append(approvedDateTime);
        sb.append(", lastLoginDate=").append(lastLoginDate);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LoginTokenDTO)) {
            return false;
        }
        LoginTokenDTO that = (LoginTokenDTO) o;
        return Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), email);
    }
}
