import React from 'react';

export function formatDate(dateString) {
    const date = new Date(dateString);

    const monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ];
  
    const monthIndex = date.getMonth();
    const year = date.getFullYear();
  
    return monthNames[monthIndex] + ' ' + year;
}
  
export function formatDateTime(dateTimeString) {
  const date = new Date(dateTimeString);

  const monthNames = [
    "Jan", "Feb", "Mar", "Apr",
    "May", "Jun", "Jul", "Aug", 
    "Sep", "Oct", "Nov", "Dec"
  ];

  const monthIndex = date.getMonth();
  const year = date.getFullYear();

  return date.getDate() + ' ' + monthNames[monthIndex] + ' ' + year + ' - ' + date.getHours() + ':' + date.getMinutes();
}  

export function getProfileName(role){
  switch(role) {
      case "[ROLE_COLLABORATOR]":
        return 'COLLABORATOR';
       
      case "[ROLE_ADMINISTRATOR]":
        return 'ADMINISTRATOR';
       
      case "[ROLE_DIRECTOR]":
        return 'DIRECTOR';
     
      case "[ROLE_REGISTEREDUSER]":
        return 'REGISTERED USER';
      }  
}

export function colorChangeState (param){

  if (param === "Completed") {
      return "#87d068";
  }
  if (param === "Cancelled") {
      return "#f5222d";
  }
  if (param === "Suspended") {
      return "#949a97";
  }
  if (param === "InProgress") {
      return "#2db7f5";
  }
  if (param === "ReadyToStart") {
      return "#3ba6a4";
  }
  if (param === "Assigned") {
      return "#76e9b9";
  }
  if (param === "Planned" || (param === "Created")) {
      return "#3b47a6";
  }

  else {
      return <div>param</div>;
  }
}