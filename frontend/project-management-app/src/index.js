import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware, createStore} from 'redux'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'
import reducers from './reducers/rootReducer'
import multi from 'redux-multi'


import './index.css';
import App from './app/App';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter} from 'react-router-dom';


const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();

let initState = {}

const persistedState = localStorage.getItem('reduxState')

if (persistedState) {
  initState = JSON.parse(persistedState)
}
const store = applyMiddleware(thunk,multi)(createStore)(reducers,initState,devTools);

store.subscribe(() => {
    localStorage.setItem('reduxState', JSON.stringify(store.getState()))
  })

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>, 
    document.getElementById('root')
);

registerServiceWorker();
