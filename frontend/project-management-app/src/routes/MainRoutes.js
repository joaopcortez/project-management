import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';

import Login from '../components/login';
import NotFound from '../components/common/NotFound';
import Signup from '../components/signup';
import Profile from '../components/profile';
import MainPage from '../mainapp/MainPage';
import Team from "../components/Team/Team";
export class MainRoutes extends Component {

    render() {
        return (


            <Switch>
                <Route exact path="/" render={(props) => <MainPage opt={0}/>}/>
                <Route path="/projects/collaborator" render={(props) => <MainPage opt={1}/>}/>
                <Route path="/projects/projectmanager" render={(props) => <MainPage opt={2}/>}/>
                <Route path="/projects/selected" render={(props) => <MainPage opt={13}/>}/>
                <Route path="/tasks/selected" render={(props) => <MainPage opt={14}/>}/>
                <Route path="/projects/director/create" render={(props) => <MainPage opt={12}/>}/>
                <Route path="/projects/director" render={(props) => <MainPage opt={11}/>}/>
                <Route path="/projects/populate" render={(props) => <MainPage opt={15}/>}/>
                <Route path="/tasks/lists" render={(props) => <MainPage opt={4}/>}/>
                <Route path="/tasks/requests" render={(props) => <MainPage opt={5}/>}/>
                <Route path="/reports/all" render={(props) => <MainPage opt={7}/>}/>
                <Route path="/search" render={(props) => <MainPage opt={9}/>}/>
                <Route path="/users/populate" render={(props) => <MainPage opt={15}/>}/>
                <Route path="/users/:email" component={Profile}/>
                <Route path="/users" render={(props) => <MainPage opt={10}/>}/>
                <Route path="/login" render={(props) => <Login/>}/>
                <Route path="/signup" component={Signup}/>
                <Route path="/team" component={Team}/>


                <Route path="/logout" component={MainPage}/>
                <Route component={NotFound}/>


            </Switch>
        );
    }
}


export default MainRoutes;


// const mapStateToProps = state => ({currentUser: state.authentication.currentUser, isAuthenticated: state.authentication.isAuthenticated })

// const mapDispatchToProps = dispatch => bindActionCreators({loadUser},dispatch)

// export default connect(mapStateToProps,mapDispatchToProps)(MainRoutes);