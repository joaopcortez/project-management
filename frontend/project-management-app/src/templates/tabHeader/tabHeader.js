import React from 'react'

import './tabHeader.css'

export default props => (
    
        <p className='tab-header'><strong>{props.title}</strong></p>
    
)