import React from 'react'
import {Button, Icon} from 'antd';

import './bottomButton.css'

const ButtonGroup = Button.Group;

export default props => (
    <section className='bot-buttons-group'>
        <Button type="primary" className="button-centered">
            <Icon type="left" />Go back
        </Button>
    </section>
    
)