import React from 'react'

import './contentHeader.css'

export default props => (
    <section className='content-header'>
        <h1><strong>{props.title}</strong>    <small>{props.subtitle}</small></h1>
    </section>
)