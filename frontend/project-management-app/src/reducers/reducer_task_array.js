const INITIAL_STATE = {
  tasks: [],
  collaboratorProjectTasks: [],
  dependenciesToAdd: [],
  taskDependencies: [],
  collaboratorCompletedTasks: [],
  collaboratorCompletedTasksLastMonth: [],
  collaboratorPendingTasks: [],
  initiatedButNotCompletedTasks: [],
  projectCompletedTasks: [],
  projectCancelledTasks: [],
  projectOverdueTasks: [],
  projectCompletedTasksLastMonth: [],
  tasksOrderByLastReport: [],
    collabRequests: []
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "LOADED_PROJECT_TASKS":
      return { ...state, tasks: action.payload };
    case "LOADED_COLLABORATOR_PROJECT_TASKS":
      return { ...state, collaboratorProjectTasks: action.payload };
    case "LOADED_TASKS_DEPENDENCIES":
      return { ...state, taskDependencies: action.payload };
    case "LOADED_AVAILABLE_TASKS_DEPENDENCIES_TO_ADD":
      return { ...state, dependenciesToAdd: action.payload };
    case "LOADED_COLLABORATOR_COMPLETED_TASKS":
      return { ...state, collaboratorCompletedTasks: action.payload };
    case "LOADED_COLLABORATOR_COMPLETED_TASKS_LAST_MONTH":
      return { ...state, collaboratorCompletedTasksLastMonth: action.payload };
    case "LOADED_COLLABORATOR_PENDING_TASKS":
      return { ...state, collaboratorPendingTasks: action.payload };
    case "LOADED_PROJECT_COMPLETED_TASKS":
      return { ...state, projectCompletedTasks: action.payload };
    case "LOADED_PROJECT_INITIATED_BUT_NOT_COMPLETED_TASKS":
      return { ...state, initiatedButNotCompletedTasks: action.payload };
    case "LOADED_PROJECT_CANCELLED_TASKS":
      return { ...state, projectCancelledTasks: action.payload };
    case "LOADED_PROJECT_OVERDUE_TASKS":
      return { ...state, projectOverdueTasks: action.payload };
    case "LOADED_PROJECT_COMPLETED_TASKS_LAST_MONTH":
      return { ...state, projectCompletedTasksLastMonth: action.payload };
    case "LOADED_USER_RECENT_ACTIVITY":
      return { ...state, tasksOrderByLastReport: action.payload };
      case "REQUESTS_LOADED":
          return { ...state, collabRequests: action.payload };
    default:
      return state;
  }
};
