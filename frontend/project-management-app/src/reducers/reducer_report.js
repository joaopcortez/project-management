const INITIAL_STATE = {
  report: "",
  editedReport: ""
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case "LOADED_EDIT_REPORT":
      return { ...state, editedReport: action.payload };
    case "LOADED_ADD_REPORTS":
      return { ...state, report: action.payload };
    default:
      return state;
  }
}
