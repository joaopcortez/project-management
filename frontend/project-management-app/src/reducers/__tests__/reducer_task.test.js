import React from 'react';
import reducer_task from "../reducer_task";
const INITIAL_STATE = {
    task: "",
    links: [],
    taskCollaborators: [],
    allTaskCollaborators: [],
    taskAvailableCollaborators: [],
    removedCollaborator: [],
    addedCollaborator: []
};



it('handes actions of type \'CURRENT_TASK\'', () => {

        const action = {
            type: 'CURRENT_TASK',
            payload: {
                _links: {
                    Add_Collaborator: {href: "http://localhost:8080/tasks/1-1", type: "Post"},
                    Add_Task_Dependency: {
                        href: "http://localhost:8080/tasks/1-1/dependenciestoadd",
                        type: "Post"
                    },
                    Remove_Collaborator: {href: "http://localhost:8080/tasks/1-1", type: "Put"},
                    Remove_Task: {href: "http://localhost:8080/tasks/1-1", type: "Delete"}
                },
                dateOfCreation: "2018-06-30 22:07:16",
                dependenciesId: [],
                description: "The development view illustrates a system from a programmer's perspective and is concerned with software management. This view is also known as the implementation view. It uses the UML Component diagram to describe system components. UML Diagrams used to represent the development view include the Package diagram.",
                estimatedEffort: 1.3,
                ganttTaskDependencies: "",
                lastReportHours: 0,
                predictedDateOfConclusion: "2018-08-11 00:00:00",
                predictedDateOfStart: "2018-06-11 00:00:00",
                projectId: "1",
                state: "ReadyToStart",
                taskId: "1-1",
                title: "Document Development view",
                unitCost: 1.2
            }


        };
        const newState = reducer_task('', action);
        expect(newState).toEqual({

            task: {
                _links: {
                    Add_Collaborator: {href: "http://localhost:8080/tasks/1-1", type: "Post"},
                    Add_Task_Dependency: {
                        href: "http://localhost:8080/tasks/1-1/dependenciestoadd",
                        type: "Post"
                    },
                    Remove_Collaborator: {href: "http://localhost:8080/tasks/1-1", type: "Put"},
                    Remove_Task: {href: "http://localhost:8080/tasks/1-1", type: "Delete"}
                },
                dateOfCreation: "2018-06-30 22:07:16",
                dependenciesId: [],
                description: "The development view illustrates a system from a programmer's perspective and is concerned with software management. This view is also known as the implementation view. It uses the UML Component diagram to describe system components. UML Diagrams used to represent the development view include the Package diagram.",
                estimatedEffort: 1.3,
                ganttTaskDependencies: "",
                lastReportHours: 0,
                predictedDateOfConclusion: "2018-08-11 00:00:00",
                predictedDateOfStart: "2018-06-11 00:00:00",
                projectId: "1",
                state: "ReadyToStart",
                taskId: "1-1",
                title: "Document Development view",
                unitCost: 1.2
            }

        })

    }
);

it('handes actions of Unknow type', () => {

        const state = {addedCollaborator: [], allTaskCollaborators: [], links: [], removedCollaborator: [], task: "", taskAvailableCollaborators: [], taskCollaborators:  []};
        const newState = reducer_task(state, {type: 'iouqoiuoiuoiquwoiuwrq'});

        expect(newState).toEqual(state);

    }
)


