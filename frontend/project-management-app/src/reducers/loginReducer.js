const INITIAL_STATE = {
  isAuthenticated: false,
  needActivation: false,
  needToChangePassword: false,
  currentUser: "",
  userProfile: "",
  isLoading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "LOGGED_IN":
      return {
        ...state,
        isAuthenticated: true,
        needActivation: false,
        needToChangePassword: false
      };
    case "LOGGED_IN_NOT_ACTIVATED":
      return { ...state, isAuthenticated: true, needActivation: true, needToChangePassword: false };
    case "USER_LOADED":
      return { ...state, currentUser: action.payload };
    case "LOGGED_OUT":
      return { ...state, currentUser: "", isAuthenticated: false };
    case "PROFILE_LOADED":
      return { ...state, userProfile: action.payload };
    case "LOADING_PROCESS":
      return { ...state, isLoading: action.isLoading };
    case "USER_ACTIVATE_SUCCESS":
      return { ...state, needActivation: false };
    case "LOGGED_IN_CHANGE_PASSWORD":
      return { ...state, isAuthenticated: true, needActivation: false, needToChangePassword: true };
    case "USER_CHANGE_PASSWORD_SUCCESS":
      return { ...state, needToChangePassword: false };
    default:
      return state;
  }
};
