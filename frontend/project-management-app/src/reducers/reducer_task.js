const INITIAL_STATE = {
  task: "",
  links: [],
  taskCollaborators: [],
  allTaskCollaborators: [],
  taskAvailableCollaborators: [],
  removedCollaborator: [],
  addedCollaborator: []
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "CURRENT_TASK":
      return { ...state, task: action.payload };
    case "CREATE_TASK":
      return { ...state, task: action.payload };
    case "ADD_DEPENDENCIES":
      return { ...state, task: action.payload };
    case "LOAD_LINKS":
      return { ...state, links: action.payload };
    case "LOAD_TASK_COLLABORATORS":
      return { ...state, taskCollaborators: action.payload };
    case "LOAD_ALL_TASK_COLLABORATORS":
      return { ...state, allTaskCollaborators: action.payload };
    case "ADD_TASK_COLLABORATORS":
      return { ...state, addedCollaborator: action.payload };
    case "LOAD_AVAILABLE_TO_ADD_TASK_COLLABORATORS":
      return { ...state, taskAvailableCollaborators: action.payload };
    case "REMOVE_TASK_COLLABORATORS":
      return { ...state, removedCollaborator: action.payload };
    case "REQUEST_MARK_COMPLETED_TASK":
      return { ...state, task: action.payload };
    case "CANCEL_REQUEST_MARK_COMPLETED_TASK":
      return { ...state, task: action.payload };
    case "CANCEL_REQUEST_MARK_COMPLETED_TASK_COLLAB":
      return { ...state, task: action.payload };
    case "MARK_COMPLETED":
      return { ...state, task: action.payload };
    case "MAKE_ASSIGNMENT_REQUEST":
      return { ...state, task: action.payload };
    case "MAKE_REMOVAL_REQUEST":
      return { ...state, task: action.payload };
    case "REMOVE_TASK":
      return { ...state, task: action.payload };
    case "CANCEL_TASK":
      return { ...state, task: action.payload };
    case "COMPLETED_TASK":
      return { ...state, task: action.payload };
    default:
      return state;
  }
};
