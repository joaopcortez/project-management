import {
    addProjectCollaborator2,
    cancelAssignmentRequests,
    cancelRemovalRequests,
    getAssignmentRequests,
    getCollaboratorsListNotInProject,
    getCompletedRequests,
    getCostProject,
    getAllActiveProjects,
    getPmProjects,
    getProject,
    getProjectCollaboratorsList,
    getProjectCostOptions,
    getProjectTasksCost,
    getRemovalRequests,
    getUnassignedProjectCollaboratorsList,
    listprojects,
    selectProjectCostOption,
    getAvailableCostOptions, getCollabRequests
} from "../util/APIUtils";
import {loadCollabRequests} from "./actions_task_array";

export const getProjects = useremail => {
  return dispatch => {
    listprojects(useremail).then(resp =>
      dispatch({
        type: "PROJECTS_LOADED",
        payload: resp
      })
    );
  };
};

export const listAvailableCostOptions = () => {
  return dispatch => {
    getAvailableCostOptions().then(resp =>
      dispatch({
        type: "PROJECT_COST_OPTIONS_LOADED",
        payload: resp
      })
    );
  };
};

export const getProjectById = (projectId, useremail) => {
  return dispatch => {
    getProject(projectId).then(resp =>
      dispatch({
        type: "PROJECT_SELECTED",
        payload: resp,
        email: useremail
      })
    );
  };
};

export const getProjectsAsPManager = useremail => {
  return dispatch => {
    getPmProjects(useremail).then(resp =>
      dispatch({
        type: "PROJECTS_AS_PM_LOADED",
        payload: resp
      })
    );
  };
};
export const getProjectsAsDirector = useremail => {
  return dispatch => {
    getAllActiveProjects().then(resp =>
      dispatch({
        type: "PROJECTS_AS_DIR_LOADED",
        payload: resp
      })
    );
  };
};

export const loadedProjectCollaboratorsList = projectId => {
  return dispatch => {
    getProjectCollaboratorsList(projectId).then(resp =>
      dispatch({
        type: "PROJECTS_COLLABORATORS_LIST_LOADED",
        payload: resp
      })
    );
  };
};

export const loadedProjectCollaboratorsListNotInProject = projectId => {
  return dispatch => {
    getCollaboratorsListNotInProject(projectId).then(resp =>
      dispatch({
        type: "PROJECTS_COLLAB_LIST_NOT_IN_PROJECT_LOADED",
        payload: resp
      })
    );
  };
};

export const loadedUnassignedProjectCollaboratorsList = projectId => {
  return dispatch => {
    getUnassignedProjectCollaboratorsList(projectId).then(resp =>
      dispatch({
        type: "PROJECTS_COLLAB_LIST_UNASSIGNED",
        payload: resp
      })
    );
  };
};

export const addProjectCollaborator = (projectId, email, cost) => {
  return dispatch => {
    addProjectCollaborator2(projectId, email, cost)
      .then(resp =>
        dispatch({
          type: "ADD_PROJECT_COLLABORATOR",
          payload: resp
        })
      )

      .then(resp => dispatch(loadedProjectCollaboratorsList(projectId)))
      .then(resp => dispatch(loadedUnassignedProjectCollaboratorsList(projectId)));
  };
};

export const acceptAddProjectCollaborator = (projectId, email, cost, projectManager) => {
  return dispatch => {
    addProjectCollaborator2(projectId, email, cost)
      .then(resp =>
        dispatch({
          type: "ADD_PROJECT_COLLABORATOR",
          payload: resp
        })
      )

      .then(resp => dispatch(loadedProjectCollaboratorsList(projectId)))
      .then(resp => dispatch(loadedUnassignedProjectCollaboratorsList(projectId)))
      .then(resp => dispatch(getProjectById(projectId, projectManager)));
  };
};

export const loadAssignmentRequests = projectId => {
  return dispatch => {
    getAssignmentRequests(projectId).then(resp =>
      dispatch({
        type: "ASSIGNEMENT_REQUESTS_LOADED",
        payload: resp
      })
    );
  };
};

export const loadRemovalRequests = projectId => {
  return dispatch => {
    getRemovalRequests(projectId).then(resp =>
      dispatch({
        type: "REMOVAL_REQUESTS_LOADED",
        payload: resp
      })
    );
  };
};

export const loadCompletedRequests = projectId => {
  return dispatch => {
    getCompletedRequests(projectId).then(resp =>
      dispatch({
        type: "COMPLETE_TASK_REQUESTS_LOADED",
        payload: resp
      })
    );
  };
};

export const loadCurrentProjectCost = projectId => {
  return dispatch => {
    getCostProject(projectId).then(resp =>
      dispatch({
        type: "CURRENT_COST_LOADED",
        payload: resp
      })
    );
  };
};

export const cancelAssignmentRequest = (projectId, taskId, userEmail, collabId, projManager) => {
  return dispatch => {
    cancelAssignmentRequests(projectId, taskId, userEmail, collabId)
      .then(resp =>
        dispatch({
          type: "ASSIGNMENT_REQUEST_CANCELLED",
          payload: resp
        })
      )
      .then(resp => dispatch(loadAssignmentRequests(projectId)))
      .then(resp => dispatch(getProjectById(projectId, projManager)));
  };
};

export const cancelAssignmentRequestCollab = (projectId, taskId, userEmail, collabId) => {
    return dispatch => {
        cancelAssignmentRequests(projectId, taskId, userEmail, collabId)
            .then(resp =>
                dispatch({
                    type: "ASSIGNMENT_REQUEST_CANCELLED_COLLAB",
                    payload: resp
                })
            )
            .then(resp => dispatch(loadCollabRequests(userEmail)))
    };
};

export const cancelRemovalRequest = (projectId, taskId, userEmail, collabId, projecManager) => {
  return dispatch => {
    cancelRemovalRequests(projectId, taskId, userEmail, collabId)
      .then(resp =>
        dispatch({
          type: "REMOVAL_REQUEST_CANCELLED",
          payload: resp
        })
      )
        .then(resp => dispatch(loadRemovalRequests(projectId)))
      .then(resp => dispatch(getProjectById(projectId, projecManager)));
  };
};

export const cancelRemovalRequestCollab = (projectId, taskId, userEmail, collabId) => {
    return dispatch => {
        cancelRemovalRequests(projectId, taskId, userEmail, collabId)
            .then(resp =>
                dispatch({
                    type: "REMOVAL_REQUEST_CANCELLED_COLLAB",
                    payload: resp
                })
            )
            .then(resp => dispatch(loadCollabRequests(userEmail)))
    };
};

export const loadCostOptions = projectId => {
  return dispatch => {
    getProjectCostOptions(projectId).then(resp =>
      dispatch({
        type: "AVAILABLE_COST_OPTIONS_LOADED",
        payload: resp
      })
    );
  };
};

export const selectCostOption = (projectId, option) => {
  return dispatch => {
    selectProjectCostOption(projectId, option)
      .then(resp =>
        dispatch({
          type: "COST_SELECTED",
          payload: resp
        })
      )
      .then(resp => dispatch(loadCurrentProjectCost(projectId)))
      .then(resp => dispatch(getTaskCosts(projectId)));
  };
};

export const getTaskCosts = projectId => {
  return dispatch => {
    getProjectTasksCost(projectId).then(resp =>
      dispatch({
        type: "TASKS_COST_LOADED",
        payload: resp
      })
    );
  };
};
