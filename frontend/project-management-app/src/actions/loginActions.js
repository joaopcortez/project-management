import { ACCESS_TOKEN, API_BASE_URL } from "../constants";
import { getCurrentUser, getUserProfile, login, changePassword } from "../util/APIUtils";
import { notification } from "antd";

const url = API_BASE_URL + "/api/auth/signin";

export const signin = loginRequest => {
  return dispatch => {
    dispatch(isLoading(true));
    login({ ...loginRequest })
      .then(resp => {
        localStorage.setItem(ACCESS_TOKEN, resp.accessToken);
        if (resp.needActivation === true && resp.needToChangePassword === false) {
          notification.warn({
            message: `PM App`,
            description: "You need to activate account!"
          });
          return dispatch({
            type: "LOGGED_IN_NOT_ACTIVATED",
            payload: resp.data
          });
        } else if (resp.needActivation === false && resp.needToChangePassword === true) {
          notification.warn({
            message: `PM App`,
            description: "Please define new password!"
          });
          return dispatch({
            type: "LOGGED_IN_CHANGE_PASSWORD",
            payload: resp.data
          });
        } else {
          notification.success({
            message: `PM App`,
            description: "You're successfully logged in."
          });
          return dispatch({
            type: "LOGGED_IN",
            payload: resp.data
          });
        }
      })

      .then(resp => dispatch(loadUser()))
      .catch(error => {
        if (error.status === 401) {
          notification.error({
            message: "PM App",
            description: "Your Username or Password is incorrect. Please try again!"
          });
        } else {
          notification.error({
            message: "PM App",
            description: error.message || "Sorry! Something went wrong. Please try again!"
          });
        }
        return dispatch(isLoading(false));
      });
  };
};

export const isLoading = bool => {
  return {
    type: "LOADING_PROCESS",
    isLoading: bool
  };
};

export const loadUser = () => {
  return dispatch => {
    getCurrentUser()
      .then(resp =>
        dispatch({
          type: "USER_LOADED",
          payload: resp
        })
      )
      .then(resp => dispatch(isLoading(false)));
  };
};

export const activateUserSuccess = () => {
  return dispatch => {
    getCurrentUser().then(resp =>
      dispatch({
        type: "USER_ACTIVATE_SUCCESS",
        payload: resp
      })
    );
  };
};

export const logout = () => {
  return {
    type: "LOGGED_OUT"
  };
};

export const loadUserProfile = email => {
  return dispatch => {
    getUserProfile(email).then(resp =>
      dispatch({
        type: "PROFILE_LOADED",
        payload: resp
      })
    );
  };
};

export const changePasswordSuccess = () => {
  return {
    type: "USER_CHANGE_PASSWORD_SUCCESS"
  };
};
