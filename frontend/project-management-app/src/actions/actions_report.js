import { addReportCollab, editReport } from "../util/APIUtils";
import {
  loadedCollaboratorReports,
  loadedCollaboratorTaskReports,
  loadedTasksReports
} from "./actions_report_array";
import { loadTask } from "./actions_task";
import {
  loadedCollaboratorPendingTasks,
  loadedProjectTasks,
  loadedCollaboratorTasks
} from "./actions_task_array";

import { notification, message } from "antd";

export const loadedEditReport = (taskId, reportId, reportQuantity, email) => {
  return dispatch => {
    editReport(taskId, reportId, reportQuantity, email)
      .then(resp =>
        dispatch({
          type: "LOADED_EDIT_REPORT",
          payload: resp
        })
      )
      .catch(error => {
        message.error("Report Not Edited");
      })
      .then(resp => dispatch(loadedTasksReports(taskId)))
      .then(resp => dispatch(loadedCollaboratorTaskReports(email, taskId)))
      .then(resp => dispatch(loadedCollaboratorReports(email)));
  };
};

export const loadedAddReports = (taskId, quantity, startDate, endDate, email, projectId) => {
  return dispatch => {
    addReportCollab(taskId, quantity, startDate, endDate, email)
      .then(resp =>
        dispatch({
          type: "LOADED_ADD_REPORTS",
          payload: resp
        })
      )
      .then(resp => message.success("Report Successfully added!"))
      .catch(error => {
        message.error("Something went wrong! Report not added.. ");
      })
      .then(resp => dispatch(loadTask(taskId)))
      .then(resp => dispatch(loadedCollaboratorPendingTasks(email)))
      .then(resp => dispatch(loadedTasksReports(taskId)))
      .then(resp => dispatch(loadedCollaboratorTaskReports(email, taskId)))
      .then(resp => dispatch(loadedCollaboratorReports(email)))
      .then(resp => dispatch(loadedCollaboratorTasks(email)))
      .then(resp => dispatch(loadedProjectTasks(projectId)));
  };
};
