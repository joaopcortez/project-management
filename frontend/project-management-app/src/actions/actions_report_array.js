import {
  collaboratorHoursInCompletedTasksLastMonth,
  listCollaboratorReports,
  listCollaboratorReportsByTask,
  listReports
} from "../util/APIUtils";

export const loadedTasksReports = taskId => {
  return dispatch => {
    listReports(taskId).then(resp =>
      dispatch({
        type: "LOADED_TASK_REPORTS",
        payload: resp
      })
    );
  };
};

export const loadedCollaboratorReports = email => {
  return dispatch => {
    listCollaboratorReports(email).then(resp =>
      dispatch({
        type: "LOADED_COLLABORATOR_REPORT_ALL_TASKS",
        payload: resp
      })
    );
  };
};

export const loadedCollaboratorTaskReports = (email, taskI) => {
  return dispatch => {
    listCollaboratorReportsByTask(email, taskI).then(resp =>
      dispatch({
        type: "LOADED_COLLABORATOR_TASK_REPORTS",
        payload: resp
      })
    );
  };
};

export const collaboratorEffortInCompletedTasksLastMonth = email => {
  return dispatch => {
    collaboratorHoursInCompletedTasksLastMonth(email).then(resp =>
      dispatch({
        type: "LOADED_COLLABORATOR_HOUR_REPORTS_LAST_MONTH",
        payload: resp
      })
    );
  };
};
