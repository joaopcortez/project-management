import {
  listUsers,
  listUsersByProfile,
  setUserProfile,
  populateUsersXMLAPI,
  populateProjectsXMLAPI
} from "../util/APIUtils";
import { notification } from "antd/lib/index";

export const loadedUsers = () => {
  return dispatch => {
    listUsers().then(resp =>
      dispatch({
        type: "LOADED_USERS",
        payload: resp
      })
    );
  };
};

export const loadCollaborators = () => {
  return dispatch => {
    listUsersByProfile("COLLABORATOR").then(resp =>
      dispatch({
        type: "LOADED_COLLABORATORS",
        payload: resp
      })
    );
  };
};

export const loadAdministrators = () => {
  return dispatch => {
    listUsersByProfile("ADMINISTRATOR").then(resp =>
      dispatch({
        type: "LOADED_ADMINISTRATORS",
        payload: resp
      })
    );
  };
};

export const loadDirectors = () => {
  return dispatch => {
    listUsersByProfile("DIRECTOR").then(resp =>
      dispatch({
        type: "LOADED_DIRECTORS",
        payload: resp
      })
    );
  };
};

export const loadRegisteredUsers = () => {
  return dispatch => {
    listUsersByProfile("REGISTEREDUSER").then(resp =>
      dispatch({
        type: "LOADED_REGISTEREDUSERS",
        payload: resp
      })
    );
  };
};

export const profileUpdated = values => {
  return dispatch => {
    setUserProfile(values)
      .then(response => {
        if (response) {
          notification.success({
            message: "Profile updated!"
          });
        } else {
          notification.warn({
            message: "Profile not updated!"
          });
        }
      })
      .then(resp =>
        dispatch({
          type: "PROFILE_UPDATED",
          payload: resp
        })
      )
      .then(resp => dispatch(loadedUsers())) //TODO : ver se e preciso
      .catch(error => {
        notification.error({
          message: "PM App",
          description: "Wrong Request!"
        });
      });
  };
};

export const populateUsersXML = (filename, path) => {
  populateUsersXMLAPI(filename, path)
    .then(response => {
      if (response) {
        notification.success({
          message: "PM App",
          description: "Import successful!"
        });
      } else {
        notification.warn({
          message: "PM App",
          description: "Import failure!"
        });
      }
    })
    .catch(error => {
      notification.error({
        message: "PM App",
        description: "Wrong Request!"
      });
    });
};

export const populateProjectsXML = (filename, path) => {
    populateProjectsXMLAPI(filename, path)
      .then(response => {
        if (response) {
          notification.success({
            message: "PM App",
            description: "Import successful!"
          });
        } else {
          notification.warn({
            message: "PM App",
            description: "Import failure!"
          });
        }
      })
      .catch(error => {
        notification.error({
          message: "PM App",
          description: "Wrong Request!"
        });
      });
  };
