import { Carousel } from "antd";
import React from "react";
import switchImg from "../switchImg.svg";

export default function MainCarrocel() {
  const carr = (
    <Carousel autoplay>
      <img className="img-responsive" src={switchImg} width="1000" height="40" />
      <div>
        <h1>"SUCCESS IS A TEAM SPORT"</h1>
      </div>
      <div>
        <h1>"IF I TRY MY BEST AND FAIL, WELL, I'VE TRIED MY BEST"</h1>
      </div>
      <div>
        <h1>"WORK HARD IN SILENCE, LET SUCCESS MAKE THE NOISE"</h1>
      </div>
    </Carousel>
  );

  return <div>{carr}</div>;
}
