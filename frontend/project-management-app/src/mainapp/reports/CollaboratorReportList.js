import React, { Component } from "react";
import { Card, Table, Alert, Icon, Tooltip } from "antd";
import "../task/CollaboratorTasks/SelfPendingTasks.css";
import ContentHeader from "../../templates/contentHeader";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {
  collaboratorEffortInCompletedTasksLastMonth,
  loadedCollaboratorReports
} from "../../actions/actions_report_array";
import "./CollaboratorReportList.css";

class CollaboratorReportList extends Component {
  columns = [
    {
      title: "Code",
      dataIndex: "code",
      key: "code"
    },
    {
      title: "Quantity",
      dataIndex: "quantity",
      key: "quantity",
      editable: true
    },
    {
      title: "Start Date",
      dataIndex: "startDate",
      key: "startDate",
      render: (text, record) => this.recordslice(record.startDate)
    },
    {
      title: "End Date",
      dataIndex: "endDate",
      key: "endDate",
      render: (text, record) => this.recordslice(record.endDate)
    },
    {
      title: "Creation Date",
      dataIndex: "reportCreationDate",
      key: "reportCreationDate",
      render: (text, record) => this.recordslice(record.reportCreationDate)
    }
  ];

  recordslice(record) {
    if (!record) {
      return;
    }
    return record.slice(0, 10);
  }

  componentWillMount() {
    const { loadedCollaboratorReports } = this.props;
    loadedCollaboratorReports(this.props.currentUser.email);
    const { collaboratorEffortInCompletedTasksLastMonth } = this.props;
    collaboratorEffortInCompletedTasksLastMonth(this.props.currentUser.email);
  }

  render() {
    return (
      <div>
        <ContentHeader title="REPORT LIST" />

        {this.props.myReports.length === 0 ? (
          <Alert message="You have no reports to display" type="info" showIcon />
        ) : (
          <div>
            <div>
              <Card className="card">
                <Icon className="icon" type="clock-circle-o" />
                <h3 className="title">Effort spent in last month</h3>
                <Tooltip title={this.props.hoursLastMonth+ " hours + "+this.props.peopleMonthLastMonth+ " people month" }>
                <div className="header">
                  <span className="content-card">{this.props.hoursLastMonth} h + {this.props.peopleMonthLastMonth} pm</span>
                </div>
                </Tooltip>
              </Card>
            </div>
            <br />
            <Table
              rowKey={record => record.code}
              dataSource={this.props.myReports}
              columns={this.columns}
            />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  hoursLastMonth: state.reports.collaboratorHoursCompletedTasksLastMonth,
  peopleMonthLastMonth: state.reports.collaboratorPeopleMonthCompletedTasksLastMonth,
  myReports: state.reports.collaboratorAllTasksReports,
  currentUser: state.authentication.currentUser
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { loadedCollaboratorReports, collaboratorEffortInCompletedTasksLastMonth },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CollaboratorReportList);
