import { Alert, Table } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  loadedCollaboratorTaskReports,
  loadedTasksReports
} from "../../actions/actions_report_array";
import EditableCell from "./EditableCell";
import { recordslice } from "../task/task/TaskDetails";
import AddReportTable from "./AddReportTable";

class EditableReportTable2 extends Component {
  constructor(props) {
    super(props);

    this.columns = [
      {
        title: "Code",
        dataIndex: "code",
        key: "code"
      },
      {
        title: "Collaborator Email",
        dataIndex: "userEmail"
      },
      {
        title: "Effort (" + this.props.project.unit + ")",
        dataIndex: "quantity",
        render: (text, record) => (
          <EditableCell
            value={text}
            code={record.code}
            initValue={record.quantity}
            onChange={this.onCellChange(record.code, "quantity")}
          />
        )
      },
      {
        title: "Start Date",
        dataIndex: "startDate",
        render: (text, record) => recordslice(record.startDate)
      },
      {
        title: "End Date",
        dataIndex: "endDate",
        render: (text, record) => recordslice(record.endDate)
      }
    ];
  }

  componentWillMount() {
    const { loadedCollaboratorTaskReports } = this.props;
    loadedCollaboratorTaskReports(this.props.currentUser.email, this.props.selectedTask.taskId);
    const { loadedTasksReports } = this.props;
    loadedTasksReports(this.props.selectedTask.taskId);
  }

  onDelete = key => {
    console.log("going to delete" + key);
  };

  onCellChange = (key, dataIndex) => {
    return value => {
      const dataSource = [...this.props.reports];
      const target = dataSource.find(item => item.key === key);
      if (target) {
        target[dataIndex] = value;
      }
    };
  };

  renderSwitch(columns) {
    let reportList = this.props.reports;
    if (this.props.isProjectManager) {
        return(<div>
            <Table
                rowKey={record => record.code}
                bordered
                dataSource={this.props.reportsPM}
                columns={columns}
            />
        </div>)
    }
    else if (reportList.length === 0) {
      return <Alert message="NO REPORTS" type="info" showIcon />;
    } else {
      return (
        <div>
          <Table
            rowKey={record => record.code}
            bordered
            dataSource={reportList}
            columns={columns}
          />
        </div>
      );
    }
  }

  render() {
    const columns = this.columns;

    return (
      <div>
        {this.props.selectedTask._links && this.props.selectedTask._links["Add Report"] ? (
          <AddReportTable
            projectIdPending={this.props.selectedTask.projectId}
            value={this.props.selectedTask.taskId}
          />
        ) : null}
        <br />
        {this.renderSwitch(columns)}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  selectedTask: state.task.task,
  reports: state.reports.collaboratorTaskReports,
  currentUser: state.authentication.currentUser,
  isProjectManager: state.projects.isProjectManager,
  reportsPM: state.reports.taskReports,
  report: state.report.report,
  project: state.projects.selectedProject
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({ loadedTasksReports, loadedCollaboratorTaskReports }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditableReportTable2);
