import React from 'react';
import {Button, DatePicker, Form, Icon, InputNumber, Modal, Popover} from 'antd';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {loadedAddReports} from "../../actions/actions_report";
import {loadTask, loadTaskCollaborators} from "../../actions/actions_task";
import {loadedCollaboratorTaskReports} from "../../actions/actions_report_array";


const FormItem = Form.Item;
const RangePicker = DatePicker.RangePicker;

const now = new Date().toISOString().slice(0, 10);
const CollectionCreateForm = Form.create()(
    class extends React.Component {

        constructor(props) {
            super(props);
            this.state = {
                visible: false,
                addDate:null
            };
        }

        componentWillMount(){

            this.setState({ addDate:this.props.addDate})
       

        }

        disabledDate(current) {

            // Can not select days after today
            return /*current &&*/  current.valueOf() > Date.now();
        }

        render() {
           
            const {visible, onCancel, onCreate} = this.props;
            const {getFieldDecorator} = this.props.form;
            const formItemLayout = {
                labelCol: {
                    xs: {span: 24},
                    sm: {span: 8},
                },
                wrapperCol: {
                    xs: {span: 24},
                    sm: {span: 16},
                },
            };

            const rangeConfig = {
                rules: [{type: 'array', required: true, message: 'Please select time!'}],
            };
            return (
                <Modal
                    visible={visible}
                    title="Add Report"
                    okText="Add"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="horizontal">

                        <strong>Task ID:</strong> {this.props.taskIdRepo}{" "}
                        <small>(Project ID: {this.props.projectIdRepo})</small>


                        <div style={{ borderBottom: "1px solid #d9d9d9", paddingTop: "1em" }} />
                    <br/>

                            <FormItem label="Effort">
                            {getFieldDecorator('effort', {
                                rules: [
                                    {
                                        required:true,
                                        message: "Please insert correct effort!"
                                    }
                                ]
                            })(
                                <InputNumber min={1} max={100}/>
                            )}
                            <span className="ant-form-text"> HOURS</span>
                        </FormItem>

                        <FormItem label="Report period">
                            {getFieldDecorator('dates', rangeConfig)(
                                <RangePicker
                                    disabledDate={this.disabledDate}


                                />
                            )}
                        </FormItem>


                    </Form>
                </Modal>
            )
                ;
        }
    }
);

class AddReportTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            addDate:''
        };
    }


    componentWillMount(){
       
    const {loadTaskCollaborators} =this.props;
    loadTaskCollaborators(this.props.value)

    this.getDate()

    }


    getDate(){


       this.props.taskCollaborators.map(e =>{
           
        if( e.projectCollaboratorEmail === this.props.currentUser.email){
             this.setState({addDate:e.collaboratorAddedToTaskDate})
    }
})
}

    showModal = () => {
        this.setState({visible: true});
    };
    handleCancel = () => {
        this.setState({visible: false});
    };


    handleCreate = () => {

        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            this.setState({visible: false});

            
            const {loadedAddReports} = this.props;
            loadedAddReports(this.props.value, values.effort, values.dates[0].format('YYYY-MM-DD hh:mm:ss'), values.dates[1].format('YYYY-MM-DD hh:mm:ss'), this.props.currentUser.email, this.props.selectProject);
            form.resetFields();


        });
    };


    saveFormRef = (formRef) => {
        this.formRef = formRef;
    };


    render() {

    


        return (
            <div>
                <Popover content="Add Report">
                    <Button type="primary" onClick={this.showModal}><Icon type="form" style={{fontSize: 20}}/>Add Report</Button>

                </Popover>

                <div>
                    <CollectionCreateForm
                        wrappedComponentRef={this.saveFormRef}
                        visible={this.state.visible}
                        onCancel={this.handleCancel}
                        onCreate={this.handleCreate}
                        projectIdRepo={this.props.projectIdPending}
                        taskIdRepo={this.props.value}
                        addDate={this.state.addDate}

                    />
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    report: state.report.report,
    currentUser: state.authentication.currentUser,
    selectedTask: state.task.task,
    taskCollaborators: state.task.taskCollaborators,
    selectProject: state.projects.selectedProject.projectId
});
const mapDispatchToProps = dispatch =>
    bindActionCreators({loadedAddReports, loadTaskCollaborators, loadedCollaboratorTaskReports, loadTask}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddReportTable));
