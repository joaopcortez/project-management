import React, { Component } from "react";
import { loadedEditReport } from "../../actions/actions_report";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Icon, Input, Popconfirm, Popover } from "antd";
import { message } from "antd/lib/index";

class EditableCell extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value,
      editable: false
    };
  }

  handleChange = e => {
    const value = e.target.value;
    this.setState({ value });
  };

  check = () => {
    this.setState({ editable: false });
    console.log(
      this.props.selectedTask.taskId,
      this.props.code,
      this.state.value,
      this.props.currentUser.email
    );
    const { loadedEditReport } = this.props;
    loadedEditReport(
      this.props.selectedTask.taskId,
      this.props.code,
      this.state.value,
      this.props.currentUser.email
    );
    if (this.props.onChange) {
      this.props.onChange(this.state.value);
    }
  };
  edit = () => {
    this.setState({ editable: true });
  };

  render() {
    const { value, editable } = this.state;
    return (
      <div className="editable-cell">
        {editable ? (
          <Input
            value={value}
            onChange={this.handleChange}
            suffix={
              <Popconfirm
                title="Are you sure you want to edit this report ?"
                onConfirm={() => {
                  this.check();
                  message.success("Report successfully edited");
                }}
                onCancel={() => {
                  this.setState({ editable: false, value: this.props.initValue });
                  message.error("You cancel this option");
                }}
                okText="Yes"
                cancelText="No"
              >
                <Icon type="check" className="editable-cell-icon-check" />
              </Popconfirm>
            }
          />
        ) : (
          <div style={{ paddingRight: 24 }}>
            {value || " "}
            {this.props.selectedTask._links && this.props.selectedTask._links["Add Report"] ? (
              <Popover content="Edit Quantity">
                <button className="editable-cell-icon" onClick={this.edit}>
                  <Icon type="edit" />{" "}
                </button>{" "}
              </Popover>
            ) : null}
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  selectedTask: state.task.task,
  reports: state.reports.collaboratorTaskReports,
  currentUser: state.authentication.currentUser,
  editReport: state.report.editReport
});
const mapDispatchToProps = dispatch => bindActionCreators({ loadedEditReport }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditableCell);
