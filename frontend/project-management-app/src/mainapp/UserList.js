import React from "react";
import { listUsers } from "../util/APIUtils";
import "./task/CollaboratorTasks/SelfPendingTasks.css";

export default class UserList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: []
    };
  }

  componentWillMount() {
    listUsers().then(users => this.setState({ users: users }));
  }

  render() {
    return (
      <div style={{ textAlign: "center" }}>
        <table>
          <tr>
            <th>Name</th>
            <th>Email ID</th>
            <th>Phone</th>
          </tr>
          {this.state.users.map(user => (
            <tr key={user.email}>
              <td>{user.name}</td>
              <td>{user.email}</td>
              <td>{user.phone}</td>
            </tr>
          ))}
        </table>
      </div>
    );
  }
}
