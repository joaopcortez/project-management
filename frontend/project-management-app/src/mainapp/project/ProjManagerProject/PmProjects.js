import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "./PmProjects.css";
import { Alert, Icon, Popover, Table } from "antd";
import ContentHeader from "../../../templates/contentHeader";
import { getProjectById, getProjectsAsPManager } from "../../../actions/projectActions";
import Project from "../Project";
import { withRouter } from "react-router-dom";


class PmProjects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: []
    };
  }

  componentWillMount() {
    const { getProjectsAsPManager } = this.props;
    getProjectsAsPManager(this.props.currentUser.email);

    const columnsManager = [
      {
        title: "Title",
        dataIndex: "name",
        key: "name",
        width: 200
      },
      {
        title: "Global Budget",
        dataIndex: "globalBudget",
        key: "globalBudget"
      },
      {
        title: "Unit",
        dataIndex: "unit",
        key: "unit"
      },
      {
        title: "Start Date",
        dataIndex: "startDate",
        key: "startDate",
        render: (text, record) => this.recordslice(record.startDate)
      },
      {
        title: "Final Date",
        dataIndex: "finalDate",
        key: "finalDate",
        render: (text, record) => this.recordslice(record.finalDate)
      },
      {
        title: "Action",
        dataIndex: "",
        key: "action",
        render: (text, record) => (
          <Popover content="View Details">
            <button onClick={() => this.viewDetails(record.projectId)} value={record.projectId}>
              <Icon type="info-circle" style={{ fontSize: 20, color: "#08c" }} />
            </button>{" "}
          </Popover>
        )
      }
    ];

    this.setState({
      columns: columnsManager
    });
  }

  viewDetails(projectId) {
    const { getProjectById } = this.props;

    getProjectById(projectId, this.props.currentUser.email);

    this.props.history.push("/projects/selected");
  }

  recordslice(record) {
    if (!record) {
      return;
    }
    return record.slice(0, 10);
  }

  seeList(param) {
    return (
      <div>
        <ContentHeader title="MY PROJECTS" subtitle="As Project Manager" />
        {param.length === 0 ? (
          <Alert message="You have no Projects as Project Manager" type="info" showIcon />
        ) : (
          <Table
            Table
            rowKey={record => record.projectId}
            dataSource={this.props.projectsAsPM}
            columns={this.state.columns}
          />
        )}
      </div>
    );
  }



  render() {
    return <div>{this.seeList(this.props.projectsAsPM)}</div>;
  }
}

const mapStateToProps = state => ({
  currentUser: state.authentication.currentUser,
  projectsAsPM: state.projects.projectsAsManager,
  selectedProject: state.projects.selectedProject
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({ getProjectById, getProjectsAsPManager }, dispatch);

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(PmProjects));
