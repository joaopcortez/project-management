import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Avatar, List } from "antd";
import AddProjectCollaborator from "./ProjManagerProject/AddProjectCollaborator";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  loadedProjectCollaboratorsList,
  loadedProjectCollaboratorsListNotInProject,
  loadedUnassignedProjectCollaboratorsList
} from "../../actions/projectActions";

import { getAvatarColor } from "../../util/Colors";
import "./CollaboratorsList.css";
import { Radio } from "antd";
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

class CollaboratorsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      option: 0,
      columns: []
    };

    this.link = this.link.bind(this);
  }

  componentWillMount() {
    const { loadedProjectCollaboratorsList } = this.props;
    loadedProjectCollaboratorsList(this.props.project.projectId);

  }

  onChange = e => {
    if (e.target.value === "unassigned") {
      this.setState({ option: 1 });
    } else {
      this.setState({ option: 0 });
    }
  };

  link(email, name) {
    return (
      <Link to={`/users/${email}`}>
        <h1>{name}</h1>
      </Link>
    );
  }

  renderSwitch(param) {
    switch (param) {
      case 0:
        return (
          <div>
            <section>
              <List
                className="listColab"
                itemLayout="Horizontal"
                dataSource={this.props.projectCollaboratorList}
                renderItem={item => (
                  <List.Item>
                    <List.Item.Meta
                      avatar={
                        <Avatar
                          className="user-avatar-circle2"
                          style={{ backgroundColor: getAvatarColor(item.userName) }}
                        >
                          {item.userName[0].toUpperCase()}
                        </Avatar>
                      }
                      title={item.userName}
                      description={item.userId}
                    />
                  </List.Item>
                )}
              />
            </section>
          </div>
        );

      case 1:
          const { loadedUnassignedProjectCollaboratorsList } = this.props;
          loadedUnassignedProjectCollaboratorsList(this.props.project.projectId);
        return (
          <div>
            <section>
              <List
                itemLayout="Horizontal"
                dataSource={this.props.unassignedProjectsCollaboratorList}
                renderItem={item => (
                  <List.Item>
                    <List.Item.Meta
                      avatar={
                        <Avatar
                          className="user-avatar-circle2"
                          style={{ backgroundColor: getAvatarColor(item.userName) }}
                        >
                          {item.userName[0].toUpperCase()}
                        </Avatar>
                      }
                      title={item.userName}
                      description={item.userId}
                    />
                  </List.Item>
                )}
              />
            </section>
          </div>
        );
      default:
        return <div />;
    }
  }
  render() {
    const role = this.props.currentUser.profile;

    return (
      <div>
        {this.props.isManager ? (
          <AddProjectCollaborator
            projectUnit={this.props.project.unit}
            projectId={this.props.project.projectId}
          />
        ) : null}

        {this.props.isManager ? (
          <div>
            <div className="radio-filter">
              <RadioGroup onChange={this.onChange} defaultValue="all">
                <RadioButton value="all">All</RadioButton>
                {role === "[ROLE_DIRECTOR]" ? null : (
                  <RadioButton value="unassigned">Unassigned Collaborators</RadioButton>
                )}
              </RadioGroup>
            </div>
          </div>
        ) : null}

        {this.renderSwitch(this.state.option)}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  projectCollaboratorList: state.projects.projectsCollaboratorList,
  isManager: state.projects.isProjectManager,
  unassignedProjectsCollaboratorList: state.projects.unassignedProjectsCollaboratorList,
  project: state.projects.selectedProject,
  currentUser: state.authentication.currentUser
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      loadedProjectCollaboratorsList,
      loadedProjectCollaboratorsListNotInProject,
      loadedUnassignedProjectCollaboratorsList
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CollaboratorsList);
