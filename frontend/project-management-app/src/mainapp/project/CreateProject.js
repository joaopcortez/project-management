import React, { Component } from "react";
import ContentHeader from "../../templates/contentHeader";
import { Form } from "antd";
import CreateProjectForm from "./CreateProjectForm";

class CreateProject extends Component {
  render() {
    const WrappedTimeRelatedForm = Form.create()(CreateProjectForm);
    return (
      <div>
        <ContentHeader title="PROJECTS" subtitle="Create" />
        <WrappedTimeRelatedForm />
      </div>
    );
  }
}

// const mapStateToProps = state => ({myProjects: state.projects.projectsAsDirector, selectedProject:state.projects.selectedProject})
// const mapDispatchToProps = dispatch =>
//     bindActionCreators({getProjectsAsDirector, getProjectById} ,dispatch)

export default CreateProject;
