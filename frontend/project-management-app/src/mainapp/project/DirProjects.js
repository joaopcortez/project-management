import React, { Component } from "react";
import "./MyProjects.css";
import { Icon, Popover, Table } from "antd";
import ContentHeader from "../../templates/contentHeader";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getProjectById, getProjectsAsDirector } from "../../actions/projectActions";
import { withRouter } from "react-router-dom";
import { recordslice } from "../task/task/TaskDetails";

class DirProjects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      option: 0,
      columns: []
    };
  }

  componentWillMount() {
    const { getProjectsAsDirector } = this.props;
    getProjectsAsDirector();

    this.setState({
      columns: [
        {
          title: "Title",
          dataIndex: "name",
          key: "name",
          width: 200
        },
        {
          title: "Project Manager",
          dataIndex: "userEmail",
          key: "userEmail"
        },
        {
          title: "Global Budget",
          dataIndex: "globalBudget",
          key: "globalBudget"
        },
        {
          title: "Unit",
          dataIndex: "unit",
          key: "unit"
        },
        {
          title: "Start Date",
          dataIndex: "startDate",
          key: "startDate",
          render: (text, record) => recordslice(record.startDate)
        },
        {
          title: "Final Date",
          dataIndex: "finalDate",
          key: "finalDate",
          render: (text, record) => recordslice(record.finalDate)
        },
        {
          title: "Action",
          dataIndex: "",
          key: "action",
          render: (text, record) => (
            <Popover content="View Details">
              <button onClick={() => this.viewDetails(record.projectId)} value={record.projectId}>
                <Icon type="info-circle" style={{ fontSize: 20, color: "#08c" }} />
              </button>
            </Popover>
          )
        }
      ]
    });
  }

  viewDetails(projectId) {
    const { getProjectById } = this.props;
    getProjectById(projectId);
    this.setState({ option: 1 });
  }

  renderSwitch(param) {
    switch (param) {
      case 0:
        return (
          <div>
            <ContentHeader title="PROJECTS" subtitle="All Active" />
            <Table
              rowKey={record => record.projectId}
              dataSource={this.props.myProjects}
              columns={this.state.columns}
            />
          </div>
        );
      case 1:
        return this.props.history.push("/projects/selected");
      default:
        return <div />;
    }
  }

  render() {
    return <div>{this.renderSwitch(this.state.option)}</div>;
  }
}
const mapStateToProps = state => ({
  myProjects: state.projects.projectsAsDirector,
  selectedProject: state.projects.selectedProject
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({ getProjectsAsDirector, getProjectById }, dispatch);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DirProjects)
);
