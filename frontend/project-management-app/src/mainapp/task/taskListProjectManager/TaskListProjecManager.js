import React, { Component } from "react";
import { Alert, Icon, message, Popconfirm, Popover, Radio, Table, Tag } from "antd";
import {
  loadedProjectCancelledTasks,
  loadedProjectCompletedTasks,
  loadedProjectCompletedTasksLastMonth,
  loadedProjectInitiatedButNotCompletedTasks,
  loadedProjectOverdueTasks,
  loadedProjectTasks
} from "../../../actions/actions_task_array";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import AddTask from "../AddTask/AddTask";
import TaskProgress from "../taskprogress/TaskProgress";
import { withRouter } from "react-router-dom";
import { cancelTasks, completedTasks, loadTask, removeTasks } from "../../../actions/actions_task";
import "./TaskListProjectManager.css";
import { colorChangeState } from "../../../util/Helpers";
import ProjectTasksDepedencies from "../taskstate/ProjectTasksDepedencies";

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

class TaskListAllProjectManager extends Component {
  constructor(props) {
    super(props);
    this.state = {
      option: 0
    };
  }

  componentWillMount() {
    const { loadedProjectTasks } = this.props;
    loadedProjectTasks(this.props.selectedProject.projectId);
  }

  onChange = e => {
    if (e.target.value === "initiatedButNotCompleted") {
      const { loadedProjectInitiatedButNotCompletedTasks } = this.props;
      loadedProjectInitiatedButNotCompletedTasks(this.props.selectedProject.projectId);
      this.setState({ option: 2 });
    } else if (e.target.value === "completed") {
      const { loadedProjectCompletedTasks } = this.props;
      loadedProjectCompletedTasks(this.props.selectedProject.projectId);
      this.setState({ option: 3 });
    } else if (e.target.value === "lastMonth") {
      const { loadedProjectCompletedTasksLastMonth } = this.props;
      loadedProjectCompletedTasksLastMonth(this.props.selectedProject.projectId);
      this.setState({ option: 4 });
    } else if (e.target.value === "cancelled") {
      const { loadedProjectCancelledTasks } = this.props;
      loadedProjectCancelledTasks(this.props.selectedProject.projectId);
      this.setState({ option: 5 });
    } else if (e.target.value === "overdue") {
      const { loadedProjectOverdueTasks } = this.props;
      loadedProjectOverdueTasks(this.props.selectedProject.projectId);
      this.setState({ option: 6 });
    } else if (e.target.value === "ganttChart") {
      this.setState({ option: 7 });
    } else {
      this.setState({ option: 0 });
    }
  };

  viewDetails(taskId) {
    //e.preventDefault();
    const { loadTask } = this.props;
    loadTask(taskId);
    this.props.history.push("/tasks/selected")
  }

  deleteTask(taskId) {
    // e.preventDefault();
    const { removeTasks } = this.props;
    removeTasks(taskId, this.props.selectedProject.projectId);
  }

  cancelTask(taskId) {
    const { cancelTasks } = this.props;
    cancelTasks(taskId, this.props.selectedProject.projectId);
  }

  completedTask(taskId) {
    const { completedTasks } = this.props;
    completedTasks(taskId, this.props.selectedProject.projectId);
  }

  title = [
    {
      title: "Progress",
      dataIndex: "",
      width: 250,
      render: (text, record) => <TaskProgress taskProgress={record} />
    },
    {
      title: "Title",
      dataIndex: "title",
      key: "title",
      width: 300
    },
    {
      title: "State",
      dataIndex: "state",
      key: "state",

      filters: [
        {
          text: "Created",
          value: "Created"
        },
        {
          text: "Planned",
          value: "Planned"
        },
        {
          text: "Assigned",
          value: "Assigned"
        },
        {
          text: "ReadyToStart",
          value: "ReadyToStart"
        },
        {
          text: "InProgress",
          value: "InProgress"
        },
        {
          text: "Completed",
          value: "Completed"
        },
        {
          text: "Cancelled",
          value: "Cancelled"
        },
        {
          text: "Suspended",
          value: "Suspended"
        }
      ],
      onFilter: (value, record) => record.state.indexOf(value) === 0,
      render: (text, record) => (
        <div>
          <Tag color={colorChangeState(record.state)}>{record.state} </Tag>
        </div>
      )
    },
    {
      title: "Effective Start Date",
      dataIndex: "effectiveStartDate",
      key: "effectiveStartDate",
      render: (text, record) => this.recordslice(record.effectiveStartDate)
    },

    {
      title: "Effective End Date",
      dataIndex: "effectiveDateOfConclusion",
      key: "effectiveDateOfConclusion",
      render: (text, record) => this.recordslice(record.effectiveDateOfConclusion)
    },

    {
      title: "Actions",
      children: [
        {
          title: "",
          dataIndex: "",
          key: "details",
          width: 10,
          render: (text, record) => (
            <span>
              <Popover content="View Details">
                <button onClick={() => this.viewDetails(record.taskId)} value={record.taskId}>
                  <Icon type="info-circle" style={{ fontSize: 20, color: "#08c" }} />
                </button>
              </Popover>
            </span>
          )
        },
        {
          title: "",
          dataIndex: "",
          key: "completed",
          width: 10,
          render: (text, record) => (
            <span>{this.getLinksMarkCompleted(record.links, record.taskId)}</span>
          )
        },
        {
          title: "",
          dataIndex: "",
          key: "cancelDelete",
          width: 10,
          render: (text, record) => (
            <span>
              {this.getLinksDelete(record.links, record.taskId)}
              {this.getLinksCancel(record.links, record.taskId)}
            </span>
          )
        }
      ]
    }
  ];

  getLinksCancel(links, taskId) {
    if (links.some(e => e.rel === "Cancel Task")) {
      return (
        <div>
          <Popconfirm
            title="Are you sure you want to cancel this task?"
            onConfirm={() => {
              this.cancelTask(taskId);
            }}
            onCancel={() => {
              message.error("You cancel this option");
            }}
            okText="Yes"
            cancelText="No"
          >
            <Popover content="Cancel Task">
              <button>
                <Icon type="close-circle" style={{ fontSize: 20, color: "#cc161d" }} />
              </button>
            </Popover>
          </Popconfirm>
        </div>
      );
    } else {
      return null;
    }
  }

  getLinksMarkCompleted(links, taskId) {
    if (links.some(e => e.rel === "Set Task Completed")) {
      return (
        <div>
          <Popconfirm
            title="Are you sure mark this task completed?"
            onConfirm={() => {
              this.completedTask(taskId);
            }}
            onCancel={() => {
              message.error("You cancel this option");
            }}
            okText="Yes"
            cancelText="No"
          >
            <Popover content="Mark Task Completed">
              <button>
                <Icon type="check-circle" style={{ fontSize: 20, color: "#27a672" }} />
              </button>
            </Popover>
          </Popconfirm>
        </div>
      );
    } else {
      return null;
    }
  }

  getLinksDelete(links, taskId) {
    if (links.some(e => e.rel === "Remove Task")) {
      return (
        <div>
          <Popconfirm
            title="Are you want to delete this task??"
            onConfirm={() => {
              this.deleteTask(taskId);
            }}
            onCancel={() => {
              message.error("You cancel this option");
            }}
            okText="Yes"
            cancelText="No"
          >
            <Popover content="Delete Task">
              <button>
                <Icon type="delete" style={{ fontSize: 20 }} />
              </button>
            </Popover>
          </Popconfirm>
        </div>
      );
    } else {
      return null;
    }
  }

  recordslice(record) {
    if (!record) {
      return;
    }
    return record.slice(0, 10);
  }

  renderSwitch(param) {
    switch (param) {
      case 0:
        return (
          <div>
            <Table
              rowKey={record => record.taskId}
              dataSource={this.props.tasks}
              columns={this.title}
            />
          </div>
        );
     
      case 2:
        if (this.props.initiatedNotCompletedTasks.length === 0) {
          return <Alert message="No Tasks To display in this option" type="info" showIcon />;
        } else {
          return (
            <div>
              <Table
                rowKey={record => record.taskId}
                dataSource={this.props.initiatedNotCompletedTasks}
                columns={this.title}
              />
            </div>
          );
        }
      case 3:
        if (this.props.completedTask.length === 0) {
          return <Alert message="No Tasks To display in this option" type="info" showIcon />;
        } else {
          return (
            <div>
              <Table
                rowKey={record => record.taskId}
                dataSource={this.props.completedTask}
                columns={this.title}
              />
            </div>
          );
        }
      case 4:
        if (this.props.completedTasksLastMonth.length === 0) {
          return <Alert message="No Tasks To display in this option" type="info" showIcon />;
        } else {
          return (
            <div>
              <Table
                rowKey={record => record.taskId}
                dataSource={this.props.completedTasksLastMonth}
                columns={this.title}
              />
            </div>
          );
        }
      case 5:
        if (this.props.cancelledTasks.length === 0) {
          return <Alert message="No Tasks To display in this option" type="info" showIcon />;
        } else {
          return (
            <div>
              <Table
                rowKey={record => record.taskId}
                dataSource={this.props.cancelledTasks}
                columns={this.title}
              />
            </div>
          );
        }
      case 6:
        if (this.props.overdueTasks.length === 0) {
          return <Alert message="No Tasks To display in this option" type="info" showIcon />;
        } else {
          return (
            <div>
              <Table
                rowKey={record => record.taskId}
                dataSource={this.props.overdueTasks}
                columns={this.title}
              />
            </div>
          );
        }
      case 7:
        return (
          <div>
            <br />
            <ProjectTasksDepedencies />
          </div>
        );
      default:
        return <div />;
    }
  }

  render() {
    return (
      <div>
        <AddTask />

        <RadioGroup className="filter" onChange={this.onChange} defaultValue="all">
          <RadioButton value="all">All</RadioButton>
          <RadioButton value="initiatedButNotCompleted">Initiated But Not Completed</RadioButton>
          <RadioButton value="completed">Completed</RadioButton>
          <RadioButton value="lastMonth">Completed Last Month</RadioButton>
          <RadioButton value="cancelled">Cancelled</RadioButton>
          <RadioButton value="overdue">Overdue</RadioButton>
          <RadioButton value="ganttChart">Gantt Chart</RadioButton>
        </RadioGroup>
        <div className="table">{this.renderSwitch(this.state.option)}</div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  task: state.task.task,
  selectedProject: state.projects.selectedProject,
  tasks: state.tasks.tasks,
  currentUser: state.authentication.currentUser,
  initiatedNotCompletedTasks: state.tasks.initiatedButNotCompletedTasks,
  completedTask: state.tasks.projectCompletedTasks,
  cancelledTasks: state.tasks.projectCancelledTasks,
  overdueTasks: state.tasks.projectOverdueTasks,
  completedTasksLastMonth: state.tasks.projectCompletedTasksLastMonth
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      loadedProjectTasks,
      loadTask,
      removeTasks,
      cancelTasks,
      completedTasks,
      loadedProjectCompletedTasks,
      loadedProjectInitiatedButNotCompletedTasks,
      loadedProjectCancelledTasks,
      loadedProjectOverdueTasks,
      loadedProjectCompletedTasksLastMonth
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(TaskListAllProjectManager)
);
