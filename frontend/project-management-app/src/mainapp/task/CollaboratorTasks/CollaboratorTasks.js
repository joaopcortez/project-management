import React, { Component } from "react";
import ContentHeader from "../../../templates/contentHeader/index";
import SelfPendingTasks from "./SelfPendingTasks";
import MyCompletedTasks from "./MyCompletedTasks";
import MyAllTasks from "./MyAllTasks";

import { Radio } from "antd";
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

class CollaboratorTaskTabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      option: 0
    };
    this.handleClick = this.handleClick.bind(this);
  }

  onChange = e => {
    if (e.target.value === "all") {
      this.setState({ option: 0 });
    }

    if (e.target.value === "pending") {
      this.setState({ option: 1 });
    }

    if (e.target.value === "completed") {
      this.setState({ option: 2 });
    }
    if (e.target.value === "completedLastMonth") {
      this.setState({ option: 3 });
    }
  };

  renderSwitch(param) {
    switch (param) {
      case 0:
        return (
          <div>
            <MyAllTasks />
          </div>
        );
      case 1:
        return (
          <div>
            <SelfPendingTasks />
          </div>
        );

      case 2:
        return (
          <div>
            <MyCompletedTasks />
          </div>
        );

      case 3:
        return (
          <div>
            <MyCompletedTasks option={2} />
          </div>
        );

      default:
        return;
    }
  }

  handleClick() {
    this.props.history.goBack();
  }

  render() {
    return (
      <div>
        <ContentHeader title="TASKS" />
        <RadioGroup onChange={this.onChange} defaultValue="all">
          <RadioButton value="all">All</RadioButton>
          <RadioButton value="pending">Pending</RadioButton>
          <RadioButton value="completed">Completed</RadioButton>
          <RadioButton value="completedLastMonth">Completed Last Month</RadioButton>
        </RadioGroup>
        {this.renderSwitch(this.state.option)}
      </div>
    );
  }
}

export default CollaboratorTaskTabs;
