import React, { Component } from "react";
import { Alert, Icon, Popover, Table, Tag } from "antd";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import {
  loadedCollaboratorCompletedTasks,
  loadedCollaboratorCompletedTasksLastMonth
} from "../../../actions/actions_task_array";
import TaskProgress from "../taskprogress/TaskProgress";
import { loadTask } from "../../../actions/actions_task";
import { colorChangeState } from "../../../util/Helpers";

class MyCompletedTasks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      option: 0
    };
  }

  componentWillMount() {
    const { loadedCollaboratorCompletedTasks } = this.props;
    loadedCollaboratorCompletedTasks(this.props.currentUser.email);
    const { loadedCollaboratorCompletedTasksLastMonth } = this.props;
    loadedCollaboratorCompletedTasksLastMonth(this.props.currentUser.email);
    {
      this.props.option === 2 ? this.setState({ option: 2 }) : this.setState({ option: 0 });
    }
  }

  viewDetails(taskId) {
    const { loadTask } = this.props;
    loadTask(taskId);
    this.setState({ option: 1 });
  }

  title = [
    {
      title: "Project ID",
      dataIndex: "projectId",
      key: "projectId"
    },
    {
      title: "Title",
      dataIndex: "title",
      key: "title",
      width: 200
    },
    {
      title: "State",
      dataIndex: "state",
      key: "state",

      render: (text, record) => <Tag color={colorChangeState(record.state)}>{record.state} </Tag>
    },
    {
      title: "Progress",
      dataIndex: "",
      render: (text, record) => <TaskProgress taskProgress={record} />
    },
    {
      title: "Date of Conclusion",
      dataIndex: "effectiveDateOfConclusion",
      key: "effectiveDateOfConclusion"
    },

    {
      title: "Action",
      dataIndex: "",
      key: "action",
      render: (text, record) => (
        <Popover content="View Details">
          <button onClick={() => this.viewDetails(record.taskId)} value={record.taskId}>
            <Icon type="info-circle" style={{ fontSize: 20, color: "#08c" }} />
          </button>
        </Popover>
      )
    }
  ];

  renderSwitch(param) {
    switch (param) {
      case 0:
        if (this.props.tasks.length === 0) {
          return (
            <div>
              <Alert message="You have no tasks completed!" type="info" showIcon />
            </div>
          );
        } else {
          return (
            <div>
              <br />
              <br />
              <Table
                rowKey={record => record.taskId}
                dataSource={this.props.tasks}
                footer={() => <div />}
                columns={this.title}
              />
            </div>
          );
        }

      case 1:
        return this.props.history.push("/tasks/selected");

      case 2:
        if (this.props.tasksLastMonth.length === 0) {
          return (
            <div>
              <Alert message="You have no tasks completed in last month!" type="info" showIcon />
            </div>
          );
        } else {
          return (
            <div>
              <br />
              <br />
              <Table
                rowKey={record => record.taskId}
                dataSource={this.props.tasksLastMonth}
                footer={() => <div />}
                columns={this.title}
              />
            </div>
          );
        }

      default:
        return;
    }
  }

  render() {
    return (
      <div>
        <br />
        {this.renderSwitch(this.state.option)}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  selectedProject: state.projects.selectedProject,
  tasks: state.tasks.collaboratorCompletedTasks,
  tasksLastMonth: state.tasks.collaboratorCompletedTasksLastMonth,
  selectedTask: state.task.task,
  currentUser: state.authentication.currentUser
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      loadedCollaboratorCompletedTasks,
      loadedCollaboratorCompletedTasksLastMonth,
      loadTask
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(MyCompletedTasks)
);
