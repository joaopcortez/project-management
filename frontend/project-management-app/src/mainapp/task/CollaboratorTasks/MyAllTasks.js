import React, { Component } from "react";
import { Alert, Icon, Popover, Table, Tag, Popconfirm, message } from "antd";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { loadTask, requestTaskCompleted, makeRemoval } from "../../../actions/actions_task";
import { withRouter } from "react-router-dom";
import { loadedCollaboratorTasks } from "../../../actions/actions_task_array";
import TaskProgress from "../taskprogress/TaskProgress";
import { colorChangeState } from "../../../util/Helpers";
import AddReport2 from "../../reports/AddReport2";

class MyAllTasks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      option: 0
    };
  }

  componentWillMount() {
    const { loadedCollaboratorTasks } = this.props;
    loadedCollaboratorTasks(this.props.currentUser.email);
  }

  viewDetails(taskId) {
    //e.preventDefault();
    const { loadTask } = this.props;
    loadTask(taskId);
    this.props.history.push("/tasks/selected");
  }

  requestRemove(taskId) {
    const { makeRemoval } = this.props;
    makeRemoval(taskId, this.props.currentUser.email);
  }

  title = [
    {
      title: "Project ID",
      dataIndex: "projectId",
      key: "projectId"
    },
    {
      title: "Title",
      dataIndex: "title",
      key: "title",
      width: 200
    },
    {
      title: "State",
      dataIndex: "state",
      key: "state",

      filters: [
        {
          text: "Created",
          value: "Created"
        },
        {
          text: "Planned",
          value: "Planned"
        },
        {
          text: "Assigned",
          value: "Assigned"
        },
        {
          text: "ReadyToStart",
          value: "ReadyToStart"
        },
        {
          text: "InProgress",
          value: "InProgress"
        },
        {
          text: "Completed",
          value: "Completed"
        },
        {
          text: "Cancelled",
          value: "Cancelled"
        },
        {
          text: "Suspended",
          value: "Suspended"
        }
      ],
      onFilter: (value, record) => record.state.indexOf(value) === 0,
      render: (text, record) => <Tag color={colorChangeState(record.state)}>{record.state} </Tag>
    },
    {
      title: "Progress",
      dataIndex: "",
      render: (text, record) => <TaskProgress taskProgress={record} />
    },
    {
      title: "Completed Request",
      dataIndex: "completedRequest",
      key: "completedRequest"
    },
    {
      title: "Actions",
      children: [
        {
          title: "",
          dataIndex: "",
          key: "info",
          width: 10,
          render: (text, record) => (
            <span>
              <Popover content="View Details">
                <button onClick={() => this.viewDetails(record.taskId)} value={record.taskId}>
                  <Icon type="info-circle" style={{ fontSize: 20, color: "#08c" }} />
                </button>
              </Popover>
            </span>
          )
        },
        {
          title: "",
          dataIndex: "",
          key: "addReport",
          width: 10,

          render: (text, record) => (
            <span>{this.getLinksAddReport(record.links, record.taskId, record.projectId)}</span>
          )
        },
        {
          title: "",
          dataIndex: "",
          key: "completed",
          width: 10,

          render: (text, record) => (
            <span>
              {this.getLinksRequestMarkCompleted(
                record.links,
                record.taskId,
                record.completedRequest
              )}
            </span>
          )
        },
        {
          title: "",
          dataIndex: "",
          key: "requestRemove",
          width: 10,

          render: (text, record) => (
            <span>{this.getLinksRequestRemoval(record.links, record.taskId)}</span>
          )
        }
      ]
    }
  ];

  onSubmit(taskId) {
    const { requestTaskCompleted } = this.props;
    requestTaskCompleted(taskId, this.props.currentUser.email);
  }

  getLinksRequestMarkCompleted(links, taskId, requestTaskCompleted) {
    if (links.some(e => e.rel === "Request Set Task Completed") && !requestTaskCompleted) {
      return (
        <div>
          <Popconfirm
            title="Are you sure mark this task completed?"
            onConfirm={() => {
              this.onSubmit(taskId);
            }}
            onCancel={() => {
              message.error("You cancel this option");
            }}
            okText="Yes"
            cancelText="No"
          >
            <Popover content="Request Mark Task Completed">
              <button>
                <Icon type="check-circle" style={{ fontSize: 20, color: "#9ea624" }} />
              </button>
            </Popover>
          </Popconfirm>
        </div>
      );
    } else {
      return null;
    }
  }

  getLinksRequestRemoval(links, taskId) {
    if (links.some(e => e.rel === "Request Remove Collaborator")) {
      return (
        <div>
          <Popconfirm
            title="Are you sure send request to be removed from this task?"
            onConfirm={() => {
              this.requestRemove(taskId);
            }}
            onCancel={() => {
              message.error("You cancel this option");
            }}
            okText="Yes"
            cancelText="No"
          >
            <Popover content="Request Removal">
              <button>
                <Icon type="user-delete" style={{ fontSize: 25 }} />
              </button>
            </Popover>
          </Popconfirm>
        </div>
      );
    } else {
      return null;
    }
  }

  getLinksAddReport(links, taskId, projectIdPending) {
    if (links.some(e => e.rel === "Add Report")) {
      return (
        <div>
          <AddReport2
            value={taskId}
            projectIdPending={projectIdPending}
            taskSelected={this.addReport}
          />
        </div>
      );
    } else {
      return null;
    }
  }

  renderSwitch(param) {
    switch (param) {
      case 0:
        if (this.props.tasks.length === 0) {
          return (
            <div>
              <br />
              <Alert message="You have no tasks assigned!" type="info" showIcon />
            </div>
          );
        } else {
          return (
            <div>
              <br />
              <br />
              <Table
                rowKey={record => record.taskId}
                dataSource={this.props.tasks}
                footer={() => <div />}
                columns={this.title}
              />
            </div>
          );
        }

      default:
        return <div />;
    }
  }

  render() {
    return <div>{this.renderSwitch(this.state.option)}</div>;
  }
}

const mapStateToProps = state => ({
  selectedProject: state.projects.selectedProject,
  tasks: state.tasks.collaboratorProjectTasks,
  selectedTask: state.task.task,
  currentUser: state.authentication.currentUser
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { loadedCollaboratorTasks, loadTask, requestTaskCompleted, makeRemoval },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(MyAllTasks)
);
