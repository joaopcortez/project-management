import React, { Component } from "react";
import { Button, DatePicker, Form, Icon, Input, InputNumber, Modal } from "antd";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { loadAddTask } from "../../../actions/actions_task";
import { bindActionCreators } from "redux";
import "./AddTask.css";

const FormItem = Form.Item;
const RangePicker = DatePicker.RangePicker;

const CollectionCreateForm = Form.create()(
  class extends Component {
    constructor(props) {
      super(props);
      this.state = {
        formValues: { dates: [] }
      };
    }

    render() {
      const { visible, onCancel, onCreate, form } = this.props;
      const { getFieldDecorator } = form;
      const rangeConfig = {
        rules: [{ required: true, type: "array", message: "Please select start and end dates" }]
      };

      const formItemLayout = {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 8 }
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 16 }
        }
      };

      return (
        <Modal
          visible={visible}
          title="Create a new task"
          okText="Create"
          onCancel={onCancel}
          onOk={onCreate}
        >
          <Form layout="vertical">
            <FormItem label="Title">
              {getFieldDecorator("title", {
                rules: [
                  {
                    required: true,
                    message: "Please insert Task title (max 70 characters)!",
                    max: 70
                  }
                ]
              })(<Input />)}
            </FormItem>
            <FormItem label="Description">
              {getFieldDecorator("description", {
                rules: [
                  {
                    required: true,
                    message: "Please insert description (max characters 500)",
                    max: 500
                  }
                ]
              })(<Input type="textarea" />)}
            </FormItem>
            <FormItem label="Estimated Effort">
              {getFieldDecorator("effort", {
                rules: [{ required: true, message: "Please input effort!" }]
              })(<InputNumber style={{ width: 200 }} min={0} type="textarea" />)}<span> {this.props.projectUnit}</span>

            </FormItem>
            <FormItem {...formItemLayout} label="Start and End Date">
              {getFieldDecorator("range-picker", rangeConfig)(<RangePicker />)}
            </FormItem>
          </Form>
        </Modal>
      );
    }
  }
);

class AddTask extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      formValues: { dates: [] }
    };
  }

  showModal = () => {
    this.setState({ visible: true });
  };
  handleCancel = () => {
    this.setState({ visible: false });
  };

  handleCreate = () => {
    const { loadAddTask } = this.props;

    const form = this.formRef.props.form;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      const rangeValue = values["range-picker"];

      loadAddTask(
        this.props.selectedProject.projectId,
        values.title,
        values.description,
        values.effort,
        rangeValue[0].format("YYYY-MM-DD"),
        rangeValue[1].format("YYYY-MM-DD")
      );
      console.log(rangeValue[0].format("YYYY-MM-DD"));

      form.resetFields();
      this.setState({ visible: false });

      // confirm({
      //     title: 'Do you create task with these items?',
      //     content: (
      //         <div>
      //             <br/>
      //             <p><strong>Project ID:</strong> {this.props.selectedProject.projectId}</p>
      //             <p><strong>Task Title:</strong> {values.title}</p>
      //             <p><strong>Task Description:</strong> {values.description}</p>
      //             <p><strong>Estimated Effort:</strong> {values.effort}</p>
      //             <p><strong>Predicted Date of Start:</strong> {rangeValue[0].format('YYYY-MM-DD')}</p>
      //             <p><strong>Predicted Date of Conclusion:</strong> {rangeValue[1].format('YYYY-MM-DD')}</p>
      //         </div>
      //     ),
      //     onOk() {
      //         console.log('OK');
      //     },
      //     onCancel() {
      //         console.log('Cancel');
      //     },
      // });
    });
  };

  saveFormRef = formRef => {
    this.formRef = formRef;
  };

  render() {
    return (
      <div>
        <Button className="button-addTask" type="primary" onClick={this.showModal}>
          <Icon type="plus-circle-o" />Create Task
        </Button>
        <div>
          <CollectionCreateForm
            wrappedComponentRef={this.saveFormRef}
            visible={this.state.visible}
            onCancel={this.handleCancel}
            onCreate={this.handleCreate}
            projectUnit={this.props.selectedProject.unit}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  selectedProject: state.projects.selectedProject,
  task: state.task.task
});
const mapDispatchToProps = dispatch => bindActionCreators({ loadAddTask }, dispatch);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AddTask)
);
