import TaskListAll from "../task/TaskListAll";
import TaskListProjecManager from "./taskListProjectManager/TaskListProjecManager";
import React from "react";
import TaskListDirector from "./TaskListDirector";

export default function TaskList(props) {
  if (props.isProjectManager === true) {
    return <TaskListProjecManager taskListAllProjectId={props.projectId} />;
  }

  if (props.role === "[ROLE_DIRECTOR]") {
    return <TaskListDirector />;
  }

  return <TaskListAll taskListAllProjectId={props.projectId} />;
}
