import {Button, Form, Icon, Modal} from 'antd';
import React from 'react';
import DependenciesListAvailabletoAdd from "./DependenciesListAvailabletoAdd";
import './TaskDependenciesAdd.css';
import {loadAddTask} from "../../../actions/actions_task";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {bindActionCreators} from "redux";

const CollectionCreateForm = Form.create()(
    class extends React.Component {
        render() {
            const { visible, onCancel, onCreate } = this.props;

            return (


                <Modal className="dependency"
                    visible={visible}
                    title="Add dependencies to task"
                    okText="Close"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <DependenciesListAvailabletoAdd taskid={this.props.taskid}/>

                </Modal>
            );
        }
    }
);

class TaskDependenciesAdd extends React.Component {
    state = {
        visible: false,
    };
    showModal = () => {
        this.setState({ visible: true });
    };
    handleCancel = () => {
        this.setState({ visible: false });
    };
    handleCreate = () => {
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);
            form.resetFields();
            this.setState({ visible: false });
        });
    };
    saveFormRef = (formRef) => {
        this.formRef = formRef;
    };
    render() {


        return (
            <div>
                <Button type="primary" onClick={this.showModal}><Icon type="plus-circle-o"/>Add Dependency</Button>
                <CollectionCreateForm
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}
                    taskid={this.props.task.taskId}
                />
            </div>
        );
    }
}
const mapStateToProps = state => ({task: state.task.task});
const mapDispatchToProps = dispatch =>
    bindActionCreators({loadAddTask} ,dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TaskDependenciesAdd));
