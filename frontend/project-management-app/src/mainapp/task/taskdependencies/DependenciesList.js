import React, { Component } from "react";
import { Alert, Icon, Popover, Table } from "antd";
import { loadedTasksDependencies } from "../../../actions/actions_task_array";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import TaskProgress from "../taskprogress/TaskProgress";
import TaskDependenciesAdd from "./TaskDependenciesAdd";
import { loadLinks, loadTask } from "../../../actions/actions_task";
import { withRouter } from "react-router-dom";

class DependenciesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      option: 0
    };
  }

  componentWillMount() {
    console.log(this.props.selectedTask.taskId);
    const { loadedTasksDependencies } = this.props;
    loadedTasksDependencies(this.props.selectedTask.taskId);
    console.log(this.props.tasks);

    const { loadLinks } = this.props;
    loadLinks(this.props.selectedTask.taskId);
  }

  viewDetails = taskId => {
    //e.preventDefault();
    const { loadTask } = this.props;
    loadTask(taskId);
    this.setState({ option: 1 });
  };

  title = [
    {
      title: "Progress",
      dataIndex: "",
      render: (text, record) => <TaskProgress taskProgress={record} />
    },
    {
      title: "TaskId",
      dataIndex: "taskId",
      key: "taskId"
    },
    {
      title: "Title",
      dataIndex: "title",
      key: "title"
    },
    {
      title: "State",
      dataIndex: "state",
      key: "state"
    },

    {
      title: "Action",
      dataIndex: "",
      key: "action",
      render: (text, record) => (
        <Popover content="View Details">
          <button onClick={() => this.viewDetails(record.taskId)} value={record.taskId}>
            <Icon type="info-circle" style={{ fontSize: 20, color: "#08c" }} />
          </button>
        </Popover>
      )
    }
  ];

  renderSwitch(param) {
    switch (param) {
      case 0:
        if (this.props.tasks.length === 0) {
          return (
            <Alert message="This task doesn't have any current dependency" type="info" showIcon />
          );
        } else {
          return (
            <div>
              <Table
                rowKey={record => record.taskId}
                dataSource={this.props.tasks}
                columns={this.title}
              />
            </div>
          );
        }
      case 1:
        return this.props.history.push("/tasks/selected");

      default:
        return <div />;
    }
  }

  render() {
    let button;

    if (this.props.links && this.props.links["Add Task Dependency"]) {
      button = <TaskDependenciesAdd />;
    } else button = null;

    return (
      <div>
        <br />
        {button}
        <br />
        <br />
        {this.renderSwitch(this.state.option)}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  selectedTask: state.task.task,
  tasks: state.tasks.taskDependencies,
  currentUser: state.authentication.currentUser,
  links: state.task.links
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({ loadedTasksDependencies, loadTask, loadLinks }, dispatch);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DependenciesList)
);
