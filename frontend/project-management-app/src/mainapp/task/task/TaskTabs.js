import { Col, Row, Tabs } from "antd";
import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import DependenciesList from "../taskdependencies/DependenciesList";
import EditableReportTable2 from "../../reports/EditableReportTable2";
import TaskCollaborators from "../taskCollaborators/TaskCollaborators";
import { connect } from "react-redux";
import TaskProgress from "../taskprogress/TaskProgress";
import { timeTask } from "./TaskDetails";

const TabPane = Tabs.TabPane;

class TaskTabs extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.history.goBack();
  }

  render() {
    return (
      <Tabs defaultActiveKey="1" onChange={this.props.key}>
        <TabPane tab="TASK DETAILS" key="1">
          <Row gutter={24}>
            <Col span={8}>
              <br />

              <p>
                <b>Description:</b>
                {this.props.selectedTask.description}
              </p>
            </Col>
            <Col span={8}>
              <span>
                <br />
                {timeTask(
                  this.props.selectedTask.predictedDateOfStart,
                  this.props.selectedTask.effectiveStartDate,
                  this.props.selectedTask.predictedDateOfConclusion,
                  this.props.selectedTask.effectiveDateOfConclusion
                )}
              </span>
            </Col>
            <Col span={8}>
              <h2>Progress</h2>
              <br />
              <TaskProgress taskProgress={this.props.selectedTask} />
              <br />
              <p>
                <b>Unit Cost: </b> <br />
                {this.props.selectedTask.unitCost}
              </p>
              <p>
                <b>estimated Effort: </b> <br />
                {this.props.selectedTask.estimatedEffort}
              </p>
              <br />
            </Col>
          </Row>
        </TabPane>
        <TabPane tab="COLLABORATORS" key="2">
          <br />
          <TaskCollaborators />
        </TabPane>
        <TabPane tab="REPORTS" key="3">
          <br />
          <EditableReportTable2 />
        </TabPane>

        <TabPane tab="DEPENDENCIES" key="4">
          <br />
          <h2>Dependencies List </h2>
          <DependenciesList />
          <br />
          <br />
        </TabPane>
      </Tabs>
    );
  }
}

const mapStateToProps = state => ({
  selectedTask: state.task.task
});

export default withRouter(connect(mapStateToProps)(TaskTabs));
