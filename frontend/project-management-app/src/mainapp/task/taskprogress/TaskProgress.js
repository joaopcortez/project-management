import { Progress } from "antd";
import React, { Component } from "react";
import "./TaskProgress.css";

const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds

class TaskProgress extends Component {
  renderDate = (predictedConclusionDate, effectiveStartDate, state) => {
    if (predictedConclusionDate == null || effectiveStartDate == null) {
      return (
        <div className="progress">
          <Progress size="small" type="line" percent={0} format={percent => ``} />
        </div>
      );
    } else if (state === "Completed") {
      return <Progress size="small" type="line" percent={100} format={percent => `√`} />;
    } else if (state === "Cancelled") {
      return (
        <Progress
          size="small"
          type="line"
          percent={100}
          status="exception"
          format={percent => `X`}
        />
      );
    } else {
      const predictedDateOfConclusion = new Date(this.props.taskProgress.predictedDateOfConclusion);
      let effectiveStartDate = new Date(this.props.taskProgress.effectiveStartDate);
      const totalDays = Math.round(
        Math.abs((predictedDateOfConclusion.getTime() - effectiveStartDate.getTime()) / oneDay)
      );
      const daysLeft = Math.round((predictedDateOfConclusion.getTime() - Date.now()) / oneDay);
      const percentDays = Math.floor(((totalDays - daysLeft) / totalDays) * 100);
      if (daysLeft < 1) {
        return (
          <div>
            Overdue!!<br />
            <Progress
              size="small"
              type="line"
              percent={percentDays}
              status="active"
              format={percent => ` `}
            />
          </div>
        );
      }
      return (
        <div className="progress">
          <Progress
            size="small"
            type="line"
            percent={percentDays}
            format={percent => <strong>{percentDays}%</strong>}
          />
        </div>
      );
    }
  };

  render() {
    return (
      <div className="progress">
        {this.renderDate(
          this.props.taskProgress.predictedDateOfConclusion,
          this.props.taskProgress.effectiveStartDate,
          this.props.taskProgress.state
        )}
      </div>
    );
  }
}

export default TaskProgress;
