import React, { Component } from "react";
import { Button, Form, Icon, Modal, Select } from "antd";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import {
  addTaskCollaborators,
  loadAvailableTaskCollaborators
} from "../../../actions/actions_task";

import { bindActionCreators } from "redux";

const FormItem = Form.Item;
const Option = Select.Option;

const CollectionCreateForm = Form.create()(
  class extends Component {
    render() {
      const { visible, onCancel, onCreate, form } = this.props;
      const { getFieldDecorator } = form;

      return (
        <Modal
          visible={visible}
          title="Add Task Collaborator"
          okText="Add"
          onCancel={onCancel}
          onOk={onCreate}
        >
          <Form layout="vertical">
            <FormItem label="Project Collaborator">
              {getFieldDecorator("email", {
                rules: [
                  {
                    required: true,
                    message: "Please input project collaborator"
                  }
                ]
              })(
                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder="Select a collaborator"
                  optionFilterProp="children"
                  onChange={this.handleChange}
                  onFocus={this.handleFocus}
                  onBlur={this.handleBlur}
                  filterOption={(input, option) =>
                    option.props.children[0].toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {this.props.collaboratorsNotInTask.map(collab => (
                    <Option key={collab.userId} value={collab.userId}>
                      {collab.userName}
                      <small> ({collab.userId})</small>
                    </Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Form>
        </Modal>
      );
    }
  }
);

class AddTaskCollaborator extends React.Component {
  state = {
    visible: false
  };
  showModal = () => {
    //const {loadAvailableTaskCollaborators} = this.props;
    this.setState({ visible: true });
    //loadAvailableTaskCollaborators(this.props.taskId);
  };
  handleCancel = () => {
    this.setState({ visible: false });
  };
  handleCreate = () => {
    const { addTaskCollaborators } = this.props;
    const form = this.formRef.props.form;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }

      addTaskCollaborators(
        this.props.taskId,
        values.email,
        this.props.selectedProject.projectId,
        this.props.currentUser.email
      );

      console.log("Received values of form: ", values);

      form.resetFields();
      this.setState({ visible: false });

      // confirm({
      //     title: 'Do you want add collaborator to this task?',
      //     content: (
      //         <div>
      //             <br/>
      //             <p><strong>Task ID:</strong> {this.props.taskId}</p>
      //             <p><strong>Project Collaborator:</strong> {values.email}</p>

      //         </div>
      //     ),
      //     onOk() {
      //         console.log('OK');
      //     },
      //     onCancel() {
      //         console.log('Cancel');
      //     },
      // });
    });
  };
  saveFormRef = formRef => {
    this.formRef = formRef;
  };

  render() {
    return (
      <div>
        <Button type="primary" onClick={this.showModal}>
          <Icon type="user-add" />Add Task Collaborator
        </Button>
        <br />
        <br />
        <CollectionCreateForm
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          onCreate={this.handleCreate}
          collaboratorsNotInTask={this.props.collaboratorsNotInTask}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  collaboratorsNotInTask: state.task.taskAvailableCollaborators,
  selectedProject: state.projects.selectedProject,
  currentUser: state.authentication.currentUser
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({ loadAvailableTaskCollaborators, addTaskCollaborators }, dispatch);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AddTaskCollaborator)
);
