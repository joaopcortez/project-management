import React, { Component } from "react";
import { Alert, Table, Tag } from "antd";
import { loadedProjectTasks } from "../../actions/actions_task_array";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import TaskProgress from "./taskprogress/TaskProgress";
import { loadTask, makeAssignment } from "../../actions/actions_task";
import { withRouter } from "react-router-dom";
import { colorChangeState } from "../../util/Helpers";
import { recordslice } from "../task/task/TaskDetails";

class TaskListAll extends Component {
  constructor(props) {
    super(props);
    this.state = {
      option: 0
    };
  }

  componentWillMount() {
    const { loadedProjectTasks } = this.props;
    loadedProjectTasks(this.props.selectedProject.projectId);
  }

  viewDetails(taskId) {
    const { loadTask } = this.props;
    loadTask(taskId);
    this.setState({ option: 1 });
  }

  title = [
    {
      title: "Progress",
      dataIndex: "",
      width: 200,
      render: (text, record) => <TaskProgress taskProgress={record} />
    },
    {
      title: "Title",
      dataIndex: "title",
      key: "title",
      width: 200
    },
    {
      title: "State",
      dataIndex: "state",
      key: "state",
      width: 200,
      filters: [
        {
          text: "Created",
          value: "Created"
        },
        {
          text: "Planned",
          value: "Planned"
        },
        {
          text: "Assigned",
          value: "Assigned"
        },
        {
          text: "ReadyToStart",
          value: "ReadyToStart"
        },
        {
          text: "InProgress",
          value: "InProgress"
        },
        {
          text: "Completed",
          value: "Completed"
        },
        {
          text: "Cancelled",
          value: "Cancelled"
        },
        {
          text: "Suspended",
          value: "Suspended"
        }
      ],
      onFilter: (value, record) => record.state.indexOf(value) === 0,
      render: (text, record) => <Tag color={colorChangeState(record.state)}>{record.state} </Tag>
    },
    {
      title: "Effective start date",
      dataIndex: "effectiveStartDate",
      key: "effectiveStartDate",
      width: 200,
      render: (text, record) => recordslice(record.effectiveStartDate)
    },
    {
      title: "Effective conclusion date",
      dataIndex: "effectiveDateOfConclusion",
      key: "effectiveDateOfConclusion",
      width: 200,
      render: (text, record) => recordslice(record.effectiveDateOfConclusion)
    }
  ];

  renderSwitch(param) {
    switch (param) {
      case 0:
        if (this.props.tasks.length === 0) {
          return <Alert message="There are no tasks to display" type="info" showIcon />;
        } else {
          return (
            <div>
              <Table
                rowKey={record => record.taskId}
                dataSource={this.props.tasks}
                footer={() => <div />}
                columns={this.title}
              />
            </div>
          );
        }
      case 1:
        return this.props.history.push("/tasks/selected");

      default:
        return <div />;
    }
  }

  render() {
    return <div>{this.renderSwitch(this.state.option)}</div>;
  }
}

const mapStateToProps = state => ({
  selectedProject: state.projects.selectedProject,
  tasks: state.tasks.tasks,
  selectedTask: state.task.task,
  currentUser: state.authentication.currentUser
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({ loadedProjectTasks, loadTask, makeAssignment }, dispatch);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(TaskListAll)
);
