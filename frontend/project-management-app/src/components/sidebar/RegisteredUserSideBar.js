import { Icon, Layout, Menu } from "antd";
import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

const { Sider } = Layout;

class RegisteredUserSideBar extends Component {
  render() {
    return (
      <Sider id="side-bar" width={200}>
        <Menu
          mode="inline"
          defaultSelectedKeys={[this.props.location.pathname]}
          defaultOpenKeys={["sub0"]}
          style={{ height: "100%" }}
        >
          <Menu.Item key="/">
            <Link to="/">
              <Icon type="dashboard" />Dashboard
            </Link>
          </Menu.Item>
        </Menu>
      </Sider>
    );
  }
}

export default withRouter(RegisteredUserSideBar);
