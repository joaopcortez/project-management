import { Icon, Layout, Menu } from "antd";
import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

const { SubMenu } = Menu;
const { Sider } = Layout;

class AdministratorSideBar extends Component {
  render() {
    return (
      <Sider id="side-bar" width={200}>
        <Menu
          mode="inline"
          defaultSelectedKeys={[this.props.location.pathname]}
          defaultOpenKeys={["sub0"]}
          style={{ height: "100%" }}
        >
          <Menu.Item key="/">
            <Link to="/">
              <Icon type="dashboard" />Dashboard
            </Link>
          </Menu.Item>
          <SubMenu
            key="sub1"
            title={
              <span>
                <Icon type="user" />Users
              </span>
            }
          >
            <Menu.Item key="/users">
              <Link to="/users">List All Users</Link>
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="sub2"
            title={
              <span>
                <Icon type="folder" />Handle Files
              </span>
            }
          >
            <Menu.Item key="/users/populate">
              <Link to="/users/populate">Import Users file</Link>
            </Menu.Item>
            <Menu.Item key="/projects/populate">
              <Link to="/projects/populate">Import Projects file</Link>
            </Menu.Item>
          </SubMenu>
        </Menu>
      </Sider>
    );
  }
}

export default withRouter(AdministratorSideBar);
