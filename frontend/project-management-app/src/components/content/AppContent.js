import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import './AppContent.css';
import {Layout} from 'antd';
import MainCarrocel from "../../mainapp/MainCarrocel";

const Content = Layout.Content;

class AppContent extends Component {
    constructor(props) {
        super(props);
        this.handleMenuClick = this.handleMenuClick.bind(this);
    }

    handleMenuClick({key}) {
        if (key === "logout") {
            this.props.onLogout();
        }
    }

    render() {
        return (
           <Content className="app-cont">
                WELCOME TO PROJECT MANAGEMENT APPLICATION
                <br/>
               <MainCarrocel/>
          </Content>

        );
    }
}


export default withRouter(AppContent);