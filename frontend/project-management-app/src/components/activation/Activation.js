import React, {Component} from 'react';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'


import {Button, Form, notification, Alert, Icon} from 'antd';
import {activateUserSuccess} from '../../actions/loginActions'
import './Activation.css';
import Link from 'react-router-dom/Link';
import {activateUser, resendEmail} from '../../util/APIUtils';
import {withRouter} from 'react-router-dom';


const FormItem = Form.Item;

class Activation extends Component {
    constructor(props) {
        super(props);
        this.handleConfirm = this.handleConfirm.bind(this);
        this.handleResend = this.handleResend.bind(this);
    }

    handleConfirm = (event) => {
        const {activateUserSuccess} = this.props;

        event.preventDefault();
        activateUser(this.text1.value)

            .then(response => {
                if (response.success) {
                    notification.success({
                        message: 'PM App',
                        description: response.message,
                    });
                    activateUserSuccess();
                    this.props.history.push("/");
                } else if (!response.success) {
                    notification.error({
                        message: 'PM App',
                        description: response.message,
                    });
                }
            }).catch(error => {
            notification.error({
                message: 'PM App',
                description: error.message || 'Sorry! Something went wrong. Please try again!'
            });
        });
    };

    handleResend(event) {

       resendEmail()
       .then(response => {
        if (response.success) {
            notification.success({
                message: 'PM App',
                description: response.message,
            });
        } else if (!response.success) {
            notification.error({
                message: 'PM App',
                description: "Email not sent!",
            });
        }
    }).catch(error => {
    notification.error({
        message: 'PM App',
        description: error.message || 'Sorry! Something went wrong. Please try again!'
    });
});
    }

    render() {
        return (
            <Form className="activation-form">
                <div className="activation-box">
                    <h1 className="activation-title"> <Icon type="exclamation-circle" style={{color: "#faad14"}}/> Your account is not yet verified!</h1>
                    <h1 className="activation-title2">Please enter your confirmation code:</h1>
                    <FormItem className="confirmation-field">
                        <input
                            className="bar"
                            size="large"
                            name="confirmationCode"
                            type="password"
                            ref={(input) => this.text1 = input}
                            placeholder="Confirmation code"/>
                    </FormItem>
                </div>
                <div className="button-container">
                    <FormItem>
                        <Button onClick={this.handleConfirm} type="primary" htmlType="submit" size="large"
                                className="activation-form-button-confirm">Confirm</Button>
                        <Button onClick={this.handleResend} type="normal" htmlType="submit" size="large"
                                className="activation-form-button-resend">Resend</Button>
                        <Link to="/login">
                            <Button type="normal" htmlType="submit" size="large"
                                    className="activation-form-button-cancel">Cancel</Button>
                        </Link>
                    </FormItem>
                </div>
            </Form>
        );

    }
}


const mapStateToProps = state => ({
    currentUser: state.authentication.currentUser,
    isAuthenticated: state.authentication.isAuthenticated,
    needActivation: state.authentication.needActivation
});

const mapDispatchToProps = dispatch =>
    bindActionCreators({activateUserSuccess}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Activation))