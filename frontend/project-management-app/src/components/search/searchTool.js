import React, {Component} from 'react';
import {Input, Radio, Table} from 'antd';
import ContentHeader from '../../templates/contentHeader';


const Search = Input.Search;
const RadioGroup = Radio.Group;


  

class SearchTool extends Component {
    constructor(props) {
        super(props);
        this.state = {
            columns:[],
            input: 'project',
            data:[]
        };
    
        this.onChange = this.onChange.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    componentWillMount(){

        const columns = [
            { title: 'Name', dataIndex: 'name', key: 'name' },
            { title: 'Id', dataIndex: 'id', key: 'id' },
            { title: 'Address', dataIndex: 'address', key: 'address' },
            { title: 'Action', dataIndex: '', key: 'x', render: (record) => <button onClick={this.viewDetails}
                                value={record.name}> Details </button> },
          ];

          this.setState({columns:columns})
    }

    viewDetails = (e) => {
        e.preventDefault();
        console.log("FUNCIONOU:" + e.target.value)
    };

    onChange = (e) => {
        console.log('radio checked', e.target.value);
        this.setState({input: e.target.value})
    };

    onSearch(){
        console.log("SEARCHING");
        this.setState({data : [
            { key: 1, name: 'John Brown', id: 32, address: 'New York No. 1 Lake Park', description: 'My name is John Brown, I am 32 years old, living in New York No. 1 Lake Park.' },
            { key: 2, name: 'Jim Green', id: 42, address: 'London No. 1 Lake Park', description: 'My name is Jim Green, I am 42 years old, living in London No. 1 Lake Park.' },
            { key: 3, name: 'Joe Black', id: 32, address: 'Sidney No. 1 Lake Park', description: 'My name is Joe Black, I am 32 years old, living in Sidney No. 1 Lake Park.' },
          ]})
    }

    
    render(){

        return(
            <div>
                <ContentHeader title='Search' subtitle='box' />
                <RadioGroup name="radiogroup" onChange={this.onChange} defaultValue={"project"}>
                    <Radio value={"project"}>Project</Radio>
                    <Radio value={"collaborator"}>Collaborator</Radio>
                    <Radio value={"task"}>Task</Radio>
                </RadioGroup>
                <br /><br />
                <Search
                placeholder={`search for ${this.state.input} id..`}
                onSearch={this.onSearch}
                style={{ width: 300 }}
                enterButton
                />
                <br /><br />
                <Search
                placeholder={`search for ${this.state.input} name..`}
                onSearch={this.onSearch}
                style={{ width: 300 }}
                enterButton
                />
                <br /><br />
                <Table
                columns={this.state.columns}
                expandedRowRender={record => <p style={{ margin: 0 }}>{record.description}</p>}
                dataSource={this.state.data}
                />
            </div>
        )
    }
    
}

export default SearchTool