import React, { Component } from "react";
import { Tag, Icon, Card, Avatar, Row, Col, Tooltip } from "antd";
import Group1 from "../../Group1.jpg";

const { Meta } = Card;
const gridStyle = {
  width: "30%",
  textAlign: "center",
  margin: "1em",
  backgroundColor: "white"
};

class Team extends Component {
  render() {
    const joaoGomesText = (
      <div>
        <p>
          <strong>Name:</strong> Joao Pedro Garcia Gomes
        </p>
        <p>
          <strong>Age:</strong> 28
        </p>
        <p>
          <strong>Email:</strong> jpgomes89@gmail.com
        </p>
      </div>
    );

    const sergioText = (
      <div>
        <p>
          <strong>Name:</strong> Sergio Valente
        </p>
        <p>
          <strong>Age:</strong> ...{" "}
        </p>
        <p>
          <strong>Email:</strong> ....
        </p>
      </div>
    );

    const lisaText = (
      <div>
        <p>
          <strong>Name:</strong> Lisa Silva
        </p>
        <p>
          <strong>Age:</strong> ...
        </p>
        <p>
          <strong>Email:</strong> ...
        </p>
      </div>
    );

    const danielText = (
      <div>
        <p>
          <strong>Name:</strong> Daniel Barbosa
        </p>
        <p>
          <strong>Age:</strong> ...
        </p>
        <p>
          <strong>Email:</strong> ...
        </p>
      </div>
    );

    const anaText = (
      <div>
        <p>
          <strong>Name:</strong> Ana Fernandes
        </p>
        <p>
          <strong>Age:</strong> ...
        </p>
        <p>
          <strong>Email:</strong> ...
        </p>
      </div>
    );
    const filipaText = (
      <div>
        <p>
          <strong>Name:</strong> Filipa Oliveira
        </p>
        <p>
          <strong>Age:</strong> ...
        </p>
        <p>
          <strong>Email:</strong> ...
        </p>
      </div>
    );
    const antonioText = (
      <div>
        <p>
          <strong>Name:</strong> Antonio Canelas
        </p>
        <p>
          <strong>Age:</strong> ...
        </p>
        <p>
          <strong>Email:</strong> ...
        </p>
      </div>
    );

    const pedroText = (
      <div>
        <p>
          <strong>Name:</strong> Pedro Cortez
        </p>
        <p>
          <strong>Age:</strong> 28
        </p>
        <p>
          <strong>Email:</strong> joaopcortez@gmail.com
        </p>
      </div>
    );

    const bebianaText = (
      <div>
        <p>
          <strong>Name:</strong> Bebiana Mendonça
        </p>
        <p>
          <strong>Age:</strong> ...
        </p>
        <p>
          <strong>Email:</strong> ...
        </p>
      </div>
    );

    return (
      <div style={{ margin: "1em" }}>
        <br /> <br />
        <Row gutter={24}>
          <br /> <br />
          <Col xs={4} sm={8} md={12} lg={16} xl={20}>
            <Card
              title={
                <span style={{ fontSize: "3em", fontFamily: "Georgia, serif" }}>About Us</span>
              }
              bordered={false}
              style={{ textAlign: "center", width: "75%", marginLeft: "auto", marginTop: "auto" }}
            >
              <div style={{ textAlign: "justify", marginTop: "1em", fontSize: "1.1em" }}>
                <img
                  style={{ float: "right", marginLeft: "1em" }}
                  src={Group1}
                  alt="Group1"
                  width="45%"
                />
                <p>
                  We are a group of people graduated with STEM (Science, Technology, Engineering and
                  Math) background that acquired skills and competencies to develop software
                  solutions in a business context, through the SWitCH() requalification program.
                </p>
                <p>
                  This web application was developed during the postgraduate course and it's a
                  result of hard working of a team that made hight efforts to find the best solution
                  to fullfilling the requirements, applying the best practices to reach high levels
                  of producitivity and quality.
                </p>
              </div>
            </Card>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col xs={4} sm={8} md={12} lg={16} xl={20}>
            <Card
              title={
                <span style={{ fontSize: "3em", fontFamily: "Georgia, serif" }}>
                  Development Team Members
                </span>
              }
              bordered={false}
              style={{
                textAlign: "center",
                width: "75%",
                marginLeft: "auto",
                marginTop: "5em",
                marginBottom: "3em"
              }}
            >
              <Tooltip placement="top" title={joaoGomesText}>
                <Card.Grid style={gridStyle}>
                  <Avatar
                    src="https://lh3.googleusercontent.com/-1MxGrfExPi8/Wz6pjja5W6I/AAAAAAAAATE/dgIq9naNEFUZLpkv1KdnrANFbL8GaVdDACEwYBhgL/w140-h140-p/foto2.jpg"
                    size="large"
                    style={{ width: "50%", height: "50%", borderRadius: "100%" }}
                  />

                  <Meta
                    title="João Gomes"
                    description={
                      <div>
                        <Icon type="linkedin" style={{ fontSize: "1.5em" }} />
                        <Icon type="facebook" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                        <Icon type="twitter" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                      </div>
                    }
                    style={{
                      fontFamily: '"Lucida Sans Unicode", "Lucida Grande", sans-serif',
                      marginTop: "1.5em"
                    }}
                  />
                </Card.Grid>
              </Tooltip>
              <Tooltip placement="top" title={sergioText}>
                <Card.Grid style={gridStyle}>
                  <Avatar
                    src="https://mug0.assets-yammer.com/mugshot/images/150x150/FkpZXQ20bXcdtwcbF7LWxfkjPSGn13H-"
                    size="large"
                    style={{ width: "50%", height: "50%", borderRadius: "100%" }}
                  />
                  <Meta
                    title="Sergio Valente"
                    description={
                      <div>
                        <Icon type="linkedin" style={{ fontSize: "1.5em" }} />
                        <Icon type="facebook" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                        <Icon type="twitter" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                      </div>
                    }
                    style={{
                      fontFamily: '"Lucida Sans Unicode", "Lucida Grande", sans-serif',
                      marginTop: "1.5em"
                    }}
                  />
                </Card.Grid>
              </Tooltip>
              <Tooltip placement="top" title={lisaText}>
                <Card.Grid style={gridStyle}>
                  <Avatar
                    src="https://mug0.assets-yammer.com/mugshot/images/150x150/9DTBzQZZwTt8Ckrt4zF84gKn3Z4KxjkD"
                    size="large"
                    style={{ width: "50%", height: "50%", borderRadius: "100%" }}
                  />

                  <Meta
                    title="Lisa Silva"
                    description={
                      <div>
                        <Icon type="linkedin" style={{ fontSize: "1.5em" }} />
                        <Icon type="facebook" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                        <Icon type="twitter" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                      </div>
                    }
                    style={{
                      fontFamily: '"Lucida Sans Unicode", "Lucida Grande", sans-serif',
                      marginTop: "1.5em"
                    }}
                  />
                </Card.Grid>
              </Tooltip>
              <Tooltip placement="top" title={danielText}>
                <Card.Grid style={gridStyle}>
                  <Avatar
                    src="https://mug0.assets-yammer.com/mugshot/images/150x150/xt6H-7DqdJDzQbLT8mThPsDFvQbLwDnK"
                    size="large"
                    style={{ width: "50%", height: "50%", borderRadius: "100%" }}
                  />
                  <Meta
                    title="Daniel Barbosa"
                    description={
                      <div>
                        <Icon type="linkedin" style={{ fontSize: "1.5em" }} />
                        <Icon type="facebook" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                        <Icon type="twitter" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                      </div>
                    }
                    style={{
                      fontFamily: '"Lucida Sans Unicode", "Lucida Grande", sans-serif',
                      marginTop: "1.5em"
                    }}
                  />
                </Card.Grid>
              </Tooltip>
              <Tooltip placement="top" title={anaText}>
                <Card.Grid style={gridStyle}>
                  <Avatar
                    src="https://media.licdn.com/dms/image/C4E03AQGJlTk2RqCq3A/profile-displayphoto-shrink_200_200/0?e=1536192000&v=beta&t=AO85ZXaasUdwv_7OlJGKioiKfDEzQQon_fnRoQziphA"
                    size="large"
                    style={{ width: "50%", height: "50%", borderRadius: "100%" }}
                  />
                  <Meta
                    title="Ana Fernandes"
                    description={
                      <div>
                        <Icon type="linkedin" style={{ fontSize: "1.5em" }} />
                        <Icon type="facebook" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                        <Icon type="twitter" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                      </div>
                    }
                    style={{
                      fontFamily: '"Lucida Sans Unicode", "Lucida Grande", sans-serif',
                      marginTop: "1.5em"
                    }}
                  />
                </Card.Grid>
              </Tooltip>
              <Tooltip placement="top" title={antonioText}>
                <Card.Grid style={gridStyle}>
                  <Avatar
                    src="https://mug0.assets-yammer.com/mugshot/images/150x150/4N3kdRRhLDdfkKZQh9Qw01m1jN0qBQXj"
                    size="large"
                    style={{ width: "50%", height: "50%", borderRadius: "100%" }}
                  />
                  <Meta
                    title="António Canelas"
                    description={
                      <div>
                        <Icon type="linkedin" style={{ fontSize: "1.5em" }} />
                        <Icon type="facebook" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                        <Icon type="twitter" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                      </div>
                    }
                    style={{
                      fontFamily: '"Lucida Sans Unicode", "Lucida Grande", sans-serif',
                      marginTop: "1.5em"
                    }}
                  />
                </Card.Grid>
              </Tooltip>
              <Tooltip placement="top" title={filipaText}>
                <Card.Grid style={gridStyle}>
                  <Avatar
                    src="https://mug0.assets-yammer.com/mugshot/images/150x150/r6hvn-PlxWZG46DfFKC9C6g4WqSCZ132"
                    size="large"
                    style={{ width: "50%", height: "50%", borderRadius: "100%" }}
                  />
                  <Meta
                    title="Filipa Oliveira"
                    description={
                      <div>
                        <Icon type="linkedin" style={{ fontSize: "1.5em" }} />
                        <Icon type="facebook" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                        <Icon type="twitter" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                      </div>
                    }
                    style={{
                      fontFamily: '"Lucida Sans Unicode", "Lucida Grande", sans-serif',
                      marginTop: "1.5em"
                    }}
                  />
                </Card.Grid>
              </Tooltip>
              <Tooltip placement="top" title={pedroText}>
                <Card.Grid style={gridStyle}>
                  <Avatar
                    src="https://i.imgur.com/AF3Cm5X.png"
                    size="large"
                    style={{ width: "50%", height: "50%", borderRadius: "100%" }}
                  />
                  <Meta
                    title="Pedro Cortez"
                    description={
                      <div>
                        <Icon type="linkedin" style={{ fontSize: "1.5em" }} />
                        <Icon type="facebook" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                        <Icon type="twitter" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                      </div>
                    }
                    style={{
                      fontFamily: '"Lucida Sans Unicode", "Lucida Grande", sans-serif',
                      marginTop: "1.5em"
                    }}
                  />
                </Card.Grid>
              </Tooltip>
              <Tooltip placement="top" title={bebianaText}>
                <Card.Grid style={gridStyle}>
                  <Avatar
                    src="https://media.licdn.com/dms/image/C5103AQHxT-h5Ejwxvg/profile-displayphoto-shrink_800_800/0?e=1536192000&v=beta&t=-gBjegX5UIBJGR9FJzgSqL3KEVG9TxqHQggY6PHeT4I"
                    size="large"
                    style={{ width: "50%", height: "50%", borderRadius: "100%" }}
                  />

                  <Meta
                    title="Bebiana Mendonça"
                    description={
                      <div>
                        <Icon type="linkedin" style={{ fontSize: "1.5em" }} />
                        <Icon type="facebook" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                        <Icon type="twitter" style={{ fontSize: "1.5em", marginLeft: "0.5em" }} />
                      </div>
                    }
                    style={{
                      fontFamily: '"Lucida Sans Unicode", "Lucida Grande", sans-serif',
                      marginTop: "1.5em"
                    }}
                  />
                </Card.Grid>
              </Tooltip>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Team;
