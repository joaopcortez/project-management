import React, {Component} from 'react';

import {loadUserProfile} from '../../actions/loginActions'
import {Avatar, Col, Row} from 'antd';
import {getAvatarColor} from '../../util/Colors';
import './Profile.css';
import {getProfileName} from '../../util/Helpers';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";


class Profile extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick() {
        this.props.history.goBack();
    }

    componentWillMount() {
        const {loadUserProfile} = this.props;
        const email = this.props.currentUser.email;
        loadUserProfile(email)
    }


    render() {

        const role = this.props.currentUser.profile;


        return (
            <div>
                <br/> <br/> <br/> <br/>
                <Row>
                    <Col xs={{span: 5, offset: 1}} lg={{span: 6, offset: 2}}>
                        {
                            this.props.userProfile ? (
                                <div className="user-profile">
                                    <div className="user-details">
                                        <div className="user-avatar">
                                            <Avatar className="user-avatar-circle"
                                                    style={{backgroundColor: getAvatarColor(this.props.userProfile.name)}}>
                                                {this.props.userProfile.name[0].toUpperCase()}
                                            </Avatar>
                                        </div>
                                        <div className="user-summary">
                                            <div className="full-name">{this.props.userProfile.name}</div>
                                            <div className="username">{getProfileName(role)}</div>
                                            <div className="email">{this.props.userProfile.email}</div>
                                        </div>
                                    </div>
                                </div>
                            ) : null
                        }

                    </Col>
                    <Col xs={{span: 11, offset: 1}} lg={{span: 6, offset: 2}}>
                        <br/><br/>
                        <br/><br/>
                        <p><b>Name: </b> {this.props.userProfile.name}</p>
                        <p><b>Email: </b> {this.props.userProfile.email}</p>
                        <p><b>Birth date: </b>{this.props.userProfile.birth}</p>
                        <p><b>Phone number: </b>{this.props.userProfile.phone}</p>
                        <p><b>Tax payer ID: </b>{this.props.userProfile.taxPayer}</p>
                    </Col>
                </Row>

            </div>
        );
    }
}

const mapStateToProps = state =>
    ({currentUser: state.authentication.currentUser, userProfile: state.authentication.userProfile});

const mapDispatchToProps = dispatch =>
    bindActionCreators({loadUserProfile}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Profile);