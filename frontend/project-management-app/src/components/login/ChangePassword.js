import React, {Component} from 'react';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import Link from 'react-router-dom/Link';
import {Button, Form, notification} from 'antd';
import {changePasswordSuccess} from '../../actions/loginActions'
import './ChangePassword.css';
import {changePassword} from '../../util/APIUtils';
import {withRouter} from 'react-router-dom';


const FormItem = Form.Item;

class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.handleConfirm = this.handleConfirm.bind(this);
    }

    isPasswordsEquals = () => {

        return (this.text2.value === this.text3.value);
    }

    handleConfirm = (event) => {
        const {changePasswordSuccess} = this.props;
        event.preventDefault();

        if(!this.isPasswordsEquals()) {
                notification.error({
                    message: 'PM App',
                    description: 'Password not changed. Please try again.',
                });
        }
        else {
        changePassword(this.text1.value, this.text2.value)

            .then(response => {
                if (response.success) {
                    notification.success({
                        message: 'PM App',
                        description: 'Password changed successfully',
                    });
                    changePasswordSuccess();
                    this.props.history.push("/");
                } else if (!response.success) {
                    notification.error({
                        message: 'PM App',
                        description: 'Password not changed. Please try again.',
                    });
                }
            }).catch(error => {
            notification.error({
                message: 'PM App',
                description: 'Sorry! Something went wrong. Please try again!'
            });
        });
    };
        }
    render() {
        return (
            <Form className="activation-form">
                <div className="activation-box">
                    <h1 className="activation-title2">Please change your password:</h1>        
                    <input
                        className="old-password-input"
                        size="large"
                        name="oldPassword"
                        type="password"
                        ref={(input) => this.text1 = input}
                        placeholder="Old Password"/>
                    <input
                        className="new-password-input"
                        size="large"
                        name="newPassword"
                        type="password"
                        ref={(input) => this.text2 = input}
                        placeholder="New Password"/>
                    <input
                        className="retype-new-password-input"
                        size="large"
                        name="retypeNewPassword"
                        type="password"
                        ref={(input) => this.text3 = input}
                        placeholder="Retype New Password"/>       
                </div>
                <div className="button-container">
                    <FormItem>
                        <Button onClick={this.handleConfirm} type="primary" htmlType="submit" size="large"
                                className="activation-form-button-confirm">Confirm</Button>
                        <Link to="/login">
                            <Button type="normal" htmlType="submit" size="large"
                                    className="activation-form-button-cancel">Cancel</Button>
                        </Link>
                    </FormItem>
                </div>
            </Form>
        );

    }
}


const mapStateToProps = state => ({
    currentUser: state.authentication.currentUser,
    isAuthenticated: state.authentication.isAuthenticated,
    needActivation: state.authentication.needActivation
});

const mapDispatchToProps = dispatch =>
    bindActionCreators({changePasswordSuccess}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ChangePassword))