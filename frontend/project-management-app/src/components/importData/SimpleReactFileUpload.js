import React from 'react';
import axios, { post } from 'axios';
import {populateUsersXML, populateProjectsXML} from '../../actions/actions_user_array';
import './SimpleReactFileUpload.css';
import {withRouter} from 'react-router-dom';
import { notification } from "antd/lib/index";

class SimpleReactFileUpload extends React.Component {

  constructor(props) {
    super(props);
    this.state ={
      file:null
    };
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.fileUpload = this.fileUpload.bind(this);
  }
  onFormSubmit(e){
    e.preventDefault(); // Stop form submit
    if(this.state.file.name.startsWith("Utilizador")) {
    this.fileUpload(this.state.file).then((response)=>{
      populateUsersXML(this.state.file.name, response.data.link);      
    }).then(this.props.history.push('/'));
  }
  else if (this.state.file.name.startsWith("Proje")) {
    this.fileUpload(this.state.file).then((response)=>{
      populateProjectsXML(this.state.file.name, response.data.link);      
    }).then(this.props.history.push('/'));
  }
  else {
    notification.warn({
      message: "PM App",
      description: "Import failure!"
    });
  }
}
  onChange(e) {
    this.setState({file:e.target.files[0]});
  }
  fileUpload(file){
    const url = 'https://file.io';
    const formData = new FormData();
    formData.append('file',file);
    const config = {
        headers: {
            'Content-type': 'multipart/form-data',
        },
    };
    return  post(url, formData,config);
  }

  render() {
    return (
      <div className="principalDiv">
        <h1>IMPORT DATA</h1>
        <h3>Please choose an xml file:</h3>
        <form className="uploadForm" onSubmit={this.onFormSubmit}>
          <input className="inputName" type="file" onChange={this.onChange} />
          <button className="uploadButton" type="submit">Upload</button>
        </form>
      </div>
   );
  }
}

export default withRouter(SimpleReactFileUpload);
