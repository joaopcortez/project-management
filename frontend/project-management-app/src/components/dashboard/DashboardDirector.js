import React, { Fragment, Component } from "react";
import { Tag, Icon, Card, Col, Row, Tooltip, Progress } from "antd";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "./Dashboard.css";
import ContentHeader from "../../templates/contentHeader";
import { getProjectsAsDirector, getProjectById } from "../../actions/projectActions";
import { withRouter } from "react-router-dom";

class DashboardDirector extends Component {
  componentWillMount() {
    const { getProjectsAsDirector } = this.props;
    getProjectsAsDirector();
  }

  handleClick = value => {
    const { getProjectById } = this.props;
    getProjectById(value);
    console.log("project ID:" + value);
    this.props.history.push("/projects/selected");
  };

  render() {
    let projectsLength = this.props.projects.length;
    let projects = this.props.projects;

    return (
      <Fragment>
        <ContentHeader title="DASHBOARD" />

        <div className="cards-header">
          <Row gutter={24}>
            <Col span={6}>
              <Card className="card-head">
                <Icon className="icon" type="folder" />
                <h3 className="title-card">Active Projects</h3>
                <div className="header-col">
                  <span className="content-card">{projectsLength}</span>
                </div>
              </Card>
            </Col>
            <Col span={6}>
              <Card className="card-head">
                <Icon className="icon" type="folder-open" />
                <h3 className="title-card">In Execution</h3>
                <div className="header-col">
                  <span className="content-card">0</span>
                </div>
              </Card>
            </Col>
            <Col span={6}>
              <Card className="card-head">
                <Icon className="icon" type="upload" />
                <h3 className="title-card">Delivery</h3>
                <div className="header-col">
                  <span className="content-card">0</span>
                </div>
              </Card>
            </Col>
            <Col span={6}>
              <Card className="card-head">
                <Icon className="icon" type="solution" />
                <h3 className="title-card">Warranty</h3>
                <div className="header-col">
                  <span className="content-card">0</span>
                </div>
              </Card>
            </Col>
          </Row>

          <Row id="dashboard" gutter={24}>
            {/* <Col span={12}>
                            <Card title="Calendar" bordered={false}>
                                <div style={{width: 300, border: '1px solid #d9d9d9', borderRadius: 4}}>
                                    <Calendar fullscreen={false} onPanelChange={onPanelChange}/>
                                </div>
                            </Card>
                        </Col> */}
            <Col span={24}>
              <Card title="Recent Created Projects" bordered={false}>
                {projects.map(proj => (
                  <Card.Grid key={proj.projectId}>
                    {proj.name.length > 26 ? <strong>{proj.name}</strong> :
                <strong>{proj.name}</strong>}
                    <br />

                    <Tooltip
                      title={"Tasks: " +
                        proj.completedTasks +
                        " Completed / " +
                        proj.initiatedButNotCompletedTasks +
                        " In Progress / " +
                        (proj.notInitiatedTasks - proj.canceledTasks) +
                        " To Do"
                      }
                    ><br/><br/>Task Resume:
                      <Progress className="yellow-bar"
                        percent={
                          Math.round(
                            ((proj.initiatedButNotCompletedTasks + proj.completedTasks) /
                              (proj.completedTasks +
                                proj.initiatedButNotCompletedTasks +
                                proj.notInitiatedTasks -
                                proj.canceledTasks)) *
                              100 *
                              100
                          ) / 100
                        }
                        successPercent={
                          (proj.completedTasks /
                            (proj.completedTasks +
                              proj.initiatedButNotCompletedTasks +
                              proj.notInitiatedTasks -
                              proj.canceledTasks)) *
                          100
                        }
                        size="small"
                      />
                    </Tooltip>
                    <div className="dash-separator" />
                    <small>
                      Global Budget: <strong>{proj.globalBudget}</strong> {proj.unit}
                    </small>
                    <br />
                    <small>
                      Current Cost: <strong>{Math.round(proj.currentCost * 100) / 100}</strong>{" "}
                      {proj.unit}
                    </small>
                    <br />
                    <br />
                    <span>
                      {proj.endDate ? (
                        <Tag color="#108ee9">Closed</Tag>
                      ) : (
                        <Tag color="#87d068">In Progress</Tag>
                      )}
                    </span>
                    <span className="btn-info">
                      <button onClick={() => this.handleClick(proj.projectId)}>
                        INFO <Icon className="icon-info" type="right-square" />
                      </button>
                    </span>
                  </Card.Grid>
                ))}
              </Card>
            </Col>

          </Row>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  projects: state.projects.projectsAsDirector
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({ getProjectsAsDirector, getProjectById }, dispatch);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DashboardDirector)
);
