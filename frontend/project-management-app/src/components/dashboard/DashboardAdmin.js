import React, { Fragment, Component } from "react";
import { Calendar, Icon, Card, Col, Row, Table } from "antd";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {loadedUsers, loadRegisteredUsers} from "../../actions/actions_user_array";
import "./Dashboard.css";
import ContentHeader from "../../templates/contentHeader";
import RegisteredList from "../../mainapp/RegisteredList";

function onPanelChange(value, mode) {
  console.log(value, mode);
}

class DashboardAdmin extends Component {
  componentWillMount() {
    const { loadedUsers } = this.props;
    loadedUsers();

    const {loadRegisteredUsers} =this.props;
    loadRegisteredUsers();
  }

  render() {
    const usersTotal = this.props.users;
    let directors = 0;
    let collab = 0;
    let regUsers = 0;
    let admins = 0;
    let i;

    for (i = 0; i < usersTotal.length; i++) {
      usersTotal[i].profile == "[ROLE_COLLABORATOR]"
        ? collab++
        : usersTotal[i].profile == "[ROLE_DIRECTOR]"
          ? directors++
          : usersTotal[i].profile == "[ROLE_REGISTEREDUSER]"
            ? regUsers++
            : admins++;
    }

    return (
      <Fragment>
        <ContentHeader title="DASHBOARD"/>

        <div className="cards-header">
          <Row gutter={24}>
            <Col span={6}>
              <Card className="card-head">
                <Icon className="icon" type="team" />
                <h3 className="title-card">Total Users</h3>
                <div className="header-col">
                  <span className="content-card">{this.props.users.length}</span>
                </div>
              </Card>
            </Col>
            <Col span={6}>
              <Card className="card-head">
                <Icon className="icon" type="user" />
                <h3 className="title-card">Directors</h3>
                <div className="header-col">
                  <span className="content-card">{directors}</span>
                </div>
              </Card>
            </Col>
            <Col span={6}>
              <Card className="card-head">
                <Icon className="icon" type="skin" />
                <h3 className="title-card">Collaborators</h3>
                <div className="header-col">
                  <span className="content-card">{collab}</span>
                </div>
              </Card>
            </Col>
            <Col span={6}>
              <Card className="card-head">
                <Icon className="icon" type="user-add" />
                <h3 className="title-card">Registered Users</h3>
                <div className="header-col">
                  <span className="content-card">{regUsers}</span>
                </div>
              </Card>
            </Col>
          </Row>

          <Row gutter={24} className="flex">
            <Col span={10}>
              <Card title="Calendar" bordered={false}>
                <div style={{ border: "1px solid #d9d9d9", borderRadius: 4 }}>
                  <Calendar fullscreen={false} onPanelChange={onPanelChange} />
                </div>
              </Card>
            </Col>
            <Col span={14}>
              <Card title="Registered Users" bordered={false} className="height">
                  <RegisteredList />
              </Card>
            </Col>
          </Row>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  users: state.users.users,
    registeredusers:state.users.registeredusers,
  currentUser: state.authentication.currentUser
});
const mapDispatchToProps = dispatch => bindActionCreators({ loadedUsers, loadRegisteredUsers }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardAdmin);
