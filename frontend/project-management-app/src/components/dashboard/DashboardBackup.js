import React, { Fragment } from "react";
import { Calendar, Card, Col, Progress, Row, Tabs, Tooltip } from "antd";

import "./Dashboard.css";
import ContentHeader from "../../templates/contentHeader";

const { TabPane } = Tabs;

const columns = [
  {
    title: "ProjectId",
    dataIndex: "projectId",
    key: "projectId"
  },
  {
    title: "Title",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Description",
    dataIndex: "description",
    key: "description"
  },
  {
    title: "Start Date",
    dataIndex: "startDate",
    key: "startDate"
  },
  {
    title: "Final Date",
    dataIndex: "finalDate",
    key: "finalDate"
  }
];

const topColResponsiveProps = {
  xs: 24,
  sm: 12,
  md: 12,
  lg: 12,
  xl: 6,
  style: { marginBottom: 24 }
};

function onPanelChange(value, mode) {
  console.log(value, mode);
}

export default class MyProjects extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {}

  render() {
    return (
      <Fragment>
        <ContentHeader title="DASHBOARD" />
        <div className="cards-header">
          <Row gutter={16}>
            <Col span={8}>
              <Card title="My Projects" bordered={false}>
                <Card.Grid>
                  <strong>Project 1</strong>
                  <br />
                  <br />

                  <Tooltip title="3 completed / 3 in progress / 4 to do">
                    <Progress percent={60} successPercent={30} size="small" />
                  </Tooltip>
                  <br />
                  <small>in progress</small>
                </Card.Grid>
                <Card.Grid>
                  <strong>Project 2</strong>
                  <br />
                  <br />
                  <Tooltip title="8 done / 1 in progress / 1 to do">
                    <Progress percent={90} successPercent={80} size="small" />
                  </Tooltip>
                  <br />
                  <small>in progress</small>
                </Card.Grid>
                <Card.Grid>
                  <strong>Project 3</strong>
                  <br />
                  <br />
                  <Tooltip title="5 to do">
                    <Progress percent={0} successPercent={0} size="small" />
                  </Tooltip>
                  <br />
                  <small>in progess</small>
                </Card.Grid>
              </Card>
            </Col>
            <Col span={8}>
              <Card title="My Tasks" bordered={false}>
                <Tooltip title="3 done / 3 in progress / 4 to do">
                  Task 1 <Progress percent={60} successPercent={30} size="small" />
                </Tooltip>
                <Tooltip title="3 done / 3 in progress / 4 to do">
                  Task 2 <Progress percent={60} successPercent={30} size="small" />
                </Tooltip>
              </Card>
            </Col>
            <Col span={8}>
              <Card title="Calendar" bordered={false}>
                <div style={{ width: 300, border: "1px solid #d9d9d9", borderRadius: 4 }}>
                  <Calendar fullscreen={false} onPanelChange={onPanelChange} />
                </div>
              </Card>
            </Col>
          </Row>
        </div>

        <Card loading={false} bordered={false} bodyStyle={{ padding: 0 }}>
          <div>
            <Tabs defaultActiveKey="1" size="large" tabBarStyle={{ marginBottom: 24 }}>
              <TabPane tab="TabPane 1" key="1">
                <Row>
                  <Col xl={16} lg={12} md={12} sm={24} xs={24}>
                    <div className="sale bar">BAR</div>
                  </Col>
                  <Col xl={8} lg={12} md={12} sm={24} xs={24}>
                    <div>
                      <h4>Ranking Title</h4>
                      <ul>
                        <li>
                          <span>opcao 1</span>
                          <span>opcao 2</span>
                          <span>opcao 3</span>
                        </li>
                      </ul>
                    </div>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="Tabpan 4" key="2">
                <Row>
                  <Col xl={16} lg={12} md={12} sm={24} xs={24}>
                    <div>BAR</div>
                  </Col>
                  <Col xl={8} lg={12} md={12} sm={24} xs={24}>
                    <div>
                      <h4>Ranking title xpto</h4>
                      <ul>
                        <li>
                          <span>opcao 1</span>
                          <span>opcao 2</span>
                          <span>opcao 3</span>
                        </li>
                      </ul>
                    </div>
                  </Col>
                </Row>
              </TabPane>
            </Tabs>
          </div>
        </Card>

        <Card
          loading={false}
          bordered={false}
          bodyStyle={{ padding: "0 0 32px 0" }}
          style={{ marginTop: 32 }}
        >
          <Tabs defaultActiveKey="1">
            <TabPane tab="Tab 1" key="1">
              Content of Tab Pane 1
            </TabPane>
            <TabPane tab="Tab 2" key="2">
              Content of Tab Pane 2
            </TabPane>
            <TabPane tab="Tab 3" key="3">
              Content of Tab Pane 3
            </TabPane>
          </Tabs>
        </Card>
      </Fragment>
    );
  }
}
