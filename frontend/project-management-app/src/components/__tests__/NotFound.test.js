import NotFound from "../common/NotFound";
import {shallow} from 'enzyme';
import React from 'react';
import {Button} from 'antd';
import {Link} from 'react-router-dom';
 

it('test NotFound snapshot', () => {
    const wrapped = shallow(<NotFound />);
    expect(wrapped).toMatchSnapshot();
});

it('renders one Button', () => {
    const wrapped = shallow(<NotFound/>);
    expect(wrapped.find(Button).length).toEqual(1);
});

it('renders one Link', () => {
    const wrapped = shallow(<NotFound/>);
    expect(wrapped.find(Link).length).toEqual(1);
});

it('Renders component', () => {
    const wrapper = shallow(<NotFound/>);
    expect(wrapper.contains(<h1 className="title"/>));
});

