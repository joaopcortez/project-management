import {shallow} from 'enzyme';
import React from 'react';
import PrivateRoute from "../../common/PrivateRoute";


it('test PrivateRoute snapshot', () => {
    const wrapped = shallow(<PrivateRoute />);
    expect(wrapped).toMatchSnapshot();
});