import {shallow} from 'enzyme';
import React from 'react';
import AdministratorSideBar from "../sidebar/AdministratorSideBar";


it('Snapshot ManageRequests', () => {
    const wrapped = shallow(<AdministratorSideBar/>);
    expect(wrapped).toMatchSnapshot();
});
