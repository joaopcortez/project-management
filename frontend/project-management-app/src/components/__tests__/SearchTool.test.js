import {shallow} from 'enzyme';
import React from 'react';
import SearchTool from "../search/searchTool";


it('Snapshot SearchTool', () => {
    const wrapped = shallow(<SearchTool/>);
    expect(wrapped).toMatchSnapshot();
});
