import AppFooter from "../footer/AppFooter";
import {shallow} from 'enzyme';
import React from 'react';
import {Layout} from 'antd';
const Footer = Layout.Footer;

it('test AppFooter snapshot', () => {
    const wrapped = shallow(<AppFooter/>);
    expect(wrapped).toMatchSnapshot();
});

it('test if AppFooter has the Footer component', () => {
    const wrapped = shallow(<AppFooter/>);
    expect(wrapped.find(Footer).length).toEqual(1);
});

it('renders <AppFooter/> component', () => {
    const wrapper = shallow(<AppFooter/>);
    expect(wrapper.contains(<div className="text">Created by Group 1 - SWitCH Program</div>));
});
