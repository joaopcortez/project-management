import {shallow} from 'enzyme';
import React from 'react';
import Login from "../login/Login";


it('Snapshot Login', () => {
    const wrapped = shallow(<Login />);
    expect(wrapped).toMatchSnapshot();
});
