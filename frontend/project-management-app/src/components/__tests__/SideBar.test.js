import {shallow} from 'enzyme';
import React from 'react';
import SideBar from "../sidebar/SideBar";
import {Sider} from "antd";


it('Snapshot SideBar', () => {
    const wrapped = shallow(<SideBar/>);
    expect(wrapped).toMatchSnapshot();
});
