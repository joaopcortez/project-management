import {shallow} from 'enzyme';
import React from 'react';
import DashboardDirector from "../dashboard/DashboardDirector";


it('test DashboardDirector snapshot', () => {
    const wrapped = shallow(<DashboardDirector />);
    expect(wrapped).toMatchSnapshot();
});