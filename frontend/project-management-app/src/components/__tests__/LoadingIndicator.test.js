import {shallow} from 'enzyme';
import React from 'react';
import LoadingIndicator from "../../common/LoadingIndicator";


it('test LoadingIndicator snapshot', () => {
    const wrapped = shallow(<LoadingIndicator />);
    expect(wrapped).toMatchSnapshot();
});